trigger CaseCommentTrigger on Case_Comment__c (after insert, after Update) {

	if(Trigger.isAfter){

		if(Trigger.isInsert){
			CaseCommentTriggerHandler.afterUpdate(Trigger.new);
		}	

		if(Trigger.isInsert || Trigger.isUpdate){
			CaseCommentTriggerHandler.updateNoOfCommentsOnCase(Trigger.new);
		}	
	}

}