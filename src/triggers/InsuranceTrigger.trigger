trigger InsuranceTrigger on Insurance__c (before insert, before update ,after insert,after update) {
    if(Trigger.isAfter && Trigger.isInsert){
          InsuranceTriggerHandler.afterInsert(Trigger.new);
        }
     if(Trigger.isbefore && Trigger.isInsert){
           InsuranceTriggerHandler.beforeInsert(Trigger.new);
        }
    if(Trigger.isAfter && Trigger.isUpdate){
        InsuranceTriggerHandler.afterUpdate(Trigger.newMap,Trigger.oldMap);
    }
}