trigger FeedItemTrigger on FeedItem (after insert) {

	if(Trigger.isAfter){
		if(Trigger.isInsert){
			FeedItemTriggerHandler.afterInsert(Trigger.newMap);
		}
	}
}