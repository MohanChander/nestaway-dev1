trigger OrderItemTrigger on OrderItem (before insert, after insert, after update) {

	if(Trigger.isAfter){
		if(Trigger.isInsert){
			OrderItemTriggerHandler.afterInsert(Trigger.newMap);
		}

		if(Trigger.isUpdate){
			OrderItemTriggerHandler.ChangePOStageToVerified(Trigger.newMap, Trigger.oldMap);
			OrderItemTriggerHandler.afterUpdate(Trigger.newMap, Trigger.oldMap);
		}
	}
}