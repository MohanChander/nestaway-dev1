trigger ReceivedProductTrigger on Received_Product__c (before insert, after Insert, before update, after Update) {
	
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            ReceivedProductTriggerHandler.afterInsert(Trigger.newMap);
        }
        
        if(Trigger.isUpdate){
            ReceivedProductTriggerHandler.afterUpdate(Trigger.newMap, Trigger.oldMap);
        }
    }
}