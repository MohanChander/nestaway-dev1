/******************************************
 * Created By : Mohan
 * Purpose    : Trigger for Custom Object : Order_Item__c
 *****************************************/
trigger OrdItemTrigger on Order_Item__c (before insert, after insert, after update) {

	if(Trigger.isAfter){
		if(Trigger.isInsert){
			OrdItemTriggerHandler.afterInsert(Trigger.newMap);
		}

		if(Trigger.isUpdate){
			OrdItemTriggerHandler.afterUpdate(Trigger.newMap, Trigger.oldMap);
		}
	}
}