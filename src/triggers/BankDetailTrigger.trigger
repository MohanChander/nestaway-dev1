trigger BankDetailTrigger on Bank_Detail__c (before insert,after insert,before update,after update)
{

    boolean fireTrigger=true;    
    Org_Param__c param = Org_Param__c.getInstance();
    
    if(!Test.IsRunningTest()){             
        if(StopRecursion.DisabledBankTrigger){
            fireTrigger=false;
           
        }
    }
     
    if(fireTrigger){   
    
      if(Trigger.isBefore)
      {
        if(Trigger.isInsert)
        { 
              BankDetailTriggerHandler.beforeInsert(trigger.new);
        }
        if(Trigger.isUpdate)
        {
              BankDetailTriggerHandler.beforeUpdate(trigger.newMap,trigger.oldMap);
        }
      }
      
      if(Trigger.isAfter && Trigger.isInsert){            
            
                BankDetailTriggerHandler.updateAccountForBankDetail(trigger.new);
                BankDetailTriggerHandler.afterInsert(trigger.newMap);
      }

       if(Trigger.isAfter && Trigger.isUpdate){            
            
                BankDetailTriggerHandler.afterUpdate(trigger.newMap,trigger.oldMap);
      }
      

      //Sync Bank Details with Web App when ever a record is inserted or Updated
      if(!param.Disable_Bank_Sync_API__c && Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)){
            
            
                BankDetailTriggerHandler.syncBank(Trigger.newMap);

      }
     
    }  

}