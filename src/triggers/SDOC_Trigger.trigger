trigger SDOC_Trigger on SDOC__SDoc__c (after insert,after update) {
    if(Trigger.isAfter){
        SDOC_TriggerHelper.afterupdate(Trigger.newMap,Trigger.oldMap);
        SDOC_TriggerHelper.afterUpdateForHic(Trigger.newMap,Trigger.oldMap);
    }

}