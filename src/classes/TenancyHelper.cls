/*  Created By : Deepak
    Purpose    : All the function related to Tenancy are done here */
public class TenancyHelper {
    // to create new tenancy record from accountid and house id
    public static Tenancy__c  createNewTenancyRecord(ID AccountID, Id HouseID){
          Tenancy__c newTen = new Tenancy__c();
          newTen.Tenant__c = AccountID;
          newTen.House__c = HouseID;
        return newTen;
    }
   
}