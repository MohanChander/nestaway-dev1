@isTest
public class OrderPurchaseOrderPDFExtensionTest {
    static testMethod void RoomTriggerTest() {
       //insert dummy Vendor account
        Account vendoracc = new account(name ='TestVendor',Contact_Email__c='ameed@warpdrivetech.in',TIN_No__c='tin12345',BillingStreet='mayur vihar -1',BillingCity='New Delhi',BillingState='Delhi',BillingPostalCode='110091',phone='2757676767') ;
        vendoracc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        insert(vendoracc);
        Contact cont = new Contact ();
        cont.FirstName = 'FirstName';
        cont.LastName = 'LastName';
        cont.Email='ameed@warpdrivetech.in';
        cont.phone='1234567890';
        cont.accountId = vendoracc.id;
        insert cont;
        //insert dummy Owner account
        Account owneracc = new account(FirstName='OWNER',LastName='test',Contact_Email__c='owner@warpdrivetech.in',BillingStreet='mayur vihar -1',BillingCity='New Delhi',BillingState='Delhi',BillingPostalCode='110091',Phone='9916566793') ;
        owneracc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        insert(owneracc);
        //insert dummy city
        City__c c = new City__c();
        c.name='New Delhi';
        insert c;
        //insert dummy house
        house__c h = new house__c(Name='white house', House_Layout__c='1 BHK', House_Owner__c=owneracc.id, City_Master__c = c.id);
        insert h;
        //insert dummy product
        Product2 p1 = new Product2(Name='Prod 1', Family='Individual', Description='Prod 1 Description',Product_Type__c ='Furnishing',productcode='342344');
        insert p1;
        // Create a pricebook entry
Id pricebookId = Test.getStandardPricebookId();

PricebookEntry standardPrice = new PricebookEntry(
    Pricebook2Id = pricebookId, Product2Id = p1.Id,
    UnitPrice = 10000, IsActive = true);
insert standardPrice;

Pricebook2 pb  = new Pricebook2(Name='Custom Pricebook', isActive=true);
insert pb ;

PricebookEntry pbe  = new PricebookEntry(
    Pricebook2Id = pb.Id, Product2Id = p1.Id,
    UnitPrice = 12000, IsActive = true);
insert pbe ;
        //insert dummy Order
        order o = new order(accountId=owneracc.id, EffectiveDate = System.today(), Status ='Draft' ,Unit_Price__c =22,house__C=h.id,Pricebook2Id  = pb.ID);
        o.RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Purchase Order').getRecordTypeId();
        insert o;
        //INSERT ORDER PRODUCT
        list<OrderItem> oilist = new list<OrderItem>();
        OrderItem oit = new OrderItem(Quantity=10.0,PricebookEntryId=pbe.id,OrderId =o.id,unitprice=10); 
        oilist.add(oit);
        oit = new OrderItem(Quantity=100.0,PricebookEntryId=pbe.id,OrderId =o.id,unitprice=10);
        oilist.add(oit);
        insert  oilist;
        
        PageReference pageRef = Page.OrderPurchaseOrderPDF;
        Test.setCurrentPage(pageRef);
        //thecontroller controller = new thecontroller();
    pageRef.getParameters().put('id',o.id);
        order orderrec = new order();
        orderrec = OrderService.getOrderRecords(pageRef.getParameters().get('id'));
     ApexPages.StandardController sC = new ApexPages.standardController(o);
        OrderPurchaseOrderPDFExtension chp= new OrderPurchaseOrderPDFExtension(sC);
    }
     static testMethod void RoomTriggerTest2() {
       //insert dummy Vendor account
        Account vendoracc = new account(name ='TestVendor',Contact_Email__c='ameed@warpdrivetech.in',TIN_No__c='tin12345',BillingStreet='mayur vihar -1',BillingCity='New Delhi',BillingState='Delhi',BillingPostalCode='110091',phone='2757676767') ;
        vendoracc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        insert(vendoracc);
        Contact cont = new Contact ();
        cont.FirstName = 'FirstName';
        cont.LastName = 'LastName';
        cont.Email='ameed@warpdrivetech.in';
        cont.phone='1234567890';
        cont.accountId = vendoracc.id;
        insert cont;
        //insert dummy Owner account
        Account owneracc = new account(FirstName='OWNER',LastName='test',Contact_Email__c='owner@warpdrivetech.in',BillingStreet='mayur vihar -1',BillingCity='New Delhi',BillingState='Delhi',BillingPostalCode='110091',phone='2757676767') ;
        owneracc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        insert(owneracc);
        //insert dummy city
        City__c c = new City__c();
        c.name='New Delhi';
        insert c;
        //insert dummy house
        house__c h = new house__c(Name='white house', House_Layout__c='1 BHK', House_Owner__c=owneracc.id, City_Master__c = c.id);
        insert h;
        //insert dummy product
        Product2 p1 = new Product2(Name='Prod 1', Family='Individual', Description='Prod 1 Description',Product_Type__c ='Furnishing',productcode='342344');
        insert p1;
        // Create a pricebook entry
Id pricebookId = Test.getStandardPricebookId();

PricebookEntry standardPrice = new PricebookEntry(
    Pricebook2Id = pricebookId, Product2Id = p1.Id,
    UnitPrice = 10000, IsActive = true);
insert standardPrice;

Pricebook2 pb  = new Pricebook2(Name='Custom Pricebook', isActive=true);
insert pb ;

PricebookEntry pbe  = new PricebookEntry(
    Pricebook2Id = pb.Id, Product2Id = p1.Id,
    UnitPrice = 12000, IsActive = true);
insert pbe ;
        //insert dummy Order
        order o = new order(accountId=owneracc.id, EffectiveDate = System.today(), Status ='Draft' ,Unit_Price__c =22,house__C=h.id,Pricebook2Id  = pb.ID, Show_Nestaway_Billing_Address__c =true,Type='sg',Category__c='Furnishing',Expected_Arrival_Date__c=system.today());
        o.RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Purchase Order').getRecordTypeId();
        insert o;
        //INSERT ORDER PRODUCT
        list<OrderItem> oilist = new list<OrderItem>();
        OrderItem oit = new OrderItem(Quantity=10.0,PricebookEntryId=pbe.id,OrderId =o.id,unitprice=10); 
        oilist.add(oit);
        oit = new OrderItem(Quantity=100.0,PricebookEntryId=pbe.id,OrderId =o.id,unitprice=10);
        oilist.add(oit);
        insert  oilist;
        
        PageReference pageRef = Page.OrderPurchaseOrderPDF;
        Test.setCurrentPage(pageRef);
        //thecontroller controller = new thecontroller();
    pageRef.getParameters().put('id',o.id);
        order orderrec = new order();
        orderrec = OrderService.getOrderRecords(pageRef.getParameters().get('id'));
     ApexPages.StandardController sC = new ApexPages.standardController(o);
        OrderPurchaseOrderPDFExtension chp= new OrderPurchaseOrderPDFExtension(sC);
    }
}