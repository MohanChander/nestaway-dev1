public class SendAPIRequestforSchedule {
    public class ScheduleDetailsWrapeerClass{
        public String House_sf_id;
    }
    public class ScheduleDetailsJsonRespWrapeerClass{        
        public boolean success;
        public String info;
        public DataObject data;
        
    }
    public class DataObject {
        public String start_date;   //2017-08-21
        public String end_date;     //2017-08-30
        public List<holidays>  holidays;
    }
    public class holidays {
        public String date_value;   //2017-07-25
    }
    @future(callout=true)
    public static void ScheduleDetailsToWebAppFuture(Id woId){    
        system.debug('sendInsuranceDetailsToWebApp1'+woId);
        ScheduleDetailsToWebApp(woId);
    }
    
    public static void ScheduleDetailsToWebApp(Id woId){   
        system.debug('ScheduleDetailsToWebApp'+woId);
        String respBody;
        try{
            List<NestAway_End_Point__c > nestURL = NestAway_End_Point__c.getall().values();  
            workOrder  wo = [select id,API_Success__c,API_Info__c,Last_API_Sync_Time__c,house__c  from workOrder    where id=:woId];    
            ScheduleDetailsWrapeerClass rp = new ScheduleDetailsWrapeerClass();
            rp.House_sf_id=wo.house__c;
            string strEndPoint= nestURL[0].House_Insurance_API__c+'&auth='+nestURL[0].Webapp_Auth__c;
            HttpResponse res;
            string jsonBody; 
            jsonBody=JSON.serialize(rp);
            res=RestUtilities.httpRequest('POST',strEndPoint,jsonBody,null);
            respBody= res.getBody();
            system.debug('***RespBody'+respBody);               
            ScheduleDetailsJsonRespWrapeerClass dataJson= new ScheduleDetailsJsonRespWrapeerClass();
            dataJson = (ScheduleDetailsJsonRespWrapeerClass)JSON.deserialize(respBody,ScheduleDetailsJsonRespWrapeerClass.Class);              
            if(dataJson!=null){
                if(res.getStatusCode()==200){
                    //wo.API_Success__c = dataJson.success;
                    wo.API_Info__c   = dataJson.info;      
                    wo.Last_API_Sync_Time__c = System.now();  
                   
            }
            }
           
        }
        catch(exception e){
            
            UtilityClass.insertErrorLog('Webservices','Insurance Api failed  error:'+e.getMessage()+' at line:'+e.getLineNumber()+' **RespBody'+respBody); 
        }                   
    }
    
}