public class OrderItemTriggerHandler {

	public static void ChangePOStageToVerified(Map<Id, OrderItem> newMap, Map<Id, OrderItem> oldMap){

		System.debug('ChangePOStageToVerified is executed');

		Map<Id, List<OrderItem>> orderItemMap = new Map<Id, List<OrderItem>>();

		for(OrderItem oi: newMap.values()){
			if(oi.Verified__c == true && oldMap.get(oi.Id).Verified__c == false){
				if(orderItemMap.containsKey(oi.OrderId)){
					orderItemMap.get(oi.OrderId).add(oi);
				} else {
					orderItemMap.put(oi.OrderId, new List<OrderItem>{oi});
				}
			}
		}

		List<Order> ordList = [select Id, Status, (select Id, Verified__c from OrderItems) from Order
							   where RecordType.Name = 'Purchase Order' and Id =: orderItemMap.keySet()];

		List<Order> updatedOrdList = new List<Order>();							   

		for(Order ord: ordList){

			Boolean poVerified = true; 

			for(OrderItem oi: ord.OrderItems){
				if(oi.verified__c == false){
					poVerified = false;
				}
			}

			ord.Status = 'PO Verified';

			if(poVerified){
				updatedOrdList.add(ord);
			}
		}

		System.debug(updatedOrdList);

		if(!updatedOrdList.isEmpty()){
			update updatedOrdList;
		}

		if(!orderItemMap.isEmpty()){
			InvoiceUtility.createInvoicesForOrders(orderItemMap);
		}
	}

	static Id poRecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get(Constants.ORDER_RT_PURCHASE_ORDER).getRecordTypeId();	

/*********************************************
 * Created By : Mohan
 * Purpose    : afterInsert Handler
 *********************************************/	
 	public static void afterInsert(Map<Id, OrderItem> newMap){ 		

        try{
		 		Set<Id> orderIdSet = new Set<Id>();
		 		List<OrderItem> oiList = new List<OrderItem>();	
		 		List<Order_Item__c> ordItemList = new List<Order_Item__c>();	

		 		for(OrderItem oi: newMap.values()){
		 			orderIdSet.add(oi.OrderId);
		 		}

		 		Map<Id, Order> orderMap = new Map<Id, Order>([select Id from Order where Id =: orderIdSet and 
		 													  RecordTypeId =: poRecordTypeId]);

		 		for(OrderItem oi: newMap.values()){

		 			if(orderMap.containsKey(oi.OrderId)){
		 				oiList.add(oi);
		 			}
		 		}

		 		if(!oiList.isEmpty()){
		 			OrderItemTriggerHelper.updateAmountOnPO(oiList, ordItemList);
		 		}

            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'After Insert on Order Product');      
            }   
 	}

/*********************************************
 * Created By : Mohan
 * Purpose    : afterUpdate Handler
 *********************************************/	
 	public static void afterUpdate(Map<Id, OrderItem> newMap, Map<Id, OrderItem> oldMap){ 		

        try{
		 		Set<Id> orderIdSet = new Set<Id>();
		 		List<OrderItem> oiList = new List<OrderItem>();	
		 		List<Order_Item__c> ordItemList = new List<Order_Item__c>();	

		 		for(OrderItem oi: newMap.values()){

		 			//check if there is change in Amount
		 			if(oi.TotalPrice != oldMap.get(oi.Id).TotalPrice){
		 				orderIdSet.add(oi.OrderId);
		 			}		 			
		 		}

		 		Map<Id, Order> orderMap = new Map<Id, Order>([select Id from Order where Id =: orderIdSet and 
		 													  RecordTypeId =: poRecordTypeId]);

		 		for(OrderItem oi: newMap.values()){

		 			if(orderMap.containsKey(oi.OrderId)){
		 				oiList.add(oi);
		 			}
		 		}

		 		if(!oiList.isEmpty()){
		 			OrderItemTriggerHelper.updateAmountOnPO(oiList, ordItemList);
		 		}

            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'After Insert on Order Product');      
            }   
 	} 	
}