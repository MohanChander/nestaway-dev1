@istest
public class CaseOffboardingTriggerHandlerTest {
     Public Static TestMethod Void doTest(){
        Id MoveIn_RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEIN).getRecordTypeId();
        Id RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Furnished House Onboarding').getRecordTypeId();
        Id MoveOutRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEOUT).getRecordTypeId();
        Id caseOccupiedFurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
        
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
         
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        insert hos;
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        accObj.PAN_Number__c='CAVPK1234V';
        insert accObj;
         Bank_Detail__c bk=new Bank_Detail__c();
            bk.Related_Account__c=accObj.id;
            bk.Account_Number__c='123456709876';
            bk.Bank_Name__c='SBI';
            bk.Branch_Name__c='Golmuri';
            bk.IFSC_Code__c='HDFC0001876';
            bk.Is_A_House_Owner__c=true;
            insert bk;
        Id OffboardingZoneRTId =  Schema.SObjectType.Zone__c.getRecordTypeInfosByName().get(Constants.ZONE_RECORD_TYPE_HOUSE_OFFBOARDING_ZONE).getRecordTypeId();
        zone__c zc= new zone__c();
        zc.RecordTypeId=OffboardingZoneRTId;
        zc.Zone_code__c ='123';
        zc.Name='Test';
        insert zc;
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        /*Booking__c b= new Booking__c();
        b.Tenant__c=accObj.Id;
        insert b;*/
        Tenancy__c b = new Tenancy__c();
        b.Tenant__c=accObj.Id;
        b.House__c=hos.id;
        insert b;
        Case c=  new Case();
        c.Status='new';
        c.HouseForOffboarding__c=hos.Id;
        c.ownerId=newuser.Id;
        c.MoveOut_Slot_End_Time__c=System.today();
        c.Moveout_Slot_Start_Time__c=System.today();
        c.Move_Out_Date__c=System.today();
        c.Booked_Object_ID__c='162';
        c.Move_Out_Status__c='In Progress';
          c.Move_Out_Type__c='Rent Default';
        c.Booked_Object_Type__c='Room';
        c.MoveOut_Executive__c=newuser.Id;
        c.AccountId=accObj.id;
        c.RecordTypeId= caseOccupiedFurnishedRecordtype;
        c.Booking_Id__c='389769';
        //c.Type='MoveOut';
        c.Room_Term__c=rt.id;        
        c.Tenant__c=accObj.id;
        c.Contract_End_Date__c=system.today();
        insert c;
        List<Case>  cList =  new List<Case>();
        cList.add(c);
        Map<id,Case> newmap =  new Map<Id,Case>();
        newmap.put(c.id,c);
        c.Moveout_Slot_Start_Time__c=System.today()+1;
        update c;
        Map<id,Case> oldmap =  new Map<Id,Case>();
        Map<id,Account> Accountmap =  new Map<Id,Account>();
        Accountmap.put(accObj.id,accObj);
        Map<id,House__c> Housemap =  new Map<Id,House__c>();
        Housemap.put(hos.id,hos);
        Map<id,id> idmap =  new Map<Id,id>();
        idmap.put(c.id,c.id);
        newmap.put(c.id,c);
        Set<id> caseid= new Set<id>();
        caseid.add(c.id);
        Test.startTest();
        CaseOffboardingTriggerHandler.onBeforeInsertCaseOffboarding(clist);
         CaseOffboardingTriggerHandler.onAfterInsertCaseOffboarding(CList,newmap);
         CaseOffboardingTriggerHandler.onBeforeUpdateandInsert(Clist);
        Test.stopTest();
    }
      Public Static TestMethod Void doTest2(){
        Id MoveIn_RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEIN).getRecordTypeId();
        Id RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Furnished House Onboarding').getRecordTypeId();
        Id MoveOutRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEOUT).getRecordTypeId();
         User newUser = Test_library.createStandardUser(1);
        insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        insert hos;
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        accObj.PAN_Number__c='CAVPK1234V';
        insert accObj;
         Bank_Detail__c bk=new Bank_Detail__c();
            bk.Related_Account__c=accObj.id;
            bk.Account_Number__c='123456709876';
            bk.Bank_Name__c='SBI';
            bk.Branch_Name__c='Golmuri';
            bk.IFSC_Code__c='HDFC0001876';
            bk.Is_A_House_Owner__c=true;
            insert bk;
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        /*Booking__c b= new Booking__c();
        b.Tenant__c=accObj.Id;
        insert b;*/
        Tenancy__c b = new Tenancy__c();
        b.Tenant__c=accObj.Id;
        b.House__c=hos.id;
        insert b;
        id caseOccupiedFurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
        id caseOccupiedUnfurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_UNFURNISHED_OFFBOARDING).getRecordTypeId();
        id caseUnoccupiedFurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_UNOCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
        id caseUnoccupiedUnfurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_UNOCCUPIED_UNFURNISHED_OFFBOARDING).getRecordTypeId();
        id ownerSattlementCaseRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OWNER_SETTLEMENT).getRecordTypeId();
        id nestSattlementCaseRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_NESTAWAY_SETTLEMENT).getRecordTypeId();
        
        Case c=  new Case();
        c.Status='new';
        c.HouseForOffboarding__c=hos.Id;
        c.ownerId=newuser.Id;
        c.MoveOut_Slot_End_Time__c=System.today();
        c.Moveout_Slot_Start_Time__c=System.today();
        c.Move_Out_Date__c=System.today();
        c.Booked_Object_ID__c='162';
        c.Move_Out_Status__c='In Progress';
        c.Move_Out_Type__c='Rent Default';
        c.Booked_Object_Type__c='Room';
        c.MoveOut_Executive__c=newuser.Id;
        c.AccountId=accObj.id;
        c.RecordTypeId= nestSattlementCaseRecordtype;
        c.Booking_Id__c='389769';
        //c.Type='MoveOut';
        c.Room_Term__c=rt.id;        
        c.Tenant__c=accObj.id;
        c.Contract_End_Date__c=system.today();
        insert c;
      
        
    }
     Public Static TestMethod Void doTest3(){
        Id MoveIn_RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEIN).getRecordTypeId();
        Id RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Furnished House Onboarding').getRecordTypeId();
        Id MoveOutRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEOUT).getRecordTypeId();
        
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        insert hos;
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        accObj.PAN_Number__c='CAVPK1234V';
        insert accObj;
         Bank_Detail__c bk=new Bank_Detail__c();
         bk.Related_Account__c=accObj.id;
         bk.Account_Number__c='123456709876';
         bk.Bank_Name__c='SBI';
         bk.Branch_Name__c='Golmuri';
         bk.IFSC_Code__c='HDFC0001876';
         bk.Is_A_House_Owner__c=true;
            insert bk;
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        /*Booking__c b= new Booking__c();
        b.Tenant__c=accObj.Id;
        insert b;*/
        Tenancy__c b = new Tenancy__c();
        b.Tenant__c=accObj.Id;
        b.House__c=hos.id;
        insert b;
        id caseOccupiedFurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
        id caseOccupiedUnfurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_UNFURNISHED_OFFBOARDING).getRecordTypeId();
        id caseUnoccupiedFurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_UNOCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
        id caseUnoccupiedUnfurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_UNOCCUPIED_UNFURNISHED_OFFBOARDING).getRecordTypeId();
        id ownerSattlementCaseRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OWNER_SETTLEMENT).getRecordTypeId();
        id nestSattlementCaseRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_NESTAWAY_SETTLEMENT).getRecordTypeId();
        
        Case c=  new Case();
        c.Status='new';
        c.HouseForOffboarding__c=hos.Id;
        c.ownerId=newuser.Id;
        c.Booked_Object_ID__c='162';
        c.Move_Out_Status__c='In Progress';
        c.Move_Out_Type__c='Rent Default';
        c.Booked_Object_Type__c='Room';
        c.MoveOut_Executive__c=newuser.Id;
        c.AccountId=accObj.id;
        c.RecordTypeId= ownerSattlementCaseRecordtype;
        c.Booking_Id__c='389769';
        //c.Type='MoveOut';
        c.Room_Term__c=rt.id;        
        c.Tenant__c=accObj.id;
        c.Contract_End_Date__c=system.today();
        insert c;
      
    }
    Public Static TestMethod Void doTest4(){
        Id MoveIn_RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEIN).getRecordTypeId();
        Id RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Furnished House Onboarding').getRecordTypeId();
        Id MoveOutRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEOUT).getRecordTypeId();
        
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        insert hos;
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        accObj.PAN_Number__c='CAVPK1234V';
        insert accObj;
         Bank_Detail__c bk=new Bank_Detail__c();
            bk.Related_Account__c=accObj.id;
            bk.Account_Number__c='123456709876';
            bk.Bank_Name__c='SBI';
            bk.Branch_Name__c='Golmuri';
            bk.IFSC_Code__c='HDFC0001876';
            bk.Is_A_House_Owner__c=true;
            insert bk;
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        /*Booking__c b= new Booking__c();
        b.Tenant__c=accObj.Id;
        insert b;*/
        Tenancy__c b = new Tenancy__c();
        b.Tenant__c=accObj.Id;
        b.House__c=hos.id;
        insert b;
        id caseOccupiedFurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
        id caseOccupiedUnfurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_UNFURNISHED_OFFBOARDING).getRecordTypeId();
        id caseUnoccupiedFurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_UNOCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
        id caseUnoccupiedUnfurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_UNOCCUPIED_UNFURNISHED_OFFBOARDING).getRecordTypeId();
        id ownerSattlementCaseRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OWNER_SETTLEMENT).getRecordTypeId();
        id nestSattlementCaseRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_NESTAWAY_SETTLEMENT).getRecordTypeId();
        
        Case c=  new Case();
        c.Status='new';
        c.HouseForOffboarding__c=hos.Id;
        c.ownerId=newuser.Id;
        c.Booked_Object_ID__c='162';
        c.Move_Out_Status__c='In Progress';
        c.Move_Out_Type__c='Rent Default';
        c.Booked_Object_Type__c='Room';
        c.MoveOut_Executive__c=newuser.Id;
        c.AccountId=accObj.id;
        c.RecordTypeId= caseOccupiedFurnishedRecordtype;
        c.Booking_Id__c='389769';
        //c.Type='MoveOut';
        c.Room_Term__c=rt.id;        
        c.Tenant__c=accObj.id;
        c.Contract_End_Date__c=system.today();
        insert c;
        c.Stage__c = Constants.CASE_STAGE_OWNER_RETAINED;
        update c;
      
    }
     Public Static TestMethod Void doTest5(){
        Id MoveIn_RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEIN).getRecordTypeId();
        Id RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Furnished House Onboarding').getRecordTypeId();
        Id MoveOutRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEOUT).getRecordTypeId();
        
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        insert hos;
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        accObj.PAN_Number__c='CAVPK1234V';
        insert accObj;
         Bank_Detail__c bk=new Bank_Detail__c();
            bk.Related_Account__c=accObj.id;
            bk.Account_Number__c='123456709876';
            bk.Bank_Name__c='SBI';
            bk.Branch_Name__c='Golmuri';
            bk.IFSC_Code__c='HDFC0001876';
            bk.Is_A_House_Owner__c=true;
            insert bk;
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        /*Booking__c b= new Booking__c();
        b.Tenant__c=accObj.Id;
        insert b;*/
        Tenancy__c b = new Tenancy__c();
        b.Tenant__c=accObj.Id;
        b.House__c=hos.id;
        insert b;
        id caseOccupiedFurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
        id caseOccupiedUnfurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_UNFURNISHED_OFFBOARDING).getRecordTypeId();
        id caseUnoccupiedFurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_UNOCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
        id caseUnoccupiedUnfurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_UNOCCUPIED_UNFURNISHED_OFFBOARDING).getRecordTypeId();
        id ownerSattlementCaseRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OWNER_SETTLEMENT).getRecordTypeId();
        id nestSattlementCaseRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_NESTAWAY_SETTLEMENT).getRecordTypeId();
        
        Case c=  new Case();
        c.Status='new';
        c.HouseForOffboarding__c=hos.Id;
        c.ownerId=newuser.Id;
        c.Booked_Object_ID__c='162';
        c.Move_Out_Status__c='In Progress';
        c.Move_Out_Type__c='Rent Default';
        c.Booked_Object_Type__c='Room';
        c.MoveOut_Executive__c=newuser.Id;
        c.AccountId=accObj.id;
        c.RecordTypeId= caseOccupiedFurnishedRecordtype;
        c.Booking_Id__c='389769';
        //c.Type='MoveOut';
        c.Room_Term__c=rt.id;        
        c.Tenant__c=accObj.id;
        c.Contract_End_Date__c=system.today();
        insert c;

        c.Stage__c = Constants.CASE_STAGE_AGREEMENT_CANCELLED;
        c.Agreement_Cancelled__c='Yes';
        update c;

        c.Stage__c = Constants.CASE_STAGE_KEY_SUBMITTED;
        c.Keys_Submitted__c ='Yes';
        update c;
       
      
    }
      Public Static TestMethod Void doTest6(){
        Id MoveIn_RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEIN).getRecordTypeId();
        Id RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Furnished House Onboarding').getRecordTypeId();
        Id MoveOutRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEOUT).getRecordTypeId();
        
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        insert hos;
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        accObj.PAN_Number__c='CAVPK1234V';
        insert accObj;
         Bank_Detail__c bk=new Bank_Detail__c();
            bk.Related_Account__c=accObj.id;
            bk.Account_Number__c='123456709876';
            bk.Bank_Name__c='SBI';
            bk.Branch_Name__c='Golmuri';
            bk.IFSC_Code__c='HDFC0001876';
            bk.Is_A_House_Owner__c=true;
            insert bk;
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        /*Booking__c b= new Booking__c();
        b.Tenant__c=accObj.Id;
        insert b;*/
        Tenancy__c b = new Tenancy__c();
        b.Tenant__c=accObj.Id;
        b.House__c=hos.id;
        insert b;
        id caseOccupiedFurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
        id caseOccupiedUnfurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_UNFURNISHED_OFFBOARDING).getRecordTypeId();
        id caseUnoccupiedFurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_UNOCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
        id caseUnoccupiedUnfurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_UNOCCUPIED_UNFURNISHED_OFFBOARDING).getRecordTypeId();
        id ownerSattlementCaseRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OWNER_SETTLEMENT).getRecordTypeId();
        id nestSattlementCaseRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_NESTAWAY_SETTLEMENT).getRecordTypeId();
        
        Case c=  new Case();
        c.Status='Open';
        c.House__c=hos.Id;
        c.ownerId=newuser.Id;
        c.Booked_Object_ID__c='162';
        c.Booked_Object_Type__c='Room';
        c.MoveOut_Executive__c=newuser.Id;
        c.AccountId=accObj.id;
        c.RecordTypeId= ownerSattlementCaseRecordtype;
        c.Booking_Id__c='389769';
        //c.Type='MoveOut';
        c.Room_Term__c=rt.id;        
        c.Tenant__c=accObj.id;
        c.Contract_End_Date__c=system.today();
        insert c;
        c.Stage__c='Cheque Acknowledgement';
        c.Cheque_Status__c = Constants.CASE_CHEQUE_STATUS_CHEQUE_NOT_RECEIVED;
        update c;
        c.Stage__c='Cheque Clearance';
        c.Cheque_Status__c = Constants.CASE_CHEQUE_STATUS_CHEQUE_BOUNCED;
        update c;
       
    }
     Public Static TestMethod Void doTest7(){
        Id MoveIn_RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEIN).getRecordTypeId();
        Id RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Furnished House Onboarding').getRecordTypeId();
        Id MoveOutRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEOUT).getRecordTypeId();
        
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        insert hos;
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        accObj.PAN_Number__c='CAVPK1234V';
        insert accObj;
         Bank_Detail__c bk=new Bank_Detail__c();
            bk.Related_Account__c=accObj.id;
            bk.Account_Number__c='123456709876';
            bk.Bank_Name__c='SBI';
            bk.Branch_Name__c='Golmuri';
            bk.IFSC_Code__c='HDFC0001876';
            bk.Is_A_House_Owner__c=true;
            insert bk;
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        /*Booking__c b= new Booking__c();
        b.Tenant__c=accObj.Id;
        insert b;*/
        Tenancy__c b = new Tenancy__c();
        b.Tenant__c=accObj.Id;
        b.House__c=hos.id;
        insert b;
        id caseOccupiedFurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
        id caseOccupiedUnfurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_UNFURNISHED_OFFBOARDING).getRecordTypeId();
        id caseUnoccupiedFurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_UNOCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
        id caseUnoccupiedUnfurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_UNOCCUPIED_UNFURNISHED_OFFBOARDING).getRecordTypeId();
        id ownerSattlementCaseRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OWNER_SETTLEMENT).getRecordTypeId();
        id nestSattlementCaseRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_NESTAWAY_SETTLEMENT).getRecordTypeId();
        
        Case c=  new Case();
        c.Status='Open';
        c.HouseForOffboarding__c=hos.Id;
        c.ownerId=newuser.Id;
        c.Booked_Object_ID__c='162';
        c.Booked_Object_Type__c='Room';
        c.MoveOut_Executive__c=newuser.Id;
        c.AccountId=accObj.id;
        c.RecordTypeId= ownerSattlementCaseRecordtype;
        c.Booking_Id__c='389769';
        //c.Type='MoveOut';
        c.Room_Term__c=rt.id;        
        c.Tenant__c=accObj.id;
        c.Contract_End_Date__c=system.today();
        insert c;
        c.Stage__c= Constants.CASE_STAGE_ACCOUNTING_ENTRY_PASSED;
        update c;
       
    }
     Public Static TestMethod Void doTest8(){
        Id MoveIn_RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEIN).getRecordTypeId();
        Id RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Furnished House Onboarding').getRecordTypeId();
        Id MoveOutRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEOUT).getRecordTypeId();
       
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.HRM__c=newuser.id;
        hos.ZAM__c=newuser.id;
        hos.Assets_Created__c=false;
        insert hos;
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        accObj.PAN_Number__c='CAVPK1234V';
        insert accObj;
         Bank_Detail__c bk=new Bank_Detail__c();
            bk.Related_Account__c=accObj.id;
            bk.Account_Number__c='123456709876';
            bk.Bank_Name__c='SBI';
            bk.Branch_Name__c='Golmuri';
            bk.IFSC_Code__c='HDFC0001876';
            bk.Is_A_House_Owner__c=true;
            insert bk;
        Id OffboardingZoneRTId =  Schema.SObjectType.Zone__c.getRecordTypeInfosByName().get(Constants.ZONE_RECORD_TYPE_HOUSE_OFFBOARDING_ZONE).getRecordTypeId();
        zone__c zc= new zone__c();
        zc.RecordTypeId=OffboardingZoneRTId;
        zc.Zone_code__c ='123';
        zc.Name='Test';
        insert zc;
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        /*Booking__c b= new Booking__c();
        b.Tenant__c=accObj.Id;
        insert b;*/
        Tenancy__c b = new Tenancy__c();
        b.Tenant__c=accObj.Id;
        b.House__c=hos.id;
        insert b;
        id caseOccupiedFurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
        id caseOccupiedUnfurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_UNFURNISHED_OFFBOARDING).getRecordTypeId();
        id caseUnoccupiedFurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_UNOCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
        id caseUnoccupiedUnfurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_UNOCCUPIED_UNFURNISHED_OFFBOARDING).getRecordTypeId();
        id ownerSattlementCaseRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OWNER_SETTLEMENT).getRecordTypeId();
        id nestSattlementCaseRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_NESTAWAY_SETTLEMENT).getRecordTypeId();
       
        Case c=  new Case();
        c.Status='new';
        c.HouseForOffboarding__c=hos.Id;
        c.ownerId=newuser.Id;
        c.MoveOut_Slot_End_Time__c=System.today();
        c.Moveout_Slot_Start_Time__c=System.today();
        c.Move_Out_Date__c=System.today();
        c.Booked_Object_ID__c='162';
        c.Move_Out_Status__c='In Progress';
        c.AccountId=accObj.id;
        c.Move_Out_Type__c='Rent Default';
        c.Booked_Object_Type__c='Room';
        c.MoveOut_Executive__c=newuser.Id;
        c.RecordTypeId= caseOccupiedFurnishedRecordtype;
        c.Booking_Id__c='389769';
        //c.Type='MoveOut';
        c.Room_Term__c=rt.id;        
        c.Tenant__c=accObj.id;
        c.Contract_End_Date__c=system.today();
        insert c;

        Case nt=new case();
        nt.ParentId=c.id;
        nt.Amount__c=34568;
        nt.RecordTypeId=nestSattlementCaseRecordtype;
        nt.Status='New';
        insert nt;
    }

}