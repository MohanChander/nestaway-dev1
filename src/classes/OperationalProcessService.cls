Public class OperationalProcessService{


// Method to send the query string for the contract versioning clone.

 public static string getContractCloneFieldsQueryString(){
 
      string queryString=null;
      
        List<Operational_Process__c> opertionalProcessLst=OperationalProcessSelector.getContractVersioningFieldsLst();
        if(opertionalProcessLst.size()>0){
            queryString='';
        }
        for(Operational_Process__c op: opertionalProcessLst){
            
              queryString += ', ' + op.Field_API_Name__c;
        }
      
       
    return queryString;
 }




}