@isTest
public class FieldServiceTestLibrary {
	
	//create Problem Object
    public static Problem__c createProblem(){
        
        Problem__c prob = new Problem__c();
        prob.Name = 'Cleaning --> Deep Cleaning';
        
        return prob;
    }


    //create Operating Hours Object
    public static void createOperatingHours(String operatingHourName){

    	OperatingHours ohs = new OperatingHours();
    	ohs.Name = operatingHourName;

    	insert ohs;

    	TimeSlot ts = new TimeSlot();
    	ts.DayOfWeek = 'Monday';
    	ts.StartTime = Time.newInstance(09, 00, 00, 00);
    	ts.EndTime = Time.newInstance(20, 00, 00, 00);
    	ts.OperatingHoursId = ohs.Id;
    	ts.Type = 'Normal';

    	insert ts;
    }

}