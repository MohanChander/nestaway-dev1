/*
 Created By baibhav

*/


public class HouseOffboardingTriggerHandler 
{
    // Commented By mohan
    public static void onAfterUpdateHouse(Map<Id, House__c> newMap, Map<Id, House__c> oldMap)
    {
      try{
            List<House__c> houseOffboardList = new List<House__c>();
            for(House__c house: newMap.values()){
              if(house.Initiate_Offboarding__c == 'Yes' && house.Initiate_Offboarding__c != oldMap.get(house.Id).Initiate_Offboarding__c){
                houseOffboardList.add(house);
              }
            }

            if(!houseOffboardList.isEmpty())
              HouseTriggerHelper.houseOffboardingCaseCreation(houseOffboardList);
        } catch(Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e, 'House Offboarding');      
        }          
    }

    public static void onBeforeUpdateHouse(List<House__c> houseList)
    {
     /*   
        for(House__c hou:houseList)
        {
         if(hou.Initiate_Offboarding__c == Constants.HOUSE_INITATE_OOBOARDING_YES)
            {
                hou.Listing__c = Constants.HOUSE_LISTING_OFF;
                hou.Payment__c= Constants.HOUSE_PAYMENT_OFF;
            }    

        }*/ 
        
    } //Commented By Mohan 
}