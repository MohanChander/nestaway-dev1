/***********************************************************
* Created By:   Sanjeev Shukla(KVP Business Solutions)
* Created Date: 18/March/2017
* Description : In this class, I wrote the functionality to show Opportunity custom Sales Path.
************************************************************/

public class OpportunitySalesPathController {
    private final Opportunity   opp;
    private final Contract      con;
    public List<String>         stageList {get;set;}
    public Map<String, Integer> stageMap {get;set;}
    public Integer              currentStatusNum {get;set;}
    public Integer              currentStageNum {get;set;}
    public OpportunitySalesPathController(ApexPages.StandardController stdController){
        this.opp            = (Opportunity)stdController.getRecord(); // get the Opportunity record
        currentStatusNum    = 0;
        currentStageNum     = 0;
        Integer index       = 0;
        stageMap            = new Map<String, Integer>(); 
        stageList           = new List<String>();
        
        /*******************************************************
        * Get Lead Status values dynamically using schema. 
        ********************************************************/
        Schema.DescribeFieldResult F = Opportunity.StageName.getDescribe();
        List<Schema.PicklistEntry> P = F.getPicklistValues();
        for(Schema.PicklistEntry PE : P){
            stageMap.put(PE.getValue(), index);
            index++;
        }
        currentStatusNum = stageMap.get(opp.StageName);
        currentStageNum  = stageMap.get(opp.StageName);
        if(opp.StageName == 'Sample Contract'){
            try{
                con = [SELECT Id, Furnishing_Plan__c FROM Contract WHERE Opportunity__c =: opp.Id ORDER BY CreatedDate DESC LIMIT 1];
            }catch(Exception ex){}
        }
        
        /*******************************************************
        * Get Lead Status values based on few conditions 
        ********************************************************/
        if(con != NULL && con.Furnishing_Plan__c == 'Owner Complete' && opp.StageName == 'Sample Contract'){
            stageList.add('House Inspection');
            stageList.add('Sample Contract');
            stageList.add('Final Contract');
        }else if(opp.StageName == 'Lost' || opp.StageName == 'Dropped'){
            List<OpportunityFieldHistory> histories = [SELECT Id, OldValue FROM OpportunityFieldHistory 
                                                       WHERE Field = 'StageName'];
            Set<String> stageSet = new Set<String>();
            for(OpportunityFieldHistory oppHis : histories){
                if(String.valueOf(oppHis.OldValue) != 'Lost' && String.valueOf(oppHis.OldValue) != 'Dropped'){
                    stageSet.add(String.valueOf(oppHis.OldValue));
                }
            }
            for(Schema.PicklistEntry PE : P){
                if(stageSet.contains(PE.getValue())){
                    stageList.add(PE.getValue());
                }
            }
            if(opp.StageName == 'Lost'){
                stageList.add('Lost');
            }else{
                stageList.add('Dropped');
            }
        }else{
            for(Schema.PicklistEntry PE : P){
                if(PE.getValue() != 'Lost' && PE.getValue() != 'Dropped'){
                    stageList.add(PE.getValue());
                }
            }
        }
    }
}