public class HouseRentPageExtension02 {

  public House__c house {get; set;}
  public List<Room_Terms__c> roomList {get; set;}
  public Id houseId {get; set;}

  public HouseRentPageExtension02(ApexPages.StandardController controller){

    houseId = ApexPages.currentPage().getParameters().get('id');
    queryMethod(houseId);

  }

  public void queryMethod(Id houseId){
    house = HouseSelector.getHouseDetailsFromHouseId(houseId);
    roomList = RoomTermsSelector.getRoomTermsFromHouseId(houseId);    
  }

  public void updateRent(){
    try{
        // Added by chandu to fire the house APi only one time.
        HouseJsonOptimizer.houseJsonObjectInitializer(Constants.ROOM_RENT_PAGE); 
        if(house.Booking_Type__c == Constants.HOUSE_BOOKING_TYPE_SHARED_HOUSE){
          List<Bed__c> bedList = new List<Bed__c>();
          for(Room_Terms__c room: roomList){
            for(Bed__c bed: room.Beds1__r)
              bedList.add(bed);
          }
          update roomList;
          update bedList;
        }
        update house;       
        //Added by chandu . fire house API to update all data one time.
        House2Json.createJSON(house.Id);
        
        queryMethod(houseId);  
      } Catch (Exception e){

      }
  }

  public void customCancel(){
    queryMethod(houseId);
  }
}