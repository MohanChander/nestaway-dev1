global class UpdateMigrationHouseWithAllInfo implements Database.Batchable<sObject>{

   global final String Query;
  

   global UpdateMigrationHouseWithAllInfo(String q){

      Query=q;
   }

   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<sObject> obj){
      
            Set<Id> oppIdSet = new Set<Id>();
            Set<Id> houseIds = new Set<id>();
            List<House__c>  newHouses=(List<House__c>)obj;
             List<Room_Terms__c> roomsToBeUpdated = new List<Room_Terms__c>();
            for(House__c house: newHouses){
                if(house.Opportunity__c!=null){   
                    oppIdSet.add(house.Opportunity__c);
                }
            }
            
         //   Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>(OpportunitySelector.getOpportunitiesWithChilds(oppIdSet));
               Map<Id, List<Room_Terms__c>> roomTermMap = new Map<Id, List<Room_Terms__c>>();
               List<Contract> contractList = new List<Contract>();    
            //query for Room Term records and populate the roomTermMap 
            for(Room_Terms__c roomTerm: RoomTermsSelector.getRoomTermsForOpportunities(oppIdSet)){
                Id oppId = roomTerm.Contract__r.Opportunity__c;
                if(roomTermMap.containsKey(oppId)){
                    roomTermMap.get(oppId).add(roomTerm);
                } else {
                    roomTermMap.put(oppId, new List<Room_Terms__c>{roomTerm});
                }                            
            }
            
              //Associate the Opportunity childs with the House 
            for(House__c house: newHouses){
                
                houseIds.add(house.id);
             /*      
                if(house.Opportunity__c!=null && oppMap.containsKey(house.Opportunity__c)){
                    
                    Opportunity opty = oppMap.get(house.Opportunity__c);                 
                   
                    
                 for(Contract con: opty.Contracts__r){
                        
                        if(con.House__c==null){
                            con.House__c = house.Id;
                            contractList.add(con);
                        }
                        
                    }
                    
                } 
                */
               
                if(!roomTermMap.isEmpty() && roomTermMap.containsKey(house.Opportunity__c) && !roomTermMap.get(house.Opportunity__c).isEmpty()){
                    for(Room_Terms__c room: roomTermMap.get(house.Opportunity__c)){
                        
                        if(room.House__c==null){
                             room.House__c = house.Id;
                             roomsToBeUpdated.add(room);
                        }
                       
                    }
                }               
               
                
            }
            
              if(!roomsToBeUpdated.isEmpty()){
                try {
                    update roomsToBeUpdated;
                } catch (Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e);                    
                }
                
            }  
            /*
            if(!contractList.isEmpty()){
                try{
                    update contractList;
                } catch (Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e);                    
                }
                
            }
            
            */
      
    }

   global void finish(Database.BatchableContext BC){
   }
}