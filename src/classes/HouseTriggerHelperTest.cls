/*  Created By : Mohan - WarpDrive Tech Works
Purpose    : Test Class for HouseTriggerHelper */

@isTest
public class HouseTriggerHelperTest {

    public static id caseOccupiedFurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
    public static id caseOccupiedUnfurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_UNFURNISHED_OFFBOARDING).getRecordTypeId();
    public static id caseUnoccupiedFurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_UNOCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
    public static id caseUnoccupiedUnfurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_UNOCCUPIED_UNFURNISHED_OFFBOARDING).getRecordTypeId();
    public static id OffboardingZoneRTId =  Schema.SObjectType.Zone__c.getRecordTypeInfosByName().get(Constants.ZONE_RECORD_TYPE_HOUSE_OFFBOARDING_ZONE).getRecordTypeId();
     public static Id ZoneRecordTypeId = Schema.SObjectType.Zone__c.getRecordTypeInfosByName().get('Property Management Zone').getRecordTypeId();
    public static Id zuApmRecordTypeId = Schema.SObjectType.Zone_and_OM_Mapping__c.getRecordTypeInfosByName().get('Assistance Property Manager').getRecordTypeId();
    public static Id zuPmRecordTypeId = Schema.SObjectType.Zone_and_OM_Mapping__c.getRecordTypeInfosByName().get('Property Manager').getRecordTypeId();
    
    
    Public Static TestMethod Void doTest1(){
         NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
            cusSet.name = 'nestaway';
            cusSet.Nestaway_URL__c = 'www.test.com';
            insert cusSet;
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
         Opportunity opp= new Opportunity();
       opp.CloseDate=system.today();
        opp.name='deepak';
        opp.StageName = 'House Inspection';
            insert opp;
 
         
           Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        Bank_Detail__c bc = new Bank_Detail__c ();
        bc.Related_Account__c = accobj.id;
        bc.IFSC_Code__c = 'UTIB0001496';
        bc.Account_Number__c  = '67890981234';
        bc.Bank_Name__c ='axix';
        bc.Branch_Name__c  ='BASAVANGUDI';
        insert bc;
        contract c= new Contract();
        c.accountid=accobj.id;
        c.Agreement_Type__c ='loi';
        c.Who_pays_Deposit__c=Constants.CONTRATC_WHO_PAY_DEPOSIT_NESTAWAY ;
        c.SD_Upfront_Amount__c = 76.00;
        c.Furnishing_Type__c = Constants.CONTRACT_FURNISHING_CONDITION_FURNISHED;
        insert c;
          zone__c zc= new zone__c();
        zc.Zone_code__c ='2363';
        zc.Name='Test';
        zc.RecordTypeId=  ZoneRecordTypeId;
        insert zc;
        Zone_and_OM_Mapping__c zu1 = new Zone_and_OM_Mapping__c();
        zu1.User__c = newuser.Id;
        zu1.Zone__c = zc.Id;
        zu1.RecordTypeId = zupmRecordTypeId;
        Insert Zu1;
        
        Zone_and_OM_Mapping__c zu2 = new Zone_and_OM_Mapping__c();
        zu2.User__c = newuser.Id;
        zu2.Property_Manager__c  = zu1.Id;
        zu2.RecordTypeId = zuApmRecordTypeId;
        Insert Zu2;
        
       City__c ct=new City__c();
       ct.name='Bangalore';
       insert ct;
    
        House__c hos= new House__c();
        hos.name='house1';
        hos.Contract__c = c.id;
        hos.Opportunity__c = opp.id;
        hos.City__c='bangalore';
        hos.Street__c ='bh';
        hos.Door_Number__c='jhs';
        hos.Onboarding_Zone__c = zc.Id;
        hos.Onboarding_Zone_Code__c = '12333';
        hos.Feedback_Executive__c = newuser.id;
        hos.Property_Manager__c =newuser.id;
        hos.Rating__c = 5;
        insert hos;
        
         Room_Terms__c rc= new Room_Terms__c();
        rc.House__c= hos.id;
        insert rc;
       
        bed__c bedc= new bed__c();
        bedc.House__c=hos.id;
       bedc.Room_Terms__c=rc.id;
        insert bedc;
        Bathroom__c bac= new Bathroom__c ();
        bac.House__c=hos.Id;
        insert bac;
         Case c1=  new Case();
         c1.HouseForOffboarding__c =hos.Id;
        c1.RecordTypeId=caseOccupiedFurnishedRecordtype;
        c1.Active_Tenants__c=3;   
        c1.Status='Open';
        insert c1;
        List<House__c> houseList= new   List<House__c>();
        houseList.add(hos);
        Set<Id> sethouse= new Set<id>();
        setHouse.add(hos.id);
        Map<id,House__c> housemap= new Map<id,House__c>();
        houseMap.put(hos.id,hos);
        Set<id> houseid = new Set<id>();
        houseid.add(hos.id);
           Test.StartTest();
        Set<Id> setOfhouse = new Set<Id>();
        setOfhouse.add(hos.id);
        HouseTriggerHelper.verifyRentonHouse(housemap,housemap);
        HouseTriggerHelper.houseOffboardingCaseCreation(houseList);
        HouseTriggerHelper.validateInitateOffboarding(houseList);
        HouseTriggerHelper.sendCalloutToOffboarded(sethouse,houseMap);
        HouseTriggerHelper.updateRoomAndBedStatus(setHouse);
        HouseTriggerHelper.initiateOffBoarding(hos.id);
        HouseTriggerHelper.checkOnboardingworkflow(houseList,housemap);
        HouseTriggerHelper.updateContactTenancyType(HouseMap);
        HouseTriggerHelper.checkBedStatus(HouseMap);
        HouseTriggerHelper.userFeedback(houseList);
        HouseTriggerHelper.houseUpdateLive(houseList);
        HouseTriggerHelper.updateRoomAndBedAndRIC(houseList);
        HouseTriggerHelper.feedbackToken(houseid);
        HouseTriggerHelper.populatePMOnHouse(houseList,true);
        //HouseTriggerHelper.generateSdocForLiveHouse(setOfhouse);
        HouseTriggerHelper.changePmMailId(houseList);
           // HouseTriggerHelper.caseOccupiedUnfurnishedRecordtypehouseUpdateLive(housemap);
        Test.stopTest();
    }

    @isTest
    public static void initiateOffBoardingTest(){

        House__c house = TestDataFactory.createHouse();
        house.Initiate_Offboarding__c = 'Yes';
        house.Offboarding_Reason__c = 'Owner has sold the Property';
        update house;

        house.Stage__c = Constants.HOUSE_STAGE_OFFBOARDED;
        update house;
    }


    @isTest
    public static void updateRoomAndBedStatusTest(){

        House__c house = TestDataFactory.createHouse();
        house.HouseId__c = '1234';
        house.Stage__c = Constants.HOUSE_STAGE_HOUSE_LIVE;
        house.Rent_Verified__c = true;
        update house;
    }  


    @isTest
    public static void validateInitateOffboardingTest(){

        House__c house = TestDataFactory.createHouse();
        house.Initiate_Offboarding__c = 'Yes';
        house.Offboarding_Reason__c = 'Owner has sold the Property';
        house.Street__c = '19th Main';
        update house;

        try {
                house.Initiate_Offboarding__c = 'No';
                update house;
            } catch (Exception e){

            }
    }      
       
}