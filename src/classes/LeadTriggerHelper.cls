/**********************************************
    Created By : Mohan
    Purpose    : Helper Class for LeadTriggerHandler
**********************************************/
public class LeadTriggerHelper {

/**********************************************
    Created By : Mohan
    Purpose    : on change of New Stage Escalation Level or Open Stage Escalation Level populate the Next Escalation Email
                 and Next Escalation User

    Edited by  : Baibhav
    Purpose    : To populate leve1,level2,leve3 Excalation email reciptent                 
**********************************************/
	public static void populateNextEscalationEmail(List<Lead> leadList,Map<id,lead> oldmap){

        System.debug('**populateNextEscalationEmail');

        try{	
                Set<Id> ownerIdSet = new Set<Id>();
        		Set<Id> userIdSet = new Set<Id>();
                Map<Id, User> userMap = new Map<Id, User>();

        		for(Lead l: leadList){
        			if(l.Escalation_User__c != null){
        				userIdSet.add(l.Escalation_User__c);
        			} else if(string.valueOf(l.OwnerId).startsWith('005')){
        				userIdSet.add(l.OwnerId);
        			}
        		}

                if(!userIdSet.isEmpty()){
                    userMap = new Map<Id, User>(UserSelector.getUserDetails(userIdSet));
                }    

        		for(Lead l: leadList){


                    if(l.Escalation_User__c != null && userMap.containsKey(l.Escalation_User__c)){
                        if(userMap.get(l.Escalation_User__c).ManagerId != null){
                            l.Next_Escalation_Email__c = userMap.get(l.Escalation_User__c).Manager.Email;
                            l.Escalation_User__c = userMap.get(l.Escalation_User__c).ManagerId;
                        }
                    } else if(string.valueOf(l.OwnerId).startsWith('005') && userMap.containsKey(l.OwnerId)){
                        if(userMap.get(l.OwnerId).ManagerId != null){
                            l.Next_Escalation_Email__c = userMap.get(l.OwnerId).Manager.Email;
                            l.Escalation_User__c = userMap.get(l.OwnerId).ManagerId;
                        }
                    }  

                    if(l.Level_1_Escalation__c == null && 
                        ((l.New_Stage_Escalation_Level__c==LeadConstants.ZAM_ESCALATION && oldmap.get(l.id).New_Stage_Escalation_Level__c != LeadConstants.ZAM_ESCALATION) 
                            || (l.Open_Stage_Escalation_Level__c==LeadConstants.ZAM_ESCALATION && oldmap.get(l.id).Open_Stage_Escalation_Level__c!=LeadConstants.ZAM_ESCALATION))){
                                l.Level_1_Escalation__c= l.Next_Escalation_Email__c;
                    }

                    if(l.Level_2_Escalation__c==null && 
                        ((l.New_Stage_Escalation_Level__c==LeadConstants.RM_ESCALATION && oldmap.get(l.id).New_Stage_Escalation_Level__c!=LeadConstants.RM_ESCALATION) 
                            || (l.Open_Stage_Escalation_Level__c==LeadConstants.RM_ESCALATION && oldmap.get(l.id).Open_Stage_Escalation_Level__c!=LeadConstants.RM_ESCALATION))){

                          l.Level_2_Escalation__c = l.Next_Escalation_Email__c;
                    }

                    if(l.Level_3_Escalation__c==null && 
                        ((l.New_Stage_Escalation_Level__c==LeadConstants.CT_ESCALATION && oldmap.get(l.id).New_Stage_Escalation_Level__c!=LeadConstants.CT_ESCALATION) 
                            || (l.Open_Stage_Escalation_Level__c==LeadConstants.CT_ESCALATION && oldmap.get(l.id).Open_Stage_Escalation_Level__c!=LeadConstants.CT_ESCALATION))){
                          l.Level_3_Escalation__c = l.Next_Escalation_Email__c;
                    }
                      			
        		}

            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'Lead - populateNextEscalationEmail');      
            }   		
	}	


/**********************************************
    Created By : Mohan
    Purpose    : Update the Next Escalation Email on Insert or On change of Lead Owner
**********************************************/
	public static void populateOwnerEmailId(List<Lead> leadList){

        System.debug('**populateOwnerEmailId');
        
        try{	
        		Set<Id> ownerIdSet = new Set<Id>();

        		for(Lead l: leadList){
        			ownerIdSet.add(l.OwnerId);
        		}

        		Map<Id, User> userMap = new Map<Id, User>(UserSelector.getUserDetails(ownerIdSet));

        		for(Lead l: leadList){
                    if(string.valueOf(l.OwnerId).startsWith('005')){

                        if(l.Record_Owner_Assigned_Time__c == null){
                            l.Record_Owner_Assigned_Time__c = System.now();
                        }

                        l.Next_Escalation_Email__c = userMap.get(l.OwnerId).Email;
                        l.Escalation_User__c = l.OwnerId;
                    }
        		}

            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'Lead - populating Owner Email Id on Lead');      
            }   		
	}	


/**********************************************
    Created By : Mohan
    Purpose    : Create Lead Escalation Tracking Case for Owner Lead
**********************************************/
    public static void createEscalationTrackingCase(List<Lead> leadList){
        try{    
                List<Case> caseList = new List<Case>();
                Set<Id> leadIdSet = new Set<Id>();
                Map<Id, Case> leadIdToCaseMap = new Map<Id, Case>();

                for(Lead l: leadList){
                    leadIdSet.add(l.Id);
                }

                for(Case c: [select Id, Lead__c from Case where Lead__c =: leadIdSet]){
                    leadIdToCaseMap.put(c.Lead__c, c);
                }

                for(Lead l: leadList){
                    if(!leadIdToCaseMap.containsKey(l.Id)){
                        Case c = new Case();
                        c.Lead__c = l.Id;
                        c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_LEAD_ESCALATION_TRACKING).getRecordTypeId();
                        c.Lead_Status__c = 'New';
                        //Added b y baibhav
                        c.Lead_Dropp_Date__c=l.Drop_Date__c;
                        c.Escalation_Trigger_time__c=l.Escalation_Time__c;
                        caseList.add(c);
                    }
                }

                if(!caseList.isEmpty()){
                    insert caseList;
                }

            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'Lead - createEscalationTrackingCase');      
            }           
    }   


/**********************************************
    Created By : Mohan
    Purpose    : Sync the Lead Status with the Escalation Tracking Case
**********************************************/
    public static void syncLeadStatusWithEscalationTrackingCase(List<Lead> leadList){

        System.debug('**syncLeadStatusWithEscalationTrackingCase');
        
        try{    
                Set<Id> leadIdSet = new Set<Id>();
                Map<Id, Case> leadIdToCaseMap = new Map<Id, Case>();
                List<Case> updatedCaseList = new List<Case>();

                for(Lead l: leadList){
                    leadIdSet.add(l.Id);
                }

                List<Case> caseList = [select Id, Lead__c, Lead_Status__c,Escalation_Trigger_time__c from Case where Lead__c =: leadIdSet];

                for(Case c: caseList){
                    leadIdToCaseMap.put(c.Lead__c, c);
                }

                for(Lead l: leadList){
                    if(leadIdToCaseMap.containsKey(l.Id)){
                        Case c = new Case();
                        c.Id = leadIdToCaseMap.get(l.Id).Id;
                        c.Lead_Status__c = l.Status;
                        //Added b y baibhav
                        c.Escalation_Trigger_time__c=l.Escalation_Time__c;
                        c.Lead_Dropp_Date__c=l.Drop_Date__c;
                        if(l.Status == 'Open'){
                            c.Lead_Open_Time__c = System.now();
                        }
                        updatedCaseList.add(c);
                    }
                }

                if(!updatedCaseList.isEmpty()){
                    Update updatedCaseList;
                }

            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'Lead - syncLeadStatusWithEscalationTrackingCase');      
            }           
    }  
/**********************************************************************************************************************************************
    Added by baibhav
    Purpose To handle the FollowUp Hours ligic 
**********************************************************************************************************************************************/
    public static void leadFollowUp(List<Lead> leadList,Map<id,Lead> oldmap){
      
      try{
           
            Lead_Escalation__mdt leMdt=[SELECT DeveloperName,Id,Label,Language,Lead_House_not_Avail_FollowUp_dys__c,
            Lead_New_Dropped_Hrs__c,Lead_New_Stage_Escalation_Hours__c,Lead_OpenDropped_Hrs__c,Lead_Open_Stage_Escalation_Hours__c,
            Lead_Owner_req_callBack_FollowUp_dys__c,MasterLabel,Max_Lead_No_of_FollowUp__c,Max_Opp_No_of_FollowUp__c,NamespacePrefix,
            New_Stage_RM_to_CT_SLA_Hrs__c,New_Stage_ZAM_to_RM_SLA_Hrs__c,Open_Stage_RM_to_CT_SLA_Hrs__c,Open_Stage_ZAM_to_RM_SLA_Hrs__c,
            QualifiedApiName FROM Lead_Escalation__mdt where QualifiedApiName='Escalation'];

            for(lead ld:leadList){

                   if(ld.Lead_followUp_Count__c==null){
                     ld.Lead_followUp_Count__c=0;
                   }
                    if(ld.Lead_followUp_Count__c>=(Integer)leMdt.Max_Lead_No_of_FollowUp__c){
                        ld.addError('Max Lead NO of Follow allow is: '+leMdt.Max_Lead_No_of_FollowUp__c);
                    }
                   else{
                          ld.Lead_followUp_Count__c=ld.Lead_followUp_Count__c+1;
                    }
  /*********************************************************************************************************/
 
                    if(ld.Follow_Up_Time__c < System.now()){
                        ld.addError('Follow up should be greater then present time');

                    }
                    else if(oldMap.get(ld.id).Follow_Up_Time__c==null || ld.Open_Stage_Escalation_Level__c!=oldMap.get(ld.id).Open_Stage_Escalation_Level__c || oldMap.get(ld.id).Follow_Up_Time__c < ld.SLA_Start_Time__c){

                        Long newdt1Long = ld.SLA_Start_Time__c.getTime();
                        Long newdt2Long = System.now().getTime();
                        Long newmilliseconds = newdt2Long - newdt1Long;
                        Long newseconds = newmilliseconds / 1000;
                        long newminutes = newseconds / 60;
                       

                        ld.Time_Spend__c=newminutes;                   
                    }
                    else if(oldMap.get(ld.id).Follow_Up_Time__c!=null && oldMap.get(ld.id).Follow_Up_Time__c < System.now()){

                        Long newdt1Long = oldMap.get(ld.id).Follow_Up_Time__c.getTime();
                        Long newdt2Long = System.now().getTime();
                        Long newmilliseconds = newdt2Long - newdt1Long;
                        Long newseconds = newmilliseconds / 1000;
                        long newminutes = newseconds / 60;
                         if(ld.Time_Spend__c==null){
                          ld.Time_Spend__c=0 ;   
                        }
                        ld.Time_Spend__c=ld.Time_Spend__c+newminutes; 

                    }

                     if(ld.Time_Spend__c==null){
                          ld.Time_Spend__c=0 ;   
                        }
                     
                    if(ld.Open_Stage_Escalation_Level__c==null){
                       Integer addmin=(Integer)((leMdt.Lead_Open_Stage_Escalation_Hours__c*60)-ld.Time_Spend__c);
                       ld.Escalation_Time__c=ld.Follow_Up_Time__c.addMinutes(addmin);
                    }
                    else if(ld.Open_Stage_Escalation_Level__c==LeadConstants.ZAM_ESCALATION){
                       Integer addmin=(Integer)((leMdt.Open_Stage_ZAM_to_RM_SLA_Hrs__c*60)-ld.Time_Spend__c);
                       ld.Escalation_Time__c=ld.Follow_Up_Time__c.addMinutes(addmin);
                    }
                    else if(ld.Open_Stage_Escalation_Level__c==LeadConstants.RM_ESCALATION){
                        Integer addmin=(Integer)((leMdt.Open_Stage_RM_to_CT_SLA_Hrs__c*60)-ld.Time_Spend__c);
                        ld.Escalation_Time__c=ld.Follow_Up_Time__c.addMinutes(addmin);
                    }
                  

                if(oldMap.get(ld.id).Follow_Up_Time__c==null){
                        Long newdt1Long = ld.Stage_Time__c.getTime();
                        Long newdt2Long = ld.Follow_Up_Time__c.getTime();
                        Long newmilliseconds = newdt2Long - newdt1Long;
                        Long newseconds = newmilliseconds / 1000;
                        long newminutes = newseconds / 60;
                        long hours=newminutes/60;
                       ld.Total_No_of_FollwUp_Hours__c=(Integer)hours;
                    }
                     else if(oldMap.get(ld.id).Follow_Up_Time__c> ld.Follow_Up_Time__c){
                        Long newdt1Long = ld.Follow_Up_Time__c.getTime();
                        Long newdt2Long = oldMap.get(ld.id).Follow_Up_Time__c.getTime();
                        Long newmilliseconds = newdt2Long - newdt1Long;
                        Long newseconds = newmilliseconds / 1000;
                        long newminutes = newseconds / 60;
                        long hours=newminutes/60;
                         if(ld.Total_No_of_FollwUp_Hours__c==null){
                               ld.Total_No_of_FollwUp_Hours__c=0;
                              }
                       ld.Total_No_of_FollwUp_Hours__c=ld.Total_No_of_FollwUp_Hours__c-hours;
                       
                   }
                    else if(oldMap.get(ld.id).Follow_Up_Time__c!=null){
                        if(System.now()>oldMap.get(ld.id).Follow_Up_Time__c){
                             Long newdt1Long = System.now().getTime();
                            Long newdt2Long =ld.Follow_Up_Time__c.getTime();
                            Long newmilliseconds = newdt2Long - newdt1Long;
                            Long newseconds = newmilliseconds / 1000;
                            long newminutes = newseconds / 60;
                            long hours=newminutes/60;
                             if(ld.Total_No_of_FollwUp_Hours__c==null){
                               ld.Total_No_of_FollwUp_Hours__c=0;
                              }
                           ld.Total_No_of_FollwUp_Hours__c=ld.Total_No_of_FollwUp_Hours__c+hours;
                        }
                        else {
                            Long newdt1Long = oldMap.get(ld.id).Follow_Up_Time__c.getTime();
                            Long newdt2Long =ld.Follow_Up_Time__c.getTime();
                            Long newmilliseconds = newdt2Long - newdt1Long;
                            Long newseconds = newmilliseconds / 1000;
                            long newminutes = newseconds / 60;
                            long hours=newminutes/60;
                              if(ld.Total_No_of_FollwUp_Hours__c==null){
                               ld.Total_No_of_FollwUp_Hours__c=0;
                              }
                            ld.Total_No_of_FollwUp_Hours__c=ld.Total_No_of_FollwUp_Hours__c+hours;
                        }                       
                   } 

                 


                   //logic for dropdate
                          if(ld.Open_Stage_Escalation_Level__c==LeadConstants.ZAM_ESCALATION){
                    Integer hrs=(Integer)(leMdt.Lead_OpenDropped_Hrs__c-leMdt.Lead_Open_Stage_Escalation_Hours__c-leMdt.Open_Stage_ZAM_to_RM_SLA_Hrs__c);
                    ld.Drop_Date__c=ld.Escalation_Time__c.addHours(hrs);
                    System.debug(hrs+'**1**'+ld.Drop_Date__c);
                   }
                   else if(ld.Open_Stage_Escalation_Level__c==LeadConstants.RM_ESCALATION){
                    Integer hrs=(Integer)(leMdt.Lead_OpenDropped_Hrs__c-leMdt.Open_Stage_ZAM_to_RM_SLA_Hrs__c-leMdt.Lead_Open_Stage_Escalation_Hours__c-leMdt.Open_Stage_RM_to_CT_SLA_Hrs__c);
                    ld.Drop_Date__c=ld.Escalation_Time__c.addHours(hrs);
                    System.debug(hrs+'**2**'+ld.Drop_Date__c);
                   }
                   else if(ld.Open_Stage_Escalation_Level__c==LeadConstants.CT_ESCALATION){
                   // Integer hrs=(Integer)(leMdt.Lead_OpenDropped_Hrs__c-leMdt.Open_Stage_RM_to_CT_SLA_Hrs__c-leMdt.Open_Stage_ZAM_to_RM_SLA_Hrs__c-leMdt.Lead_Open_Stage_Escalation_Hours__c);
                    //ld.Drop_Date__c=ld.Escalation_Time__c.addHours(hrs);
                    //System.debug(hrs+'**3**'+ld.Drop_Date__c);
                   }
                   else{
                    Integer hrs= (Integer)(leMdt.Lead_OpenDropped_Hrs__c-leMdt.Lead_Open_Stage_Escalation_Hours__c);
                    ld.Drop_Date__c=ld.Escalation_Time__c.addHours(hrs);
                    System.debug(hrs+'**4**'+ld.Drop_Date__c);

                   }

                  // ld.Drop_Date__c=((ld.Drop_Date__c.addDays(System.Now().Date().daysBetween(ld.Follow_Up_Time__c.Date())).addHours(ld.Follow_Up_Time__c.Time().Hour()-System.now().Time().Hour()))).addMinutes(ld.Follow_Up_Time__c.Time().minute()-System.now().Time().minute());

                      System.debug('****yeswedwy'+ld.Drop_Date__c);
                   
    /***************************************************************************************/
                   if(ld.Follow_Up_Reason__c==Constants.LEAD_OWNER_REQ_FOLLOWUP && ld.Stage_Time__c.Date().daysBetween(ld.Follow_Up_Time__c.Date())>(Integer)leMdt.Lead_Owner_req_callBack_FollowUp_dys__c){
                    ld.addError('Total Followup cannot be more then '+(Integer)leMdt.Lead_Owner_req_callBack_FollowUp_dys__c+' days');
                   }
                   else if(ld.Follow_Up_Reason__c==Constants.LEAD_HOUSE_NOT_FOLLOWUP && ld.Stage_Time__c.Date().daysBetween(ld.Follow_Up_Time__c.Date())>(Integer)leMdt.Lead_House_not_Avail_FollowUp_dys__c){
                    ld.addError('Total Followup cannot be more then '+(Integer)leMdt.Lead_House_not_Avail_FollowUp_dys__c+' days');
                   }
      
            }

        } Catch(Exception e) {
                 System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                 UtilityClass.insertGenericErrorLog(e);
            }
    }   
 /**********************************************
    Created By : Baibhav
    Purpose    : 1) To Create Escalation task
                                
**********************************************/ 
  public static void CreateEscalationtask(List<Lead> leadlist){
    try{
      Id  generalTask = Schema.SObjectType.Task.getRecordTypeInfosByName().get(Constants.TASK_RT_GENERAL_TASK).getRecordTypeId();
        List<task> inTaskList=new List<Task>();
        for(Lead ld:leadlist){
           Task tk=new Task();
           tk.OwnerId=ld.Escalation_User__c;
           tk.Subject='Escaltion :'+ld.lastname; 
           tk.WhoID=ld.id;
           tk.RecordtypeID=generalTask;
           tk.ActivityDate=System.Today();
           inTaskList.add(tk);
        }
        if(!inTaskList.isEmpty()){
            System.debug('asd***'+inTaskList);
            insert inTaskList;
        }
      }catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'Opportunity - EscaltionTaskCreation');      
            } 
  }  
  
}