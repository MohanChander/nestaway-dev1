public with sharing class CaseConstants {
public static String CASE_RT_Furnished_House_Onboarding='Furnished House Onboarding';
    public static String CASE_RT_Unfurnished_House_Onboarding='Unfurnished House Onboarding';
    public static String CASE_RT_SERVICE_REQUEST = 'Service Request';
    
    //Case Cheque Status
    public static String CASE_CHEQUE_STATUS_CHEQUE_NOT_RECEIVED = 'Cheque not Received';
    public static String CASE_CHEQUE_STATUS_CHEQUE_BOUNCED = 'Cheque Bounced';
    
    // case type value for closing workorder
    public static String CASE_TYPE_MOVEIN='Movein';
    public static String CASE_TYPE_MOVEOUT='Moveout';
    public static String CASE_TYPE_ONBOARDING='Onboarding';
    
    // case status values
    //Status  
    public static String CASE_STATUS_TENANT_CANCELLED='Tenant Cancelled';
    public static String CASE_STATUS_NEW='New';
    public static String CASE_STATUS_KEY_HANDLING='Key Handling';
    public static String CASE_STATUS_MAINTENANCE='Maintenance';
    public static String CASE_STATUS_FUR_DTH_WIFI_CONNECT='Furnishing and DTH / Wifi Connection';
    public static String CASE_STATUS_VERIFICATION='Verification';
    public static String CASE_STATUS_PHOTOGRAPHY='Photography';
    public static String CASE_STATUS_CLOSED='Closed';
    public static String CASE_STATUS_FURNISHING='Furnishing';
    public static String CASE_STATUS_DTH='DTH';
    public static String CASE_STATUS_WIFI='wifi';
    public static String CASE_STATUS_MAKE_HOUSE_LIVE = 'Make House Live';
    public static String CASE_STATUS_OPEN = 'Open';
    public static String CASE_STATUS_WORK_IN_PROGRESS = 'Work in Progress';
    public static String CASE_STATUS_WAITING_ON_CUSTOMER = 'Waiting on Customer';
    public static String CASE_STATUS_REOPENED = 'Reopened';
    public static String CASE_STATUS_RESOLVED = 'Resolved';
    public static String CASE_STATUS_DROPPED = 'Dropped';
    public static String CASE_STATUS_WATING_ON_TENANT_APPROVAL = 'Waiting on Tenant Approval';
    public static String CASE_STATUS_MOVE_IN_SCHEDULED = 'Move in Scheduled';
    public static String CASE_STATUS_MOVE_OUT_INSPECTION_PENDING = 'Move Out Inspection Pending';
    public static String CASE_STATUS_IN_PROGRESS = 'Progress';
    public static String CASE_STATUS_IN_PAID= 'Paid';
    public static String CASE_STATUS_IN_ACCOUNTIN_ENTRY= 'Accounting Entry Passed';
    public static String CASE_STATUS_IN_NEW_PAYMENT_VERIFIED= 'Payment Verified';
    public static String CASE_STATUS_IN_NEW_PAYMENT_INITIATED = 'Payment initiated';
    public static String CASE_STATUS_CASH_DEPOSITED= 'Cash Deposited with Finance';
    public static String CASE_STATUS_CASH_ACKNOWLEDGED= 'Cash Acknowledged By Finance';
    public static String CASE_STATUS_INVOICES_UPDATE= 'Invoices Updated and Checked';
    public static String CASE_STATUS_OWNER_APPROVED= 'Owner Approved';
    public static String CASE_STATUS_REJECTED='Rejected';
    public static String CASE_STATUS_CANCELLED = 'Cancelled';
    
    
    /*Move out Case Status*/
    public static String CASE_STATUS_CASE_AUTOCLOSED = 'Auto Rejected by System';
    public static String CASE_STATUS_CASE_CLOSED = 'Closed';
    public static String CASE_STATUS_MOVEDOUTALLOK = 'Moved-Out all OK';
    public static String CASE_STATUS_MOVEDOUTWITHISSUE = 'Moved Out with Issues';
    public static String CASE_STATUS_MOVEDOUT_REFUNDREQUESTED = 'Refund Requested'; 
    public static String CASE_STATUS_MOVEDOUT_MOVEOUTSCHEDULED = 'Schedule MoveOut'; //Move Out Scheduled 
    public static String CASE_STATUS_MOVE_OUT_SCHEDULED = 'Move Out Inspection Pending';
    
    public static String CASE_LEAD_STATUS_DROPPED = 'Dropped';

    
    //Case Stage
    public static String CASE_STAGE_NEW='New';
    public static String CASE_STAGE_OWNER_RETENTION_CALL='Owner Retention';
    public static String CASE_STAGE_ZAM_APPROVAL='ZAM Approval';
    
    //Owner Retained - changed to House Retained - Use the New one for New Code
    public static String CASE_STAGE_OWNER_RETAINED='House Retained';
    public static String CASE_STAGE_HOUSE_RETAINED='House Retained';
    
    public static String CASE_STAGE_HOUSE_OFFBOARED='House Offboarded';
    public static String CASE_STAGE_TANANTS_MOVEOUT='Tenant Move Out';
    public static String CASE_STAGE_HOUSE_UNFURNISHED='House Unfurnishing';
    public static String CASE_STAGE_HOUSE_DISCONNECTED='House Disconnection';
    
    public static String CASE_RECOVERABLE_TENANT_OWNER='Tenant & Owner';
    public static String CASE_RECOVERABLE_TENANT_NESTAWAY='Tenant & NestAway';
    public static String CASE_RECOVERABLE_OWNER='Owner';
    public static String CASE_RECOVERABLE_TENANT='Tenant';
    public static String CASE_RECOVERABLE_OWNER_NESTAWAY='Owner & NestAway';
     public static String CASE_RECOVERABLE_TENANT_OWNER_NESTAWAY='Tenant & Owner & NestAway';
    
    
    
    
    
    //Full & Final Amount Generation - Changed to Proposed Account Settlement - Use the new one for new Code
    public static String CASE_STAGE_FULL_FINAL_AMT = 'Proposed Account Settlement';
    public static String CASE_STAGE_FULL_PROPOSED_ACCOUNT_SETTLEMENT = 'Proposed Account Settlement';
    
    public static String CASE_STAGE_FINAL_INSPECTION='Final Inspection';
    public static String CASE_STAGE_HOUSE_UNDERGOING='House Undergoing Maintenance';
    public static String CASE_STAGE_KEY_SUBMITTED='Keys Submission';
    public static String CASE_STAGE_PROFILE_UPDATE='Profile Updates';
    public static String CASE_STAGE_PENDING_SD_PAYMENT='SD Payment';
    public static String CASE_STAGE_PENDING_DOCUMENT='Document Verification';
    public static String CASE_STATUS_MOVE_IN_WIP = 'Move In WIP';
    public static String CASE_STAGE_AGREEMENT_CANCELLED = 'Agreement Cancellation';
    public static String CASE_STAGE_OFFBOARDING_COMPLET = 'offboarding Complete';
    public static String CASE_STAGE_ACCOUNTING_ENTRY_PASSED = 'Accounting Entry Passed';
    public static String CASE_STAGE_INVOICE_UPDATED_AND_CHECKED = 'Invoice Updated and Checked';
    
    
    
    // Case stages completed values
    public static String CASE_STAGES_COMPLETED_PROFILE_COMPLETED='Profile Completed';
    public static String CASE_STAGES_COMPLETED_DOCUMENT_COMPLETED='Document Completed';
    public static String CASE_STAGES_COMPLETED_DOCUMENT_VERIFIED='Document Verified';
    public static String CASE_STAGES_COMPLETED_SD_PAYMENT_RECEIVED='SD Payment Received';
    public static String CASE_STAGES_COMPLETED_OWNER_APPROVED='Owner Approved';
    public static String CASE_STAGES_COMPLETED_MOVE_IN_SCHEDULED='Move in Scheduled';
    public static String CASE_STAGES_COMPLETED_CONTRACT_GENERATED='Contract Generated';        
    
    
    
    //case type
    public static String CASE_TYPE_OFFBOARDING = 'Offboarding';
    
    
    
    // case origin values
    public static String CASE_ORIGIN_WEB = 'Web';
    public static String CASE_ORIGIN_EMAIL = 'Email';
    
    // Case Booked_Object_Type__c Picklist values
    public static String CASE_BOOKING_OBJECT_TYPE_HOUSE = 'House';
    public static String CASE_BOOKING_OBJECT_TYPE_ROOM = 'Room';
    public static String CASE_BOOKING_OBJECT_TYPE_BED = 'Bed';
    
    //Case Issue Level Picklist Values
    public static String CASE_ISSUE_LEVEL_HOUSE_LEVEL = 'House Level';
    public static String CASE_ISSUE_LEVEL_ROOM_LEVEL = 'Room Level';
    
    //End
    public static String CASE_BOOKING_STATUS_CANCELLED = 'Cancelled';
    public static String CASE_ORIGN_SYSTEM='System Generated'; 
    
    // CAse Refund status Value
      public static String CASE_STATUS_REFUND_REQUEST_SUCCESS  = 'SUCCESS';
   
     public static Id getRecordTypeIdByName(string recordTypeName){
        return Schema.SObjectType.Case.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
    }
    

}