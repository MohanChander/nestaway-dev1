public class LeadConstants {
	 public static String ZAM_ESCALATION='ZAM Escalation';
	 public static String RM_ESCALATION='RM Escalation';
	 public static String CT_ESCALATION='CT Escalation';
	 public static String LEAD_STATUS_NEW='New';
	 public static String LEAD_STATUS_OPEN='Open';
	 public static String LEAD_STATUS_DROPPED='Dropped';

}