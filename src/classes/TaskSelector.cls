public class TaskSelector {

	//get all the Task Details related to a Case
	public static List<Task> getTaskDetialsRelatedToCase(Set<Id> caseIdSet){
		return [select Id, Status, WhatId, HO_Type__c, HO_Sub_Type__c from Task where whatId =: caseIdSet 
			    and Status !=: Constants.TASK_STATUS_COMPLETED];
	}
	public static List<Task> getOwnerTaskRelatedToseviceCase(Set<Id> caseIdSet)
	{
		id ownerTskrecordtypeId=Schema.SObjectType.Task.getRecordTypeInfosByName().get(Constants.TASK_RT_OWNER_FOLLOW_UP).getRecordTypeId();
		return [select Id, Status, WhatId, HO_Type__c, HO_Sub_Type__c from Task where whatId =: caseIdSet 
			    and RecordtypeId=:ownerTskrecordtypeId];
	}
}