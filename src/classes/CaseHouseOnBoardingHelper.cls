public class CaseHouseOnBoardingHelper {
    Public Static Id furnieshedRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Furnished_House_Onboarding).getRecordTypeId();  
    public static void insertCaseOnBoarding(Set<Id> SetOfContract){
        system.debug('insertCaseOnBoarding');
        List<Case> caseList = new List<Case>();
        List<Case> updateCaseList = new List<Case>();
        List<Contract> contList = [Select Id,House__c From Contract Where id in: SetOfContract];
        for(Contract each : contList){
            Case newCase = new Case();
           // newcase.ParentId= each.house__C;
            newcase.status = 'New';
            newcase.House__c= each.House__c;
            newCase.RecordTypeId= furnieshedRTId;
            caseList.add(newCase);
        }
        if(!caseList.isEmpty()){
            insert caseList;
        }
        for(Case each : CaseList){
            each.status =Constants.CASE_STATUS_FUR_DTH_WIFI_CONNECT;
            updateCaseList.add(each);
        }
        if(!updateCaseList.isEmpty()){
            update updateCaseList;
        }
        
    }
}