/*  Created By: Mohan
    Purpose   : Calculate the TargetDate of the Milestone */

global class MilestoneTimeCalculator implements Support.MilestoneTriggerTimeCalculator { 

     global Integer calculateMilestoneTriggerTime(String caseId, String milestoneTypeId){

        try{
                Case c = [SELECT Priority, Escalation_Level__c, Problem__r.SLA_1__c, Problem__r.SLA_2__c,
                          Owner_Inclusion__c, Require_visit__c, Service_Visit_Time__c, CreatedDate,
                          Problem__r.SLA_3__c, Problem__r.SLA_4__c, SLA_1__c FROM Case WHERE Id=:caseId];

                MilestoneType mt = [SELECT Name FROM MilestoneType WHERE Id=:milestoneTypeId];

                //if there is Owner Inclusion on the Case add additional 2 days in the SLA
                Integer ownerInclusionTime = 0;
                Integer visitTime = 0;
                Integer slaTime = 0;
                if(c.Owner_Inclusion__c != null && c.Owner_Inclusion__c == 'true'){
                    ownerInclusionTime = 1440 *2;
                } 

                if(c.Require_visit__c && c.Service_Visit_Time__c != null && c.Service_Visit_Time__c > c.CreatedDate){
                    Long serviceVisitTime = c.Service_Visit_Time__c.getTime();
                    Long createdDateTime = c.CreatedDate.getTime();
                    visitTime = Integer.valueOf((serviceVisitTime - createdDateTime)/60000);
                }                

                if(c.Escalation_Level__c == null){                    
                    slaTime = c.Problem__r.SLA_1__c != null ? (Integer)c.Problem__r.SLA_1__c * 1440 : Integer.valueOf(Label.Default_Level_1_SLA);
                    return slaTime + ownerInclusionTime + visitTime;
                } else if(c.Escalation_Level__c == 'Level 1'){
                    slaTime = c.Problem__r.SLA_2__c != null ? (Integer)c.Problem__r.SLA_2__c * 1440 : Integer.valueOf(Label.Default_Level_2_SLA);
                    return slaTime + ownerInclusionTime + visitTime;
                } else if(c.Escalation_Level__c == 'Level 2'){
                    slaTime = c.Problem__r.SLA_3__c != null ? (Integer)c.Problem__r.SLA_3__c * 1440 : Integer.valueOf(Label.Default_Level_3_SLA);
                    return slaTime + ownerInclusionTime + visitTime;
                } else {
                    slaTime = c.Problem__r.SLA_4__c != null ? (Integer)c.Problem__r.SLA_4__c * 1440 : Integer.valueOf(Label.Default_Level_4_SLA);
                    return slaTime + ownerInclusionTime + visitTime;
                }  
            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'Milestone Time Calculator Exception');
                    return 4 *1440;      
            }            
    }
}