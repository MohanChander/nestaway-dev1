/*  Created By   : Mohan - WarpDrive Tech Works
Purpose      : All the queries related to the House Object are here */

public class HouseSelector {
    
    public static List<House__c> getHousesWithRoomsAndBeds(Set<Id> houseIdSet){
        
        return [select Id, Stage__c,HouseId__c , (select Id, House__c, Status__c,name from Room_Terms__r),(select Id, House__c, Status__c from Beds__r) from House__c
                where Id =: houseIdSet];
    }
    public static List<House__c> getHousesWithRoomsAndBedsandRoomInspection(Set<Id> houseIdSet){
        
        return [select Id, Stage__c,HouseId__c, (select Id, House__c, Status__c,name from Room_Terms__r),(select Id,name,House__c from Bathrooms__r)  from House__c
                where Id =: houseIdSet];
    }
    
    
    public static House__c getHouseDetailsFromHouseId(Id houseId){
        
        return [Select id,name, Stage__c,Is_House_Live__c ,Description_Message__c ,House_Layout__c, Booking_Type__c, Opportunity__c, Opportunity__r.AccountId, Last_API_Sync_Time__c,
                Api_House_Info__c, Api_House_Success__c,  Actual_House_rent__c,Base_House_Rent__c,Contract__c,
                Contract__r.Booking_Type__c, Data_Migration__c, HouseId__c, Updated_Child_Name_with_House_Id__c
                from house__c where id = : houseId];
    }
    
    public static List<House__c> getHousesWithRentVerification(Set<Id> houseIdSet){
        return [select Id, Rent_Verified__c,Opportunity__c, Locality__c,Sub_Locality_Level_2__c,Locality_Address__c,Sub_Locality__c ,Locality_city__c from House__c where Id =: houseIdSet];
    }
    
    public static List<House__c> getHousesWithCaseDetails(Set<Id> houseIdSet){
        
        Id furnishedOnboardingRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Furnished_House_Onboarding).getRecordTypeId();
        Id unfurnishedOnboardingRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Unfurnished_House_Onboarding).getRecordTypeId();
        
        return [select Id, Stage__c, (select Id, Status from Cases__r where 
                                      RecordTypeId =: furnishedOnboardingRecordTypeId or RecordTypeId =: unfurnishedOnboardingRecordTypeId ) from House__c 
                where Id =: houseIdSet];
    }
    
    public static List<House__c> getHouseStatus(Set<Id> houseIdSet){
        return [select Id, Stage__c from House__c where Id =: houseIdSet and 
                (Stage__c =: Constants.HOUSE_STAGE_HOUSE_LIVE or Stage__c =: Constants.HOUSE_STAGE_OFFBOARDED)];
    }
    
    // Added by baibhav
    public static Map<id,House__c> getHousesWithOpenCase(Set<Id> houseIdSet){
        
        return new Map<id,House__c>([select Id, Stage__c,ZAM_Approved_for_House_Offboarding__c from House__c 
                                     where Id =: houseIdSet]);
    }
    
    //get MIMO Checklist details related to the House
    public static List<House__c> getMimoChecklistDetailsForHosue(Set<Id> houseIdSet){
        return [select Id, (select Id from Checklist1__r where Type_Of_HIC__c =: Constants.CHECKLIST_TYPE_MIMO_CHECK),
                (select Id, CheckFor__c from Room_Inspection1__r where Type_of_RIC__c =: Constants.ROOM_INSPECTION_TYPE_OF_RIC_MIMO_CHECK),
                (select Id, CheckFor__c from Bathrooms1__r where Type_of_BIC__c =: Constants.BATHROOM_TYPE_OF_BIC_MIMO_CHECK) 
                from House__c where Id =: houseIdSet];
    }
    
    //get List of Houses from Set of House Id's
    public static List<House__c> getHouseListFromIdSet(Set<Id> houseIdSet){
        
        return [select Id, City__c from House__c where Id =: houseIdSet];
    }
     public static List<House__c> getHouseListFromOppidSet(Set<Id> oppIdSet){

        return [select Id, City__c,Opportunity__c from House__c where Opportunity__c =: oppIdSet];
    }
    // to get the house map
    public static Map<Id,house__c> getHouseMapForContract(Set<Id> houseIds_Set){
       return new  map<id,house__c> ([Select id,name,contract__c,contract__r.Who_pays_Society_Maintenance__c,
                                                          House_Owner__c,HouseId__c,Onboarding_Zone_Code__c,HRM__c ,Booking_Type__c      
                                                          from house__C where id in : houseIds_Set]);
    }
}