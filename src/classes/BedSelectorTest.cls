@isTest
public Class  BedSelectorTest {
    public Static TestMethod void testMethod1(){
         Set<Id> setOfOppid = new Set<Id>();
        Set<Id> setOfRt = new Set<Id>();
          Opportunity opp = new Opportunity();
            opp.Name ='Test Opp'; 
            opp.CloseDate = System.Today();
            opp.StageName = 'Quote Creation';
            opp.State__c = 'Karnataka';
            insert opp;
        setOfOppid.add(opp.Id);
        Room_Terms__c  rt=  new Room_Terms__c ();
        rt.Name='Test1';
        insert rt;
        setOfRt.add(rt.Id);
        BedSelector.getBedsForOpportunities(setOfOppid);
        BedSelector.getBedAggregationFromRoomTerms(setOfRt);
    }
}