public with sharing class ContractService {
	
	public static void checklistUrlToContract(List<contract> contList){
		Set<id> oppidSet=new Set<id>();
		for(contract cnt:contList){
			oppidSet.add(cnt.Opportunity__c);
		}

		Map<id,House_Inspection_Checklist__c> oppMapToHic=new Map<id,House_Inspection_Checklist__c>();
		list<House_Inspection_Checklist__c> hicList=new list<House_Inspection_Checklist__c>();

		if(!oppidSet.isEmpty()){
			hicList=ChecklistSelector.getChecklistWithRoomsAndKeys(oppidSet);
		}
		if(!hicList.isEmpty()){
			for(House_Inspection_Checklist__c hic:hicList){
				oppMapToHic.put(hic.Opportunity__c,hic);
			}
		}
		for(contract cnt:contList){
			if(oppMapToHic.containsKey(cnt.Opportunity__c)){
				cnt.Checklist_URL__c=oppMapToHic.get(cnt.Opportunity__c).Checklist_URL__c;
			}
		}

	}
}