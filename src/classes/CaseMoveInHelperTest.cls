@Istest
public class CaseMoveInHelperTest {
     Public Static TestMethod Void doTest9(){
        Id MoveIn_RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEIN).getRecordTypeId();
        Id RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Furnished House Onboarding').getRecordTypeId();
        Id MoveOutRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEOUT).getRecordTypeId();
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        insert hos;
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        /*Booking__c b= new Booking__c();
        b.Tenant__c=accObj.Id;
        insert b;*/
        Tenancy__c b = new Tenancy__c();
        b.Tenant__c=accObj.Id;
         b.House__c=hos.id;
        insert b;
        Case c=  new Case();
        c.Status=Constants.CASE_STATUS_MOVEDOUTWITHISSUE;
        c.House__c=hos.Id;
        c.ownerId=newuser.Id;
        c.MoveIn_Slot_End_Time__c=System.today();
        c.MoveIn_Slot_Start_Time__c=System.today();
        c.Move_Out_Date__c=System.today();
        c.Booked_Object_ID__c='162';
        c.Move_Out_Status__c='In Progress';
        c.Move_Out_Type__c='Rent Default';
        c.Booked_Object_Type__c='Room';
        c.MoveIn_Executive__c=newuser.Id;
        c.RecordTypeId= MoveIn_RecordTypeId;
        c.Settle_Amount_To_Be_deducted__c=3762;
        c.Booking_Id__c='389769';
        c.Type='MOveOUt';
        c.Room_Term__c=rt.id;
        c.Tenant__c=accObj.id;
        c.Contract_End_Date__c=system.today();
        insert c;
        List<Case>  cList =  new List<Case>();
        cList.add(c);
     CaseMoveInHelper.createMoveInOrMoveOutCheckWorkOrder(cList);
      
    }


}