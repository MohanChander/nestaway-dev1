@IsTest
public class TaskTriggerHandlerAddOn1Test {
 public static  Id keyHandlingRecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Key Handling').getRecordTypeId();
        public static   Id operationTaskRecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Operation Task').getRecordTypeId();
    public static id casfurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Furnished_House_Onboarding).getRecordTypeId();
    public static id casunfurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Unfurnished_House_Onboarding).getRecordTypeId();
     public static    Id TenantRecordTypeID = Schema.SObjectType.Task.getRecordTypeInfosByName().get(Constants.TASK_RT_MOVEIN_TASKS).getRecordTypeId();
    public Static TestMethod void TaskTest(){
        Test.StartTest();
        NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
        cusSet.name = 'nestaway';
        cusSet.Nestaway_URL__c = 'www.test.com';
        insert cusSet;
        
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        accObj.BillingCity            ='Test';
        accObj.BillingCountry         ='India';
        accObj.BillingPostalCode      ='560068';
        accObj.BillingState           ='Karnataka'; 
        accObj.BillingStreet          ='Test'; 
        insert accObj;
        
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        
        insert hos;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Quote Creation';
        insert opp;
        
        House_Inspection_Checklist__c houseIC = new House_Inspection_Checklist__c();
        houseIC.Opportunity__c = opp.Id;
        houseIC.Status__c = 'House Inspection In Progress';           
        insert houseIC;
        Case c=  new Case();
        c.House__c=hos.Id;
        c.Status='Tenant Docs Verification Pending';
        c.Owner_Approval_API_Msg__c='Error in owner approval API.';
        c.Owner_Approval_Sent_To_Webapp__c=false;
        c.Stage_Status__c='Pending Profile Updates';
        c.RecordTypeId=casfurnishedRecordtype;
        insert c;
        
        Set<Id> setOfCase = new Set<id>();
        setOfcase.add(c.id);
        Task t = new Task();
        t.ActivityDate  = Date.today();
        t.Subject       = 'House Visit';
        t.Status        = 'Open';
        t.Priority      = 'Normal';
        t.WhatId        = houseIC.Id;
        t.Number_of_Keys_for_Balcony__c='1';
        t.Number_of_Keys_for_BedRoom__c='1';
        t.Number_of_Keys_for_Cupboards__c='1';
        t.Number_of_Keys_for_Kitchen__c='1';
        t.Number_of_Keys_for_Main_Door__c='1';
        t.Number_of_Keys_for_Servant_Room__c='1';
        t.Number_of_Keys_for_Storeroom__c='1';
        insert t;
        
        
        map<id,Task> oldmap = new map<id,Task>();
        oldmap.put(t.id,t);
        Task t1 = new Task(id=t.id);
        t1.ActivityDate  = Date.today();
        t1.Subject       = 'House Visit';
        t1.Status        = 'Open';
        t1.Priority      = 'Normal';
        t1.WhatId        = houseIC.Id;
        t1.Number_of_Keys_for_Balcony__c='2';
        t1.Number_of_Keys_for_BedRoom__c='2';
        t1.Number_of_Keys_for_Cupboards__c='2';
        t1.Number_of_Keys_for_Kitchen__c='2';
        t1.Number_of_Keys_for_Main_Door__c='2';
        t1.Number_of_Keys_for_Servant_Room__c='2';
        t1.Number_of_Keys_for_Storeroom__c='2';
         t1.RecordTypeId=TenantRecordTypeID;
        update t1;
        Detail__c dc= new Detail__c();
        dc.Case__c=c.id;
        dc.Task_Id__c=t1.id;
        insert dc;
        map<id,Task> newmap = new map<id,Task>();
        newmap.put(t1.id,t1);
        List<Task> taskList= new   List<Task>();
        taskList.add(t1);
        TaskTriggerHandlerAddOn1.triggerOwnerApprovalInWebApp(c.Id);
        TaskTriggerHandlerAddOn1.validateIfTenantVerificationTasksAreComplete(setOfcase,newmap,oldmap);
           TaskTriggerHandlerAddOn1.afterInsert(newmap);
        
    }
    
    public Static TestMethod void TaskTest1(){
        Test.StartTest();
        NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
        cusSet.name = 'nestaway';
        cusSet.Nestaway_URL__c = 'www.test.com';
        insert cusSet;
        
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        accObj.BillingCity            ='Test';
        accObj.BillingCountry         ='India';
        accObj.BillingPostalCode      ='560068';
        accObj.BillingState           ='Karnataka'; 
        accObj.BillingStreet          ='Test'; 
        insert accObj;
        
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        
        insert hos;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Quote Creation';
        insert opp;
        
        House_Inspection_Checklist__c houseIC = new House_Inspection_Checklist__c();
        houseIC.Opportunity__c = opp.Id;
        houseIC.Status__c = 'House Inspection In Progress';           
        insert houseIC;
        Case c=  new Case();
        c.House__c=hos.Id;
        c.Status='Tenant Verification Pending';
        c.Owner_Approval_API_Msg__c='Error in owner approval API.';
        c.Owner_Approval_Sent_To_Webapp__c=false;
        c.Stage_Status__c='Pending Profile Updates';
        c.RecordTypeId=casfurnishedRecordtype;
        insert c;
        
        Set<Id> setOfCase = new Set<id>();
        setOfcase.add(c.id);
        Task t = new Task();
        t.ActivityDate  = Date.today();
        t.Subject       = 'House Visit';
        t.Status        = 'Open';
        t.Priority      = 'Normal';
        t.WhatId        = houseIC.Id;
        t.Number_of_Keys_for_Balcony__c='1';
        t.Number_of_Keys_for_BedRoom__c='1';
        t.Number_of_Keys_for_Cupboards__c='1';
        t.Number_of_Keys_for_Kitchen__c='1';
        t.Number_of_Keys_for_Main_Door__c='1';
        t.Number_of_Keys_for_Servant_Room__c='1';
        t.Number_of_Keys_for_Storeroom__c='1';
        insert t;
        
        
        map<id,Task> oldmap = new map<id,Task>();
        oldmap.put(t.id,t);
        Task t1 = new Task(id=t.id);
        t1.ActivityDate  = Date.today();
        t1.Subject       = 'House Visit';
        t1.Status        = 'Completed';
        t1.Priority      = 'Normal';
        t1.WhatId        = c.Id;
        t1.Number_of_Keys_for_Balcony__c='2';
        t1.Number_of_Keys_for_BedRoom__c='2';
        t1.Number_of_Keys_for_Cupboards__c='2';
        t1.Number_of_Keys_for_Kitchen__c='2';
        t1.Number_of_Keys_for_Main_Door__c='2';
        t1.Number_of_Keys_for_Servant_Room__c='2';
        t1.Number_of_Keys_for_Storeroom__c='2';
        t1.RecordTypeId=TenantRecordTypeID;
        update t1;
        Detail__c dc= new Detail__c();
        dc.Case__c=c.id;
        dc.Task_Id__c=t1.id;
        insert dc;
        map<id,Task> newmap = new map<id,Task>();
        newmap.put(t1.id,t1);
        List<Task> taskList= new   List<Task>();
        taskList.add(t1);
        TaskTriggerHandlerAddOn1.triggerOwnerApprovalInWebApp(c.Id);
        TaskTriggerHandlerAddOn1.validateIfTenantVerificationTasksAreComplete(setOfcase,newmap,oldmap);
        TaskTriggerHandlerAddOn1.afterInsert(newmap);
        
        
    }
    
      public Static TestMethod void TaskTest3(){
        Test.StartTest();
        NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
        cusSet.name = 'nestaway';
        cusSet.Nestaway_URL__c = 'www.test.com';
        insert cusSet;
        
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        accObj.BillingCity            ='Test';
        accObj.BillingCountry         ='India';
        accObj.BillingPostalCode      ='560068';
        accObj.BillingState           ='Karnataka'; 
        accObj.BillingStreet          ='Test'; 
        insert accObj;
        
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        
        insert hos;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Quote Creation';
        insert opp;
        
        House_Inspection_Checklist__c houseIC = new House_Inspection_Checklist__c();
        houseIC.Opportunity__c = opp.Id;
        houseIC.Status__c = 'House Inspection In Progress';           
        insert houseIC;
        Case c=  new Case();
        c.House__c=hos.Id;
        c.Status='Tenant Docs Verification Pending';
        c.Owner_Approval_API_Msg__c='Error in owner approval API.';
        c.Owner_Approval_Sent_To_Webapp__c=false;
        c.Stage_Status__c='Pending Profile Updates';
        c.RecordTypeId=casfurnishedRecordtype;
        insert c;
        
        Set<Id> setOfCase = new Set<id>();
        setOfcase.add(c.id);
        Task t = new Task();
        t.ActivityDate  = Date.today();
        t.Subject       = 'House Visit';
        t.Status        = 'Open';
        t.Priority      = 'Normal';
        t.WhatId        = houseIC.Id;
        t.Number_of_Keys_for_Balcony__c='1';
        t.Number_of_Keys_for_BedRoom__c='1';
        t.Number_of_Keys_for_Cupboards__c='1';
        t.Number_of_Keys_for_Kitchen__c='1';
        t.Number_of_Keys_for_Main_Door__c='1';
        t.Number_of_Keys_for_Servant_Room__c='1';
        t.Number_of_Keys_for_Storeroom__c='1';
        insert t;
        
        
        map<id,Task> oldmap = new map<id,Task>();
        oldmap.put(t.id,t);
        Task t1 = new Task(id=t.id);
        t1.ActivityDate  = Date.today();
        t1.Subject       = 'House Visit';
        t1.Status        = 'Completed';
        t1.Priority      = 'Normal';
        t1.WhatId        = c.Id;
        t1.Number_of_Keys_for_Balcony__c='2';
        t1.Number_of_Keys_for_BedRoom__c='2';
        t1.Number_of_Keys_for_Cupboards__c='2';
        t1.Number_of_Keys_for_Kitchen__c='2';
        t1.Number_of_Keys_for_Main_Door__c='2';
        t1.Number_of_Keys_for_Servant_Room__c='2';
        t1.Number_of_Keys_for_Storeroom__c='2';
        t1.RecordTypeId=TenantRecordTypeID;
        update t1;
        Detail__c dc= new Detail__c();
        dc.Case__c=c.id;
        dc.Task_Id__c=t1.id;
        insert dc;
        map<id,Task> newmap = new map<id,Task>();
        newmap.put(t1.id,t1);
        List<Task> taskList= new   List<Task>();
        taskList.add(t1);
        TaskTriggerHandlerAddOn1.triggerOwnerApprovalInWebApp(c.Id);
        TaskTriggerHandlerAddOn1.validateIfTenantVerificationTasksAreComplete(setOfcase,newmap,oldmap);
        TaskTriggerHandlerAddOn1.afterInsert(newmap);
        
        
    }
      public Static TestMethod void TaskTest2(){
        Test.StartTest();
          NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
            cusSet.name = 'nestaway';
            cusSet.Nestaway_URL__c = 'www.test.com';
            insert cusSet;
        
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        accObj.BillingCity            ='Test';
        accObj.BillingCountry         ='India';
        accObj.BillingPostalCode      ='560068';
        accObj.BillingState           ='Karnataka'; 
        accObj.BillingStreet          ='Test'; 
        insert accObj;
        
        House__c hos= new House__c();
        hos.name='house1';
       // hos.House_Lattitude__c=2.2;
        hos.Assets_Created__c=false;
        
        insert hos;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Quote Creation';
        insert opp;
        
        House_Inspection_Checklist__c houseIC = new House_Inspection_Checklist__c();
        houseIC.Opportunity__c = opp.Id;
        houseIC.Status__c = 'House Inspection In Progress';           
        insert houseIC;
      
        Case c=  new Case();
        c.House__c=hos.Id;
        c.Status='Tenant Docs Verification Pending';
        c.Owner_Approval_API_Msg__c='Error in owner approval API.';
        c.Owner_Approval_Sent_To_Webapp__c=false;
        c.Stage_Status__c='Pending Profile Updates';
        c.RecordTypeId=casfurnishedRecordtype;
        insert c;
          
        Set<Id> setOfCase = new Set<id>();
        setOfcase.add(c.id);
     
        Task t = new Task();
        t.ActivityDate  = Date.today();
          t.Subject       = 'Tenant Documents Verification.';      
          t.Status        = 'Open';
        t.Priority      = 'Normal';
        t.Current_Latitude__c=2.389;
        t.Current_Longitude__c=2.11;
        t.WhatId        = c.Id;
        t.Number_of_Keys_for_Balcony__c='1';
            t.Number_of_Keys_for_BedRoom__c='1';
            t.Number_of_Keys_for_Cupboards__c='1';
            t.Number_of_Keys_for_Kitchen__c='1';
            t.Number_of_Keys_for_Main_Door__c='1';
            t.Number_of_Keys_for_Servant_Room__c='1';
            t.Number_of_Keys_for_Storeroom__c='1';
           t.RecordTypeId=TenantRecordTypeID;
          
        insert t;
         
     
        map<id,Task> oldmap = new map<id,Task>();
        oldmap.put(t.id,t);
         Task t1 = new Task(id=t.id);
        t1.ActivityDate  = Date.today();
        t1.Current_Latitude__c=27.389;
        t1.Current_Longitude__c=22.11;
        t1.Subject       = 'Tenant Documents Verification.';
        t1.Status        =  'Completed';
        t1.Priority      = 'Normal';
        t1.WhatId        =c.Id;
         t1.RecordTypeId=TenantRecordTypeID;
        t1.Cost__c = 10;
         t1.HIC__c=houseIC.id;
         t1.Task_Related_To__c='HIC';
        t1.HO_Type__c='Furnishing and DTH / Wifi Connection';
        t1.Task_Type__c='House Onboarding';
        t1.Number_of_Keys_for_Balcony__c='3';
            t1.Number_of_Keys_for_BedRoom__c='3';
            t1.Number_of_Keys_for_Cupboards__c='3';
            t1.Number_of_Keys_for_Kitchen__c='3';
            t1.Number_of_Keys_for_Main_Door__c='3';
            t1.Number_of_Keys_for_Servant_Room__c='3';
            t1.Number_of_Keys_for_Storeroom__c='3';
        update t1;
      
          map<id,Task> newmap = new map<id,Task>();
        newmap.put(t1.id,t1);
           List<Task> taskList= new   List<Task>();
        taskList.add(t1);
       TaskTriggerHandlerAddOn1.triggerOwnerApprovalInWebApp(c.Id);
        TaskTriggerHandlerAddOn1.validateIfTenantVerificationTasksAreComplete(setOfcase,newmap,oldmap);
        TaskTriggerHandlerAddOn1.afterInsert(newmap);
               
        Test.StopTest();
        
    }
     public Static TestMethod void TaskTest10(){
        Test.StartTest();
          NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
            cusSet.name = 'nestaway';
            cusSet.Nestaway_URL__c = 'www.test.com';
            insert cusSet;
        
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        accObj.BillingCity            ='Test';
        accObj.BillingCountry         ='India';
        accObj.BillingPostalCode      ='560068';
        accObj.BillingState           ='Karnataka'; 
        accObj.BillingStreet          ='Test'; 
        insert accObj;
        
        House__c hos= new House__c();
        hos.name='house1';
       // hos.House_Lattitude__c=2.2;
        hos.Assets_Created__c=false;
        
        insert hos;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Quote Creation';
        insert opp;
        
        House_Inspection_Checklist__c houseIC = new House_Inspection_Checklist__c();
        houseIC.Opportunity__c = opp.Id;
        houseIC.Status__c = 'House Inspection In Progress';           
        insert houseIC;
      
        Case c=  new Case();
        c.House__c=hos.Id;
        c.Status='Tenant Docs Verification Pending';
        c.Owner_Approval_API_Msg__c='Error in owner approval API.';
        c.Owner_Approval_Sent_To_Webapp__c=false;
        c.Stage_Status__c='Pending Profile Updates';
        c.RecordTypeId=casfurnishedRecordtype;
        insert c;
          
        Set<Id> setOfCase = new Set<id>();
        setOfcase.add(c.id);
     
        Task t = new Task();
        t.ActivityDate  = Date.today();
        t.Subject       = 'Tenant Documents Verification.';
        t.Status        = 'Open';
        t.Priority      = 'Normal';
        t.Current_Latitude__c=2.389;
        t.Current_Longitude__c=2.11;
        t.WhatId        = c.Id;
        t.Number_of_Keys_for_Balcony__c='1';
            t.Number_of_Keys_for_BedRoom__c='1';
            t.Number_of_Keys_for_Cupboards__c='1';
            t.Number_of_Keys_for_Kitchen__c='1';
            t.Number_of_Keys_for_Main_Door__c='1';
            t.Number_of_Keys_for_Servant_Room__c='1';
            t.Number_of_Keys_for_Storeroom__c='1';
           t.RecordTypeId=TenantRecordTypeID;
            
        insert t;
         
     
        map<id,Task> oldmap = new map<id,Task>();
        oldmap.put(t.id,t);
         Task t1 = new Task(id=t.id);
        t1.ActivityDate  = Date.today();
        t1.Current_Latitude__c=27.389;
        t1.Current_Longitude__c=22.11;
        t1.Subject       = 'Tenant SD Payment Verification.';
        t1.Status        =  'Completed';
        t1.Priority      = 'Normal';
        t1.WhatId        =c.Id;
         t1.RecordTypeId=TenantRecordTypeID;
        t1.Cost__c = 10;
         t1.HIC__c=houseIC.id;
         t1.Task_Related_To__c='HIC';
        t1.HO_Type__c='Furnishing and DTH / Wifi Connection';
        t1.Task_Type__c='House Onboarding';
        t1.Number_of_Keys_for_Balcony__c='3';
            t1.Number_of_Keys_for_BedRoom__c='3';
            t1.Number_of_Keys_for_Cupboards__c='3';
            t1.Number_of_Keys_for_Kitchen__c='3';
            t1.Number_of_Keys_for_Main_Door__c='3';
            t1.Number_of_Keys_for_Servant_Room__c='3';
            t1.Number_of_Keys_for_Storeroom__c='3';
        update t1;
      
          map<id,Task> newmap = new map<id,Task>();
        newmap.put(t1.id,t1);
           List<Task> taskList= new   List<Task>();
        taskList.add(t1);
       TaskTriggerHandlerAddOn1.triggerOwnerApprovalInWebApp(c.Id);
        TaskTriggerHandlerAddOn1.validateIfTenantVerificationTasksAreComplete(setOfcase,newmap,oldmap);
        TaskTriggerHandlerAddOn1.afterInsert(newmap);
               
        Test.StopTest();
        
    }
   
}