public without sharing class CreateIssuesPageController {

    public WorkOrder wo {get; set;} 
    public List<IssueWrapper> issueWrapperList {get; set;}
    public Boolean hasError{get; set;}
    public Boolean readOnly{get; set;}


    public CreateIssuesPageController(ApexPages.StandardController stdController) {
        this.wo = (WorkOrder)stdController.getRecord();
        readOnly=false;
        wo = [select Id,Status,Settlement_Deduction_Amount__c,RP_API_Success__c,RP_API_Data__c,RP_API_Info__c,Move_Out_RP_Creation_DateTime__c,
        Webapp_RP_Invoice_Id__c,RP_API_Fired__c,CaseId,recordType.Name from WorkOrder where Id =: wo.Id];

        if(wo.RP_API_Fired__c == true){
           // hasError = true;         
           // String addErrorMessage='RP is already generated';
          //  ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,addErrorMessage));
          //  return;
          readOnly=true;          
        }

        List<Issue__c> issueList = [select Id, Issue_Item__c, Amount__c, Comment__c from Issue__c 
                                    where Work_Order__c =: wo.Id];

        issueWrapperList = new List<IssueWrapper>();

        if(issueList.isEmpty()){
            IssueWrapper iw = new IssueWrapper(new Issue__c()); 
            issueWrapperList.add(iw);
        } else {
            for(Issue__c issue: issueList){
                IssueWrapper iw = new IssueWrapper(issue); 
                issueWrapperList.add(iw);                
            }
        }   
    }   

    //on click of Add Row button 
    public PageReference addRow(){
        IssueWrapper iw = new IssueWrapper(new Issue__c()); 
        issueWrapperList.add(iw);   
        return null;
    }

    //onclick of delete - delete the row
    public PageReference deleteRow(){

        List<IssueWrapper> tempIssueWrapper = new List<IssueWrapper>();

        for(IssueWrapper iw: issueWrapperList){
            if(iw.isToBeDeleted == false){
                tempIssueWrapper.add(iw);
            } else {

                //check if the issue is an existing record in Salesforc i.e has an Id then delete the record from salesforce db
                if(iw.Issue.Id != null){
                    delete iw.issue;
                }                
            }
        }

        issueWrapperList = new List<IssueWrapper>();
        issueWrapperList = tempIssueWrapper;

        tempIssueWrapper = new List<IssueWrapper>();

        return null;
    }

    //on click of Save button
    public PageReference onSave(){

        Decimal settlementAmount = 0;

        List<Issue__c> tempIssueList = new List<Issue__c>();

        for(IssueWrapper iw: issueWrapperList){
            tempIssueList.add(iw.issue);
            settlementAmount = settlementAmount + iw.issue.Amount__c;
        }

        wo.Settlement_Deduction_Amount__c = settlementAmount;
        update wo;

        //make an API call to create a Settlement Case - if Success
        
        /*
        Id settlementCaseRtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Move Out Settlement').getRecordTypeId();

      
        Case settlementCase = new Case();
        settlementCase.ParentId = c.Id;
        settlementCase.RecordTypeId = settlementCaseRtId;
        settlementCase.Settle_Amount_To_Be_deducted__c = settlementAmount;
        insert settlementCase;
        */

        for(Issue__c issue: tempIssueList){
            issue.Work_Order__c = wo.Id;
            issue.Case__c=wo.caseId;
        }

        if(!tempIssueList.isEmpty()){
            upsert tempIssueList;
        }

        PageReference pageRef = new PageReference('/' + wo.Id);
        pageRef.setRedirect(true);
        return pageRef;
    }    
    
    // On click of Save and Create RP.
     public PageReference onSaveCreateRp(){
        boolean returnToWO=false; 
        Decimal settlementAmount = 0;
       try{
            Map<Id,Workorder> mapTocheckchecklistValidaiton = new Map<Id,Workorder>();
            mapTocheckchecklistValidaiton.put(wo.id,wo);
            
             Map<Id,boolean> respMap=WorkOrderTriggerHelper.checklistFilledValidation(mapTocheckchecklistValidaiton,null);
            if(wo.CaseId==null){        
                
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Move out case id is missing.'));
            
            
            }
            else if(issueWrapperList==null || issueWrapperList.size()==0){
                
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please create at least one row.'));
            }  
            else if(respMap!=null && respMap.containsKey(wo.id) && respMap.get(wo.id)==true){
                
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please complete all checklist before creating RP.'));
            }           
            else{
                List<Issue__c> tempIssueList = new List<Issue__c>();
               
                string notes='';
                Boolean hasErrorInFields=false;
                string Msg='';
                for(IssueWrapper iw: issueWrapperList){
                    tempIssueList.add(iw.issue);
                    settlementAmount = settlementAmount + iw.issue.Amount__c;
                    notes+=iw.issue.Issue_Item__c+'('+iw.issue.Comment__c+') ₹ '+iw.issue.Amount__c+', ';
                    
                    if(iw.issue.Issue_Item__c==null ||  iw.issue.Issue_Item__c=='' || iw.issue.Issue_Item__c=='--None--'){                      
                        hasErrorInFields=true;
                        Msg='Please select Issue Item.';
                    }
                    if(iw.issue.Comment__c==null || iw.issue.Comment__c==''){                       
                        hasErrorInFields=true;
                        Msg='Please enter comment in all items.';
                    }
                    if(iw.issue.Amount__c==null || iw.issue.Amount__c<0){                       
                        hasErrorInFields=true;
                        Msg='Please enter amount or amount value greate than zero.';
                    }
                }
                  
                if(!hasErrorInFields){

                    notes=notes.removeEnd(', ');  
                    //Chandu: Varibel to receive the rp jsong resp.
                    SendAPIRequests.MoveoutWithIssueRPSettlementRespWrapper apiResp;
                    //END  
                    
                    apiResp=SendAPIRequests.createMoveOutSettlementRPINWebApp(wo.CaseId,settlementAmount,notes);
                    
                    boolean hasRPSuccess=false;
                    
                    //Start: Chandu: adding new fields for tracking api resp data.
                    if(apiResp!=null){
                        
                        wo.RP_API_Success__c =apiResp.success;                     
                        wo.RP_API_Data__c=apiResp.jsonRespBody;
                        wo.RP_API_Info__c=apiResp.info;
                        if(apiResp.success!=null && apiResp.success=='true'){
                           wo.Move_Out_RP_Creation_DateTime__c = System.now(); 
                           wo.RP_API_Fired__c=true;
                           hasRPSuccess=true;
                        }
                        else{
                            
                          wo.Move_Out_RP_Last_Try_DateTime__c = System.now();
                          returnToWO=false;
                          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'RP Creation in webapp is failed. Please try again or contact your system administrator.'));
                        }
                        if(apiResp.data!=null && apiResp.data.salesforce_mappable_id!=null){
                            wo.Webapp_RP_Invoice_Id__c=apiResp.data.salesforce_mappable_id;
                        }                   
                        
                    }
                    else{
                        wo.RP_API_Success__c ='false';
                        wo.Webapp_RP_Invoice_Id__c= '';
                        wo.RP_API_Data__c='';
                        wo.RP_API_Info__c='RP Creation API Failed.';
                        wo.Move_Out_RP_Last_Try_DateTime__c = System.now();
                        returnToWO=false;
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'RP Creation in webapp is failed. Please try again or contact your system administrator.'));
                                            
                        
                    }
                    //END
                    
                    //  Chandu Fire second API to release the beds
                    
                    boolean bedReleaseAPISuccess=false;
                    boolean methodSuccess=false;
                     case caserecord;
                    if(hasRPSuccess){
                        
                         caserecord=CaseMIMOFunctionality.bedReleaseOnInspectionCOmpleteMethod(wo.CaseId,CONSTANTS.CASE_STATUS_MOVEDOUTWITHISSUE,true);
                        
                        if(caserecord.Bed_Released_API_Status__c!=null && caserecord.Bed_Released_API_Status__c.containsIgnoreCase('True')){
                            bedReleaseAPISuccess=true;
                            
                            try{
                                /*Code added by chandu from create settlement button.*/
                                Id MoveIn_RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEIN).getRecordTypeId();
                                Id MoveOutRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEOUT).getRecordTypeId();
                                /*Code added by Ameed from DEV*/
                                Id caseSettlement_internal_RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_SETTLEMENT_INTERNAL_TRANSFER).getRecordTypeId();

                                Id caseSettlement_RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Case_Settlement).getRecordTypeId();
                                
                                List<Group> InternalTrasnfer = [Select id,name from Group where name =: Constants.QUEUE_NAME_INTERNAL_FINANCE  AND Type= 'Queue'];
                                List<Group> DefaultSubcaseQueue = [Select id,name from Group where name =: Constants.QUEUE_NAME_FINANCE  AND Type= 'Queue'];

                                System.debug('***** DefaultSubcaseQueue '+DefaultSubcaseQueue);

                                List<Group> lstQueueRecords = new list<group>();
                                    lstQueueRecords = [Select id,name from Group where (name =: Constants.QUEUE_NAME_RENTDEFAULT_FINANCE OR name =: Constants.QUEUE_NAME_CANCELLATION_FINANCE OR
                                                                name =: Constants.QUEUE_NAME_SD_DEFAULT_FINANCE OR name =: Constants.QUEUE_NAME_OwnerRejected_FINANCE OR
                                                                name =: Constants.QUEUE_NAME_CANCELLATIONONTRIAL_FINANCE OR name =: Constants.QUEUE_NAME_MOVEOUT_FINANCE )
                                                                AND Type= 'Queue'];
                                id rentDefault_queue;
                                id cancelation_queeue;
                                id sd_default_queue;
                                id cancelationOntrial_queue;
                                id moveoutFinance_Queue;
                                id ownerrejected_Queue;
                               
                                for(group each : lstQueueRecords )  {
                                    if(each.name == Constants.QUEUE_NAME_RENTDEFAULT_FINANCE ) {
                                        rentDefault_queue = each.id;
                                    }
                                    else if(each.name == Constants.QUEUE_NAME_CANCELLATION_FINANCE) {
                                        cancelation_queeue = each.id;
                                    }
                                    else if(each.name == Constants.QUEUE_NAME_SD_DEFAULT_FINANCE ) {
                                        sd_default_queue = each.id ;
                                    }
                                    else if(each.name == Constants.QUEUE_NAME_CANCELLATIONONTRIAL_FINANCE ) {
                                        cancelationOntrial_queue = each.id;
                                    }
                                    else if(each.name == Constants.QUEUE_NAME_MOVEOUT_FINANCE ) {
                                        moveoutFinance_Queue = each.id;
                                    }
                                    else if(each.name == Constants.QUEUE_NAME_OwnerRejected_FINANCE) {
                                        ownerrejected_Queue = each.id;
                                    }
                                }                   
                                
                                //chandu:
                                  caserecord.Settle_Amount_To_Be_deducted__c=settlementAmount;
                                  caserecord.status = Constants.CASE_STATUS_MOVEDOUTWITHISSUE;
                                //END
                                
                                system.debug('***caserecord'+caserecord);
                                list <Bank_Detail__c> tenatbankdetailslist = new list <Bank_Detail__c> ();

                                if(caserecord != null  && caserecord.Tenant__c != null) {
                                    tenatbankdetailslist = [Select id,Account_Number__c, Bank_Name__c, Branch_Name__c,
                                                             IFSC_Code__c, Name from Bank_Detail__c 
                                                            where Related_Account__c =: caserecord.Tenant__c Order By createdDate Asc] ;

                                    system.debug('***tenatbankdetailslist'+tenatbankdetailslist);                           
                                }
                                                 

                                /*previous code (16 aug,2017)
                            * if(caserecord.status == Constants.CASE_STATUS_MOVEDOUTALLOK){
                            */
                                if(caserecord.status == Constants.CASE_STATUS_MOVEDOUT_REFUNDREQUESTED && caserecord.Initiate_Settlement__c=='Yes'){
                                    system.debug(' coming inside');
                                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Move Out Settlement case has been already created.'));                         
                                    
                                }
                                else if(caserecord.status == Constants.CASE_STATUS_MOVEDOUT_MOVEOUTSCHEDULED  || caserecord.status == Constants.CASE_STATUS_NEW  || caserecord.status ==Constants.CASE_STATUS_MOVE_OUT_SCHEDULED ){
                                    system.debug(' coming inside');                             
                                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Settlement case can not be created.'));        
                                }
                                else if(caserecord.status == Constants.CASE_STATUS_MOVEDOUTWITHISSUE) {
                                    if(caserecord.Settle_Amount_To_Be_deducted__c == null ){                                    
                                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Deducted Amount is needed for settlement case.')); 
                                    }
                                    else if(caserecord.Settle_Amount_To_Be_deducted__c != null  && (caserecord.Bed_Released_API_Message__c == null  || caserecord.Bed_Released_API_Status__c == null || !caserecord.Bed_Released_API_Status__c.contains('True')) ){
                                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Settlement case can not be created.Bed is not yet released.'));    
                                        return null;
                                    }
                                  
                                    /*Code added by Ameed from DEV*/
                                    else {                                  
                                        
                                        
                                        if(caserecord.Settle_Amount_To_Be_deducted__c != null && caserecord.status == Constants.CASE_STATUS_MOVEDOUTWITHISSUE && caserecord.Bed_Released_API_Message__c !=null && caserecord.Bed_Released_API_Status__c !=null && caserecord.Bed_Released_API_Status__c.contains('True')) {                                   
                                            boolean isITCase=false;

                                            if(caserecord.parentId !=null && caserecord.Parent_Case_Type__c =='Internal Transfer'){

                                                isITCase=true;
                                            }
                                            
                                            case subcase = new Case();
                                            
                                             if(isITCase){
                                                 subcase.OwnerId=InternalTrasnfer[0].id;
                                                 subcase.recordtypeid=caseSettlement_internal_RecordTypeId;
                                                 subcase.Subject='Move Out Settlement - Internal transfer';
                                                 subcase.Origin='Web';
                                                 subcase.Priority='High';
                                                 subcase.Description='Move Out Settlement - Internal transfer - has been created';                                           
                                                 subcase.Type='MoveOut Settlement - Internal Transfer';
                                                 
                                                 
                                             }
                                             else{
                                                subcase.OwnerId=DefaultSubcaseQueue[0].id;
                                                subcase.recordtypeid=caseSettlement_RecordTypeId;
                                                subcase.Subject='Move Out Settlement';
                                                subcase.Origin=caserecord.Origin;
                                                subcase.Priority=caserecord.Priority;
                                                subcase.Description=caserecord.Description;
                                                subcase.Transaction_ID__c=caserecord.Transaction_ID__c;                                         
                                                subcase.Type='MoveOut Settlement';
                                                
                                                if(caserecord.Move_Out_Type__c != NULL && caserecord.Move_Out_Type__c == constants.MOVEOUTTYPE_TENANT_MOVEOUT && moveoutFinance_Queue != null ) {
                                                    subcase.OwnerId = moveoutFinance_Queue;
                                                }
                                                else if(caserecord.Move_Out_Type__c != NULL && caserecord.Move_Out_Type__c == constants.MOVEOUTTYPE_Cancellation && cancelation_queeue != null ) {
                                                    subcase.OwnerId = cancelation_queeue;
                                                }
                                                else if(caserecord.Move_Out_Type__c != NULL && caserecord.Move_Out_Type__c == constants.MOVEOUTTYPE_CANCELLTIONONTRIAL && cancelationOntrial_queue != null ) {
                                                    subcase.OwnerId = cancelationOntrial_queue;
                                                }
                                                else if(caserecord.Move_Out_Type__c != NULL && caserecord.Move_Out_Type__c == constants.MOVEOUTTYPE_RENTDEFAULT && rentDefault_queue != null ) {
                                                    subcase.OwnerId = rentDefault_queue;
                                                }
                                                else if(caserecord.Move_Out_Type__c != NULL && caserecord.Move_Out_Type__c == constants.MOVEOUTTYPE_OWNERREJECTED && ownerrejected_Queue != null ) {
                                                    subcase.OwnerId = ownerrejected_Queue;
                                                }
                                                else if(caserecord.Move_Out_Type__c != NULL && caserecord.Move_Out_Type__c == constants.MOVEOUT_SDDEFAULT && sd_default_queue != null ) {
                                                    subcase.OwnerId = sd_default_queue;
                                                }
                                             }
                                            /*round robin logic for assignment to run*/

                                            /*populate tenant bank details on sdub case */
                                            if(tenatbankdetailslist != null && tenatbankdetailslist.size() > 0) {
                                                if(tenatbankdetailslist[0].Account_Number__c != null )
                                                    subcase.Tenant_bank_A_c_No__c = tenatbankdetailslist[0].Account_Number__c;
                                                if(tenatbankdetailslist[0].Branch_Name__c != null )
                                                    subcase.Tenant_Bank_Branch_Name__c = tenatbankdetailslist[0].Branch_Name__c;
                                                if(tenatbankdetailslist[0].IFSC_Code__c != null )
                                                    subcase.Tenant_Bank_IFSC_Code__c = tenatbankdetailslist[0].IFSC_Code__c;
                                                if(tenatbankdetailslist[0].Bank_Name__c != null )
                                                    subcase.Tenant_Bank_Name__c = tenatbankdetailslist[0].Bank_Name__c;
                                                if(tenatbankdetailslist[0].name != null)
                                                    subcase.A_c_Holder_Name_Tenant__c = tenatbankdetailslist[0].name ;
                                            }
                                            
                                            
                                            subcase.ContactId=caserecord.ContactId;
                                            subcase.Status='New';                                       
                                            if(caserecord.Settle_Amount_To_Be_deducted__c != null)
                                                subcase.Settle_Amount_To_Be_deducted__c=caserecord.Settle_Amount_To_Be_deducted__c;
                                            if(caserecord.Settlement_Amount_Calculated_Via_System__c != null)
                                                subcase.Settlement_Amount_Calculated_Via_System__c=caserecord.Settlement_Amount_Calculated_Via_System__c;                               
                                            
                                            if(caserecord.Settlement_Amount_Calculated_Via_System__c != null && caserecord.Settle_Amount_To_Be_deducted__c != null)
                                                subcase.Net_Amount_to_Be_Refunded__c=caserecord.Settlement_Amount_Calculated_Via_System__c +caserecord.Settle_Amount_To_Be_deducted__c;
                                            if(caserecord.House_Id__c != null)
                                                subcase.MoveOut_House_ID__c = caserecord.House_Id__c;
                                            
                                            subcase.Deduction_Reason__c=caserecord.Deduction_Reason__c;                                     
                                            subcase.Parentid=caserecord.id;
                                            subcase.Move_Out_Type__c=caserecord.Move_Out_Type__c;
                                            subcase.Move_Out_Date__c=caserecord.Move_Out_Date__c;
                                            subcase.Booked_Object_Type__c = caserecord.Booked_Object_Type__c ;
                                            subcase.House__c = caserecord.House__c;
                                            subcase.AccountId = caserecord.AccountId ;
                                            
                                            insert subcase;
                                            if(caserecord.Settle_Amount_To_Be_deducted__c != null && caserecord.Settlement_Amount_Calculated_Via_System__c != null )
                                                caserecord.Net_Amount_to_Be_Refunded__c =caserecord.Settlement_Amount_Calculated_Via_System__c + caserecord.Settle_Amount_To_Be_deducted__c;
                                            caserecord.Status= Constants.CASE_STATUS_MOVEDOUT_REFUNDREQUESTED ;
                                            caserecord.Initiate_Settlement__c='No';
                                            //Updating rp field once API is fired.
                                            caserecord.RP_API_Fired__c=true;                                        
                                            methodSuccess=true;
                                        }
                                        
                                    }
                                }
                                else {
                                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Settlement case has already been created.'));
                                    
                                    return null;
                                }
                                
                            }
                            catch(Exception e){

                                System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                                UtilityClass.insertGenericErrorLog(e);
                                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'An internal error has occurred. Please contact your system administrator.'));
                                return null;
                            }                       
                            
                            
                        }
                        else{
                            
                             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Bed Release API failed.Please contact your system administrator.'));
                             readOnly=true; 
                        }
                        
                        
                        
                    }
                    else{
                        
                        // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'RP Creation in API is .'));
                    }

                      
                    wo.Settlement_Deduction_Amount__c = settlementAmount;
                    if(methodSuccess){
                        wo.Status='Moved Out with Issues';
                    }
                    update wo;

                    for(Issue__c issue: tempIssueList){
                        issue.Work_Order__c = wo.Id;
                        issue.Case__c=wo.caseId;
                    }

                    if(!tempIssueList.isEmpty()){
                        upsert tempIssueList;
                    }

                    if(methodSuccess){
                        
                     if(caserecord!=null)   
                          update caserecord;
                     
                     PageReference pageRef = new PageReference('/' + wo.Id);
                     pageRef.setRedirect(true);
                     return pageRef;
                    } 
                }
                else{
                     ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,Msg));
                    
                }               
           }
            
       }
        catch(Exception e){
            
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'An internal error has occurred. Please contact your system administrator.'));
            System.debug('***Exception -> '+e+e.getLineNumber()+e.getMessage());
        }
 
        return null; 
     }

    //issue Wrapper - To support the remove link on the page
    public class IssueWrapper{
        public Boolean isToBeDeleted {get; set;}
        public Issue__c issue {get; set;}

        public issueWrapper(Issue__c issue){
            this.issue = issue;
            this.isToBeDeleted = false;
        }
    }
}