/*  Created By : Mohan
    Purpose    : Trigger invocation for Case - RecordType - Service Request are handled in this Handler   */

public class CaseServiceRequestTriggerHandler {
   
    public static Id legalRecordTypeId  = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Legal').getRecordTypeId();  
    public static Id ServiceRequestRecordTypeId  = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_SERVICE_REQUEST).getRecordTypeId();  
    public static Id leadEscalationRtId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_LEAD_ESCALATION_TRACKING).getRecordTypeId();  
    public static Id unverifiedCaseQueueId {get; set;}
    public static Id unassignedCaseQueueId {get; set;}

    // Static flag used to control the Case closure access of non admin Users
    public static Boolean canCloseCase = false;

    static {
        Set<String> caseQueues = new Set<String>{ Constants.QUEUE_UNASSIGNED_CASE_QUEUE, Constants.QUEUE_UNVERIFIED_SERVICE_REQUESTS};
        List<Group> groupList = [select Id, Name, DeveloperName from Group where Name =: caseQueues];
        for(Group g: groupList){
            if(g.Name == Constants.QUEUE_UNASSIGNED_CASE_QUEUE)
                unassignedCaseQueueId = g.Id;
            else if(g.Name == Constants.QUEUE_UNVERIFIED_SERVICE_REQUESTS)
                unverifiedCaseQueueId = g.Id;
        }
    }
    
    // Before Insert oprations for the Cases with Recordtype Service Request   
    public static void beforeInsert(List<Case> newCases){
        
        try{    
                List<Case> caseList = new List<Case>();                          //Case List for Which Problem Related info needs to be updated
                List<Case> assignmentCaseList = new List<Case>();                //Case List for which assignment needs to be performed
                Map<Id, Id> accountMap = new Map<Id, Id>();
                Set<Id> accountIdSet = new Set<Id>(); 
                List<Case> caseLegalList = new List<Case>();
                List<Case> caseFollowerAssignmentList = new List<Case>();
                List<Case> serviceRequestCaseList = new List<Case>();

                for(Case c: newCases){
                     if(c.RecordTypeId == legalRecordTypeId && c.Nestaway_Role__c != Null && c.Nestaway_Role__c == 'Independent Capacity') {
                        caseLegalList.add(c);
                    }
                    if(c.AccountId != null && c.RecordTypeId == ServiceRequestRecordTypeId)
                        accountIdSet.add(c.AccountId);                    
                }

                if(!accountIdSet.isEmpty()){
                    List<Contact> contactList = [select Id, AccountId from Contact where AccountId =: accountIdSet
                                                 and Account.RecordType.Name =: Constants.ACCOUNT_RECORD_TYPE_PERSON_ACCOUNT];

                    for(Contact con: contactList){
                        accountMap.put(con.AccountId, con.Id);
                    }
                }            

                //Id defaultEntitlementId =  [select Id from Entitlement where Account.Name = 'NestAway' limit 1].Id;
                
                for(Case c: newCases){

                    if(c.RecordTypeId == ServiceRequestRecordTypeId){
                        serviceRequestCaseList.add(c);
                    }

                    if(c.AccountId != null && c.ContactId == null && c.RecordTypeId == ServiceRequestRecordTypeId)
                        c.ContactId = !accountMap.isEmpty() && accountMap.containsKey(c.AccountId) && accountMap.get(c.AccountId) != null ? accountMap.get(c.AccountId) : null;

                    if(c.RecordTypeId == ServiceRequestRecordTypeId && (c.Issue_level__c == Constants.CASE_ISSUE_LEVEL_HOUSE_LEVEL || c.Issue_level__c == Constants.CASE_ISSUE_LEVEL_ROOM_LEVEL) && c.Verified_by_all_Tenants__c == false && c.Status == Constants.CASE_STATUS_WATING_ON_TENANT_APPROVAL){
                        c.OwnerId = unverifiedCaseQueueId;
                        caseList.add(c);
                    } else if(c.RecordTypeId == ServiceRequestRecordTypeId && c.Origin == Constants.CASE_ORIGIN_EMAIL){
                        assignmentCaseList.add(c);
                    } else if(c.RecordTypeId == ServiceRequestRecordTypeId && c.Problem__c != null){
                        //c.EntitlementId = defaultEntitlementId;
                        caseList.add(c);
                        assignmentCaseList.add(c);
                    } else if(c.RecordTypeId == ServiceRequestRecordTypeId){
                        System.debug('*******************in the assignment Case List');
                        assignmentCaseList.add(c);
                    }

                    //populate the labour cost and Service Visit Cost - to support the legacy WebApp System added by : Shivansh & Vishal
                    if(c.RecordTypeId == ServiceRequestRecordTypeId && c.Service_Cost_Actual__c != null &&
                        c.Service_Cost_Actual__c > 0){
                        c.Labour_Cost__c = c.Service_Cost_Actual__c;
                    } 

                    if(c.RecordTypeId == ServiceRequestRecordTypeId && c.Service_Visit_Cost_Actual__c != null &&
                        c.Service_Visit_Cost_Actual__c  > 0){
                        c.Service_visit_cost__c = c.Service_Visit_Cost_Actual__c;
                    }                     
                }
                
            if(!caseList.isEmpty()){
                CaseServiceRequestHandler.UpdateProblemRelatedInfo(caseList, true);
            }

            if(!caseLegalList.isEmpty()){
                CaseLegalHelper.beforeInsert(caseLegalList);
            }

            if(!assignmentCaseList.isEmpty()){
                CaseServiceRequestAssignment.serviceRequestCaseAssignment(assignmentCaseList);
            }

            //Once the Case Queue is Assigned from the Problem Type assign the follower
            for(Case c: newCases){
                if(c.Queue__c != null){
                    caseFollowerAssignmentList.add(c);
                }
            }

            if(!caseFollowerAssignmentList.isEmpty()){
                CaseServiceRequestHandler.updateFollowerOnCase(caseFollowerAssignmentList);
            }

            if(!serviceRequestCaseList.isEmpty()){
                CaseSnMTriggerHelper.updateRequestorExpectedResolutionTime(serviceRequestCaseList);
            }
              
        } catch(Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e);
        }
    }
    

    // Before Update oprations for the Cases with Recordtype Service Request
    public static void beforeUpdate(Map<Id, Case> newMap, Map<Id, Case> oldMap){
        
        System.debug('*******************beforeUpdate');

        try{          
                List<Case> caseList = new List<Case>();       //cases for whome Problem is changed and Assignment needs to fire
                List<Case> changedQueueCaseList = new List<Case>();
                List<Case> tenantVerifiedCaseList = new List<Case>();
                List<Case> ownerChangedCaseList = new List<Case>();
                List<Case> requestorEscalatedCaseList = new List<Case>();
                List<Case> resolvedCaseList = new List<Case>();
                Map<Id,Case> MapOfLegaLCase = new Map<Id,Case>();
                List<Case> caseFollowerAssignmentList = new List<Case>();
                List<Case> ownerChangeCases = new List<Case>();

                for(Case c: newMap.values()){
                    if(c.RecordTypeId == legalRecordTypeId && c.Nestaway_Role__c != Null && c.Nestaway_Role__c == 'Independent Capacity') {
                        MapOfLegaLCase.put(c.id,c);
                    }
                    Case oldCase = oldMap.get(c.Id);


                    //update Market Price and Actual Price
                    if(c.RecordTypeId == ServiceRequestRecordTypeId){  

                        /*
                        Mohan - Owner Inclusion is deployed now for all zones
                        //stop Owner Inclusion for non deployed zones
                        set<String> deployedZones = new Set<String>(Label.SnM_Deployed_Zones.split(';'));
                        if(!deployedZones.contains(c.Zone_Code__c) && (c.Owner_Inclusion__c != oldCase.Owner_Inclusion__c ||
                            c.Owner_Inclusion_comment__c != oldCase.Owner_Inclusion_comment__c || 
                            c.Owner_Share_approx__c != oldCase.Owner_Share_approx__c)){
                            c.addError('Owner Inclusion changes are not allowed on the Zone ' + c.Zone_Code__c +
                                        '. Please contact your Admin');
                        }
                        */

                        // When the Case Status is Changing from Resolved to Reopend Capture the Reopen Count 
                        if(c.Status == Constants.CASE_STATUS_REOPENED && oldCase.Status == Constants.CASE_STATUS_RESOLVED){
                            if(c.Reopen_Count__c > 0){
                                c.Reopen_Count__c = c.Reopen_Count__c + 1;
                            } else {
                                c.Reopen_Count__c = 1;
                            }
                        }

                        /*  Validation : Case cannot be closed by a non admin user from the Standard UI
                            non admin user can close the case when he clicks the Generate Invoice button
                            -- access to be controlled using the a Static flag */
                        Profile sysAdminProfile = [select Id, Name from Profile where Name = 'System Administrator'];

                        if(c.Status != oldCase.Status && c.Status == 'Closed' && UserInfo.getProfileId() != sysAdminProfile.Id 
                            && !canCloseCase){
                            c.addError('You do not have access to Close the Case');
                        }

                        if(c.Owner_Inclusion__c == 'true' && c.Owner_Inclusion__c != oldMap.get(c.Id).Owner_Inclusion__c){
                            c.Owner_approval_status__c = 'Pending';
                            c.Deadline_For_Owner_Approval__c = System.now().addDays(2);
                        }     

                        if(c.Owner_Inclusion__c == 'false' && c.Owner_Inclusion__c != oldMap.get(c.Id).Owner_Inclusion__c){
                            c.Owner_approval_status__c = '';
                            c.Status = Constants.CASE_STATUS_WORK_IN_PROGRESS;
                        }                                          

                        if(c.Express_Service_Cost__c == null){
                            c.Express_Service_Cost__c = 0;
                        }

                        if(c.Service_Visit_Cost_Actual__c == null){
                            c.Service_Visit_Cost_Actual__c = 0;
                        }

                        if(c.Service_Cost_Actual__c  == null){
                            c.Service_Cost_Actual__c = 0;
                        }

                        if(c.Service_visit_cost__c   == null){
                            c.Service_visit_cost__c  = 0;
                        }

                        if(c.Service_Cost__c == null){
                            c.Service_Cost__c = 0;
                        }                                                

                        c.Total_Actual_Price__c = c.Express_Service_Cost__c + c.Service_Visit_Cost_Actual__c + c.Service_Cost_Actual__c;
                        c.Total_Market_Price__c = c.Express_Service_Cost__c + c.Service_visit_cost__c + c.Service_Cost__c;                      
                    }

                    //when Problem is changed on the Case run the Assignment Logic
                    if(c.RecordTypeId == ServiceRequestRecordTypeId && c.Problem__c != oldCase.Problem__c){
                        caseList.add(c);
                    }  

                    //when verified by all tenants is true - run the assignment logic
                    if(c.RecordTypeId == ServiceRequestRecordTypeId && c.Verified_by_all_Tenants__c == true && c.Verified_by_all_Tenants__c != oldCase.Verified_by_all_Tenants__c)  
                        tenantVerifiedCaseList.add(c);

                    //when Case Queue is Changed Assignment logic to start
                    if(c.RecordTypeId == ServiceRequestRecordTypeId && c.Problem__c == oldCase.Problem__c && c.Queue__c != oldCase.Queue__c){
                        changedQueueCaseList.add(c);
                    }

                    //when Case Owner is changed populate the Next Email to field
                    if(c.RecordTypeId == ServiceRequestRecordTypeId && c.OwnerId != oldCase.OwnerId && c.Problem__c == oldCase.Problem__c && c.Queue__c == oldCase.Queue__c){
                        ownerChangedCaseList.add(c);
                    }

                    if(c.RecordTypeId == ServiceRequestRecordTypeId && c.Owner_Inclusion__c == 'Yes' && c.Owner_Inclusion__c != oldCase.Owner_Inclusion__c){
                        c.Owner_approval_status__c = 'Pending';
                    }

                    //if there is change in Requestor Escalation Level change calculate the Requestor Expected Resolution Time
                    if(c.RecordTypeId == ServiceRequestRecordTypeId && c.Requestor_Esclation_Level__c != oldCase.Requestor_Esclation_Level__c){
                        System.debug('********inside the if block');
                        requestorEscalatedCaseList.add(c);
                    }   

                    //Sync the labour cost and Service Visit Cost - to support the legacy WebApp System added by : Shivansh & Vishal
                    if(c.RecordTypeId == ServiceRequestRecordTypeId && c.Labour_Cost__c != oldCase.Labour_Cost__c){
                        c.Service_Cost_Actual__c = c.Labour_Cost__c;
                    } 

                    if(c.RecordTypeId == ServiceRequestRecordTypeId && c.Service_visit_cost__c != oldCase.Service_visit_cost__c){
                        c.Service_Visit_Cost_Actual__c = c.Service_visit_cost__c;
                    }   

                    //Perform some Validations on Case Resolution
                    if(c.RecordTypeId == ServiceRequestRecordTypeId && c.Status == Constants.CASE_STATUS_RESOLVED &&
                        c.Status != oldCase.Status){
                        resolvedCaseList.add(c);
                    } 

                    if(c.RecordTypeId == ServiceRequestRecordTypeId){

                        if(c.Status == Constants.CASE_STATUS_RESOLVED && c.Status != oldCase.Status){
                            c.Resolved_Time__c = System.now();
                        }   

                        //When No of Agent Comments > 0 that means 1st Agent Comment - 1st response time
                        if(c.No_of_Agent_Comments__c > 0 && c.First_Response_Time__c == null){
                            c.First_Response_Time__c = System.now();
                        }   

                        //capture the 1st Escalation Time
                        if(c.Requestor_Esclation_Level__c == '1' && c.Requestor_Esclation_Level__c != oldCase.Requestor_Esclation_Level__c){
                            c.Requester_First_Escalation_Time__c = System.now();
                        }  

                        //if the Case Owner is changing invoke the Validator method
                        if(c.OwnerId != oldCase.OwnerId){
                            ownerChangeCases.add(c);
                        }                                   
                    }                                       
                }      
                
                if(!MapOfLegaLCase.isEmpty()){
                    CaseLegalHelper.beforeupdate(MapOfLegaLCase);
                }

                if(!caseList.isEmpty()){
                    CaseServiceRequestHandler.UpdateProblemRelatedInfo(caseList, false);
                    CaseServiceRequestAssignment.serviceRequestCaseAssignment(caseList);  
                }     

                if(!tenantVerifiedCaseList.isEmpty())
                    CaseServiceRequestAssignment.serviceRequestCaseAssignment(tenantVerifiedCaseList);    

                if(!changedQueueCaseList.isEmpty())
                    CaseServiceRequestAssignment.serviceRequestCaseAssignment(changedQueueCaseList);

                if(!ownerChangedCaseList.isEmpty())
                    CaseServiceRequestHandler.populateNextEmailTofield(ownerChangedCaseList);

                if(!requestorEscalatedCaseList.isEmpty()){
                    CaseSnMTriggerHelper.updateRequestorExpectedResolutionTime(requestorEscalatedCaseList);
                }     

                if(!resolvedCaseList.isEmpty()){
                    CaseSnMTriggerHelper.caseResolutionValidator(resolvedCaseList);
                }      

                //Once the Case Queue is Assigned from the Problem Type assign the follower
                for(Case c: newMap.values()){
                    if(c.Queue__c != null){
                        caseFollowerAssignmentList.add(c);
                    }
                }

                if(!caseFollowerAssignmentList.isEmpty()){
                    CaseServiceRequestHandler.updateFollowerOnCase(caseFollowerAssignmentList);
                }  

                if(!ownerChangeCases.isEmpty()){
                    CaseServiceRequestHandler.ownerChangeValidator(ownerChangeCases);
                }                       
            
            } catch(Exception e){
                System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                UtilityClass.insertGenericErrorLog(e, 'Case Service Request - BeforeUpdate');
            }
    }


    /*  Created By : Mohan
        Purpose    : After Update operations on Case with RecordType Service Request are handled from this method */
    public static void afterUpdate(Map<Id, Case> newMap, Map<id, Case> oldMap){

        try{    
                Set<Id> esclatedCaseIdSet = new Set<Id>();
                Set<Id> resolvedCaseIdSet = new Set<Id>();
                Map<Id,Case>  MapofMimoAndService = new Map<Id,Case>();
                 List<Case> caseList = new List<CAse>();
                List<Case> caseListMimo = new List<CAse>();
                List<Case> OwnerInclusionCaseList = new List<Case>();
                List<Case> ownerApprovalStatusChangeCaseList = new List<Case>();
                List<Case> closedCaseList = new List<Case>();
                List<Case> leadTrackingCaseList = new List<Case>();
                List<Case> caseOwnerList = new List<Case>();
                Map<Id, Case> rescheduledCaseMap = new Map<Id, Case>();
                Map<Id, Case> statusChangedCaseMap = new Map<Id, Case>();
                Set<String> fslDeployedZonesSet = new Set<String>(Label.FSL_Deployed_Zones.split(';'));

                for(Case c: newMap.values()){

                    // added by deepak
                    // to chnage the workorder caseownerreamil
                     if(c.RecordTypeId == ServiceRequestRecordTypeId && c.ownerid != null && (c.ownerid != oldMap.get(c.Id).ownerid)){ 
                         system.debug('coming here');
                        caseOwnerList.add(c);
                    }

                    if(c.RecordTypeId == ServiceRequestRecordTypeId && c.Escalation_Level__c != oldMap.get(c.Id).Escalation_Level__c){                        
                        esclatedCaseIdSet.add(c.Id);
                    }

                    if(c.RecordTypeId == ServiceRequestRecordTypeId && c.Status == Constants.CASE_STATUS_RESOLVED && c.Status != oldMap.get(c.Id).Status)
                        resolvedCaseIdSet.add(c.Id);
                     if(c.RecordTypeId == ServiceRequestRecordTypeId && c.Status == Constants.CASE_STATUS_RESOLVED && c.Status != oldMap.get(c.Id).Status)
                        caseList.add(c);

                    if(c.RecordTypeId == ServiceRequestRecordTypeId && c.Status == Constants.CASE_STATUS_CLOSED && c.Status != oldMap.get(c.Id).Status){
                         closedCaseList.add(c);
                    }
                    if(c.RecordTypeId == ServiceRequestRecordTypeId && c.Status == Constants.CASE_STATUS_CLOSED && c.Status != oldMap.get(c.Id).Status && c.ParentId != null){
                        caseListMimo.add(c);
                        MapofMimoAndService.put(c.id,c);
                    }
                        

                     if(c.RecordTypeId == ServiceRequestRecordTypeId && c.Owner_approval_status__c != oldMap.get(c.Id).Owner_approval_status__c &&
                        (c.Owner_approval_status__c == 'Approved' || c.Owner_approval_status__c == 'Rejected')){
                        ownerApprovalStatusChangeCaseList.add(c);
                     }

                    //Make an API call when Owner Inclusion is Yes to Web App
                    if(c.RecordTypeId == ServiceRequestRecordTypeId && c.Owner_Inclusion__c == 'true' && 
                        c.Owner_Inclusion__c != oldMap.get(c.Id).Owner_Inclusion__c){
                        CaseSnMTriggerHelper.sendOwnerInclusionDetails(c.Id);
                        OwnerInclusionCaseList.add(c);
                    }  

                    //if the Escalation Level of the Lead Tracking Case is changed update the Lead record
                    //Modified By baibhav
                    if(c.RecordTypeId == leadEscalationRtId && 
                        (c.New_Stage_Escalation_Level_for_Lead__c != oldMap.get(c.Id).New_Stage_Escalation_Level_for_Lead__c ||
                        c.Open_Stage_Escalation_Level_for_Lead__c != oldMap.get(c.Id).Open_Stage_Escalation_Level_for_Lead__c
                        || c.Escalation_Trigger_time__c != oldMap.get(c.Id).Escalation_Trigger_time__c
                        || (c.Lead_Status__c != oldMap.get(c.Id).Lead_Status__c && c.Lead_Status__c==Constants.CASE_LEAD_STATUS_DROPPED ))){
                            leadTrackingCaseList.add(c);
                    } 

                    if(c.RecordTypeId == ServiceRequestRecordTypeId){

                        /* All the Functionality related to FSL - Module - Initial Phase of Deployment
                           Filter out Cases only which belong to the deployed Zones */
                        if(fslDeployedZonesSet.contains(c.Zone_Code__c)){

                            //When the Tenant reschedules the time - invoke the rescheduling of the Service Appointment flow
                            if(c.Service_Visit_Time__c != oldMap.get(c.Id).Service_Visit_Time__c && c.Require_visit__c == true){
                                rescheduledCaseMap.put(c.Id, c);
                            }

                            //When the Case is Auto Rejected by System then Drop the WorkOrder 
                            if(c.Status != oldMap.get(c.Id).Status && (c.Status == CaseConstants.CASE_STATUS_CASE_AUTOCLOSED ||
                                c.Status == CaseConstants.CASE_STATUS_CANCELLED)){
                                    statusChangedCaseMap.put(c.Id, c);
                            }
                        }
                    }                                  
                }

                if(!esclatedCaseIdSet.isEmpty()){
                    CaseServiceRequestHandler.onCaseEsclation(esclatedCaseIdSet);
                }

                if(!resolvedCaseIdSet.isEmpty()){
                    CaseServiceRequestHandler.completeMilestones(resolvedCaseIdSet);
                }
                if(!caseListMimo.isEmpty()){
                    CaseServiceRequestMiMoHandler.UpdateMiMoRicBicHic(caseListMimo);
                }
               if(!MapofMimoAndService.isEmpty()){
                 CaseServiceRequestMiMoHandler.UpdateMoveInCaseStausToClosed(MapofMimoAndService);
              }
                if(!ownerApprovalStatusChangeCaseList.isEmpty()){
                    CaseSnMTriggerHelper.ownerApprovalStatusChangeCaseComment(ownerApprovalStatusChangeCaseList);
                }

                if(!OwnerInclusionCaseList.isEmpty()){
                    CaseSnMTriggerHelper.ownerInclusionCaseComment(OwnerInclusionCaseList);
                }
            
               if(!caseOwnerList.isEmpty()){
                   ServiceRequestTriggerHelper.changeOwnerEmailOnWorkOrder(caseOwnerList);   
                }
                if(!closedCaseList.isEmpty()){
                    CaseSnMTriggerHelper.dropTaskAndWorkOrderOnCaseClosure(closedCaseList);
                }

                if(!leadTrackingCaseList.isEmpty()){
                  //  CaseLeadEscalationTrackingTriggerHelper.syncLeadEscalationLevels(leadTrackingCaseList);
                }

                if(!rescheduledCaseMap.isEmpty()){
                    CaseServiceRequestHandler.rescheduleVendorWorkOrder(rescheduledCaseMap);
                }

                System.debug('**statusChangedCaseMap: ' + statusChangedCaseMap + '\n Size of statusChangedCaseMap: ' + statusChangedCaseMap.size());

                if(!statusChangedCaseMap.isEmpty()){
                    CaseServiceRequestHandler.updateVendorWorkOrderForCaseChanges(statusChangedCaseMap);
                }

            } catch(Exception e){
                System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                UtilityClass.insertGenericErrorLog(e);
            }
    }


/**********************************************
    Created By : Mohan
    Purpose    : AfterInsert handler for Service Request Cases
**********************************************/
    public static void afterInsert(Map<Id, Case> newMap){

        System.debug('**afterInsert CaseServiceRequestTriggerHandler');

        try{

            List<Case> caseWithServiceAppointmentList = new List<Case>();
            Set<String> fslDeployedZonesSet = new Set<String>(Label.FSL_Deployed_Zones.split(';'));

                for(Case c: newMap.values()){
                    if(c.RecordTypeId == ServiceRequestRecordTypeId){

                        //Check if the Case requires a visit and create Wo and Service Appointment
                        if(c.Require_visit__c == true && c.Service_Visit_Time__c != null && fslDeployedZonesSet.contains(c.Zone_Code__c)){
                            caseWithServiceAppointmentList.add(c);
                        }
                    }
                }

                if(!caseWithServiceAppointmentList.isEmpty()){
                    CaseServiceRequestHandler.createVendorWorkOrder(caseWithServiceAppointmentList);
                }
            } catch(Exception e){
                    System.debug('**Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'After Insert CaseServiceRequestHandler');      
            }   
    }
}