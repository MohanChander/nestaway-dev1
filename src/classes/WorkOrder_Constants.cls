/*  Created By   : Mohan - WarpDrive Tech Works
Created Date : 29/09/2017
Purpose      : All the constants, Picklist values, RecordType Name are accessed from this class  for Workorder*/

public class WorkOrder_Constants {
     //WorkOrder Status Move In WorkOrder
     public static String WORKORDER_RT_MOVE_IN_WORK_ORDER ='Move In WorkOrder';
    public static String WorkORDER_COMPLETE_STATUS_Moved_OUT_WITH_ISSUE='Moved Out with Issues';
    public static String WorkORDER_COMPLETE_STATUS_MOVED_OUT_ALL_OKAY='Moved Out All Okay';
    public static String WorkORDER_COMPLETE_STATUS_MOVEOUT_RECORDTYPE ='Completed';
    public static String WorkORDER_START_STATUS_MOVEOUT_RECORDTYPE ='Yet to be called';
    public static String WorkORDER_ONHOLD_STATUS_MOVEOUT_RECORDTYPE ='Ring but No Response';
    public static String WORKORDER_STATUS_MOVED_IN_WITH_ISSUES = 'Moved-in with issues';
    public static String WORKORDER_STATUS_MOVED_IN_ALL_OK = 'Moved-in all OK';
    public static String WORKORDER_STATUS_CANCELED = 'Canceled';
    public static String WORKORDER_STATUS_VERIFIED = 'Verified';
    public static String WORKORDER_STATUS_COMPLETED= 'Completed';
    public static String WORKORDER_RT_QUOTATION_VENDER = 'Quotation from vender';
    public static String WORKORDER_TYPE_SD_RECOVERY= 'SD Recovery';
    public static String CONTRACT_SENT_TO_TENANT= 'Contract Sent to Tenant';
    public static String CONTRACT_SENT_FOR_OWNER_SIGNATURE= 'Contract Sent for Owner Signature';
    public static String WORKORDER_STATUS_CANCELLED = 'Cancelled';

        
        
        //Work Order Record types;
    public static String MoveOutWorkOrder_RT='MoveOut WorkOrders';
    public static String WORKORDER_OFFBOARDING_CALL_TO_OWNER='Offboarding Call To Owner';
    public static String WORKORDER_OFFBOARDING_ZAM_TAK='Offboarding ZAM Task';
    public static String WORKORDER_RT_OFFBOARDING_FINAL_INSPECTION='Offboarding Final Inspection Completed';
    public static String WORKORDER_RT_OFFBOARDING_FULL_FINAL_AMT='Offboarding Full & Final Amount';
    public static String WORKORDER_RT_OFFBOARDING_HOUSE_UNFURNISHED ='Offboarding House Unfurnished';
    public static String WORKORDER_RT_OFFBOARDING_HOUSE_MAINTENANCE='Offboarding House Undergoing Maintenance';
    public static String WORKORDER_RT_OFFBOARDING_TENANT_MOVE_OUT='Offboarding Tenants Moved Out';
    public static String WORKORDER_RT_OFFBOARDING_HOUSE_DISCONNECTED='Offboarding House Disconnected';
    public static String WORKORDER_RT_OFFBOARDING_KEYS_SUBMITTED='Offboarding Keys Submitted';     
    public static String WORKORDER_RT_IT_SD_DEFAULT='Internal Transfer SD Default';
    public static String WORKORDER_RT_OFFBOARDING_INSURANCE='Insurance workorder';
    public static String WORKORDER_RT_ReadOnly_MOVEOUT = 'Move Out Checklist Inspection Read Only';
    public static String WORKORDER_RT_ReadOnly_MOVEIN = 'Move In Checklist Inspection Read Only';
    public static String WORKORDER_RT_VENDER='Vendor Work Order';
    public static String WORKORDER_RT_E_SIGNED ='MoveIn E-Signed Agreement';
    public static String WORKORDER_RT_INVOICE = 'Invoice';
      
    
    
    //Work Order Status
    
    public static String WORKORDER_STATUS_CONT_OFFBOARDING='Continue Offboarding';
    public static String WORKORDER_STATUS_RETAINED_HOUSE='Retain House';
    public static String WORKORDER_STATUS_RESOLVED='Resolved';
    public static String WORKORDER_STATUS_WORK_IN_PROGRESS = 'Work in Progress';
    public static String WORKORDER_STATUS_WORK_DONE = 'Work Done';
    public static String WORKORDER_STATUS_REJECTED_BY_TENANT = 'Rejected by Tenant';
    public static String WORKORDER_STATUS_PRECHECK_DONE = 'Precheck Done';
    public static String WORKORDER_STATUS_DROPPED = 'Dropped';
    
    
    public static String WORKORDER_STATUS_AUTO_CLOSED ='Auto Closed';
    public static String WORKORDER_TYPE_IT_SD_RECOVERY= 'Internal Transfer - SD Recovery';
    public static String WORKORDER_RT_KEYS= 'Keys';
    public static String WORKORDER_RT_KEYS_READ_ONLY= 'Keys ReadOnly';     
    public static String WORKORDER_RT_CONTRACTUPLOADED= 'Contract Uploaded WorkOrder';  

    public static String WORKORDER_RT_KEY_HANDLIND='Key Handling';
    public static String WORKORDER_RT_ONBOADING='On-Boarding WorkOrder'; 
    public static String WORKORDER_RT_ONBOADING_CHECKLIST='On-Boarding Checklist WorkOrder'; 
    public static String WORKORDER_Service_Request_WorkOrder ='Service Request WorkOrder';
    public static String WAIVE_OFF_WorkOrder ='Waive Off Work Order';


}