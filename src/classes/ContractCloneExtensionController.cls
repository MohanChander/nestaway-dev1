public  with sharing class ContractCloneExtensionController {


    // empty constructor
    public ContractCloneExtensionController (ApexPages.StandardController controller) {

    }    
   
    
    @RemoteAction
    public static CloneContractReturnWrapper cloneContract(String conId) {
       
             cloneContractReturnWrapper returnWrap = new cloneContractReturnWrapper();
             
             
             try{
                  // adding the validation rules to clone contract.
                  Set<Id> conIds = new Set<Id>();
                  conIds.add(conId);
                  List<Contract> conLst=ContractSelector.getContractsWithVersioningInfo(conIds);             
                  contract con = new contract();
                  if(conLst.size()>0 && conLst.get(0).Opportunity__c!=null){
                      con=conLst.get(0);
                      Id cloneContractRT = ContractConstants.getRecordTypeIdByName(ContractConstants.CONTRACT_RT_CLONE_CONTRACT);                     
                      List<Contract> otherContractsLst=ContractSelector.getInProgressVersionContractsOfOpportunity(new set<Id>{con.Opportunity__c},new set<Id>{con.id});
                                                             
                    // check if user can press this button today or not
                     Org_Param__c param = Org_Param__c.getInstance();
                    
                    // Check the user profile if he can press the button.
                    
                    Set<string> profileNames = new Set<string>();
                    string defaultProfile='System Administrator';
                    profileNames.add(defaultProfile.toUpperCase());
                    
                    if(param.Profile_Which_Can_Clone_Contract__c!=null){
                        
                        List<string> profileSplit=param.Profile_Which_Can_Clone_Contract__c.split(',');
                        for(string pf: profileSplit){
                            
                            profileNames.add(pf.toUpperCase());
                        }
                    }
                    
                    Map<Id,Profile> allOrgProfile=new Map<Id,profile>([select id,name from profile]);
                    string currentUserProfileName;
                    if(allOrgProfile.containsKey(UserInfo.getProfileId())){
                        
                        currentUserProfileName=allOrgProfile.get(UserInfo.getProfileId()).name.toUpperCase();
                    }
                    
                    if(currentUserProfileName==null || !profileNames.contains(currentUserProfileName)){
                        
                        returnWrap.isSuccess=false;
                        returnWrap.contID=conId;
                        returnWrap.message='You dont have permission to create contract vesion.';
                    }
              
                    else if(con.isActive__c==null || con.isActive__c==false || con.House__c==null || con.House__r.stage__c==null || con.House__r.stage__c!='House Live'){
                          
                            returnWrap.isSuccess=false;
                            returnWrap.contID=conId;
                            returnWrap.message='You can create version of active contract after house become live.';
                          
                      } 
                    
                    else if(otherContractsLst.size()>0){
                        
                            returnWrap.isSuccess=false;
                            returnWrap.contID=conId;
                            returnWrap.message='In Progrees version contract is presnet on this contract. Please work on the same.';
                        
                    }                
                      else{
                      
                        Contract originalContract = new Contract(id=conId);
                        sObject originalSObject = (sObject) originalContract;
                        //get the query string for the cloned fields:-
                        string queryString=OperationalProcessService.getContractCloneFieldsQueryString();
                        List<sObject> originalSObjects = new List<sObject>{originalSObject};

                        List<sObject> clonedSObjects = SObjectAllFieldCloner.cloneObjects(
                              originalSobjects,
                              originalSobject.getsObjectType(),null,queryString);
                             
                        Contract clonedContract = (Contract)clonedSObjects.get(0);                
                        system.debug('*****clonedContract'+clonedContract);
                        clonedContract.Parent_Contract__c=conId;
                        clonedContract.recordtypeId=cloneContractRT;
                        clonedContract.isCloneContract__c=true;
                        clonedContract.Status='Draft';
                        clonedContract.Approval_Status__c='Draft';
                        clonedContract.API_Info__c=null;
                        clonedContract.API_Sucess__c=null;
                        clonedContract.Agreement_Approval_Code__c=null;
                        clonedContract.Owner_Approval_Status__c=null;
                        clonedContract.Owner_Approved_By_API__c=false;      
                        clonedContract.Manually_Approved_By_ZM__c=false;
                        clonedContract.Is_Commercial_Field_Changed__c=false;
                        clonedContract.Archived__c=false;
                        clonedContract.City_Manager__c=null;
                        clonedContract.isActive__c=false;
                        clonedContract.Sync_Failed_Number__c=null;
                        clonedContract.New_Version_Active_Date__c=null;
                        clonedContract.Non_Commercial_Fields_Active_Date__c=null;
                        clonedContract.Commercial_Fields_Active_Date__c=null;
                        clonedContract.Sync_Commercial_Fields_Date__c=null;
                        clonedContract.Stop_Commercial_Field_Sync__c=false;
                        clonedContract.Commercial_Field_Sync_Was_Stop__c=false;
                        clonedContract.Awaiting_HRM_Approval_Time__c=null;
                        clonedContract.House_Term_Change_URL__c=null;                       
                        insert clonedContract;
                        
                        // Insert the clone of the room in the same contract.
                        
                        List<Room_Terms__c> roomTermLst=[select id from Room_Terms__c where Contract__c=:con.id];
                        if(roomTermLst.size()>0){
                             
                                Room_Terms__c roomTerm = new Room_Terms__c();
                                List<sObject> roomOriginalSObject = (List<sObject>) roomTermLst;

                                List<sObject> roomOriginalSObjects =roomOriginalSObject;

                                List<sObject> roomClonedSObjects = SObjectAllFieldCloner.cloneObjects(
                                roomOriginalSObjects,
                                roomOriginalSObject.getsObjectType(),'Parent_Room_Term__c',null);

                                List<Room_Terms__c> clonedRoomTerms = (List<Room_Terms__c>)roomClonedSObjects; 
                                for(Room_Terms__c rt: clonedRoomTerms){
                                    
                                    rt.House__c=null;
                                    rt.Room_Inspection__c=null;
                                    rt.Contract__c=null;                                    
                                    rt.Version_Contract__c=clonedContract.id;
                                    rt.New_Monthly_Base_Rent_Per_Room__c=rt.Monthly_Base_Rent_Per_Room__c;
                                }
                                
                                insert clonedRoomTerms;
                        }
                         
                        returnWrap.isSuccess=true;
                        returnWrap.contID=clonedContract.id;
                        returnWrap.message='Contract Cloned Successfully.';
                      }
                   }
                   else{
                       
                        returnWrap.isSuccess=false;
                        returnWrap.contID=conId;
                        returnWrap.message='Invalid contract id.';
                   }
             }
            catch(Exception e){
                
                system.debug('****exception in remote clone mehtod '+e.getMessage()+' at line'+e.getLineNumber());
                returnWrap.isSuccess=false;
                returnWrap.contID=conId;
                returnWrap.message='An internal error has occurred. Please contact your system administrator.';
            }            
               
       
        return returnWrap;
    }
    
    public class CloneContractReturnWrapper{
        
        public boolean isSuccess {get;set;}
        public string contID {get;set;}
        public string message {get;set;}
        
    }

}