// added by chandu to call the existing future method from this class.


public class MIMOFunctionQueueableClass implements Queueable {
    
    Set<Id> caseRecIds;
    string methodName;
    
    public MIMOFunctionQueueableClass(Set<Id> caseRecIds,string methodName){
        
         this.caseRecIds=caseRecIds;
         this.methodName=methodName;
    }
    
    public void execute(QueueableContext context) {
              
         if(methodName=='triggerOwnerApprovalInWebApp'){
             
                for(Id caseRecId: caseRecIds){
                
                    TaskTriggerHandlerAddOn1.triggerOwnerApprovalInWebApp(caseRecId);
                 }  
             
         }
         if(methodName=='triggerAutoOwnerApprovalConfirmationInWebApp'){
             
             for(Id caseRecId: caseRecIds){
                
                 CaseMIMOFunctionality.triggerAutoOwnerApprovalConfirmationInWebApp(caseRecId);
            }  
         }       
                       
              
    }
}