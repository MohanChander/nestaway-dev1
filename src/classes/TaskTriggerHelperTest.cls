@Istest
public class TaskTriggerHelperTest {
    
    public static id casfurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Furnished_House_Onboarding).getRecordTypeId();
    public static id casunfurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Unfurnished_House_Onboarding).getRecordTypeId();
     public static    Id TenantRecordTypeID = Schema.SObjectType.Task.getRecordTypeInfosByName().get(Constants.TASK_RT_MOVEIN_TASKS).getRecordTypeId();
    public Static TestMethod void TaskTest(){
        Test.StartTest();
        NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
        cusSet.name = 'nestaway';
        cusSet.Nestaway_URL__c = 'www.test.com';
        insert cusSet;
        
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        accObj.BillingCity            ='Test';
        accObj.BillingCountry         ='India';
        accObj.BillingPostalCode      ='560068';
        accObj.BillingState           ='Karnataka'; 
        accObj.BillingStreet          ='Test'; 
        insert accObj;
        
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        
        insert hos;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Quote Creation';
        insert opp;
        
        House_Inspection_Checklist__c houseIC = new House_Inspection_Checklist__c();
        houseIC.Opportunity__c = opp.Id;
        houseIC.Status__c = 'House Inspection In Progress';           
        insert houseIC;
        Case c=  new Case();
        c.House__c=hos.Id;
        c.Status='Tenant Docs Verification Pending';
        c.Owner_Approval_API_Msg__c='Error in owner approval API.';
        c.Owner_Approval_Sent_To_Webapp__c=false;
        c.Stage_Status__c='Pending Profile Updates';
        c.RecordTypeId=casfurnishedRecordtype;
        insert c;
        
        Set<Id> setOfCase = new Set<id>();
        setOfcase.add(c.id);
        Task t = new Task();
        t.ActivityDate  = Date.today();
        t.Subject       = 'House Visit';
        t.Status        = 'Open';
        t.Priority      = 'Normal';
        t.WhatId        = houseIC.Id;
        t.Number_of_Keys_for_Balcony__c='1';
        t.Number_of_Keys_for_BedRoom__c='1';
        t.Number_of_Keys_for_Cupboards__c='1';
        t.Number_of_Keys_for_Kitchen__c='1';
        t.Number_of_Keys_for_Main_Door__c='1';
        t.Number_of_Keys_for_Servant_Room__c='1';
        t.Number_of_Keys_for_Storeroom__c='1';
        insert t;
        
        
        map<id,Task> oldmap = new map<id,Task>();
        oldmap.put(t.id,t);
        Task t1 = new Task(id=t.id);
        t1.ActivityDate  = Date.today();
        t1.Subject       = 'House Visit';
        t1.Status        = 'Open';
        t1.Priority      = 'Normal';
        t1.WhatId        = houseIC.Id;
        t1.House__c=hos.id;
        t1.Number_of_Keys_for_Balcony__c='2';
        t1.Number_of_Keys_for_BedRoom__c='2';
        t1.Number_of_Keys_for_Cupboards__c='2';
        t1.Number_of_Keys_for_Kitchen__c='2';
        t1.Number_of_Keys_for_Main_Door__c='2';
        t1.Number_of_Keys_for_Servant_Room__c='2';
        t1.Number_of_Keys_for_Storeroom__c='2';
         t1.RecordTypeId=TenantRecordTypeID;
        update t1;
        Detail__c dc= new Detail__c();
        dc.Case__c=c.id;
        dc.Task_Id__c=t1.id;
        insert dc;
        map<id,Task> newmap = new map<id,Task>();
        newmap.put(t1.id,t1);
        List<Task> taskList= new   List<Task>();
        taskList.add(t1);
        TaskTriggerHelper.UpdateCaseStatusToClosed(setOfcase);
        TaskTriggerHelper.makeHouseLiveTaskValidator(newmap,newmap);
        test.stoptest();
        
    }

}