public class SendAPIForContract {
    public class ContractDetailsWrapeerClass{
        public String move_in_sf_id{get;set;}
        public String tenant_state{get;set;}
        public String tenant_street_address{get;set;}
        public String tenant_city{get;set;}
        public integer tenant_pincode{get;set;}
        public String tenant_country{get;set;}
        public String owner_state{get;set;}
        public String owner_street_address{get;set;}
        public String owner_city{get;set;}
        public integer owner_pincode{get;set;}
        public String owner_country{get;set;}
        public String document_id{get;set;} 
    }
    public class ContractDetailsJsonRespWrapeerClass{        
        public string success;
        public string info;
        public ContractDataWrapper data;
    }
    public class ContractDataWrapper{
       public boolean signed_by_tenant ;
        public boolean signed_by_both_parties;
        public boolean document_estamped ;
        public String link_to_sign ;
        
        
    }
    @future(callout=true)
    public static void sendContractDetailsToWebAppFuture(Id ConID){    
        sendContractDetailsToWebApp(ConID);
    }
    
    public static void sendContractDetailsToWebApp(Id ConID){   
        system.debug('sendContractDetailsToWebApp'+ConID);
        String respBody;
        try{
            List<NestAway_End_Point__c > nestURL = NestAway_End_Point__c.getall().values();  
            Contract  Con = [select id, MoveIn_Case__c,Owner_City__c,Owner_Country__c,Owner_Pincode__c,Owner_State__c,Owner_Street_Address__c, 
                             Tenant_City__c,Tenant_Country__c,Tenant_Pincode__c,Tenant_State__c,Tenant_Street_Address__c
                             from  Contract   where id=:ConID];
            system.debug('Contract'+Con);
            Attachment attach = [select id,Name from Attachment where ParentId = :ConID and name LIKE '%pdf'  order by CreatedDate desc limit 1];
            system.debug('Attachment'+attach);

            ContractDetailsWrapeerClass rp = new ContractDetailsWrapeerClass();
            if(Con.MoveIn_Case__c != null){
                rp.move_in_sf_id = Con.MoveIn_Case__c;
            }
            if(Con.Tenant_State__c != null){
                rp.tenant_state = Con.Tenant_State__c;
            }
            if(Con.Tenant_Street_Address__c != null){
                rp.tenant_street_address = Con.Tenant_Street_Address__c;
            }
            if(con.Tenant_City__c != null){
                rp.tenant_city = con.Tenant_City__c;
            }
            if(con.Tenant_Pincode__c != null){
                rp.tenant_pincode = Integer.valueOf(con.Tenant_Pincode__c);
            }
            if(con.Tenant_Country__c != null){
                rp.tenant_country = con.Tenant_Country__c;
            }
            if(con.Owner_State__c != null){
                rp.owner_state = con.Owner_State__c;
            }
            if(con.Owner_Street_Address__c != null){
                rp.owner_street_address = con.Owner_Street_Address__c;
            }
            if(con.Owner_City__c != null){
                rp.owner_city = con.Owner_City__c;
            }  
            if(con.Owner_Pincode__c != null) {
                rp.owner_pincode = Integer.valueOf(con.Owner_Pincode__c);
            }
            if(con.Owner_Country__c != null){
                rp.owner_country  = con.Owner_Country__c;
            }
            if(attach.id != null){
               rp.document_id =  attach.id;
            }
            string strEndPoint= nestURL[0].EStamping_API__c	+'&auth='+nestURL[0].Webapp_Auth__c;
            system.debug('strEndPoint'+strEndPoint);
            HttpResponse res;
            string jsonBody; 
            jsonBody=JSON.serialize(rp);
            system.debug('jsonBody'+jsonBody);
            res=RestUtilities.httpRequest('POST',strEndPoint,jsonBody,null);
            respBody= res.getBody();
            system.debug('***RespBody'+respBody);               
            ContractDetailsJsonRespWrapeerClass dataJson= new ContractDetailsJsonRespWrapeerClass();
            dataJson = (ContractDetailsJsonRespWrapeerClass)JSON.deserialize(respBody,ContractDetailsJsonRespWrapeerClass.Class);              
            if(dataJson!=null){
                if(res.getStatusCode()==200){
                    Con.API_Success__c = dataJson.success;
                    Con.API_Info__c   = dataJson.info;      
                    Con.Last_API_Sync_Time__c = System.now();  
                    if(dataJson.data != null){
                        if(dataJson.data.document_estamped != null){
                        Con.Document_Estamped__c = dataJson.data.document_estamped;
                            }
                    if(dataJson.data.link_to_sign != null){
                            Con.Link_To_Sign__c = dataJson.data.link_to_sign;
                                }
                    if(dataJson.data.signed_by_both_parties != null){
                         Con.Signed_By_Both_Parties__c = dataJson.data.signed_by_both_parties;
                    }    
                    if(!dataJson.data.signed_by_tenant != null)
                            Con.Signed_By_Tenant__c = dataJson.data.signed_by_tenant;
                    }  
                    else
                    {
                        Con.API_Success__c = dataJson.success;
                        Con.API_Info__c   = dataJson.info;      
                        Con.Last_API_Sync_Time__c = System.now(); 
                        
                    }
                }
            }
            if(Con.id!=null){         
                Update Con;         
            }
        }
        catch(exception e){
            Contract Con1 = new Contract(id = ConID);
            Con1.API_Success__c = 'False';
            Con1.API_Info__c   = String.valueOf(e);     
            Con1.Last_API_Sync_Time__c = System.now(); 
            update Con1;
            //  UtilityClass.insertErrorLog('Webservices','Insurance Api failed :'+insId+' error:'+e.getMessage()+' at line:'+e.getLineNumber()+' **RespBody'+respBody); 
        }       
        
    }
    
}