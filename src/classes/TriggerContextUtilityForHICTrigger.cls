public class TriggerContextUtilityForHICTrigger {
    public static Boolean afterInsertFlag = true;
    public static Boolean afterUpdateFlag = true;
    public static Boolean beforeInsertFlag = true;
    public static Boolean beforeUpdateFlag = true;
    

    public static boolean isAfterInsertFirstRun() {
        return afterInsertFlag;
    }
    
    public static void setAfterInsertFirstRunFalse(){
        afterInsertFlag = false;
    }
    
    
    public static boolean isAfterUpdateFirstRun() {
        return afterUpdateFlag;
    }
    
    public static void setAfterUpdateFirstRunFalse(){
        afterUpdateFlag = false;
    }
    
    
    public static boolean isBeforeUpdateFirstRun() {
        return beforeUpdateFlag;
    }
    
    public static void setBeforeUpdateFirstRunFalse(){
        beforeUpdateFlag = false;
    }
    
    
    public static boolean isBeforeInsertFirstRun() {
        return beforeInsertFlag;
    }
    
    public static void setBeforeInsertFirstRunFalse(){
        beforeInsertFlag = false;
    }
}