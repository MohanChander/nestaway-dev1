@isTest
Public Class CaseTriggerHelperTest {
    
    Public Static TestMethod Void doTest(){
                Id MoveIn_RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEIN).getRecordTypeId();

         User newUser = Test_library.createStandardUser(1);
        insert newuser;
         Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
         House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        insert hos;
        Test.startTest();
          Tenancy__c b = new Tenancy__c();
        b.Tenant__c=accObj.Id;
         b.house__c=hos.id;
        insert b;
        Case c=  new Case();
        c.Status='New';
        c.House__c=hos.Id;
        c.ownerId=newuser.Id;
        c.Tenancy__c=b.id;
        insert c;
        Set<id> setofCase = new Set<Id>();
        setOfcase.add(c.id);
        List<Case>  cList =  new List<Case>();
        cList.add(c);
        Map<id,Case> mapOfCase =  new Map<Id,Case>();
        for(case cc:cList){
            mapOfCase.Put(cc.id,cc);
        }
        CaseTriggerHelper.createMakeHouseLiveTask(mapOfCase,mapOfCase);
        CaseTriggerHelper.updateHouseStatusToHouseLive(mapOfCase);
        CaseTriggerHelper.UpdateRelatedWhenSD_Default(setOfcase,mapOfCase);
        CaseTriggerHelper.UpdateRelatedWhenSDisPaid(setOfcase,mapOfCase);
        CaseTriggerHelper.UpdateRelatedWhenprofileUpdated(setOfcase,mapOfCase);
        CaseTriggerHelper.updateMoveInCasesWhenDocsStatusChange(setOfcase,MoveIn_RecordTypeId);
        CaseTriggerHelper.UpdateRelatedWhenDocumentsAreComplete(setOfcase,mapOfCase);
           CaseTriggerHelper.UpdateRelatedWhenDocumentsVerified(setOfcase,mapOfCase);
        CaseTriggerHelper.ownerApprovedBooking(setOfcase,mapOfCase);
        CaseTriggerHelper.cancelBooking(setOfcase,mapOfCase);
        CaseTriggerHelper.onboardingWorkflowValidation(mapOfCase,mapOfCase);
        CaseTriggerHelper.dropOnboardingWorkflowTasks(Clist);
         Test.stopTest();
    }
}