/**********************************************
    Created By : Mohan
    Purpose    : Helper Class for Case - SnM module
**********************************************/

public class CaseSnMTriggerHelper {

/**********************************************
    Created By : Mohan
    Purpose    : When Owner Inclusion is true add a Case Comment
**********************************************/
  public static void ownerInclusionCaseComment(List<Case> caseList){

            System.debug('****************ownerInclusionCaseComment');

        try{   
              List<Case_Comment__c> ccList = new List<Case_Comment__c>();
              Set<Id> caseIdSet = new Set<Id>();

              for(Case c: caseList){
                caseIdSet.add(c.Id);
              }     

              List<Case> queriedCaseList = [select Id, Problem__r.SLA_1__c, Problem__r.SLA_2__c, Problem__r.SLA_3__c, 
                                            Problem__r.SLA_4__c, Escalation_Level__c, CreatedDate from Case where 
                                            Id =: caseIdSet];                        
              for(Case c: queriedCaseList){

                Integer expectedResolutionDays = 0;
                Integer slaTime = 0;

                if(c.Escalation_Level__c == null){
                    slaTime = c.Problem__r.SLA_1__c != null ? (Integer)c.Problem__r.SLA_1__c * 1440 : (Integer.valueOf(Label.Default_Level_1_SLA)/1440);
                } else if(c.Escalation_Level__c == 'Level 1'){
                    slaTime = c.Problem__r.SLA_2__c != null ? (Integer)c.Problem__r.SLA_2__c * 1440 : (Integer.valueOf(Label.Default_Level_2_SLA)/1440);
                } else if(c.Escalation_Level__c == 'Level 2'){
                    slaTime = c.Problem__r.SLA_3__c != null ? (Integer)c.Problem__r.SLA_3__c * 1440 : (Integer.valueOf(Label.Default_Level_3_SLA)/1440);
                } else {
                    slaTime = c.Problem__r.SLA_4__c != null ? (Integer)c.Problem__r.SLA_4__c * 1440 : (Integer.valueOf(Label.Default_Level_4_SLA)/1440);
                }  

                expectedResolutionDays = slaTime + 2;

                Case_Comment__c cc = new Case_Comment__c();
                String expectedResolutionTime = String.valueOf(c.CreatedDate.addDays(expectedResolutionDays).format('dd/MM/yy h:mm a'));
                cc = CaseCommentGenerator.generateOwnerInclusionUpdatedSLAComment(expectedResolutionTime, c.Id);
                cc.System_Generated_Comment__c = true;
                ccList.add(cc);
              }

              if(!ccList.isEmpty()){
                insert ccList;
              }
        } catch (Exception e){
          System.debug('**Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + 
                       '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + 
                       '\nStack Trace ' + e.getStackTraceString());
          UtilityClass.insertGenericErrorLog(e, 'Owner Inclusion Case Comment generation');  
        }    
  }    

/**********************************************
    Created By : Mohan
    Purpose    : Owner Approval Status change Case Comment
**********************************************/
  public static void ownerApprovalStatusChangeCaseComment(List<Case> caseList){

            System.debug('****************ownerApprovalStatusChangeCaseComment');

        try{   
              List<Case_Comment__c> ccList = new List<Case_Comment__c>();
              for(Case c: caseList){
                Case_Comment__c cc = new Case_Comment__c();
                  if(c.Owner_approval_status__c == 'Approved'){
                    cc = CaseCommentGenerator.generateOwnerApprovalComment(c.Id);
                    ccList.add(cc);
                  } else if(c.Owner_approval_status__c == 'Rejected'){
                    cc = CaseCommentGenerator.generateOwnerRejectionComment(c.Id);
                    ccList.add(cc);
                  }                
              }

              if(!ccList.isEmpty()){
                insert ccList;
              }
        } catch (Exception e){
            UtilityClass.insertGenericErrorLog(e, 'Updation of Requestor Expected Resolution Time');  
        }    
  }      

/**********************************************
    Created By : Mohan
    Purpose    : Update the requestor Current escalation Time on Requestor escalation
**********************************************/
  public static void updateRequestorExpectedResolutionTime(List<Case> caseList){

            System.debug('****************updateRequestorExpectedResolutionTime');

        try{   
              Set<Id> problemIdSet = new Set<Id>();        
              for(Case c: caseList){
                problemIdSet.add(c.Problem__c);
              }

              Map<Id, Problem__c> problemMap = new Map<Id, Problem__c>(ProblemSelector.getProblemListFromSet(problemIdSet));

              for(Case c: caseList){

                //if Expected Resolution Time is null - means old case - do not update Current resolution time for Old cases
                if(c.Requestor_Expected_Resolution_Time__c == null && c.CreatedDate != null){
                  continue;
                }

                Datetime caseCreatedDate = System.now();

                if(c.CreatedDate != null){
                  caseCreatedDate = c.CreatedDate;
                }                

                //if there is Owner Inclusion on the Case add additional 2 days in the SLA
                Integer ownerInclusionTime = 0;
                Integer visitTime = 0;
                Integer slaTime = 0;
                if(c.Owner_Inclusion__c != null && c.Owner_Inclusion__c == 'true'){
                    ownerInclusionTime = 1440 *2;
                } 

                if(c.Require_visit__c && c.Service_Visit_Time__c != null && c.Service_Visit_Time__c > caseCreatedDate){
                    Long serviceVisitTime = c.Service_Visit_Time__c.getTime();
                    Long createdDateTime = caseCreatedDate.getTime();
                    visitTime = Integer.valueOf((serviceVisitTime - createdDateTime)/60000);
                }                

                if(c.Escalation_Level__c == null){    
                    if(problemMap.containsKey(c.Problem__c) && problemMap.get(c.Problem__c).SLA_1__c != null){
                      slaTime = Integer.valueOf(problemMap.get(c.Problem__c).SLA_1__c) * 1440;
                    } else {
                      slaTime = Integer.valueOf(Label.Default_Level_1_SLA);
                    }                         
                } else if(c.Escalation_Level__c == 'Level 1'){
                    if(problemMap.containsKey(c.Problem__c) && problemMap.get(c.Problem__c).SLA_2__c != null){
                      slaTime = Integer.valueOf(problemMap.get(c.Problem__c).SLA_2__c) * 1440;
                    } else {
                      slaTime = Integer.valueOf(Label.Default_Level_2_SLA);
                    }                       
                } else if(c.Escalation_Level__c == 'Level 2'){
                    if(problemMap.containsKey(c.Problem__c) && problemMap.get(c.Problem__c).SLA_3__c != null){
                      slaTime = Integer.valueOf(problemMap.get(c.Problem__c).SLA_3__c) * 1440;
                    } else {
                      slaTime = Integer.valueOf(Label.Default_Level_3_SLA);
                    }   
                } else {
                    if(problemMap.containsKey(c.Problem__c) && problemMap.get(c.Problem__c).SLA_4__c != null){
                      slaTime = Integer.valueOf(problemMap.get(c.Problem__c).SLA_4__c) * 1440;
                    } else {
                      slaTime = Integer.valueOf(Label.Default_Level_4_SLA);
                    }   
                }  

                c.Requestor_Current_Resolution_Time__c = caseCreatedDate.addMinutes(slaTime + ownerInclusionTime + visitTime);

                if(c.CreatedDate == null){
                  c.Requestor_Expected_Resolution_Time__c = c.Requestor_Current_Resolution_Time__c;
                }
              }
        } catch (Exception e){
            UtilityClass.insertGenericErrorLog(e, 'Updation of Requestor Expected Resolution Time');  
        }    
  }  
  
/**********************************************
    Created By : Mohan
    Purpose    : Make an API call to WebApp with Owner Inclusion Details
**********************************************/
  @Future(callout=true)
  public static void sendOwnerInclusionDetails(Id CaseId){

            System.debug('****************sendOwnerInclusionDetails');

        try{           
                Case c = [select Id, CaseNumber, Deadline_For_Owner_Approval__c, Owner_Share_approx__c, 
                          Owner_Inclusion_comment__c from Case where Id =: caseId];                                          
                List<NestAway_End_Point__c> nestURL = NestAway_End_Point__c.getall().values();       
                String authToken=nestURL[0].Webapp_Auth__c;
                String EndPoint = nestURL[0].SnM_Notification__c +'?auth='+authToken;
                
                System.debug('***EndPoint  '+EndPoint);
                
                string jsonBody='';
                WBMIMOJSONWrapperClasses.OwnerInclusionNotificationJSON ownerInclusionObject = new WBMIMOJSONWrapperClasses.OwnerInclusionNotificationJSON();
                ownerInclusionObject.method = 'notify_for_owner_approval';
                ownerInclusionObject.ticket_number = c.CaseNumber;
                ownerInclusionObject.deadline_for_owner_approval = c.Deadline_For_Owner_Approval__c;
                ownerInclusionObject.approx_cost = c.Owner_Share_approx__c;
                ownerInclusionObject.inclusion_comment = c.Owner_Inclusion_comment__c;

                jsonBody = JSON.serialize(ownerInclusionObject);
                System.debug('***jsonBody  '+jsonBody);
                
                HttpResponse res;
                res=RestUtilities.httpRequest('POST',EndPoint,jsonBody,null);
                
                String respBody = res.getBody();
                system.debug('****respBody'+ respBody);
        } catch (Exception e){
            UtilityClass.insertGenericErrorLog(e, 'Invoice Generation API');  
        }    
  }

     /*****************************************************************************************************************************************************************************************************************************************
 Added by baibhav
 purpose: to create Hrm task For SNM 
 ********************************************************************************************************************************************************************************************************************************************/  
     public static void HrmTaskForService(List<case> caseList)
    {
      try
      {      sYSTEM.DEBUG('******Generate****');
            Id taskRecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get(Constants.TASK_RT_OWNER_FOLLOW_UP).getRecordTypeId();
            List<task> insertTask=new List<task>();
            Set<id> houIdSet = new Set<id>();
            set<id> casetenantIdSet =new Set<id>();
            for(Case c:caseList)
            {
               houIdSet.add(c.house1__c);
               casetenantIdSet.add(c.Accountid);
            } 
            Map<Id,house__c> houMap=new Map<id,house__c>();
            Map<id,Account> accMap=new Map<Id,Account>();
            if(!casetenantIdSet.isEmpty())
            { System.debug(casetenantIdSet);
              accMap=new Map<id,Account>([select id,name from Account where id=:casetenantIdSet]);
            }

            if(!houIdSet.isEmpty())
            {
              houMap=new Map<Id,house__c>([Select id,houseID__c,House_Owner__r.name,House_Owner__r.Phone,HRM__c,House_Owner__r.PersonEmail from house__c where id=:houIdSet]); 
            }
           for(Case c:caseList)
            { 
              Task ta= new task();
              ta.whatid=c.id;
              ta.recordtypeId=taskRecordTypeId;
              ta.Description=c.Owner_Inclusion_comment__c;
              ta.Case_number__c=c.CaseNumber;
              if(C.house1__c!=null)
              ta.House__c=c.house1__c;
              
              if(accMap.get(c.Accountid).name!=null)
               {
                  ta.Tenant_owner_name__c=accMap.get(c.Accountid).name;
               }
              sYSTEM.DEBUG('******Generate0****');
              if(houMap.containsKey(c.house1__c))
              {
                  if(houMap.get(c.house1__c).House_Owner__r.name!=null)
                   {
                      ta.Owner_Name__c=houMap.get(c.house1__c).House_Owner__r.name;
                   }
                  if(houMap.get(c.house1__c).House_Owner__r.Phone!=null)
                  {
                      ta.Owner_Phone__c=houMap.get(c.house1__c).House_Owner__r.Phone;
                  }
                  if(houMap.get(c.house1__c).hRM__c!=null)
                  {
                      ta.OwnerId=houMap.get(c.house1__c).hRM__c;
                  }
                   if(houMap.get(c.house1__c).House_Owner__r.PersonEmail!=null)
                  {
                      ta.Owner_Email__c=houMap.get(c.house1__c).House_Owner__r.PersonEmail;
                  }
                  sYSTEM.DEBUG('******Generate1****');
               }
               ta.status='Open';
               ta.subject='Owner follow up Task for house: '+houMap.get(c.house1__c).houseid__c;
               ta.ActivityDate=System.today().addDays(2);
      
              insertTask.add(ta);
            }
            if(!insertTask.isEmpty())
            {
            sYSTEM.DEBUG('******Generate2****');
            insert insertTask;
            }
      } catch(Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e);
        }
    }
 /*****************************************************************************************************************************************************************************************************************************************
 Added by baibhav
 purpose: to Owner task status in SNM
 ********************************************************************************************************************************************************************************************************************************************/  
  public static void updateOwnerTask(List<case> newList)
  {
        try
        {
            Set<id> casIdSet = new Set<id>();
            List<Task> tsklist=new List<task>();
            List<Task> updatetsklist=new List<Task>();
            for(case cs:newList)
            {
                casIdSet.add(cs.id);
            }

            if(!casIdSet.isEmpty())
            tsklist =TaskSelector.getOwnerTaskRelatedToseviceCase(casIdSet);
            
            for(Case cs:newList)
            {
                for(Task tsk:tsklist)
                {
                    if(tsk.whatid==cs.id)
                    {
                        if(cs.Owner_approval_status__c=='Auto Approved')
                        {
                            tsk.status='Auto Approved';
                        }
                        if(cs.Owner_approval_status__c=='Approved')
                        {
                            tsk.status='Owner Approved';
                        }
                        if(cs.Owner_approval_status__c=='Rejected')
                        {
                            tsk.status='Rejected';
                        }
                        updatetsklist.add(tsk);
                    }
                }
         
            }
           if(!updatetsklist.isEmpty())
           {
              update updatetsklist;
           }
        } Catch(Exception e)
          {
             System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e); 
          }
  } 


/**********************************************************************************************************************************************
    Added by baibahv
    Purpose for creating SNM settlement case
            1) Perform GST and Tax Calculations for Vendor Settlement Case
**********************************************************************************************************************************************/
    public static void CreateSettlementCase(set<id> pcasIDSet)
    {  System.debug('Bony2');
        try
           {   Id verfictionSettlement =Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_VENDOR_SETTLEMENT).getRecordTypeId();  
                         List<Case> newCase =new List<Case>();
                         list<workorder> wrkList=new list<workorder>(); 
                         Map<id,Case> parCaseMap=new Map<id,Case>(); 
                         Map<id,user> usrMAp=new Map<id,user>();
                         Set<id> accid =new Set<Id>();
                         Set<id> contid =new Set<Id>();

                         Map<Id, Account> accountMap;
                         Map<Id, Contact> vendorContactMap = new Map<Id, Contact>();

                         if(!pcasIDSet.isEmpty())
                         {
                            parCaseMap = CaseSelector.getMapCasefromCaseID(pcasIDSet);
                             wrkList=WorkOrderSelector.getvenderWorkOrders(pcasIDSet);
                         }
                          Map<Id,workorder> wrkMapCasId=new Map<Id,workorder>();
                            Set<Id> ownerSet =new Set<id>();
                            for(Workorder wrk:wrkList)
                            {
                                ownerSet.add(wrk.ownerId);
                                wrkMapCasId.put(wrk.caseId,wrk);
                            }
                            if(!ownerSet.isEmpty()) 
                            {
                                usrMAp =UserSelector.getUserDetailsMap(ownerSet);
                            }
                            
                            for(User us:usrMAp.values())
                            {
                              accid.add(us.Accountid);
                              if(us.Vendor_Type__c=='Inhouse'){
                                  contid.add(us.Contactid);}
                            }

                            accountMap = new Map<Id, Account>([select Id, Name, Tax_Section__r.TDS_Rates__c, TIN_No__c,
                                                               City__r.Name, BillingState, Tax_Section__r.Name, Tax_Section__r.Type__c,
                                                               GST_Registered__c, GST_NO__c from 
                                                               Account where Id =: accid]);
                            List<Contact> vendorContactList = [select Id, Name, Phone, AccountId from Contact where AccountId =: accid and
                                                               Type_of_Contact__c = 'Vendor'];
                                                                                         

                            for(Contact con: vendorContactList){
                              vendorContactMap.put(con.AccountId, con);
                            }                                                 
                             
                            List<Bank_Detail__c> bnkList=new List<Bank_Detail__c>();
                            if(!accid.isEmpty()){  
                                bnkList =BankDetailSelector.getBankDetailByAccount(accid);
                              }
                              
                            List<Bank_Detail__c> contbnkList = new List<Bank_Detail__c>();
                            if(!contid.isEmpty()){
                              contbnkList =BankDetailSelector.getBankDetailByContact(contid);
                            }

                           System.debug('Bony2'+bnkList);
                           Map<id,Bank_Detail__c> bnkMapToAccID =new Map<id,Bank_Detail__c>();
                           Map<id,Bank_Detail__c> bnkMapToConID =new Map<id,Bank_Detail__c>();

                          for(Bank_Detail__c bk:bnkList)
                             {
                               bnkMapToAccID.put(bk.Related_Account__c,bk);
                             }
                             for(Bank_Detail__c bk:contbnkList)
                             {
                               bnkMapToConID.put(bk.Contact__c,bk);
                             }
                         System.debug('Bony3');
                         for(Case cas:parCaseMap.values())
                         {
                           if(wrkMapCasId.containsKey(cas.id) && usrMAp.containsKey(wrkMapCasId.get(cas.id).ownerid))  
                           {  System.debug('Bony3-1');

                              Id vendorId = usrMAp.get(wrkMapCasId.get(cas.id).ownerid).AccountId;

                              Case ca=new Case();
                              ca.Parentid=cas.id;
                              ca.RecordTypeId=verfictionSettlement;
                              ca.subject='Settlement Case';
                              ca.AccountId = vendorId;
                              if(vendorContactMap.containsKey(vendorId)){                              
                              ca.ContactId = vendorContactMap.get(vendorId).Id;
                              }
                              if(accountMap.containsKey(vendorId)){
                              ca.GST_Registered__c = accountMap.get(vendorId).GST_Registered__c;
                              ca.GST_NO__c = accountMap.get(vendorId).GST_NO__c;
                              ca.TDS_Rates__c = accountMap.get(vendorId).Tax_Section__r.TDS_Rates__c;
                              ca.TDS_section__c = accountMap.get(vendorId).Tax_Section__r.Name;
                              ca.TDS_type__c = accountMap.get(vendorId).Tax_Section__r.Type__c; 
                              }                             
                              ca.House__c = cas.House1__c;
                              ca.Recoverable__c = cas.Recoverable__c;
                              ca.Material_Cost__c=cas.Material_Cost__c;
                              ca.Labour_Cost__c=cas.Labour_Cost__c;
                                System.debug('Bony3-1-2');

                              // Service Visit Cost is not applicable for In House Vendor
                              if(usrMAp.get(wrkMapCasId.get(cas.id).ownerid).Vendor_Type__c=='Inhouse'){
                                ca.Service_visit_cost__c = 0;
                                if(bnkMapToConID.containsKey(usrMAp.get(wrkMapCasId.get(cas.id).ownerid).contactid)){

                                if(bnkMapToConID.get(usrMAp.get(wrkMapCasId.get(cas.id).ownerid).Contactid).name!=null)
                                ca.A_c_Holder_Name_Tenant__c=bnkMapToConID.get(usrMAp.get(wrkMapCasId.get(cas.id).ownerid).Contactid).name;

                                if(bnkMapToConID.get(usrMAp.get(wrkMapCasId.get(cas.id).ownerid).Contactid).Account_Number__c!=null)
                                ca.Tenant_bank_A_c_No__c=bnkMapToConID.get(usrMAp.get(wrkMapCasId.get(cas.id).ownerid).Contactid).Account_Number__c;

                                if(bnkMapToConID.get(usrMAp.get(wrkMapCasId.get(cas.id).ownerid).Contactid).IFSC_Code__c!=null)
                                ca.Bank_IFSC_code__c=bnkMapToConID.get(usrMAp.get(wrkMapCasId.get(cas.id).ownerid).Contactid).IFSC_Code__c;

                                if(bnkMapToConID.get(usrMAp.get(wrkMapCasId.get(cas.id).ownerid).Contactid).Bank_Name__c!=null)
                                ca.Tenant_Bank_Name__c=bnkMapToConID.get(usrMAp.get(wrkMapCasId.get(cas.id).ownerid).Contactid).Bank_Name__c;

                                if(bnkMapToConID.get(usrMAp.get(wrkMapCasId.get(cas.id).ownerid).Contactid).Branch_Name__c!=null)
                                ca.Tenant_Bank_Branch_Name__c=bnkMapToConID.get(usrMAp.get(wrkMapCasId.get(cas.id).ownerid).Contactid).Branch_Name__c;

                                }
                              }
                              else{
                                ca.Service_visit_cost__c = cas.Service_visit_cost__c ;
                              }

                              ca.Base_Amount__c = ((ca.Service_visit_cost__c + cas.Labour_Cost__c) * (100.00/118));

                              /* Check if the Vendor is providing Services in the same City 
                                 if Yes - Apply CGST and SGST
                                 else Apply IGST */
                              if(accountMap.containsKey(vendorId) && accountMap.get(vendorId).BillingState == cas.State__c){
                                ca.CGST_Service_Cost__c = ca.Base_Amount__c * (0.09);
                                ca.SGST_Service_Cost__c = ca.Base_Amount__c * (0.09);
                              } else {
                                ca.IGST_Service_Cost__c = ca.Base_Amount__c * (0.18);
                              }

                              //if the Cost is not recoverable - calculate TDS else TDS is not applicable
                              if(cas.Recoverable__c == 'No'){
                                if(accountMap.get(vendorId).Tax_Section__c!=null && accountMap.get(vendorId).Tax_Section__r.TDS_Rates__c!=null){
                                    ca.TDS_amount_deducted__c = (accountMap.get(vendorId).Tax_Section__r.TDS_Rates__c/100) * ca.Base_Amount__c;
                                  }
                              } else {
                                ca.TDS_amount_deducted__c = 0;
                              }
                               System.debug('Bony3-1-1-1-1');
                              if(cas.Labour_Cost__c==null){
                                cas.Labour_Cost__c=0;
                               }
                              if(cas.Service_visit_cost__c==null){
                                cas.Service_visit_cost__c=0;
                               }
                              if(cas.Material_Cost__c==null){
                                cas.Material_Cost__c=0;
                               }
                               if(cas.TDS_amount_deducted__c==null){
                                cas.TDS_amount_deducted__c=0;
                               }
                              ca.Net_Amount_to_Be_Refunded__c = cas.Labour_Cost__c + ca.Service_visit_cost__c + cas.Material_Cost__c - ca.TDS_amount_deducted__c;

                              System.debug('Bony3-1'+bnkMapToAccID.get(usrMAp.get(wrkMapCasId.get(cas.id).ownerid).AccountId));
                              if(bnkMapToAccID.containsKey(usrMAp.get(wrkMapCasId.get(cas.id).ownerid).AccountId) && usrMAp.get(wrkMapCasId.get(cas.id).ownerid).Vendor_Type__c!='Inhouse')
                              { 
                                if(bnkMapToAccID.get(usrMAp.get(wrkMapCasId.get(cas.id).ownerid).AccountId).name!=null)
                                ca.A_c_Holder_Name_Tenant__c=bnkMapToAccID.get(usrMAp.get(wrkMapCasId.get(cas.id).ownerid).AccountId).name;

                                if(bnkMapToAccID.get(usrMAp.get(wrkMapCasId.get(cas.id).ownerid).AccountId).Account_Number__c!=null)
                                ca.Tenant_bank_A_c_No__c=bnkMapToAccID.get(usrMAp.get(wrkMapCasId.get(cas.id).ownerid).AccountId).Account_Number__c;
                                
                                if(bnkMapToAccID.get(usrMAp.get(wrkMapCasId.get(cas.id).ownerid).AccountId).IFSC_Code__c!=null)
                                ca.Bank_IFSC_code__c=bnkMapToAccID.get(usrMAp.get(wrkMapCasId.get(cas.id).ownerid).AccountId).IFSC_Code__c;

                                if(bnkMapToAccID.get(usrMAp.get(wrkMapCasId.get(cas.id).ownerid).AccountId).Bank_Name__c!=null)
                                ca.Tenant_Bank_Name__c=bnkMapToAccID.get(usrMAp.get(wrkMapCasId.get(cas.id).ownerid).AccountId).Bank_Name__c;

                                if(bnkMapToAccID.get(usrMAp.get(wrkMapCasId.get(cas.id).ownerid).AccountId).Branch_Name__c!=null)
                                ca.Tenant_Bank_Branch_Name__c=bnkMapToAccID.get(usrMAp.get(wrkMapCasId.get(cas.id).ownerid).AccountId).Branch_Name__c;
                              }
                              newCase.add(ca);
                            }
                         } 
                         if(!newCase.isEmpty())
                         {System.debug('Bony4');
                           insert newCase;
                         }

           }catch (Exception e)
           {
              System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + 
                            e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + 
                            '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
              UtilityClass.insertGenericErrorLog(e, 'Vendor Settlement Case Creation');
           }
    }

/**********************************************
    Created By : Mohan
    Purpose    : Drop the Owner Follow up task when the Case is Closed by Tenant
                 Drop the Vendor WorkOrder when the Tenant Closes the Case
**********************************************/
  public static void dropTaskAndWorkOrderOnCaseClosure(List<Case> caseList){

            System.debug('****************ownerApprovalStatusChangeCaseComment');

        try{   
              Set<Id> caseIdSet = new Set<Id>();

              for(Case c: caseList){
                caseIdSet.add(c.Id);
              }

              List<WorkOrder> woList = [select Id, Status from WorkOrder where CaseId =: caseIdSet
                                        and RecordType.Name =: Constants.WORKORDER_RT_VENDER and 
                                        Status =: Constants.WORK_STATUS_OPEN];

              List<Task> taskList = [select Id, Status from Task where WhatId =: caseIdSet
                                     and RecordType.Name =: Constants.TASK_RT_OWNER_FOLLOW_UP
                                     and Status =: Constants.TASK_STATUS_OPEN]; 
                                     
              for(WorkOrder wo: woList){
                wo.Status = 'Dropped';
              }     

              for(Task t: taskList){
                t.Status = 'Dropped';
              }  

              if(!woList.isEmpty()){
                update woList;
              }  

              if(!taskList.isEmpty()){
                update taskList;
              }                                                                                 
        } catch (Exception e){
            UtilityClass.insertGenericErrorLog(e, 'Dropping Work Order and Owner Follow up Task on Case Closure');  
        }    
  }    

/**********************************************
    Created By : Mohan
    Purpose    : Validate Case Resolution : User cannot resolve the case without 
                 resolving all the Vendor Work Orders
**********************************************/
  public static void caseResolutionValidator(List<Case> caseList){

        System.debug('****************caseResolutionValidator');

        try{  
              Set<Id> caseIdSet = new Set<Id>();
              Map<Id, WorkOrder> caseWithOpenWoMap = new Map<Id, WorkOrder>();

              for(Case c: caseList){
                caseIdSet.add(c.Id);
              }       

              List<WorkOrder> woList = [select Id, CaseId from WorkOrder where CaseId =: caseIdSet
                                        and isClosed = false and RecordType.Name =: Constants.WORKORDER_RT_VENDER];

              for(WorkOrder wo: woList){
                caseWithOpenWoMap.put(wo.CaseId, wo);
              }   

              for(Case c: caseList){
                if(caseWithOpenWoMap.containsKey(c.Id)){
                  c.addError('Please resolve the Open Vendor Work Orders before resolving the Case');
                }
              }                                     

        } catch (Exception e){
            UtilityClass.insertGenericErrorLog(e, 'Case Resolution Validation - Service Request Case');  
        }    
  }          

}