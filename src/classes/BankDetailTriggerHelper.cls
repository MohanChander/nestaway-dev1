public class BankDetailTriggerHelper 
{
    //Added by baibhav
    // Purpose: to populate Bank name
/*    public static void bankName(List<Bank_Detail__c> newlist)
    {
        for(Bank_Detail__c bk:newlist)
        {   
            if(bk.Account_Firstname__c!=null)
            {
             bk.name=bk.Account_Firstname__c;
            }
                      
            if(bk.Bank_Name__c!=null)
            {
             bk.name+='-'+bk.Bank_Name__c;
            }
            if(bk.Account_Number__c!=null)
            {
               bk.name+='-'+bk.Account_Number__c.left(4);
            }
        } 
       
     
    }*/

  /*****************************************************************************************************************************************************************************************************************************************
 Added by baibhav
 purpose: bank detail  update on Sd payment 
 ********************************************************************************************************************************************************************************************************************************************/  

    public static void updateSDPaymentBank(List<Bank_Detail__c> bnkList)
       {
        try
        {
            System.debug('bony-sd');
            set<id> bnkIdSet=new set<id>();
            list<workOrder> wrkList=new list<workOrder>();
             
            List<Workorder> updateWrklIst=new List<Workorder>();
            for(Bank_Detail__c bnk:bnkList)
            {
                bnkIdSet.add(bnk.id);
            }
            System.debug('bony-sd-bnk'+bnkIdSet);
            if(!bnkIdSet.isEmpty())
            {
              wrkList=WorkOrderSelector.getSdPaymentWorkOrdersBybnkid(bnkIdSet);
            }
            System.debug('bony-sd-wrk'+wrkList);
           
          
              for(Bank_Detail__c bnk:bnkList)
              {
                if(!wrkList.isEmpty()){
                    for(Workorder wk:wrkList){

                       if(wk.Bank_Detail__c==bnk.id){
                            if(bnk.Account_Number__c!=null)
                            {
                                wk.Account_Number__c=bnk.Account_Number__c;
                            } 
                            if(bnk.Bank_Name__c!=null)
                             {
                                wk.Bank_Name__c=bnk.Bank_Name__c;
                             }
                            if(bnk.IFSC_Code__c!=null)
                             {
                                wk.IFSC_Code__c=bnk.IFSC_Code__c;
                             } 
                            if(bnk.Branch_Name__c!=null)
                             {
                                wk.Branch_Name__c=bnk.Branch_Name__c;
                             } 
                            updateWrklIst.add(wk);
                        }
                    }
                }
                
              }  
            
            if(!updateWrklIst.isEmpty())
            {
                update updateWrklIst;
            }
        }catch(Exception e) {
            System.debug('******Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + 
                         '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + 
                          e.getCause() + '\nStack Trace ' + e.getStackTraceString());  
            UtilityClass.insertGenericErrorLog(e, 'SD payment'); 
        }
    }

/***********************************************
 Added by baibhav
 purpose: bank detail  API for IFSC validation 
 ***********************************************/
    @future(CallOut=True)
    public static void IfscApiVlidation(id bnkid){

     Bank_Detail__c bnk = [select id,Bank_Name__c,Branch_Name__c,IFSC_Api_Info__c,IFSC_Api_Sucess__c,
                           IFSC_Code__c,Is_IFSC_Api_sucess__c from Bank_Detail__c where id=:bnkid ];

      try{
             id bankIfsc=Schema.SobjectType.Bank_Detail__c.getRecordTypeInfosByName().get('Bank detail IFSC').getRecordTypeId();
             id bank=Schema.SobjectType.Bank_Detail__c.getRecordTypeInfosByName().get('bank Details').getRecordTypeId();

            //set the flag to true so that synkBank future is not fired
            BankDetailTriggerHandler.is_IFSC_API_update = true;

            HttpRequest request = new HttpRequest();
             List<NestAway_End_Point__c > nestURL = NestAway_End_Point__c.getall().values();       
             String EndPoint = nestURL[0].IFSC_API__c +'/'+bnk.IFSC_Code__c;
             System.debug('****Endpoint'+EndPoint);

             request.setEndpoint(EndPoint);
             request.setMethod('GET');
             request.setHeader('Content-Type', 'application/json;charset=UTF-8');
             Http http = new Http();
             HttpResponse response = http.send(request);
             
             System.debug('**respBody: ' + response.getBody());
    //         HttpResponse response =RestUtilities.httpRequest('POST',EndPoint,null,null);
             if(response.getStatusCode() == 200){
               System.debug('***body'+response.getBody());
               bnk.Is_IFSC_Api_sucess__c=true;
               bnk.IFSC_Api_Info__c=response.getBody();
               bnk.IFSC_Api_Sucess__c=String.valueOf(response.getStatusCode()+'-True');
               WBMIMOJSONWrapperClasses.BankDetailforIFSC bnkifsc=new WBMIMOJSONWrapperClasses.BankDetailforIFSC(); 
               bnkifsc=(WBMIMOJSONWrapperClasses.BankDetailforIFSC)JSON.deserialize(response.getBody(),  WBMIMOJSONWrapperClasses.BankDetailforIFSC.class);
               bnk.Bank_Name__c=bnkifsc.bank;
               bnk.Branch_Name__c=bnkifsc.branch;
               bnk.Last_API_Sync_Time__c=system.now();
             }
             else if(response.getStatusCode() == 404){
               bnk.Is_IFSC_Api_sucess__c=true;
               bnk.IFSC_Api_Info__c='IFSC code is not valid. '+response.getBody();
               bnk.IFSC_Api_Sucess__c=String.valueOf(response.getStatusCode()+'-false');
             }
             else if(response.getStatusCode() != 200 && response.getStatusCode() != 404){
                bnk.Is_IFSC_Api_sucess__c=false;
                bnk.recordTypeId=bank;
                bnk.IFSC_Api_Info__c=response.getBody();
                bnk.IFSC_Api_Sucess__c=String.valueOf(response.getStatusCode()+'-false');
                System.debug('******Code1'+String.valueOf(response.getStatusCode()));
             }
             System.debug('******Code1'+String.valueOf(response.getStatusCode()));
             update bnk;
             if(response.getStatusCode() == 200){
               SendAPIRequests.bankSync(bnk.id);
             }
          }catch (Exception e){
             bnk.Is_IFSC_Api_sucess__c=false;  
             update bnk;
             System.debug('******Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());  
                UtilityClass.insertGenericErrorLog(e, 'IFSC API'); 
          }


    }

}