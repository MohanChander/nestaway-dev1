/*  Created By deeepak 
*  Purpose : To help functionality*/
public class SdocHelper {
     // Added By Deepak
    // To returen the ID Of Sdoc template - Sdoc_Template_Name
    public static String getSdocTemplateId(){
        String SDOCNestawayHicId;
        system.debug('**Label.Sdoc_Template_Name'+Label.Sdoc_Template_Name);
        SDOC__SDTemplate__c  SDOCNestawayHic =[Select id from SDOC__SDTemplate__c  where name= : Label.Sdoc_Template_Name ];
        if(SDOCNestawayHic.id != null){
            SDOCNestawayHicId = SDOCNestawayHic.id;
        }
        return SDOCNestawayHicId;
    }
}