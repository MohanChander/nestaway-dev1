/**********************************************************************************
Added by Baibhav
Purpose: For Onboading case
**********************************************************************************/
public class CaseOnboardingHelper {
/**********************************************************************************
Added by Baibhav
Purpose: For Creation of Onboarding Workorder
         1) Invoke on Creation of Onboading case for Insert of Onboading Workorder
         2) Invoke on Updation of Onboading case for Insert of Onboading Workorder
**********************************************************************************/
    public static void OnboadringWorkorder(List<Case> caselist, Id wrkRecordTypeId,String sub) {
        
        try{
            List<workorder> instWrkList=new List<workorder>();
             List<workorder> woList=new List<workorder>();
            for(Case ca:caselist){
                Workorder wrk=new Workorder();
                wrk.RecordTypeId=wrkRecordTypeId;
                wrk.CaseId=ca.id;
                wrk.Case_Status__c=ca.Status;
                //wrk.StartDate=System.now();
                wrk.Subject=sub+'-'+ca.casenumber;
                wrk.Status=Constants.WORK_STATUS_OPEN;
                wrk.House__c=ca.House__c;
                if(sub=='Make House Live' || sub=='Photography'){
                    wrk.ownerid=ca.ownerid;
                }
                else{
                   
                    woList.add(wrk); 
                }
                instWrkList.add(wrk);

            } 
            if(!instWrkList.isEmpty()){
                if(!woList.isEmpty()){
                         WorkOrderAPMAssignment.assignmentOfOwners(woList);
                     }
                insert instWrkList;
            }
            } Catch(Exception e){
                  System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                  UtilityClass.insertGenericErrorLog(e,'OnBoarding Workorder');
            }

        
    }
/**********************************************************************************
Added by Baibhav
Purpose: For Dropping of Onboarding Workorder
         1) Invoke on Closeing of Onboading case for Insert of Onboading Workorder
       
**********************************************************************************/
    public static void OnboadringWorkorderDrop(Map<id,Case> caseMap) {
    
    try{   
         List<Workorder> wrkList=WorkOrderSelector.getOnboardinfWorkOrdersBycaseid(caseMap.keySet());
         if(!wrkList.isEmpty()){
                for(workorder wrk:wrkList){
                    wrk.status=Constants.WORKORDER_STATUS_DROPPED;
                }
                update wrkList;
         }
     } Catch(Exception e){
                  System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                  UtilityClass.insertGenericErrorLog(e,'OnBoarding Workorder drop');
            }
    }

/**********************************************************************************
Added by Baibhav
Purpose: For Creatring Sub case depending field value of hic ,ric & bic         
       
**********************************************************************************/
    @future
    Public static void ToCreateServiceCase(Set<id> caseSet){
        try{
                List<House_Inspection_Checklist__c> houseICList = new List<House_Inspection_Checklist__c>();
                 Set<Id> setOfHic = new Set<Id>();
                 Set<Id> setOfhou = new Set<Id>();
                 Map<id,id> mapHicTocase=new Map<id,Id>();
                 Map<id,id> mapHicToOwner=new Map<id,Id>();
                 Map<id,id> mapHicToAPM=new Map<id,Id>();
                 List<Sobject> soblist=new List<Sobject>();

                 Set<String> objSet=new Set<String>{'House_Inspection_Checklist__c','Room_Inspection__c','Bathroom__c'};

                 List<Case> caseList=[select id,Checklist__c,Accountid,House__r.House_Owner__c,House__r.APM__c from case where id=:caseSet];

                 for(Case ca:caseList){
                    if(ca.Checklist__c!=null){
                            setOfHic.add(ca.Checklist__c);
                            mapHicTocase.put(ca.Checklist__c,ca.id);
                            mapHicToOwner.put(ca.Checklist__c,ca.House__r.House_Owner__c);
                            mapHicToAPM.put(ca.Checklist__c,ca.House__r.APM__c);

                       }
                  }
        
                     if(!setOfHic.isEmpty()){
                      // Dynamic Query
                       List<Operational_Process__c> allOp =[select id,Field_API_Name__c,Type_of_Object__c from Operational_Process__c where Type_of_Object__c =:objSet];
        
                        Map<string,Set<string>>  fieldNamemap = new Map<string,Set<string>>();
        
                        for(Operational_Process__c op:allOp){
                            Set<string> str=new Set<string>();
                            if(fieldNamemap.containsKey(op.Type_of_Object__c)){
                               str= fieldNamemap.get(op.Type_of_Object__c);
                            }
                            str.add(op.Field_API_Name__c);
                            fieldNamemap.put(op.Type_of_Object__c,str);
                        }
        
                        string baseQueryquery='select id';
        
                        for(string x: fieldNamemap.get('House_Inspection_Checklist__c')){
        
                           baseQueryquery+=','+x;
                        }
        
                        if(fieldNamemap.containsKey('Room_Inspection__c')){
                         baseQueryquery+=',(';
                          string childQUery='Select Id';
                            
                           for(string x : fieldNamemap.get('Room_Inspection__c')){
                                childQUery+=','+x;
                           }
                           
                           baseQueryquery+=childQUery+',House__c from Room_Inspection__r)';
                        }
                        if(fieldNamemap.containsKey('Bathroom__c')){
                         baseQueryquery+=',(';
                          string childQUery='Select Id';
                            
                           for(string x : fieldNamemap.get('Bathroom__c')){
                                childQUery+=','+x;
                           }
                           
                           baseQueryquery+=childQUery+',House__c from Bathrooms__r)';
                           baseQueryquery+=',(';
                           baseQueryquery+=childQUery+',House__c from AttachedBathroom__r)'; 
                           
                        }
        
                        string filterQueryquery=',House__c, from House_Inspection_Checklist__c where id =:setOfHic';
        
                        baseQueryquery+=filterQueryquery;
        
                        houseICList=Database.query(baseQueryquery);
        
                    }
                    for(House_Inspection_Checklist__c hic :houseICList){
                            for(Room_Inspection__c ric : hic.Room_Inspection__R){
                                soblist.add(ric);
                            }
                            for(Bathroom__c bic : hic.Bathrooms__r){
                                soblist.add(bic);
                            }
                            for(Bathroom__c bic1 : hic.AttachedBathroom__r){
                                soblist.add(bic1);
                            }
                            soblist.add(hic);
                    }

                    if(!soblist.isEmpty()){
                       StageUtilityClass.forCreatingCaseFromObjectFields(soblist,objSet,mapHicTocase,mapHicToOwner,mapHicToAPM); 
                    }
         }
        Catch (Exception e){
          System.debug('**Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + 
                       '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + 
                       '\nStack Trace ' + e.getStackTraceString());

          UtilityClass.insertGenericErrorLog(e, 'passing checklist for case creation');  
        }  
    }

}