/***********************************************************************************
Added by baibhav
date  16-11-2016
***********************************************************************************/
@Istest
public with sharing class WorkOrderSnMTriggerHelpertest {
     public static TestMethod void  Test1()
    {
        Id venderRecordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId(); 
        Id vendorWORtId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(Constants.WORKORDER_RT_VENDER).getRecordTypeId();
        Profile pro=[select id from Profile where name='Partner'];
        Profile pro1=[select id from Profile where name='Area Operations manager'];

        Problem__c pb=new Problem__c();
        pb.Name='Electrical ➢ Fan ➢ Not Provided';
        pb.Skills__c='Electrical';
        pb.Priority__c='Medium';
        insert pb;

        Account acc=new Account();
        acc.name='vendor1';
         acc.RecordtypeId=venderRecordTypeID;
         acc.Active__c=true;
        acc.Vendor_Type__c='Inhouse';
        acc.PAN_Number__c='CAVPK1234V';
        insert acc;

          Bank_Detail__c bk=new Bank_Detail__c();
            bk.Related_Account__c=acc.id;
            bk.Account_Number__c='123456709876';
            bk.Bank_Name__c='SBI';
            bk.Branch_Name__c='Golmuri';
            bk.IFSC_Code__c='SBIN0010823';
            bk.Is_A_House_Owner__c=true;
            insert bk;

         NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
         cusSet.name = 'nestaway';
         cusSet.Nestaway_URL__c = 'www.test.com';
         insert cusSet;

      //  Userrole ur=[select id from userRole limit 1 ];

        Contact co=new Contact();
        co.firstname='v1';
        co.lastname='test';
        co.Accountid=acc.id;
        co.Type_of_Contact__c='vendor';
        insert co;

        user us=new User();
        us.Alias='Vt231';
        us.Email='V23t.@gmail.com';
        us.Username='V23t.@gmail.com';
        us.IsActive=true;
        us.Contactid=co.id;
        us.ProfileId=pro.id;
       // us.Accountid=acc.id;
        us.Vendor_Capability__c='Electrical;Plumbing;Generalist';
        us.firstname='vend';
        us.lastname='qwer';
        us.TimeZoneSidKey='Asia/Kolkata';
        us.LocaleSidKey='en_IN';
        us.EmailEncodingKey='   ISO-8859-1';
        us.Phone='9876543212';
        us.LanguageLocaleKey='en_US';
        us.IsActive =true;
        us.portalrole='Manager';
        insert us;

        user us1=new User();
        us1.Alias='Vt2331';
        us1.Email='V23t.@gmail.com';
        us1.Username='V23t213344.@gmail.com';
        us1.IsActive=true;
        us1.ProfileId=pro1.id;
        us1.firstname='vend';
        us1.lastname='qwerer';
        us1.TimeZoneSidKey='Asia/Kolkata';
        us1.LocaleSidKey='en_IN';
        us1.EmailEncodingKey='ISO-8859-1';
        us1.Phone='9876543212';
        us1.LanguageLocaleKey='en_US';
        us1.IsActive =true;
        insert us1;

        Id PersonAccRecordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Constants.Person_Acoount).getRecordTypeId();
        Account accOw=new Account();
        accOw.firstname='Owner';
        accOw.lastname='test';
        accOw.recordtypeId=PersonAccRecordTypeID;
        accOw.Owner__c=true;
        accOw.PAN_Number__c='CAVPK1234V';
        insert accOw;

         Bank_Detail__c bk1=new Bank_Detail__c();
            bk1.Related_Account__c=accOw.id;
            bk1.Account_Number__c='123456709876';
            bk1.Bank_Name__c='SBI';
            bk1.Branch_Name__c='Golmuri';
            bk1.IFSC_Code__c='SBIN0010823';
            bk1.Is_A_House_Owner__c=true;
            insert bk1;

          zone__c zc= new zone__c();
        zc.Zone_code__c ='123';
        zc.Name='HSR';
        insert zc;

        Zone_and_OM_Mapping__c zm=new Zone_and_OM_Mapping__c();
        zm.Zone__c=zc.id;
        zm.isActive__c=true;
        zm.User__c=us.id;
        insert zm;

        House__c houseObj = new House__c();
        houseObj.Name = 'TestHouse';
        houseObj.Stage__c = 'House Draft';
        houseObj.Onboarding_Zone__c=zc.id;
        houseObj.House_Owner__c=accOw.id;
        houseObj.HouseId__c='1234';
        insert houseObj;

        id serviceRT=Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_SERVICE_REQUEST).getRecordTypeId();
        Case cas=new Case();
        cas.RecordtypeID=serviceRT;
        cas.problem__c=pb.id;
        cas.house1__c=houseObj.id;
        cas.Preferred_Visit_Time__c=System.now();
        cas.Recoverable__c='Yes';
        cas.Recoverable_From__c=Constants.CASE_RECOVERABLE_TENANT_NESTAWAY;
        cas.Accountid=accOw.id;
        insert cas;

      test.startTest();

        workorder wrk=new workorder();
                wrk.StartDate=System.now();
                wrk.EndDate=System.now().addHours(2);
                wrk.Tenant__c=cas.Accountid;
                wrk.Tenant_PhoneNo__c=cas.Account.Phone;
               // wrk.Google_Map_Link__c='https://www.google.com/maps/search/?api=1&query='+cas.House1__r.House_Lattitude__c+','+cas.House1__r.House_Longitude__c;
                wrk.Caseid=cas.id;
                wrk.RecordTypeId=vendorWORtId;
                //wrk.subject=cas.subject;
                wrk.Problem__c=cas.Problem__c;
              //  wrk.Description=cas.Description;
                wrk.House__c=cas.House1__c;
                wrk.HouseID__c=cas.House1__r.HouseId__c;
                wrk.status='Open';
                wrk.Auto_Generated_OTP__c=4256;
                insert wrk;

                System.runAs(us1)
              {

                wrk.StartDate=System.now().addHours(2);
                wrk.EndDate=System.now().addHours(4);
                update wrk;

                wrk.Status=Constants.WORKORDER_STATUS_WORK_IN_PROGRESS;
                wrk.Work_Start_Time__c=System.Now().addHours(3);  
                wrk.Enter_OTP__c=4256;

                update wrk;

               
             }

                test.StopTest();
            }
   public static TestMethod void  Test2()
    {
        Id venderRecordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId(); 
        Id vendorWORtId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(Constants.WORKORDER_RT_VENDER).getRecordTypeId();
        Profile pro=[select id from Profile where name='Partner'];
        Profile pro1=[select id from Profile where name='Area Operations manager'];

        Problem__c pb=new Problem__c();
        pb.Name='Electrical ➢ Fan ➢ Not Provided';
        pb.Skills__c='Electrical';
        pb.Priority__c='Medium';
        insert pb;

        Account acc=new Account();
        acc.name='vendor1';
         acc.RecordtypeId=venderRecordTypeID;
         acc.Active__c=true;
        acc.Vendor_Type__c='Inhouse';
        acc.PAN_Number__c='CAVPK1234V';
        insert acc;

          Bank_Detail__c bk=new Bank_Detail__c();
            bk.Related_Account__c=acc.id;
            bk.Account_Number__c='123456709876';
            bk.Bank_Name__c='SBI';
            bk.Branch_Name__c='Golmuri';
            bk.IFSC_Code__c='SBIN0010823';
            bk.Is_A_House_Owner__c=true;
            insert bk;

         NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
         cusSet.name = 'nestaway';
         cusSet.Nestaway_URL__c = 'www.test.com';
         insert cusSet;

      //  Userrole ur=[select id from userRole limit 1 ];

        Contact co=new Contact();
        co.firstname='v1';
        co.lastname='test';
        co.Accountid=acc.id;
        co.Type_of_Contact__c='vendor';
        insert co;

        user us=new User();
        us.Alias='Vt231';
        us.Email='V23t.@gmail.com';
        us.Username='V23t.@gmail.com';
        us.IsActive=true;
        us.Contactid=co.id;
        us.ProfileId=pro.id;
       // us.Accountid=acc.id;
        us.Vendor_Capability__c='Electrical;Plumbing;Generalist';
        us.firstname='vend';
        us.lastname='qwer';
        us.TimeZoneSidKey='Asia/Kolkata';
        us.LocaleSidKey='en_IN';
        us.EmailEncodingKey='   ISO-8859-1';
        us.Phone='9876543212';
        us.LanguageLocaleKey='en_US';
        us.IsActive =true;
        us.portalrole='Manager';
        insert us;

        user us1=new User();
        us1.Alias='Vt2331';
        us1.Email='V23t.@gmail.com';
        us1.Username='V23t213344.@gmail.com';
        us1.IsActive=true;
        us1.ProfileId=pro1.id;
        us1.firstname='vend';
        us1.lastname='qwerer';
        us1.TimeZoneSidKey='Asia/Kolkata';
        us1.LocaleSidKey='en_IN';
        us1.EmailEncodingKey='ISO-8859-1';
        us1.Phone='9876543212';
        us1.LanguageLocaleKey='en_US';
        us1.IsActive =true;
        insert us1;

        Id PersonAccRecordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Constants.Person_Acoount).getRecordTypeId();
        Account accOw=new Account();
        accOw.firstname='Owner';
        accOw.lastname='test';
        accOw.recordtypeId=PersonAccRecordTypeID;
        accOw.Owner__c=true;
        accOw.PAN_Number__c='CAVPK1234V';
        insert accOw;

         Bank_Detail__c bk1=new Bank_Detail__c();
            bk1.Related_Account__c=accOw.id;
            bk1.Account_Number__c='123456709876';
            bk1.Bank_Name__c='SBI';
            bk1.Branch_Name__c='Golmuri';
            bk1.IFSC_Code__c='SBIN0010823';
            bk1.Is_A_House_Owner__c=true;
            insert bk1;

          zone__c zc= new zone__c();
        zc.Zone_code__c ='123';
        zc.Name='HSR';
        insert zc;

        Zone_and_OM_Mapping__c zm=new Zone_and_OM_Mapping__c();
        zm.Zone__c=zc.id;
        zm.isActive__c=true;
        zm.User__c=us.id;
        insert zm;

        House__c houseObj = new House__c();
        houseObj.Name = 'TestHouse';
        houseObj.Stage__c = 'House Draft';
        houseObj.Onboarding_Zone__c=zc.id;
        houseObj.House_Owner__c=accOw.id;
        houseObj.HouseId__c='1234';
        insert houseObj;

        id serviceRT=Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_SERVICE_REQUEST).getRecordTypeId();
        Case cas=new Case();
        cas.RecordtypeID=serviceRT;
        cas.problem__c=pb.id;
        cas.house1__c=houseObj.id;
        cas.Preferred_Visit_Time__c=System.now();
        cas.Recoverable__c='Yes';
        cas.Recoverable_From__c=Constants.CASE_RECOVERABLE_TENANT_NESTAWAY;
        cas.Accountid=accOw.id;
        insert cas;

      test.startTest();

        workorder wrk=new workorder();
                wrk.StartDate=System.now();
                wrk.EndDate=System.now().addHours(2);
                wrk.Tenant__c=cas.Accountid;
                wrk.Tenant_PhoneNo__c=cas.Account.Phone;
               // wrk.Google_Map_Link__c='https://www.google.com/maps/search/?api=1&query='+cas.House1__r.House_Lattitude__c+','+cas.House1__r.House_Longitude__c;
                wrk.Caseid=cas.id;
                wrk.RecordTypeId=vendorWORtId;
                //wrk.subject=cas.subject;
                wrk.Problem__c=cas.Problem__c;
              //  wrk.Description=cas.Description;
                wrk.House__c=cas.House1__c;
                wrk.HouseID__c=cas.House1__r.HouseId__c;
                wrk.status=Constants.WORKORDER_STATUS_WORK_IN_PROGRESS;
                wrk.Auto_Generated_OTP__c=4256;
                 wrk.StartDate=System.now().addHours(2);
                wrk.EndDate=System.now().addHours(4);
                 wrk.Work_Start_Time__c=System.Now().addHours(3);  
                wrk.Enter_OTP__c=4256;
                insert wrk;

                System.runAs(us1)
              {

                

               

                wrk.Status=Constants.WORKORDER_STATUS_WORK_DONE;
                wrk.Work_End_Time__c=System.Now().addHours(5);

                update wrk;
               
                wrk.Material_Cost__c=500;
                wrk.Material__c='test';
                Update wrk;

                Invoice__c inv=new Invoice__c();
                inv.Work_Order__c=wrk.id;
                inv.Amount__c=234567;
                inv.Vendor__c=acc.id;
                insert inv;

                Attachment atch=new Attachment();
                atch.ParentId =inv.id;
                atch.Name='Unit Test Attachment';
                Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
                atch.body=bodyBlob;
                insert atch; 

                ContentDocumentLink cdl = Test_library.createContentDocumentLink();
                cdl.LinkedEntityId = inv.Id;
                insert cdl; 

                 wrk.status= Constants.WORKORDER_STATUS_RESOLVED;
                 Update wrk;
             }

                test.StopTest();
            }
    public static TestMethod void  Test3()
    {
        Id venderRecordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId(); 
        Id vendorWORtId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(Constants.WORKORDER_RT_VENDER).getRecordTypeId();
        Profile pro=[select id from Profile where name='Partner'];
        Profile pro1=[select id from Profile where name='Area Operations manager'];

        Problem__c pb=new Problem__c();
        pb.Name='Electrical ➢ Fan ➢ Not Provided';
        pb.Skills__c='Electrical';
        pb.Priority__c='Medium';
        insert pb;

        Account acc=new Account();
        acc.name='vendor1';
         acc.RecordtypeId=venderRecordTypeID;
         acc.Active__c=true;
        acc.Vendor_Type__c='Inhouse';
        acc.PAN_Number__c='CAVPK1234V';
        insert acc;

          Bank_Detail__c bk=new Bank_Detail__c();
            bk.Related_Account__c=acc.id;
            bk.Account_Number__c='123456709876';
            bk.Bank_Name__c='SBI';
            bk.Branch_Name__c='Golmuri';
            bk.IFSC_Code__c='SBIN0010823';
            bk.Is_A_House_Owner__c=true;
            insert bk;

         NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
         cusSet.name = 'nestaway';
         cusSet.Nestaway_URL__c = 'www.test.com';
         insert cusSet;

      //  Userrole ur=[select id from userRole limit 1 ];

        Contact co=new Contact();
        co.firstname='v1';
        co.lastname='test';
        co.Accountid=acc.id;
        co.Type_of_Contact__c='vendor';
        insert co;

        user us=new User();
        us.Alias='Vt231';
        us.Email='V23t.@gmail.com';
        us.Username='V23t.@gmail.com';
        us.IsActive=true;
        us.Contactid=co.id;
        us.ProfileId=pro.id;
       // us.Accountid=acc.id;
        us.Vendor_Capability__c='Electrical;Plumbing;Generalist';
        us.firstname='vend';
        us.lastname='qwer';
        us.TimeZoneSidKey='Asia/Kolkata';
        us.LocaleSidKey='en_IN';
        us.EmailEncodingKey='   ISO-8859-1';
        us.Phone='9876543212';
        us.LanguageLocaleKey='en_US';
        us.IsActive =true;
        us.portalrole='Manager';
        insert us;

        user us1=new User();
        us1.Alias='Vt2331';
        us1.Email='V23t.@gmail.com';
        us1.Username='V23t213344.@gmail.com';
        us1.IsActive=true;
        us1.ProfileId=pro1.id;
        us1.firstname='vend';
        us1.lastname='qwerer';
        us1.TimeZoneSidKey='Asia/Kolkata';
        us1.LocaleSidKey='en_IN';
        us1.EmailEncodingKey='ISO-8859-1';
        us1.Phone='9876543212';
        us1.LanguageLocaleKey='en_US';
        us1.IsActive =true;
        insert us1;

        Id PersonAccRecordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Constants.Person_Acoount).getRecordTypeId();
        Account accOw=new Account();
        accOw.firstname='Owner';
        accOw.lastname='test';
        accOw.recordtypeId=PersonAccRecordTypeID;
        accOw.Owner__c=true;
        accOw.PAN_Number__c='CAVPK1234V';
        insert accOw;

         Bank_Detail__c bk1=new Bank_Detail__c();
            bk1.Related_Account__c=accOw.id;
            bk1.Account_Number__c='123456709876';
            bk1.Bank_Name__c='SBI';
            bk1.Branch_Name__c='Golmuri';
            bk1.IFSC_Code__c='SBIN0010823';
            bk1.Is_A_House_Owner__c=true;
            insert bk1;

          zone__c zc= new zone__c();
        zc.Zone_code__c ='123';
        zc.Name='HSR';
        insert zc;

        Zone_and_OM_Mapping__c zm=new Zone_and_OM_Mapping__c();
        zm.Zone__c=zc.id;
        zm.isActive__c=true;
        zm.User__c=us.id;
        insert zm;

        House__c houseObj = new House__c();
        houseObj.Name = 'TestHouse';
        houseObj.Stage__c = 'House Draft';
        houseObj.Onboarding_Zone__c=zc.id;
        houseObj.House_Owner__c=accOw.id;
        houseObj.HouseId__c='1234';
        insert houseObj;

        id serviceRT=Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_SERVICE_REQUEST).getRecordTypeId();
        Case cas=new Case();
        cas.RecordtypeID=serviceRT;
        cas.problem__c=pb.id;
        cas.house1__c=houseObj.id;
        cas.Preferred_Visit_Time__c=System.now();
        cas.Recoverable__c='Yes';
        cas.Recoverable_From__c=Constants.CASE_RECOVERABLE_TENANT_NESTAWAY;
        cas.Accountid=accOw.id;
        insert cas;

      test.startTest();

        workorder wrk=new workorder();
                wrk.StartDate=System.now();
                wrk.EndDate=System.now().addHours(2);
                wrk.Tenant__c=cas.Accountid;
                wrk.Tenant_PhoneNo__c=cas.Account.Phone;
               // wrk.Google_Map_Link__c='https://www.google.com/maps/search/?api=1&query='+cas.House1__r.House_Lattitude__c+','+cas.House1__r.House_Longitude__c;
                wrk.Caseid=cas.id;
                wrk.RecordTypeId=vendorWORtId;
                //wrk.subject=cas.subject;
                wrk.Problem__c=cas.Problem__c;
              //  wrk.Description=cas.Description;
                wrk.House__c=cas.House1__c;
                wrk.HouseID__c=cas.House1__r.HouseId__c;
                wrk.status=Constants.WORKORDER_STATUS_WORK_DONE;
                wrk.Auto_Generated_OTP__c=4256;
                 wrk.StartDate=System.now().addHours(2);

                wrk.EndDate=System.now().addHours(4);
                wrk.Work_End_Time__c=System.Now().addHours(5); 
                wrk.Work_Start_Time__c=System.Now().addHours(3); 
                wrk.Enter_OTP__c=4256;
                wrk.Material_Cost__c=500;
                wrk.Material__c='test';
                insert wrk;

                System.runAs(us1)
              {

               
                wrk.Material_Cost__c=700;
                wrk.Material__c='test';
                Update wrk;

                Invoice__c inv=new Invoice__c();
                inv.Work_Order__c=wrk.id;
                inv.Amount__c=234567;
                inv.Vendor__c=acc.id;
                insert inv;

                Attachment atch=new Attachment();
                atch.ParentId =inv.id;
                atch.Name='Unit Test Attachment';
                Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
                atch.body=bodyBlob;
                insert atch; 

                ContentDocumentLink cdl = Test_library.createContentDocumentLink();
                cdl.LinkedEntityId = inv.Id;
                insert cdl;                

                 wrk.status= Constants.WORKORDER_STATUS_RESOLVED;
                 Update wrk;
             }

                test.StopTest();
            }
 
}