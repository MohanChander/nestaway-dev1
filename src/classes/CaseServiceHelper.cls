/**********************************************************************************
Added by Baibhav
Purpose: For sevice case helper         
       
**********************************************************************************/
public  class CaseServiceHelper {
/**********************************************************************************
Added by Baibhav
Purpose: for updating Hic ric bir on close of service case         
       
**********************************************************************************/
	public static void ServiceCaseClosingUpdatingChecklist(List<Case> caseList){

        StopRecursion.CASE_SWITCH=false;
        StopRecursion.DisabledHICTrigger=false;
        try{
        		Set<Id> opressid=new Set<Id>();
        		for(case ca:caseList){
        			if(ca.Operational_Process__c!=null){
        				opressid.add(ca.Operational_Process__c);
        			}
        		}
                
                Map<id,Operational_Process__c> opMap=new Map<id,Operational_Process__c>();
        		if(!opressid.isEmpty()){
                  opMap=new Map<id,Operational_Process__c>([select id,Field_API_Name__c,Field_Data_Type__c,Closing_Value__c,New_Field_Value__c,Problem__c,Problem__r.id from Operational_Process__c where Id=:opressid]);
        		}
        
        		List<House_Inspection_Checklist__c> hicListUpdate=new List<House_Inspection_Checklist__c>();
        		List<Room_Inspection__c> ricListUpdate=new List<Room_Inspection__c>();
        		List<Bathroom__c> bicListUpdate=new List<Bathroom__c>();
        
        		for(case ca:caseList){
        
        			if(ca.MimoRic__c!=null && ca.MimoBic__c==null){
        
        				Room_Inspection__c ric=new Room_Inspection__c();
        				ric.id=ca.MimoRic__c;
        
        				if(opMap.get(ca.Operational_Process__c).Closing_Value__c!=null 
        					&& opMap.get(ca.Operational_Process__c).Field_Data_Type__c==Operational_ProcessConstants.FIELD_DATA_TYPE_PICKLIST){
        
        						ric.put(opMap.get(ca.Operational_Process__c).Field_API_Name__c,opMap.get(ca.Operational_Process__c).Closing_Value__c);
        					}
        					else if(opMap.get(ca.Operational_Process__c).Field_Data_Type__c==Operational_ProcessConstants.FIELD_DATA_TYPE_INTEGER){
                               ric.put(opMap.get(ca.Operational_Process__c).Field_API_Name__c,0);
        					}
        					ricListUpdate.add(ric);
        
        			}
        			else if(ca.MimoBic__c!=null && ca.MimoRic__c==null){
        
        				Bathroom__c bic=new Bathroom__c();
        				bic.id=ca.MimoBic__c;
        
        				if(opMap.get(ca.Operational_Process__c).Closing_Value__c!=null 
        					&& opMap.get(ca.Operational_Process__c).Field_Data_Type__c==Operational_ProcessConstants.FIELD_DATA_TYPE_PICKLIST){
        
        						bic.put(opMap.get(ca.Operational_Process__c).Field_API_Name__c,opMap.get(ca.Operational_Process__c).Closing_Value__c);
        					}
        					else if(opMap.get(ca.Operational_Process__c).Field_Data_Type__c==Operational_ProcessConstants.FIELD_DATA_TYPE_INTEGER){
                               bic.put(opMap.get(ca.Operational_Process__c).Field_API_Name__c,0);
        					}
        					bicListUpdate.add(bic);
        
        			}
        			else if(ca.MimoBic__c==null && ca.MimoRic__c==null && ca.Checklist__c!=null){
        
        				House_Inspection_Checklist__c hic=new House_Inspection_Checklist__c();
        				hic.id=ca.Checklist__c;
        
        				if(opMap.get(ca.Operational_Process__c).Closing_Value__c!=null 
        					&& opMap.get(ca.Operational_Process__c).Field_Data_Type__c==Operational_ProcessConstants.FIELD_DATA_TYPE_PICKLIST){
        
        						hic.put(opMap.get(ca.Operational_Process__c).Field_API_Name__c,opMap.get(ca.Operational_Process__c).Closing_Value__c);
        					}
        					else if(opMap.get(ca.Operational_Process__c).Field_Data_Type__c==Operational_ProcessConstants.FIELD_DATA_TYPE_INTEGER){
                               hic.put(opMap.get(ca.Operational_Process__c).Field_API_Name__c,0);
        					}
        					hicListUpdate.add(hic);
        
        			}
        		}
        		if(!hicListUpdate.isEmpty()){
        			update hicListUpdate;
        		}
        		if(!bicListUpdate.isEmpty()){
        			update bicListUpdate;
        		}
        		if(!ricListUpdate.isEmpty()){
        			update ricListUpdate;
        		}
        
        	}catch (Exception e){
          System.debug('**Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + 
                       '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + 
                       '\nStack Trace ' + e.getStackTraceString());

          UtilityClass.insertGenericErrorLog(e, 'checklist update on case close');  
        }  
        }
}