// creadted by deepak
// created for legal case
public class CaseLegalHelper {
    public static void beforeupdate(Map<Id,case> newMap){
        system.debug('CaseLegalHelper');
        Map<Id,String> mapOFDefendant = new Map<Id,String>();
        Map<Id,String> mapOFPetitioner = new Map<Id,String>();
        Set<Id> setOFDefendant = new Set<Id>();
        Set<Id> setOFPetitioner =new Set<Id>();
        List<Account> accListDefendant =  new  List<Account>();
        List<Account> accListPetitioner =  new  List<Account>();
        for(Case each : newMap.values()){ 
            setOFDefendant.add(each.Defendant__c);
            setOFDefendant.add(each.Petitioner_Plaintiff__c );
        }
        if(!setOFDefendant.isEmpty()){
            accListDefendant = [select Id,Name from Account where id in:setOFDefendant];
            for(Account each: accListDefendant){
                mapOFDefendant.put(each.id,each.name);
            }
        }
        
        for(Case each :  newMap.values()){
            
            if(mapOFDefendant.get(each.Defendant__c) !=null &&!(mapOFDefendant.get(each.Defendant__c).contains('Nestaway')) && mapOFDefendant.get(each.Petitioner_Plaintiff__c) !=null &&!(mapOFDefendant.get(each.Petitioner_Plaintiff__c).contains('Nestaway'))){
                newMap.get(each.id).adderror('One of the Account should be NestAway Account  when NestAway Role Is Independent Capacity');
            }
        }
    }
    public static void beforeInsert(List<Case> caseLegalList){
        Map<Id,String> mapOFDefendant = new Map<Id,String>();
        Map<Id,String> mapOFPetitioner = new Map<Id,String>();
        Set<Id> setOFDefendant = new Set<Id>();
        Set<Id> setOFPetitioner =new Set<Id>();
        List<Account> accListDefendant =  new  List<Account>();
        List<Account> accListPetitioner =  new  List<Account>();
        
        system.debug('caseList'+caseLegalList);
        for(Case each : caseLegalList){ 
            setOFDefendant.add(each.Defendant__c);
            setOFDefendant.add(each.Petitioner_Plaintiff__c );
        }
        if(!setOFDefendant.isEmpty()){
            accListDefendant = [select Id,Name from Account where id in:setOFDefendant];
            for(Account each: accListDefendant){
                mapOFDefendant.put(each.id,each.name);
            }
        }
        
        
        for(Case each :  caseLegalList){
            
            if(mapOFDefendant.get(each.Defendant__c) !=null &&!(mapOFDefendant.get(each.Defendant__c).contains('NestAway')) && mapOFDefendant.get(each.Petitioner_Plaintiff__c) !=null &&!(mapOFDefendant.get(each.Petitioner_Plaintiff__c).contains('NestAway'))){
                caseLegalList[0].adderror('One of the Account should be NestAway Account  when NestAway Role Is Independent Capacity ');
            }
        }
    }
}