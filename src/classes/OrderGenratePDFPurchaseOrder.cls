/*******************************************************************************************************************************
Description: It's controller class of 'PurcahseOrderPDF' page. 
This will display order and orderline items data in PDF format.
DevelopedBy: WarpDrive Tech Works
********************************************************************************************************************************/
public class OrderGenratePDFPurchaseOrder {
    
    public id iPageOrderId{get;set;}
    public boolean isError{get;set;}
    public String purchaseOrderNumber {get;set;}
    public order orderrec {get;set;}
    
    
    public OrderGenratePDFPurchaseOrder(ApexPages.StandardController stdController){
        isError = false;
        iPageOrderId = ApexPages.currentPAge().getParameters().get('id');
        orderrec = new order();
        orderrec = OrderService.getOrderRecords(iPageOrderId);
        System.debug( '***orderrec '+orderrec);
        purchaseOrderNumber = orderrec.OrderNumber;
    }
    /*method to insert attachement on purchase orders - */
    public void saveAttachement(){
        PageReference pdf;
        try {
            pdf = Page.OrderPurchaseOrderPDF;
            
            pdf.getParameters().put('id',iPageOrderId);
            
            Attachment attach = new Attachment();   // create the new attachment
            Blob body;                              // get the content of pdf
            body = pdf.getContentAsPDF();           // returns the output of the page as a PDF
            system.debug('***body'+body);
            
            attach.Name = 'PurchaseOrder - '+purchaseOrderNumber+'.pdf';
            attach.IsPrivate = false;
            attach.Body = body;
            // attach the pdf to the account
            attach.ParentId = iPageOrderId;
            insert attach;
            orderrec.Status = 'PDF Generated';
            update orderrec;
        }
        catch(Exception e){
             isError = true;
            System.debug('***Exception in getting the PDF content and creating attachment -- \n ');
            System.debug('***Exception at '+e.getLineNumber()+' -- '+e.getMessage());
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Error'+e.getMessage()));    
           
        }
       // return String.ValueOf(isError);
        
    }
    public Void sendEmail(){
        try{
            
            PageReference pdf;
            
            String sendToCCAddressSet= null;
            Set <String> sendToAddressSet = new Set<String>();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            System.debug('***orderrec.Vendor__c '+orderrec.Vendor__c);
            account acc = OrderService.getvendorDetails(orderrec.Vendor__c);
            System.debug('***acc '+acc);
            if(acc.Contact_Email__c != null)
                sendToCCAddressSet = (acc.Contact_Email__c);
            
            list <contact> vendorContacts = OrderService.getvendorContacts(acc.id);
            if(vendorContacts.size()>0)
            {
                for(contact c : vendorContacts){
                    if(orderrec.House__r.City_Master__c != null  && c.City_Master__c !=null){
                        if(orderrec.House__r.City_Master__c == c.City_Master__c){
                            sendToAddressSet.add(c.email);
                        }
                    }
                }
            }
            if(sendToAddressSet.size() < 1){
                if(sendToCCAddressSet != null && sendToCCAddressSet != '')
                    sendToAddressSet.add(sendToCCAddressSet);
            }
            if(sendToAddressSet.size() > 0 && sendToCCAddressSet != null){
                mail.setCCAddresses( new String[]{sendToCCAddressSet});
            }
            System.debug('***sendToAddressSet'+sendToAddressSet);
            System.debug('***sendToAddressSet.size()'+sendToAddressSet.size());
            System.debug(' mail.setCCAddresses '+ mail);
            if(sendToAddressSet.size() > 0 && sendToAddressSet!=null){
                //Change set to list
                list <string> sendTo = new list<String>();
                sendTo.addAll(sendToAddressSet);
                System.debug('***sendTo'+sendTo);
                pdf = Page.OrderPurchaseOrderPDF;
                pdf.getParameters().put('id',iPageOrderId);
                Blob pdfAtachment;
                pdfAtachment = pdf.getContentAsPDF();   
                System.debug('***pdfAtachment'+pdfAtachment);
                
                Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
                attachment.setContentType('application/pdf');
                attachment.setFileName('Purchase Order');
                attachment.setInline(false);
                attachment.setBody(pdfAtachment);
                mail.setSubject('Purchase order from Nestaway Technologies Pvt. Ltd. ' + Date.today().month() + ' ' + Date.today().year());
                mail.setPlainTextBody('Hi,'+'\n '+'Please find the attachment');
                mail.setFileAttachments(new Messaging.EmailFileAttachment[]{ attachment });
                mail.setSaveAsActivity( false );
                mail.setToAddresses(sendTo);
                
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                
                Attachment attach = new Attachment();   // create the new attachment
                attach.Name = 'Copy of PO PDF sent to vendor - '+purchaseOrderNumber+'.pdf';
                attach.IsPrivate = false;
                attach.Body = pdfAtachment;
                // attach the pdf to the account
                attach.ParentId = iPageOrderId;
                insert attach;
                
                orderrec.Status = 'Email Sent to Vendor';
                update orderrec;
            }
            else{
                isError = true;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'NO Email Id Specified for this order&#39; Vendor.'));
                
                System.debug('***Error in sending Email.');
            }
        }
        catch(Exception e){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Error '+e.getMessage()));    
            isError = true;
            //System.debug('***Exception in Sending Email to vendors -- \n ');
            System.debug('***Exception at '+e.getLineNumber()+' -- '+e.getMessage()+'\n ***trace'+e.getStackTraceString());
            
        }
        //return String.ValueOf(isError);
    }
    public PageReference redirectToParentPage(){
        PageReference redirectPage =  new PageReference('/'+iPageOrderId);
        redirectPage.setRedirect(true);
        System.debug('****redirectPage');
        return redirectPage;
    }
    
}