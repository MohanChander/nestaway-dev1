@isTest
public class TestRoomTriggerHandler {
    static testMethod void RoomTriggerTest() {
        //Create Dummy contract
        Test.StartTest();
        NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
        cusSet.name = 'nestaway';
        cusSet.Nestaway_URL__c = 'www.test.com';
        insert cusSet;
        
        Id recordTypeDccId = [Select Id from RecordType where Name=:'Document Collection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id;
        List<House_Inspection_Checklist__c> houseList = new List<House_Inspection_Checklist__c>();
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Final Contract';
        
        insert opp;
        
        PriceBook2 priceBk = new PriceBook2();
        priceBk.Name = 'TestPriceBook';
        // priceBk.isStandard = FALSE;
        insert priceBk;
        
        Product2 prod = new Product2();
        prod.Name = 'testProd';
        prod.isActive = TRUE;
        insert prod;
        
        Contract cont = new Contract();
        cont.Name = 'TestContract';
        cont.AccountId = accObj.id;
        cont.Opportunity__c = opp.id;
        cont.PriceBook2Id = priceBk.Id;
        cont.status = 'Draft';
        cont.Maximum_number_of_tenants_allowed__c = '1';
        cont.Booking_Type__c = 'Shared House';
        insert cont;
        
        cont.Status = 'Final Contract';
        cont.Approval_Status__c = 'Approved by Central Team';
        cont.Furnishing_Plan__c = 'Nestaway furnished';
        update cont;
        system.debug('****Contract***'+cont);
        
        Contract cont2 = new Contract();
        cont2.Name = 'TestContract2';
        cont2.AccountId = accObj.id;
        cont2.Opportunity__c = opp.id;
        cont2.PriceBook2Id = priceBk.Id;
        cont2.status = 'Draft';
        cont2.Maximum_number_of_tenants_allowed__c = '1';
        cont2.Booking_Type__c = 'Full House';
        insert cont2;
        
        cont2.Status = 'Final Contract';
        cont2.Approval_Status__c = 'Approved by Central Team';
        cont2.Furnishing_Plan__c = 'Nestaway furnished';
        update cont2;
        system.debug('****Contract***'+cont2);
        
        
        House_Inspection_Checklist__c houseIC1 = new House_Inspection_Checklist__c();
        houseIC1.RecordTypeId = recordTypeDccId ;
        houseIC1.Opportunity__c = opp.Id;
        houseIC1.Status__c = 'Draft'; 
        houseIC1.Doc_Reviewer_Approved__c = TRUE;       
        houseList.add(houseIC1);
        
        insert houseList;
        
        City__c city = new City__c ();
        city.name = 'Bengaluru';
        city.Rent_Min_Limit__c  = 90;
        
        Insert city;
        
        House__c houseObj = new House__c();
        houseObj.Name = 'TestHouse';
        houseObj.Stage__c = 'House Draft';
        houseObj.Opportunity__c = opp.Id;
        houseObj.City_Master__c = city.id;
        insert houseObj;
        
        houseObj.Stage__c = 'House Live';
        update houseObj;
        
        City__c city2 = new City__c ();
        city2.name = 'Bengaluru';
        //city2.Rent_Min_Limit__c  = null;
        
        Insert city2;
        
        House__c houseObj2 = new House__c();
        houseObj2.Name = 'TestHouse';
        houseObj2.Stage__c = 'House Draft';
        houseObj2.Opportunity__c = opp.Id;
        houseObj2.City_Master__c = city.id;
        insert houseObj2;
        
        houseObj2.Stage__c = 'House Live';
        update houseObj2;
        
        list<Room_Terms__c> RoomTermList = new list<Room_Terms__c>();
        
        Room_Terms__c roomTermRec = new Room_Terms__c();
        roomTermRec.Name = 'Test Room';
        roomTermRec.Actual_Room_Rent__c = 10000;
        roomTermRec.Monthly_Base_Rent_Per_Bed__c = 5000;
        roomTermRec.Monthly_Base_Rent_Per_Room__c = 10000;
        roomTermRec.Number_of_beds__c  = String.valueOf(2);
        roomTermRec.House__c = houseObj.id;
        roomTermRec.Contract__c = cont.id;	
        
        Room_Terms__c roomTermRec2 = new Room_Terms__c();
        roomTermRec2.Name = 'Test Room2';
        roomTermRec2.Actual_Room_Rent__c = 10000;
        roomTermRec2.Monthly_Base_Rent_Per_Bed__c = 5000;
        roomTermRec2.Monthly_Base_Rent_Per_Room__c = 10000;
        roomTermRec2.Number_of_beds__c  = String.valueOf(2);
        roomTermRec2.House__c = houseObj.id;
        roomTermRec2.Contract__c = cont.id;	
        
        for(Integer i=0;i<5;i++){
            
        }
       
        /*
        Room_Terms__c roomTermRec = new Room_Terms__c();
        roomTermRec.Name = 'Test Room';
        roomTermRec.Actual_Room_Rent__c = 10000;
        roomTermRec.Monthly_Base_Rent_Per_Bed__c = 5000;
        roomTermRec.Monthly_Base_Rent_Per_Room__c = 10000;
        roomTermRec.Number_of_beds__c  = String.valueOf(2);
        roomTermRec.House__c = houseObj.id;
        roomTermRec.Contract__c = cont.id;	
        
        Insert roomTermRec;
        
        roomTermRec.Actual_Room_Rent__c = 15000;
        //roomTermRec.Number_of_beds__c  = String.valueOf(3);
        
        update roomTermRec;
        
        roomTermRec.Actual_Room_Rent__c = 1000;
        //roomTermRec.Number_of_beds__c  = String.valueOf(2);
        
        update roomTermRec;
        
        roomTermRec.Actual_Room_Rent__c = 1000;
        roomTermRec.Number_of_beds__c  = String.valueOf(3);
        
        update roomTermRec;
        
        roomTermRec.Actual_Room_Rent__c = 100000;
        roomTermRec.Number_of_beds__c  = String.valueOf(5);
        
        update roomTermRec;
        
        roomTermRec.Actual_Room_Rent__c = 1000;
        roomTermRec.Number_of_beds__c  = String.valueOf(2);
        
        update roomTermRec;
        
        roomTermRec.Actual_Room_Rent__c = 100000;
        roomTermRec.Number_of_beds__c  = String.valueOf(1);
        
        update roomTermRec;
        
        roomTermRec.Number_of_beds__c  = null;
        roomTermRec.Actual_Room_Rent__c = 100000;
        
        update roomTermRec;
        
        
        Room_Terms__c roomTermRec2 = new Room_Terms__c();
        roomTermRec2.Name = 'Test2 Room';
        roomTermRec2.Actual_Room_Rent__c = 15000;
        roomTermRec2.Monthly_Base_Rent_Per_Bed__c = 5000;
        roomTermRec2.Monthly_Base_Rent_Per_Room__c = 10000;
        roomTermRec2.Number_of_beds__c  = String.valueOf(2);
        roomTermRec2.House__c = houseObj.id;
        roomTermRec2.Contract__c = cont.id;	
        
        insert roomTermRec2;
        
        Room_Terms__c roomTermRec3 = new Room_Terms__c();
        roomTermRec3.Name = 'Test3 Room';
        roomTermRec3.Actual_Room_Rent__c = 15000;
        roomTermRec3.Monthly_Base_Rent_Per_Bed__c = 5000;
        roomTermRec3.Monthly_Base_Rent_Per_Room__c = 10000;
        //roomTermRec3.Number_of_beds__c  = String.valueOf(2);
        roomTermRec3.House__c = houseObj.id;
        roomTermRec3.Contract__c = cont.id;	
        
        insert roomTermRec3;*/
        
        /*houseObj.City_Master__c = city2.id;
        update houseObj;
        
        houseObj.City_Master__c = city2.id;
        update houseObj;
        
        Room_Terms__c roomTermRec4 = new Room_Terms__c();
        roomTermRec4.Name = 'Test4 Room';
        roomTermRec4.Actual_Room_Rent__c = 2000;
        roomTermRec4.Monthly_Base_Rent_Per_Bed__c = 5000;
        roomTermRec4.Monthly_Base_Rent_Per_Room__c = 5000;
        roomTermRec3.Number_of_beds__c  = String.valueOf(10);
        roomTermRec4.House__c = houseObj.id;
        roomTermRec4.Contract__c = cont.id;	
        
        insert roomTermRec4;*/
        
        
       /* Room_Terms__c roomTermRec2 = new Room_Terms__c();
        roomTermRec2.Name = 'Test Room';
        roomTermRec2.Actual_Room_Rent__c = 10000;
        roomTermRec2.Monthly_Base_Rent_Per_Bed__c = 5000;
        roomTermRec2.Monthly_Base_Rent_Per_Room__c = 10000;
        roomTermRec2.Number_of_beds__c  = null;
        roomTermRec2.House__c = houseObj.id;
        roomTermRec2.Contract__c = cont.id;	
        
        Insert roomTermRec2;
        
        
        roomTermRec2.Actual_Room_Rent__c = 15000;
        roomTermRec2.Number_of_beds__c  = String.valueOf(2);
        
        update roomTermRec2;
        
        roomTermRec2.Actual_Room_Rent__c = 5000;
        roomTermRec2.Number_of_beds__c  = String.valueOf(2);
        
        update roomTermRec2;*/
        
       /* list <bed__c> bd = new list <bed__C> ();
        bd = [Select id,Actual_Rent__c from bed__c where room_terms__c =:roomTermRec.id];
        if(bd != null){
            bd[0].Actual_Rent__c =3000;
            update bd[0];
        }
        if(bd != null){
            bd[0].Actual_Rent__c =6000;
            update bd[0];
        }*/
       /* if(bd != null){
            bd[0].Actual_Rent__c =8000;
            update bd[0];
        }*/
        
        Test.stopTest();
        
    }    
    
    
}