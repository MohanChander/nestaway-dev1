/*  Created by   : Mohan
	Created Date : 28/05/2017
	purpose      : Utility Class for Asset creation */
public class AssetCreationUtility {
	
	/* purpose : once the Onboarding verification is done all the Assets are automatically created */
	public static void assetCreationAfterOnboardingVerification(Set<Id> houseIdSet){

		List<Asset> newAssetList = new List<Asset>();
		List<House__c> houseList = new List<House__c>();

		//query for the HIC record from the HouseId
		List<House_Inspection_Checklist__c> hicList = HouseInspectionCheckListSelector.getHICListFromHouseIdSet(houseIdSet);

		for(House_Inspection_Checklist__c hic: hicList){

			//creating TV Asset
			if(hic.Tv__c != null && hic.Tv__c != 'Not Available'){
				Asset newAsset = new Asset();
				newAsset.AccountId = hic.Opportunity__r.AccountId;
				newAsset.House__c = hic.House__c;
				newAsset.Name = 'TV';
				newAsset.Make__c = (hic.TV_Brand__c != null) ? hic.TV_Brand__c : '';
				newAsset.Model__c = (hic.TV_Model__c != null) ? hic.TV_Model__c : '';
				newAsset.Serial_Number__c = (hic.Tv_Serial_No__c != null) ? hic.Tv_Serial_No__c : '';
				if(hic.Warranty_Start_Date_For_TV__c != null)  newAsset.Warranty_Start_Date__c = hic.Warranty_Start_Date_For_TV__c;
				if(hic.Warranty_End_Date_For_TV__c != null)  newAsset.Warranty_End_Date__c = hic.Warranty_End_Date_For_TV__c;
				newAssetList.add(newAsset);
			}

			//Creating Fridge Asset
			if(hic.Fridge__c != null && hic.Tv__c != 'Not Available'){
				Asset newAsset = new Asset();
				newAsset.Name = 'Fridge';
				newAsset.House__c = hic.House__c;
				newAsset.AccountId = hic.Opportunity__r.AccountId;
				newAsset.Make__c = (hic.Fridge_Make__c != null) ? hic.Fridge_Make__c : '';
				newAsset.Model__c = (hic.Fridge_Model__c != null) ? hic.Fridge_Model__c : '';
				newAsset.Serial_Number__c = (hic.Fridge_Serial_No__c != null) ? hic.Fridge_Serial_No__c : '';
				if(hic.Warranty_Start_Date_For_Fridge__c != null)  newAsset.Warranty_Start_Date__c = hic.Warranty_Start_Date_For_Fridge__c;
				if(hic.Warranty_End_Date_For_Fridge__c != null)  newAsset.Warranty_End_Date__c = hic.Warranty_End_Date_For_Fridge__c;
				newAssetList.add(newAsset);
			}		

			//Creating Washing Machine Asset
			if(hic.Washing_Machine__c != null && hic.Washing_Machine__c != 'Not Available'){
				Asset newAsset = new Asset();
				newAsset.Name = 'Washing Machine';
				newAsset.House__c = hic.House__c;
				newAsset.AccountId = hic.Opportunity__r.AccountId;
				newAsset.Make__c = (hic.Washing_Machine_Brand__c != null) ? hic.Washing_Machine_Brand__c : '';
				newAsset.Model__c = (hic.Washing_Machine_Model__c != null) ? hic.Washing_Machine_Model__c : '';
				newAsset.Serial_Number__c = (hic.Washing_Machine_Serial_No__c != null) ? hic.Washing_Machine_Serial_No__c : '';
				newAssetList.add(newAsset);
			}	

			//Creating Kitchen Package Asset
			if(hic.Kitchen_Package__c != null && (hic.Kitchen_Package__c == 'Completely Present' || hic.Kitchen_Package__c == 'Partially Present')){
				Asset newAsset = new Asset();
				newAsset.Name = 'Kitchen Package';
				newAsset.House__c = hic.House__c;
				newAsset.AccountId = hic.Opportunity__r.AccountId;
				newAssetList.add(newAsset);
			}				

			//update the Asset Created checkbox on the House
			if(hic.House__c != null){
				House__c house = new House__c();
				house.Id = hic.House__c;
				house.Assets_Created__c = true;
				houseList.add(house);
			}			
		}

		if(!newAssetList.isEmpty()){
			try{
				insert newAssetList;
			} catch (Exception e){
				System.debug('Error Message: ' + e.getMessage() + ' LineNumber: ' + e.getLineNumber() + ' Exception Type: ' + e.getTypeName() + ' Cause: ' + e.getCause());
			}
		}	

		if(!houseList.isEmpty()){
			try{
				insert houseList;
			} catch (Exception e){
				System.debug('Error Message: ' + e.getMessage() + ' LineNumber: ' + e.getLineNumber() + ' Exception Type: ' + e.getTypeName() + ' Cause: ' + e.getCause());
			}
		}			
	}
}