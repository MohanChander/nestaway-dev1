//Added By deepak
// for Api call to create service Request case for Move In Inspection CheckList 
public class ServiceRequestCaseCreationAPI {
    
    public class ServiceRequestWrapeerClass{
        public String  user_sf_id{get ;set;}
        public String subject{get ;set;}
        public String issue_level{get;set;}
        public String relationship_type{get;set;}
        public String house_sf_id{get;set;}
        public String description{get;set;}
        public DateTime service_visit_datetime{get;set;}
        public String parent_id{get;set;}
        
    }
    public class ServiceRequestJsonRespWrapeerClass{        
        public string success;
        public string info;  
         public DataWrapper data;
    }
  
    public class DataWrapper{
         public String id;
    }
        
        public string status{get;set;}  
    @future(callout=true)
    public static void serviceRequestDetailsToWebAppFuture(String CaseAccountId,String caseSubject,String caseIssueLevel,String caseRelationshipType, String caeHouseSfid,String caseDescription, DAteTime caseServiceVisitDateTime, String caseParentId,String category,String MimoId, String TypeOfInspection,String SubCategory){ 
        
        sendInsuranceDetailsToWebApp(CaseAccountId,caseSubject,caseIssueLevel,caseRelationshipType,caeHouseSfid,
                                     caseDescription,caseServiceVisitDateTime,caseParentId,category,MimoId,TypeOfInspection,SubCategory);
    }
    
    public static void sendInsuranceDetailsToWebApp(String CaseAccountId,String caseSubject,String caseIssueLevel,String caseRelationshipType,  String caeHouseSfid,String caseDescription, DAteTime caseServiceVisitDateTime, String caseParentId ,String category,String MimoId, String TypeOfInspection,String SubCategory){  
        
        String serviceRequestdCaseId;
        String respBody;
        system.debug('caeHouseSfid'+caeHouseSfid);
        try{
            List<NestAway_End_Point__c > nestURL = NestAway_End_Point__c.getall().values();  
            ServiceRequestWrapeerClass rp = new ServiceRequestWrapeerClass();
            rp.user_sf_id =CaseAccountId;
            rp.subject = caseSubject;
            rp.issue_level=caseIssueLevel;
            rp.relationship_type=caseRelationshipType;
            rp.house_sf_id= caeHouseSfid;
            rp.description=caseDescription;
            rp.service_visit_datetime= caseServiceVisitDateTime;
            rp.parent_id= caseParentId;
            
            string strEndPoint= nestURL[0].Missing_Checklist_API__c +'&auth='+nestURL[0].Webapp_Auth__c;
            System.debug('**End POint Url'+strEndPoint);
            
            HttpResponse res;
            string jsonBody; 
            jsonBody=JSON.serialize(rp);
            
            System.debug('**jsonBody ' + jsonBody);
            
            res=RestUtilities.httpRequest('POST',strEndPoint,jsonBody,null);
            respBody= res.getBody();
            
            system.debug('**RespBody' + respBody); 
            
            
            ServiceRequestJsonRespWrapeerClass dataJson= new ServiceRequestJsonRespWrapeerClass();
            dataJson = (ServiceRequestJsonRespWrapeerClass)JSON.deserialize(respBody,ServiceRequestJsonRespWrapeerClass.Class);              
            system.debug('dataJson'+dataJson);     
            if(dataJson!=null){
                if(res.getStatusCode()==200){
                    system.debug('dataJson.data.id'+dataJson.data.id);
                    serviceRequestdCaseId= dataJson.data.id;
                }
            }
           if(serviceRequestdCaseId != null){
                Case c = new Case(id = serviceRequestdCaseId );
                c.Category__c= category;
                 c.Created_Automatically_during_MiMo__c =true;
                c.Sub_Category__c = SubCategory;
                if(TypeOfInspection=='hic'){
                    c.TypeOfInspection__c='hic';
                    c.MimoHic__c=MimoId;  
                }
                if(TypeOfInspection=='Ric'){
                    c.MimoRic__c=MimoId;
                     c.TypeOfInspection__c='ric';
                }
                if(TypeOfInspection=='Bic'){
                    c.MimoBic__c=MimoId;
                     c.TypeOfInspection__c='bic';
                }
                 update c;
            }
             
        }
        
        catch(exception e){
            System.debug('**Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertErrorLog('Webservices','Service Request Api failed error:'+e.getMessage()+' at line:'+e.getLineNumber()+' **RespBody'+respBody); 
        }                  
    }
}