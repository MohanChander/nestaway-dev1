/*  Created By : Mohan
    Purpose    : All the Queries related to WorkOrder Object are here */

public class WorkOrderSelector {

    //get WOLI from WorkOrders
    public static List<WorkOrder> getWorkOrdersWithWOLI(Set<Id> woIdSet){
        return [select Id, Status, (select Id, Status from WorkOrderLineItems) from WorkOrder
                where Id =: woIdSet];
    }

    public static List<WorkOrder> getTanentOffBoardingWorkOrders(Set<Id> houseIdSet){
        Id tanentoffboardingRecodTypeId = Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RT_OFFBOARDING_TENANT_MOVE_OUT).getRecordTypeId();
        return [select Id, Status,case.id,House__c from WorkOrder
                where house__c =:houseIdSet and RecordTypeId =:tanentoffboardingRecodTypeId and 
                Case.Status =: Constants.CASE_STATUS_OPEN];
    }

    public static List<WorkOrder> getWoWithChecklists(Set<Id> woIdSet){

        String hicString = UtilityServiceClass.getQueryString('House_Inspection_Checklist__c');
        String ricString = UtilityServiceClass.getQueryString('Room_Inspection__c');
        String bicString = UtilityServiceClass.getQueryString('Bathroom__c');
        String queryString = 'Select Id, CaseId, (' + hicString + ' from Checklist__r), ('  +
                              ricString + ' from Room_Inspection__r), (' +
                              bicString + ' from Bathrooms__r) from WorkOrder where Id =: woIdSet';
        List<WorkOrder> woList = (List<WorkOrder>)Database.Query(queryString);
        return woList;
    }
    public static List<WorkOrder> getPaymentSDWorkOrders(Set<Id> houseIdSet){
        Id sdpaymentRecordType = Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RECORD_SD_PAYMENT).getRecordTypeId();
        return [select Id, Status, House__c from WorkOrder
                where house__c =:houseIdSet and RecordTypeId =:sdpaymentRecordType ];
    }
    public static List<WorkOrder> getKeysWorkOrders(Set<Id> oppIdSet)
    {
        Id keysWrkRecordType = Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RT_KEYS).getRecordTypeId();
        return [Select id, Opportunity__c, Status from WorkOrder where RecordTypeId=:keysWrkRecordType and Status!='Recieved' and Opportunity__c=:oppIdSet];
    }
    public static List<WorkOrder> getvenderWorkOrders(Set<Id> caseidSet)
    {
        Id wrkRecordType = Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RT_VENDER).getRecordTypeId();
        return [Select id,CaseId, Opportunity__c, ownerID, Status from WorkOrder where CaseId=:caseidSet and RecordTypeId=:wrkRecordType and Status=:Constants.WORKORDER_STATUS_RESOLVED];
    }
        //added by baibhav
    public static List<WorkOrder> getSdPaymentWorkOrdersBybnkid(Set<Id> bnkIdset)
    {
        Id sdpaymentRecordType = Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RECORD_SD_PAYMENT).getRecordTypeId();
        Id sdpaymenReadOnlyRecordType = Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RECORD_SD_PAYMENT_READ_ONLY).getRecordTypeId();
       return [Select id,CaseId,Bank_Name__c,Branch_Name__c,IFSC_Code__c,Bank_Detail__c,Account_Number__c, Opportunity__c,
     // chandu: removing the completed status from work order
    //   ownerID, Status from WorkOrder where Bank_Detail__c=:bnkIdset and (RecordTypeId=:sdpaymentRecordType or RecordTypeId=:sdpaymenReadOnlyRecordType) and (Status='Open' or (Reverse__c='Reverse Initiate' and Status='Completed'))];
         ownerID, Status from WorkOrder where Bank_Detail__c=:bnkIdset and (RecordTypeId=:sdpaymentRecordType or RecordTypeId=:sdpaymenReadOnlyRecordType) and (Status='Open' or Reverse__c='Reverse Initiate')];
    }
    public static List<WorkOrder> getOnboardinfWorkOrdersBycaseid(Set<Id> casIdset)
    {
       id keyhandlingWrk=Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(Constants.WORKORDER_RT_KEY_HANDLIND).getRecordTypeId();
      id onBoardingWrk=Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RT_ONBOADING).getRecordTypeId();
      id onBoardingChecklistWrk=Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RT_ONBOADING_CHECKLIST).getRecordTypeId();
      return [Select id,CaseId, Opportunity__c, ownerID, Status from WorkOrder where CaseId=:casIdset and (RecordTypeId=:keyhandlingWrk or RecordTypeId=:onBoardingWrk or RecordTypeId=:onBoardingChecklistWrk) and status=:Constants.WORK_STATUS_OPEN];
    }
    // 
    // 
     public static List<WorkOrder> listOfWorkOrderToBeClosed(Set<Id> casIdset)
    {
       id moveInWorkOrderRTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Move In  WorkOrder').getRecordTypeId();
     return [Select id, Status from WorkOrder where CaseId=:casIdset and RecordTypeId=:moveInWorkOrderRTId];
    }
    public static LisT<workorder> getNonCompletWorkorderforcase(Set<id> casIdSet){
        
        return [Select id,Case_Status__c,status,subject,caseId from workorder where CaseId=:casIdSet and status!=:WorkOrder_Constants.WORKORDER_STATUS_COMPLETED];
    }
}