/*
* Description: It's trigger handler class of Order object's trigger, to update opportunity records based on few conditions.
*/



public class SalesOrderTriggerHandler {

    static Id purchaseOrderRtId = Schema.SObjectType.Order.getRecordTypeInfosByName().get(Constants.ORDER_RT_PURCHASE_ORDER).getRecordTypeId();

    Public Static Void AfterUpdate(Map<Id,Order> newMap,Map<Id,Order> oldMap){
        set<id> oppIdSet = new set<id>();
        list<Opportunity>lstOppUpdate = new list<Opportunity>();
        
        for(Order each : newMap.values()){
            if(each.Status != oldMap.get(each.Id).Status && each.Status == 'Submitted'){
                oppIdSet.add(each.OpportunityId);    
            }else if(each.Status != oldMap.get(each.Id).Status && each.Status == 'Cancelled'){
                Opportunity Opp = new Opportunity(Id= each.OpportunityId,StageName='Lost',Lost_Reason__c='Sales Order Cancelled');
                lstOppUpdate.add(opp);
            }   
        }
        
        List<Opportunity> oppList = [Select id,StageName from Opportunity where Id IN : oppIdSet];
        for(Opportunity opp : oppList){
            opp.StageName = 'Final Contract';
        }
        update oppList;
        
        if(lstOppUpdate.size()>0){
            try{
                update lstOppUpdate;
            }catch(exception ex){
                system.debug('==exception while updating opportunity in SOTriggerHandler=='+ex);
            }
        }
    }


    /*Created By: Mohan : WarpDrive Tech Works
      Purpose           : Create Purchase Orders for the Sales Order when the Generate PO check box is checked
      Execution        : After Update */
    
   
    public static void generatePurchaseOrders(Map<Id,Order> newMap,Map<Id,Order> oldMap){

        Set<Id> orderIdSet = new Set<Id>();
        
        for(Order each : newMap.values()){
            if(each.Generate_PO__c != oldMap.get(each.Id).Generate_PO__c && each.Generate_PO__c == true && !(each.Total_Items__c > 0)){
                each.addError('There are no products associated with the Order');
            } else if(each.Generate_PO__c != oldMap.get(each.Id).Generate_PO__c && each.Generate_PO__c == true){
                orderIdSet.add(each.Id); 
            }
        }        

        //Query for OrderItems 
        List<Order> ordList = OrderSelector.getOrderWithOLIFromIdSet(orderIdSet);

        for(Order ord: ordList){

            Map<Id, List<OrderItem>> vendorOrderItemMap = new Map<Id, List<OrderItem>>();   //stores the vendor Id and the related OrderItems

            for(OrderItem oi: ord.OrderItems){
                if(vendorOrderItemMap.containsKey(oi.Vendor__c)){
                    vendorOrderItemMap.get(oi.Vendor__c).add(oi);
                } else {
                    vendorOrderItemMap.put(oi.Vendor__c, new List<OrderItem>{oi});
                }
            }

            List<Order> newOrderList = new List<Order>();
            List<OrderItem> newOrderItemList = new List<OrderItem>();

            //using unit of work pattern
            RelationshipUtiltiy relUtil = new RelationshipUtiltiy();

            if(!vendorOrderItemMap.isEmpty()){
                for(Id vendorId: vendorOrderItemMap.keySet()){
                    Order newOrder = new Order();
                    newOrder.Vendor__c = vendorId;
                    newOrder.EffectiveDate = System.today();
                    newOrder.Status = Constants.ORDER_STATUS_APPROVED;
                    newOrder.Approval_Status__c = Constants.ORDER_APPROVAL_STATUS_APPROVED;
                    newOrder.AccountId = ord.AccountId;
                    newOrder.Pricebook2Id = ord.Pricebook2Id;
                    newOrder.House_PO__c = ord.House__c;
                    newOrder.RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get(Constants.ORDER_RT_PURCHASE_ORDER).getRecordTypeId();
                    newOrderList.add(newOrder);

                    for(OrderItem oi: vendorOrderItemMap.get(vendorId)){
                        OrderItem ordItem = new OrderItem();
                        ordItem = oi.clone();
                        ordItem.isCreatedFromSalesOrder__c = true;

                        //register the relationship - UOW
                        relUtil.registerRelationship(ordItem, newOrder, OrderItem.OrderId);
                        newOrderItemList.add(ordItem);
                    }
                }
            }

            insert newOrderList;

            //resolve the relationship - UOW
            relUtil.resolve('OrderItem');
            insert newOrderItemList;
        }
    }  // end of generatePurchaseOrders method

/**********************************************
    Created By : Mohan
    Purpose    : before Insert Handler for Order Object
**********************************************/
    public static void beforeInsert(List<Order> orderList){

        System.debug('**beforeInsert Order Trigger');

        try{    
                Id poApproverId;

                List<Order> poList = new List<Order>();

                //Select the PO Approver from the Org Param 
                List<Org_Param__c> orgParamList = [select Purchase_Order_Approver__c from Org_Param__c];

                if(!orgParamList.isEmpty() && orgParamList[0].Purchase_Order_Approver__c != null){
                    List<User> userList = [select Id from User where UserName =: orgParamList[0].Purchase_Order_Approver__c];

                    if(!userList.isEmpty()){
                        poApproverId = userList[0].Id;
                    }
                }

                for(Order ord: orderList){

                    //if the Vendor does not have bank details do not allow insert and Approver profile validation
                    if(ord.RecordTypeId == purchaseOrderRtId && (ord.Vendor__c != null ||
                        ord.Approver__c != null)){
                            poList.add(ord);
                    }

                    //if the Approver for a Purchase Order is not entered then use the Default PO Approver
                    if(ord.RecordTypeId == purchaseOrderRtId && ord.Approver__c == null){
                        ord.Approver__c = poApproverId;
                    }
                }

                System.debug('***************poList: ' + poList + '\n Size of poList: ' + poList.size());

                if(!poList.isEmpty()){
                    orderTriggerHelper.purchaseOrderValidator(poList);
                }

            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'before Insert Handler - Order Obj');      
            }           
    }   


/**********************************************
    Created By : Mohan
    Purpose    : before Update Handler for Order Object
**********************************************/
    public static void beforeUpdate(Map<Id, Order> newMap, Map<Id, Order> oldMap){

        System.debug('**beforeUpdate Order Trigger');

        try{    
                List<Order> approverAddedPOList = new List<Order>();

                for(Order ord: newMap.values()){

                    Order oldOrd = oldMap.get(ord.Id);

                    //if Approver is changed perform PO validation
                    if(ord.Approver__c != oldOrd.Approver__c && ord.Approver__c != null){
                        approverAddedPOList.add(ord);
                    }
                }

                if(!approverAddedPOList.isEmpty()){
                    orderTriggerHelper.purchaseOrderValidator(approverAddedPOList);
                }

            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'before Update Handler - Order Obj');      
            }           
    }          
     
}