public class ResourceAbsenceTriggerHandler {

/***************************
Created By : Mohan
Purpose    : When a Resource Absence is created - Unschedule and Schedule the Appointments again
****************************/
    public static void rescheduleServiceAppointments(Map<Id, ResourceAbsence> newMap){
        try {   
                Id vendorWoRtId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(WorkOrder_Constants.WORKORDER_RT_VENDER).getRecordTypeId();
                
                List<ServiceAppointment> serviceAppList = new List<ServiceAppointment>();
                Set<Id> woIdSet = new Set<Id>();

                for(ResourceAbsence ra: newMap.values()){

                    //Query for Service Appointment
                    List<ServiceAppointment> saList = [select Id, ParentRecordId, Status from ServiceAppointment where Id in
                                                       (select ServiceAppointmentId from AssignedResource where 
                                                       ServiceResourceId =: ra.ResourceId) and 
                                                       ((SchedStartTime >: ra.Start and SchedStartTime <: ra.End) or
                                                         (SchedEndTime >: ra.Start and SchedEndTime <: ra.End))];

                    serviceAppList.addAll(saList);                                                         

                } 

                for(ServiceAppointment sa: serviceAppList){
                    sa.Status = ServiceAppointmentConstants.STATUS_NONE;
                }      

                if(!serviceAppList.isEmpty()){
                    update serviceAppList;
                } 

                for(ServiceAppointment sa: serviceAppList){
                    woIdSet.add(sa.ParentRecordId);
                }

                Map<Id, WorkOrder> woMap = new Map<Id, WorkOrder>([select Id, CaseId, Street, City, State, Country, PostalCode 
                                                                   from WorkOrder where RecordTypeId =: vendorWoRtId and
                                                                   Id =: woIdSet and isClosed = false]);

                if(!woMap.isEmpty()){
                    WorkOrderFSLService.scheduleServiceAppointmentsForWorkOrders(woMap);
                }


        } catch(Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e, 'FSL Module - Invoice Work Order Creation');
        }
    }   
}