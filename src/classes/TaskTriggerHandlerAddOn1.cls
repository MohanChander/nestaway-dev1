/*From dev console*/

public class TaskTriggerHandlerAddOn1 {
    
    public static boolean checkRecursion = false;
    public static boolean checkrecAfterInserT = false;
    
     /* Purpose: Method to trigger owner Approval process in the WebAPP */
    @future (callout=true)
    public static void triggerOwnerApprovalInWebApp(id caseRecId) {
        list <task> tasklistTocreate = new list<task> ();
        map <id,case> caseMapToupdate = new map <id,case> ();
        //case caseRec = new case(id = caseRecId);
        case caseRec = new case();
        caseRec = [Select id,ownerId,Owner_Approval_API_Msg__c,Owner_Approval_Sent_To_Webapp__c,Stage_Status__c
                                  from case where id =: caseRecId ];
        try{
            System.debug('*** caseRecId'+caseRecId);
            Id TenantRecordTypeID = Schema.SObjectType.Task.getRecordTypeInfosByName().get(Constants.TASK_RT_MOVEIN_TASKS).getRecordTypeId();
            
            List<NestAway_End_Point__c > nestURL = NestAway_End_Point__c.getall().values();       
            String authToken=nestURL[0].Webapp_Auth__c;
            
            //https://2debeab3.ngrok.io/api/v3/sf_integration/move_in_sf_cases/send_owner_approval
            
            String strEndPoint = nestURL[0].triggerOwnerApproval__c+'&auth='+authToken;
            System.debug('***EndPoint  '+strEndPoint);
            
            string jsonBody='';
            WBMIMOJSONWrapperClasses.MoveInDates moveInCaseId = new WBMIMOJSONWrapperClasses.MoveInDates();
            moveInCaseId.case_id = caseRecId;
            jsonBody=JSON.serialize(moveInCaseId);
            
            System.debug('***jsonBody  '+jsonBody);
            
            HttpResponse res;
            res=RestUtilities.httpRequest('POST',strEndPoint,jsonBody,null);
            String respBody = res.getBody();
            
           // System.debug('***respBody '+respBody);
             
            
            if(res.getStatusCode() <= 500){
                System.debug('***ownerApproval triggered');
                
                /* Create Tasks and update case status */
                
               
              //  caseRec.status = 'Owner Approval Pending';  
                caseRec.Owner_Approval_API_Msg__c='success  :-'+respBody;
                caseRec.Owner_Approval_Sent_To_Webapp__c=true;
                caseMapToupdate.put(caseRec.id,caseRec);
               
                
          
            }
            else{
                
                caseRec.Owner_Approval_API_Msg__c='Error in owner approval API.  :-'+respBody;
                caseRec.Owner_Approval_Sent_To_Webapp__c=false;
                caseRec.Stage_Status__c='Owner Approval Failed';
                caseMapToupdate.put(caseRec.id,caseRec);
            }
            
                string defaultOwnerId = UserInfo.getUserId();
                
                if(caseRec.ownerId!=null && string.valueOf(caseRec.ownerId).startsWith('005')){
                    defaultOwnerId = caseRec.ownerId;
                }
                
                task t = new task();
                t.OwnerId=  defaultOwnerId;
                t.RecordTypeId = TenantRecordTypeID;
                t.priority= 'High';
                t.status = 'Not Started';
                t.System_created__c = true;
                t.ActivityDate = Date.Today().addDays(1) ;                    
                t.House__c = caseRec.House__c ;
                t.WhatId = caseRec.id;
                if(caseRec.House_Id__c != null )
                    t.House_Id__c = caseRec.House_Id__c ;
                t.subject= 'Owner approval needed for tenant checkIn.' ; 
                t.description = 'Need approval from the house Owner to onboard the Tenent to the house.';
                t.Task_Type__c = Constants.Tenant_TYPE_OwnerApproval ;
                tasklistTocreate.add(t);
        }   
        catch(exception e) {
            System.debug('***exception '+e.getLineNumber()+'  '+e.getMessage()+' \n '+e);
                caseRec.Owner_Approval_API_Msg__c='Exception in owner approval API.  :-'+'***exception '+e.getLineNumber()+'  '+e.getMessage();
                caseRec.Owner_Approval_Sent_To_Webapp__c=false;
                caseRec.Stage_Status__c='Owner Approval Failed';
                caseMapToupdate.put(caseRec.id,caseRec);
        }
        if(tasklistTocreate != null && tasklistTocreate.size() > 0)
            Insert tasklistTocreate;
        
        if(caseMapToupdate != null && caseMapToupdate.size() > 0)
            update caseMapToupdate.values();
    }

    public static void validateIfTenantVerificationTasksAreComplete(set<id> caseIdsFromTask ,map<id,task> updatedTaskmap ,map <id,task> newMap) {
       Id TenantRecordTypeID = Schema.SObjectType.Task.getRecordTypeInfosByName().get(Constants.TASK_RT_MOVEIN_TASKS).getRecordTypeId();

        map <id,case> caseMap_FromTasks = new map<id,case>([Select id,SD_Received__c,Documents_Uploaded__c,Documents_Verified__c,Tenant_Profile_Verified__c,Status,                                                   House__c,House_Id__c 
                                                            from case where id in : caseIdsFromTask]) ;
        
        map <id,case> caseMapToUpdate = new map <id,case>();
        list <task> tasklistTocreate = new list<task> ();
        
        for(task each : updatedTaskmap.values()) {

      
            
            if(caseIdsFromTask.contains(each.whatId) && caseMap_FromTasks.get(each.WhatId).Tenant_Profile_Verified__c == false   ) {
                
                if(newMap.get(each.id).Task_Type__c != null && newMap.get(each.id).Task_Type__c ==Constants.Tenant_RT_Verification)
                    newMap.get(each.id).addError('You cannot close the Tenant Profile Verification task because Tenant profile is not complete.');
                
                    
            }
            if(caseIdsFromTask.contains(each.whatId) && caseMap_FromTasks.get(each.WhatId).Documents_Verified__c  == false) {
               
               if(newMap.get(each.id).Task_Type__c != null && newMap.get(each.id).Task_Type__c ==Constants.Tenant_TYPE_VERIFYDOCUMENTS)
                    newMap.get(each.id).addError('You cannot close the Tenant Document Verification task because all Tenant documents are not verified.'); 
            }
            
            
            if(caseIdsFromTask.contains(each.whatId) && caseMap_FromTasks.get(each.WhatId).Documents_Uploaded__c  == false) {
               
               if(newMap.get(each.id).Task_Type__c != null && newMap.get(each.id).Task_Type__c ==Constants.Tenant_RT_DocumentComplete)
                    newMap.get(each.id).addError('You cannot close the Tenant Document Complete task because all mandatory Tenant documents are not Uploaded.'); 
            }
            
            if(caseIdsFromTask.contains(each.whatId) && caseMap_FromTasks.get(each.WhatId).SD_Received__c  != true) {
                
                if(newMap.get(each.id).Task_Type__c != null && newMap.get(each.id).Task_Type__c == Constants.Tenant_Type_SDPayement)
                    newMap.get(each.id).addError('You cannot close the Tenant SD Payment Verification task because SD has not yet been submitted.'); 
            }
                       
        }
    }
   
    public static void afterInsert(map<id,task> newMap) {

       /*
        if(checkrecAfterInserT == false) {
            
            checkrecAfterInserT = true;

            Id TenantRecordTypeID = Schema.SObjectType.Task.getRecordTypeInfosByName().get(Constants.TASK_RT_MOVEIN_TASKS).getRecordTypeId();
            
            set<id> caseIds = new set<id>();
            list<task> allTasksOfCase = new list<task> ();
            list <task> tasklistTocreate = new list<task> ();
            map <id,case> caseMapToupdate = new map <id,case> ();
            map<id,list<task>> caseAndTaskMap = new map<id,list<task>>();
            map<id,boolean> VerificationTaskCompleteForCaseMap = new map<id,boolean>();
            map<id,case> SdandDocsVerifCompleteForCaseMap = new map<id,case>();

            for(task each :newMap.values()) {
                if(each.RecordTypeId == TenantRecordTypeID ) {
                    caseIds.add(each.whatId);
                }

            }

            allTasksOfCase = [Select id, RecordTypeId, Subject, status, System_created__c, WhatId,
                               priority, description, Task_Type__c, ActivityDate, House__c, House_Id__c
                              from task 
                       where whatId in : caseIds and status = 'Completed' and RecordTypeId = : TenantRecordTypeID ];

            for(task each : allTasksOfCase) {
                if(caseAndTaskMap.containsKey(each.WhatId) ) {
                    caseAndTaskMap.get(each.WhatId).add(each);
                }
                else {
                    list<task> temptaskList = new list<task>();
                    temptaskList.add(each);
                    caseAndTaskMap.put(each.WhatId,temptaskList);
                }
            }

            System.debug('***caseAndTaskMap '+caseAndTaskMap+'*****\n'+caseAndTaskMap.keySet());
            map<id,case> caseMapFrom_caseAndTaskMap = new map<id,case>([Select id, House__c, House_Id__c,Requires_Owner_Approval__c,status
                                                                         from case where id in : caseAndTaskMap.keySet()]);

            for(id each : caseAndTaskMap.keySet()) {
                System.debug('***case  '+each+' \n ***Task list - '+caseAndTaskMap.get(each));
                Integer bothprofileVerificationTaskComplete = 0;
                if(caseMapFrom_caseAndTaskMap.containsKey(each) && caseMapFrom_caseAndTaskMap.get(each).status == 'Tenant Verification Pending' ) {
                    System.debug('**caseMapFrom_caseAndTaskMap.get(each).status -> '+caseMapFrom_caseAndTaskMap.get(each).status );
                    for(task eachTask : caseAndTaskMap.get(each) ) {
                        System.debug('***caseAndTaskMap.get(each'+eachTask.Subject+eachTask.RecordTypeId+(eachTask.status));
                        if(eachTask.Subject!= null && eachTask.Subject == 'Tenant Document Complete Verification.' && eachTask.RecordTypeId == TenantRecordTypeID && (eachTask.status) == 'Completed') {
                            bothprofileVerificationTaskComplete++;   
                        }
                        if(eachTask.Subject!= null && eachTask.Subject == 'Tenant Profile Verification.' && eachTask.RecordTypeId == TenantRecordTypeID && (eachTask.status) == 'Completed') {
                            bothprofileVerificationTaskComplete++;   
                        }
                        System.debug('**** *bothprofileVerificationTaskComplete  '+bothprofileVerificationTaskComplete);
                    }
                    if(bothprofileVerificationTaskComplete >= 2) {
                        VerificationTaskCompleteForCaseMap.put(each,true);
                        //checkrecAfterInserT = false;
                    }    
                }

                //Tenant  Documents Verification Pending

                Integer bothSD_andDocsVerified = 0;
                if(caseMapFrom_caseAndTaskMap.containsKey(each) && caseMapFrom_caseAndTaskMap.get(each).status == 'Tenant Docs Verification Pending' ) {
                    System.debug('**caseMapFrom_caseAndTaskMap.get(each).status 2-> '+caseMapFrom_caseAndTaskMap.get(each).status );
                    for(task eachTask : caseAndTaskMap.get(each) ) {
                        System.debug('***caseAndTaskMap.get(each2'+eachTask.Subject+eachTask.RecordTypeId+(eachTask.status));
                        if(eachTask.Subject!= null && eachTask.Subject == 'Tenant Documents Verification.' && eachTask.RecordTypeId == TenantRecordTypeID  && (eachTask.status) == 'Completed') {
                            bothSD_andDocsVerified++;   
                        }
                        if(eachTask.Subject!= null && eachTask.Subject == 'Tenant SD Payment Verification.' && eachTask.RecordTypeId == TenantRecordTypeID  && (eachTask.status) == 'Completed') {
                            bothSD_andDocsVerified++;   
                        }
                        System.debug('**** *bothSD_andDocsVerified2  '+bothSD_andDocsVerified);
                    }
                    if(bothSD_andDocsVerified >= 2) {
                        SdandDocsVerifCompleteForCaseMap.put(each,caseMapFrom_caseAndTaskMap.get(each));
                    } 
                    //caseMapFrom_caseAndTaskMap.get(each).status = 'Tenant Docs Verification Complete';
                }

            }
        
            System.debug('****VerificationTaskCompleteForCaseMap  ' + VerificationTaskCompleteForCaseMap);
            
            list <case> caseList = new list <case>([Select id, House__c, House_Id__c,status,
                                                    tenant__r.Documents_Verified__c
                                                    from case where id in : VerificationTaskCompleteForCaseMap.keySet()]);
            
            for(case each : caseList) {
                each.status = 'Tenant Docs Verification Pending';  
                caseMapToupdate.put(each.id,each);
                task t = new task();
                t.RecordTypeId = TenantRecordTypeID;
                t.subject= 'Tenant Documents Verification.'; 
                t.WhatId = each.id;
                t.priority= 'High';
                t.status = 'Not Started';
                t.description = 'Verify Tenants Documents are complete or not.';
                t.System_created__c = true;
                t.Task_Type__c = Constants.Tenant_TYPE_VERIFYDOCUMENTS ;
                t.ActivityDate = Date.Today().addDays(1) ;                    
                t.House__c = each.House__c ;
                if(each.House_Id__c != null )
                    t.House_Id__c = each.House_Id__c ;
                tasklistTocreate.add(t);
            }
            if(tasklistTocreate != null && tasklistTocreate.size() > 0)
              //  Insert tasklistTocreate;
            
            if(caseMapToupdate != null && caseMapToupdate.size() > 0)
                update caseMapToupdate.values();

            tasklistTocreate.clear();
            caseMapToupdate.clear(); 

            if(SdandDocsVerifCompleteForCaseMap != null && SdandDocsVerifCompleteForCaseMap.size() > 0) {
                System.debug('****SdandDocsVerifCompleteForCaseMap if bloc '+SdandDocsVerifCompleteForCaseMap);
                for(case each : SdandDocsVerifCompleteForCaseMap.values()) {
                    
                    if(each.Requires_Owner_Approval__c == true) {
                        each.status = 'SD and Tenant Docs verified';  
                        caseMapToupdate.put(each.id,each);
                        task t = new task();
                        t.RecordTypeId = TenantRecordTypeID;
                        t.priority= 'High';
                        t.status = 'Not Started';
                        t.System_created__c = true;
                        t.ActivityDate = Date.Today().addDays(1) ;                    
                        t.House__c = each.House__c ;
                        t.WhatId = each.id;
                        if(each.House_Id__c != null )
                            t.House_Id__c = each.House_Id__c ;
                        t.subject= 'Owner approval needed for tenant checkIn.'; 
                        t.description = 'Need approval from the house Owner to onboard the Tenent to the house.';
                        t.Task_Type__c = Constants.Tenant_TYPE_OwnerApproval ;
                        tasklistTocreate.add(t);
                    }
                    else if(each.Requires_Owner_Approval__c !=true  ) {
                        System.debug('*****Move in Scheduled');
                        each.status = 'Move in Scheduled';
                        caseMapToupdate.put(each.id,each);
                    }
                 }
            }
            if(caseMapToupdate != null && caseMapToupdate.size() > 0) {
                System.debug('*** caseMapToupdate '+caseMapToupdate);
                update caseMapToupdate.values();
            }
            if(tasklistTocreate != null && tasklistTocreate.size() > 0) {
                System.debug('*** tasklistTocreate '+tasklistTocreate);
                Insert tasklistTocreate;
            }
        }
        
        */
     }
    
    /*Check validation on moveout schedule tasks*/
    public static void checkValidationForMoveOutInspectionSchedule(map<id,task> newMap_Tasks, set<id> caseSetIds, set<id>TaskIds) {
        map<id,case> caseMapFromTasks;
        map<id,task> caseAndTaskMap = new map<id,task>();
        for(task each : newMap_Tasks.values()){
            caseAndTaskMap.put(each.whatId,each);
        }
        caseMapFromTasks = new map<id,case>([Select id,MoveOut_Slot_Start_Time__c,
                                MoveOut_Slot_End_Time__c,MoveOut_Executive__c 
                            from case where id IN : caseSetIds]);
        for(case each : caseMapFromTasks.values())  {
            if((each.MoveOut_Slot_Start_Time__c == null) || (each.MoveOut_Slot_End_Time__c == null) || (each.MoveOut_Executive__c == null) ) {
                /* Throw validation on task */
                if(caseAndTaskMap.containsKey(each.id)){
                    task relatedTask = caseAndTaskMap.get(each.Id);
                    newMap_Tasks.get(relatedTask.id).addError('You cannot close the task if MoveOut Slot start time,MoveOut Slot end time and MoveOut Executive is not present in the parent case.');
                }
            }
        }              
    }
    public static void onClosureOfMoveOutInspectionScheduleTasks(set<id> caseSetIds) {
        map<id,case> caseMapFromTasks;
        caseMapFromTasks = new map<id,case>([Select id,status
                            from case where id IN : caseSetIds]);
        for(case each : caseMapFromTasks.values())  {
            each.status = Constants.CASE_STATUS_MOVE_OUT_SCHEDULED;
            /*This will create a checklist work order*/
        } 
        if(caseMapFromTasks != null && caseMapFromTasks.size() > 0)
            update caseMapFromTasks.values();                
    }
    
    public static void beforeUpdate(map <id,task> newMap, map <id,task> oldMap ) {
       
        Id TenantRecordTypeID = Schema.SObjectType.Task.getRecordTypeInfosByName().get(Constants.TASK_RT_MOVEIN_TASKS).getRecordTypeId();
        Id TaskrecordType_moveOutinspection = Schema.SObjectType.Task.getRecordTypeInfosByName().get(Constants.TASK_RT_MOVEOUT_SCHEULE_INSPECTION_TASKS).getRecordTypeId();
        map<id,task> updatedTaskmap = new map <id,task> ();
        set<id> caseIdsFromTask = new set <id> ();
        set<id> taskids_moveoutscheduleask = new set<id> ();
        set<id> caseIds_FromMoveoutscheduleask = new set<id> ();
        for(task each :newMap.values()) {
            
            if(each.status != oldMap.get(each.id).Status && each.RecordTypeId == TenantRecordTypeID && String.valueOf(each.status) == 'Completed' && each.Task_Type__c != null && ( each.Task_Type__c == 'Tenant Document Complete' || each.Task_Type__c == 'Tenant Verification' || each.Task_Type__c == 'SD Verification' || each.Task_Type__c == 'Tenant Documents Verification') ) {
                updatedTaskmap.put(each.id,each);
                caseIdsFromTask.add(each.whatId);
            }
            if(each.RecordTypeId == TaskrecordType_moveOutinspection) {
                if(each.status != oldMap.get(each.id).Status && each.status != null && String.valueOf(each.status) == 'Completed') {
                    //Check if moveout executive is filled 
                    taskids_moveoutscheduleask.add(each.id);
                    caseIds_FromMoveoutscheduleask.add(each.whatId);
                }
            }
            
          
           if(each.status != null && each.status != oldMap.get(each.id).Status && each.status== 'Completed') {
               each.Task_Completed_Time__c=system.now();
            }
            
        }
        if(updatedTaskmap != null && updatedTaskmap.size() > 0) {
                validateIfTenantVerificationTasksAreComplete(caseIdsFromTask, updatedTaskmap , newMap);
        }
        if(taskids_moveoutscheduleask != null && taskids_moveoutscheduleask.size() > 0) {
            checkValidationForMoveOutInspectionSchedule(newMap, caseIds_FromMoveoutscheduleask, taskids_moveoutscheduleask);
        }
    }
    
    public static void afterUpdate(map <id,task> newMap, map <id,task> oldMap ) {
        Id TenantRecordTypeID = Schema.SObjectType.Task.getRecordTypeInfosByName().get(Constants.TASK_RT_MOVEIN_TASKS).getRecordTypeId();
        Id TaskrecordType_moveOutinspection = Schema.SObjectType.Task.getRecordTypeInfosByName().get(Constants.TASK_RT_MOVEOUT_SCHEULE_INSPECTION_TASKS).getRecordTypeId();
        set<id> caseIds = new set<id>();
        set<id> moveOutCaseIdsfromMoveOuttasks = new set<id>();
        list<task> allTasksOfCase = new list<task> ();
        list <task> tasklistTocreate = new list<task> ();
        map <id,case> caseMapToupdate = new map <id,case> ();
        map<id,list<task>> caseAndTaskMap = new map<id,list<task>>();
        map<id,boolean> VerificationTaskCompleteForCaseMap = new map<id,boolean>();
        map<id,case> SdandDocsVerifCompleteForCaseMap = new map<id,case>();


        for(task each :newMap.values()) {
           
            if(each.RecordTypeId == TaskrecordType_moveOutinspection && each.status != oldmap.get(each.id).status && each.status != null && each.status == 'Completed' ) {
                if(each.whatId != null)
                    moveOutCaseIdsfromMoveOuttasks.add(each.whatId);
            }
        }       
        
        if(moveOutCaseIdsfromMoveOuttasks != null && moveOutCaseIdsfromMoveOuttasks.size() > 0) {
            onClosureOfMoveOutInspectionScheduleTasks(moveOutCaseIdsfromMoveOuttasks) ;
        }        
        
    }
    
}