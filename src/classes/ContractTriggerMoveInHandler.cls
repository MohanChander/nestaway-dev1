public class ContractTriggerMoveInHandler {
    public Static Void AfterUpdate(map<Id,Contract> newMap,map<Id,Contract> oldMap){
       Set<id> setofmoveincase= new Set<Id>();
        for(Contract each : newMap.values()){
          //  if(each.Status =='Final Contract' && (each.status!=oldmap.get(each.id).status)){
            if(each.Status =='Final Contract' && (each.Send_Contract_To_Web__c)){
                if(each.MoveIn_Case__c != null){
               setofmoveincase.add(each.MoveIn_Case__c);
                   }
            }
        }    
        List<Case> caselist=[select id,Contract_Generated__c from case where id in:setofmoveincase];
        if(!caselist.isEmpty()){
            CaseMIMOFunctionalityHelper.UpdatemoveinContractGenerated(caselist);
        }
    }
}