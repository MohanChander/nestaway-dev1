public class ContactTriggerHandler {

    public static void beforeInsert(List<Contact> contactList){
    //Added by baibhav to throw error if no bank on account  
    try{
    id venderRecordtypeId= Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();  
    Set<Id> accIdSet=new Set<Id>();
    System.debug('****1***');
    for(Contact cn:contactList){
          accIdSet.add(cn.Accountid);
        }
    Map<id,Account> accMap=new Map<id,Account>();
    List<Bank_Detail__c> bnkList=new List<Bank_Detail__c>();
    if(!accIdSet.isEmpty()){
            accMap=new Map<id,Account>([Select id,RecordTypeId,name from account where id=:accIdSet and RecordTypeId=:venderRecordtypeId]);
        }
    if(!accMap.isEmpty()){
           bnkList=[Select id, Related_Account__c from Bank_Detail__c where Related_Account__c=:accMap.keySet()];
        }
    Map<id,List<Bank_Detail__c>> accIdMapBnkList = new Map<id,List<Bank_Detail__c>>();
    System.debug('****2***');
    for(Bank_Detail__c bnk:bnkList){
           List<Bank_Detail__c> bklist=new List<Bank_Detail__c>();
           if(accIdMapBnkList.containsKey(bnk.Related_Account__c)){
                bklist=accIdMapBnkList.get(bnk.Related_Account__c);
              } 
              bklist.add(bnk);
              accIdMapBnkList.put(bnk.Related_Account__c,bklist);
        }
      
             System.debug('****3***');
             for(Contact cn:contactList) {
                  if(accMap.containskey(cn.Accountid) && (!accIdMapBnkList.containskey(cn.Accountid) || accIdMapBnkList.get(cn.Accountid).size()==0)){
                    System.debug('****4**');
                    cn.addError('Please Add Bank Details to Account');
                  }
                }

          
      }catch(Exception e) {
            System.debug('******Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + 
                         '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + 
                          e.getCause() + '\nStack Trace ' + e.getStackTraceString());  
            UtilityClass.insertGenericErrorLog(e, 'Bank not present on Account'); 
        }

    }
}