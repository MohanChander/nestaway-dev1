public class HouseRentPageExtension {
    /*
    public String message{get;set;}
    public boolean showMessage {get;set;}
    public boolean bshowFullHouse {get;set;}
    public boolean bshowSharedHouse {get;set;}
    public String sfull_HouseName {get;set;}
    public Decimal dfullBaseHouserent {get;set;}
    public Decimal dfullHouserent {get;set;}
    public String sfullHouselayout {get;set;}
    public String sfullHouseStage {get;set;}
    public String sSharedHouseName {get;set;}
    public Decimal dSharedHouserent {get;set;}
    public Decimal dSharedBaseHouserent {get;set;}
    public String sSharedHouselayout {get;set;}
    public String sSharedHouseStage {get;set;}
    public map<id,list<bed__c>> roomtermAndBedMap {get;set;}
    public list<Room_Terms__c> filteredRoomtermlist {get;set;}
    
    public House__c houseToUpdate {get;set;}
    
    public id roomIdFromVF {get;set;}
    public id bedIdFromVf {get;set;}
    
    public boolean validationpassed {get;set;}
    
    public HouseRentPageExtension(ApexPages.StandardController controller) {
        try{
            
            message = '';showMessage = false; bshowFullHouse = True; bshowSharedHouse = True;
            sfull_HouseName = '-'; dfullHouserent = 0; sfullHouselayout = '-'; sfullHouseStage = '-';
            sSharedHouseName = '-';dSharedHouserent = 0;sSharedHouselayout = '-';sSharedHouseStage = '-';
            dSharedBaseHouserent = 0; dfullBaseHouserent = 0 ; 
            
            validationpassed = false;
            roomIdFromVF = null;
            bedIdFromVf = null;
            
            id houseid = ApexPages.currentPage().getParameters().get('id');
            
            house__c h = [Select id,name, Stage__c, House_Layout__c,
                          Actual_House_rent__c,Base_House_Rent__c,Contract__c,
                          Contract__r.Booking_Type__c
                          from house__c where id = : houseId];
            
            if(h.Contract__r.Booking_Type__c == 'Full House'){
                bshowFullHouse = true;
                bshowSharedHouse = false;
                houseToUpdate = h;
                displayFullHouseDetails(houseToUpdate);
            }
            else{
                bshowFullHouse = false;
                bshowSharedHouse = true;
                houseToUpdate = h ;
                displaySharedHouseDetails(houseToUpdate);
            }
        }
        catch(exception e){
            System.debug('***Exception at '+e.getLineNumber()+e.getMessage()+e.getCause()+e);
        }
    }
    
    public void displayFullHouseDetails(house__c house){
        try{
            house__c h = new house__c();
            h = house;
            
            if(h!= null){
                
                if(h.name !=null)
                    sfull_HouseName = h.name;
                if(h.Base_House_Rent__c != null)
                    dfullBaseHouserent = h.Base_House_Rent__c;
                if(h.Actual_House_rent__c != null){
                    dfullHouserent = h.Actual_House_rent__c;
                }
                if(h.House_Layout__c !=null){
                    sfullHouselayout = h.House_Layout__c; 
                }
                if(h.Stage__c !=null){
                    sfullHouseStage = h.Stage__c;  
                }
            }
        }
        catch(exception e){
            System.debug('Exception e '+e.getLineNumber()+e.getMessage()+e.getCause());
        }
    }
    public void displaySharedHouseDetails(house__c house){
        
        try{
            house__c h = new house__c();
            h = house;
            System.debug('**h - '+h);
            if(h!= null){
                
                if(h.name !=null)
                    sSharedHouseName = h.name;
                if(h.Base_House_Rent__c != null)
                    dSharedBaseHouserent = h.Base_House_Rent__c;
                if(h.Actual_House_rent__c != null){
                    dSharedHouserent  = h.Actual_House_rent__c;
                }
                if(h.House_Layout__c !=null){
                    sSharedHouselayout = h.House_Layout__c; 
                }
                if(h.Stage__c !=null){
                    sSharedHouseStage = h.Stage__c;  
                }
            }
            map<id,Room_Terms__c> houseIdRoomTremsMap = new map <id,room_terms__c>();
            list<Room_Terms__c> roomtermlist = new list<Room_Terms__c> ();
            filteredRoomtermlist = new list<Room_Terms__c> ();
            roomtermAndBedMap =  new map<id,list<bed__c>>();
            roomtermlist = [Select id,name, Actual_Room_Rent__c,Monthly_Base_Rent_Per_Bed__c,
                            Monthly_Base_Rent_Per_Room__c,City_Master__c,
                            Number_of_beds__c,Status__c,City_Master__r.Rent_Min_Limit__c,
                            (Select id,name,Actual_Rent__c,Initial_Rent__c,Status__c from Beds1__r ) 
                            from room_terms__c where house__c =:  h.id];
            
            if(roomtermlist.size() > 0){
                message = '';
                showMessage = false;
                for(room_terms__c rt : roomtermlist){
                    if(rt.Beds1__r.size() > 0){
                        filteredRoomtermlist.add(rt);
                    }
                }
                System.debug('****filteredRoomtermlist - > ' +filteredRoomtermlist );
                for(room_terms__c rt : filteredRoomtermlist) {
                    for(bed__c bd : rt.Beds1__r) {
                        
                        if(roomtermAndBedMap.containsKey(rt.Id)){
                            list <bed__c> templist_bed = roomtermAndBedMap.get(rt.Id);   
                            templist_bed.add(bd);
                            roomtermAndBedMap.put(rt.Id,templist_bed);
                        }
                        else
                        {
                            roomtermAndBedMap.put(rt.Id, new  list <bed__c> {bd});
                        }
                    }
                }
            }
            else{
                message = 'There are no room associated with this house';
                showMessage = true;
            }
        }
        catch(exception e){
            message = 'Exception occured - '+e.getLineNumber()+e.getMessage()+e.getCause();
            showMessage = true;
            System.debug('Exception e '+e.getLineNumber()+e.getMessage()+e.getCause());
        }
        
    }


    //method to check if the Room rent falls under the Min and Max
    //Min is 90%/based on the city and Max is sum all the bed rents 
    public void checkValidation1RoomRent() {

        Integer minlimit;
        Decimal newRoomRent=0;

        for(Integer i=0;i<filteredRoomtermlist.size();i++){
            newRoomRent= filteredRoomtermlist[i].Actual_Room_Rent__c; 

            //Check for the minLimit
            if(filteredRoomtermlist[i].City_Master__r.Rent_Min_Limit__c != null)
                minlimit = Integer.valueOf(filteredRoomtermlist[i].City_Master__r.Rent_Min_Limit__c);    
            else
                minlimit = Integer.valueOf(Label.Rent_Min_Limit);

            Decimal bedrentTotal = 0;

            for(Integer j=0;j<roomtermAndBedMap.get(filteredRoomtermlist[i].id).size();j++){
                bedrentTotal = bedrentTotal+roomtermAndBedMap.get(filteredRoomtermlist[i].id)[j].Actual_Rent__c;
            }

            //validation block
            if(newRoomRent < bedrentTotal*minlimit/100 ){
                validationpassed = false;  
                message = 'Actual room rent of '+filteredRoomtermlist[i].name +' cannot be less than '+minlimit+' % of the sum of rents of all the associated beds.';
                showMessage = true;
                break;
            }
            else if(newRoomRent > bedrentTotal){
                validationpassed = false;  
                message = 'Actual room rent of '+filteredRoomtermlist[i].name +' cannot be greater than the sum of rents of all the associated beds.';
                showMessage = true; 
                break;
            }
        }      
    }
    
    
    public void checkvalidationRoomRent(){
        try{
            System.debug('**** filteredRoomtermlist '+filteredRoomtermlist+'\n****'+roomIdFromVF); 
            list<id> lid = new list<id>();
            Room_Terms__c roomtermRec = new Room_Terms__c();
            Decimal roomrent = 0;
            
            roomtermRec = [Select id,name, Actual_Room_Rent__c,Monthly_Base_Rent_Per_Bed__c,
                           Monthly_Base_Rent_Per_Room__c,
                           Number_of_beds__c,Status__c,City_Master__c,City_Master__r.Rent_Min_Limit__c,
                           (Select id,name,Actual_Rent__c,Initial_Rent__c,Status__c from Beds1__r ) 
                           from room_terms__c where id =: roomIdFromVF];
            
            Integer minlimit;
            if(roomtermRec.City_Master__r.Rent_Min_Limit__c != null)
                minlimit = Integer.valueOf(roomtermRec.City_Master__r.Rent_Min_Limit__c);    
            else
                minlimit = Integer.valueOf(Label.Rent_Min_Limit);
            
            if(roomtermRec.Beds1__r.size() > 0){
                for(bed__c bd : roomtermRec.Beds1__r) {
                    roomrent = roomrent+bd.Actual_Rent__c;
                }
                if(roomtermRec.Actual_Room_Rent__c < roomrent*minlimit/100){
                    validationpassed = false;  
                    message = 'Actual room rent cannot be less than '+minlimit+' % of the sum of rents of all the associated beds.';
                    showMessage = true;
                }
                else if(roomtermRec.Actual_Room_Rent__c > roomrent){
                    validationpassed = false;  
                    message = 'Actual room rent cannot be greater than the sum of rents of all the associated beds.';
                    showMessage = true;
                    
                }
                else{
                    validationpassed = true;
                    message = '';
                    showMessage = false;
                }
                
            }
            else{
                validationpassed = true;
            }
        }
        catch(exception e){
            System.debug('Exception e '+e.getLineNumber()+e.getMessage()+e.getCause()+e);
        }
    }
    public void updateRoomRent(){
        try{
            System.debug('**** filteredRoomtermlist '+filteredRoomtermlist+'\n****'+bedIdFromVf); 
            bed__c bed = [Select id,Room_Terms__c  from bed__c where id =: bedIdFromVf ];
            Room_Terms__c roomTerm = [Select id,Actual_Room_Rent__c 
                                      from Room_Terms__c where id = :bed.Room_Terms__c];
            
        }
        catch(exception e){
            System.debug('****Exception e '+e.getLineNumber()+e.getMessage()+e.getCause()+e);
        }
    }
    public PageReference cancel(){
        PageReference newPage = new PageReference('/houseId');
        newPage.setRedirect(false);
        return null;
    }

    public void newSave(){
        Account acc = new Account();
        try {
            insert acc;
        } Catch(Exception e){
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'why not working here?'+e));
        }
    }

    //on click of save - method gets executed
    public PageReference saveMethod(){
        System.debug('save method'+houseToUpdate+houseToUpdate.id);
        
        PageReference newPage = new PageReference('/'+houseToUpdate.id);
        try
        {
            checkValidation1RoomRent();
            if(validationpassed == true){
                
                update filteredRoomtermlist;
            }
            
            System.debug('****houseToUpdate'+houseToUpdate);
            if(houseToUpdate != null ){
                if(houseToUpdate.Contract__r.Booking_Type__c == 'Full House' &&  houseToUpdate.Actual_House_rent__c != dfullHouserent){
                    houseToUpdate.Actual_House_rent__c = dfullHouserent;
                    System.debug('***FullHOuse -> '+houseToUpdate);
                    update houseToUpdate;
                }
                else if(houseToUpdate.Contract__r.Booking_Type__c == 'Shared House' &&  houseToUpdate.Actual_House_rent__c != dSharedHouserent){
                    houseToUpdate.Actual_House_rent__c = dSharedHouserent;
                    System.debug('***dSharedHouse -> '+houseToUpdate);
                    update houseToUpdate;
                }
            }
            newPage.setRedirect(true);
            
            return null;
        }
        catch(exception e){
            System.debug('****Exception '+e.getLineNumber()+e.getMessage()+e.getCause()+e);
            return null;
        }
    }  */
   
}