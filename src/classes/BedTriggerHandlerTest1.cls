@istest
public class BedTriggerHandlerTest1 {
     public static id caseOccupiedFurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
    public static id caseOccupiedUnfurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_UNFURNISHED_OFFBOARDING).getRecordTypeId();
    public static id caseUnoccupiedFurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_UNOCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
    public static id caseUnoccupiedUnfurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_UNOCCUPIED_UNFURNISHED_OFFBOARDING).getRecordTypeId();
    public static id OffboardingZoneRTId =  Schema.SObjectType.Zone__c.getRecordTypeInfosByName().get(Constants.ZONE_RECORD_TYPE_HOUSE_OFFBOARDING_ZONE).getRecordTypeId();
    
    
    public Static TestMethod void houseTest(){
     
            NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
            cusSet.name = 'nestaway';
            cusSet.Nestaway_URL__c = 'www.test.com';
            insert cusSet;
          User newUser = Test_library.createStandardUser(1);
          newUser.Phone='1234567890';
        insert newuser;
           Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
       
        
         Opportunity opp = new Opportunity();
            opp.Name ='Test Opp'; 
            opp.CloseDate = System.Today();
            opp.StageName = 'Quote Creation';
            opp.State__c = 'Karnataka';
            insert opp;
         contract c= new Contract();
        c.accountid=accobj.id;
        c.Opportunity__c=opp.id;
        c.Furnishing_Type__c = Constants.CONTRACT_FURNISHING_CONDITION_FURNISHED;
        insert c;
        
          zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        Room_Terms__c rc= new Room_Terms__c();
        insert rc;
        City__c ci = new City__c();
        ci.name='bangalore';
        insert ci;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Contract__c=c.id;
        hos.City__c='bangalore';
        hos.House_Owner__c=accObj.id;
        hos.Assets_Created__c=false;
        hos.ZAM__c= newUser.id;  
        hos.Stage__c = Constants.HOUSE_STAGE_HOUSE_DRAFT;
        hos.Opportunity__c=opp.id;
        hos.Initiate_Offboarding__c='Yes';
        hos.OwnerId=newuser.id;
        insert hos;
         Test.StartTest();
         
       
        bed__c bedc= new bed__c();
        bedc.House__c=hos.id;
        bedc.Room_Terms__c=rc.id;
        insert bedc;
        Bathroom__c bac= new Bathroom__c ();
        bac.House__c=hos.Id;
        insert bac;
       
        List<bed__c> bedList= new   List<bed__c>();
       bedList.add(bedc);
        Set<Id> setbed= new Set<id>();
        setbed.add(bedc.id);
        Map<id,bed__c> bedMap= new Map<id,bed__c>();
       bedMap.put(bedc.id,bedc);
          
            BedTriggerHandler.afterupdate(bedList,bedMap,bedMap);
        BedTriggerHandler.sendWebEntityHouseJson(bedMap);
         BedTriggerHandler.onAfterUpdatebedBed(bedList,bedList);
            Test.stopTest();
    }
 
}