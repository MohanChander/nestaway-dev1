/**********************************************
    Created By : Mohan
    Purpose    : All the service methods related to Care Module 
**********************************************/
public class CaseCareModuleService {

/**********************************************
    Created By : Mohan
    Purpose    : Validate if the Case Owner is the user Present inside the Queue associated with the Case
**********************************************/
    public static Map<Id, Boolean> validateCaseOwnerInsideQueue(List<Case> caseList){

        System.debug('**validateCaseOwnerInsideQueue');

        Id serviceRequestRtId = Schema.SObjectType.Zone_and_OM_Mapping__c.getRecordTypeInfosByName().get(Constants.ZONE_MAPPING_SERVICE_REQUEST_MAPPING_RECORD_TYPE).getRecordTypeId();

        Map<Id, Boolean> returnCaseMap = new Map<Id, Boolean>();
        Map<String, Set<Id>> queueWithUserMap = new Map<String, Set<Id>>();
        Set<String> queueSet = new Set<String>();    
            
        for(Case c: caseList){
            queueSet.add(c.Queue__c);
        }

        List<GroupMember> grpMemberList = GroupAndGroupMemberSelector.getQueueMembers(queueSet, 'Case');
        List<Zone_and_OM_Mapping__c> zoneWithUserList = 
                ZoneAndUserMappingSelector.getZoneAndUserMappingForParticularZoneType(serviceRequestRtId);

        // Populate the CaseLoadMap
        for(Zone_and_OM_Mapping__c zu: ZoneWithUserList){
            if(zu.Available_Queues__c != null){
                for(String queue: zu.Available_Queues__c.split(';')){
                    if(queueWithUserMap.containsKey(queue)){
                        queueWithUserMap.get(queue).add(zu.user__c);
                    } else {
                        queueWithUserMap.put(queue, new Set<Id>{zu.user__c});
                    }
                }
            }
        } 

        for(GroupMember gm: grpMemberList){
            if(queueWithUserMap.containsKey(gm.Group.Name)){
                queueWithUserMap.get(gm.Group.Name).add(gm.UserOrGroupId);
            } else {
                queueWithUserMap.put(gm.Group.Name, new Set<Id>{gm.UserOrGroupId});
            }            
        }     
        
        for(Case c: caseList){
            if(queueWithUserMap.containsKey(c.Queue__c)){
                if(queueWithUserMap.get(c.Queue__c).contains(c.OwnerId)){
                    returnCaseMap.put(c.Id, true);
                } else {
                    returnCaseMap.put(c.Id, false);
                }
            } else {
                returnCaseMap.put(c.Id, false);
            }
        }

        System.debug('**returnCaseMap ' + returnCaseMap);

        return returnCaseMap;          
    }
}