public with sharing class ChecklistConstants {
	  // Checklist object constant
    public static String CHECKLIST_RT_House_Inspection_Checklist='House Inspection Checklist';
    public static String CHECKLIST_RT_Document_Collection_Checklist='Document Collection Checklist';
    public static String CHECKLIST_RT_Onboarding_Full_Layout='Onboarding Full Layout';
    public static String CHECKLIST_RT_Onboarding_Furnished_BHK='Onboarding Furnished BHK';
    public static String CHECKLIST_RT_Onboarding_Furnished_Room='Onboarding Furnished Room';
    public static String CHECKLIST_RT_Onboarding_Furnished_Studio='Onboarding Furnished Studio';
    public static String CHECKLIST_RT_Onboarding_Precheck_Furnished_BHK='Onboarding Precheck Furnished BHK';
    public static String CHECKLIST_RT_Onboarding_Precheck_Furnished_Room='Onboarding Precheck Furnished Room';
    public static String CHECKLIST_RT_Onboarding_Precheck_Furnished_Studio='Onboarding Precheck Furnished Studio';
    public static String CHECKLIST_RT_Onboarding_Precheck_Unfurnished_BHK='Onboarding Precheck Unfurnished BHK';
    public static String CHECKLIST_RT_Onboarding_Precheck_Unfurnished_Room='Onboarding Precheck Unfurnished Room';
    public static String CHECKLIST_RT_Onboarding_Precheck_Unfurnished_Studio='Onboarding Precheck Unfurnished Studio';
    public static String CHECKLIST_RT_Onboarding_Verification_Furnished_BHK='Onboarding Verification Furnished BHK';
    public static String CHECKLIST_RT_Onboarding_Verification_Furnished_Room='Onboarding Verification Furnished Room';
    public static String CHECKLIST_RT_Onboarding_Verification_Furnished_Studio='Onboarding Verification Furnished Studio';
    public static String CHECKLIST_RT_Onboarding_Verification_Unfurnished_BHK='Onboarding Verification Unfurnished BHK';
    public static String CHECKLIST_RT_Onboarding_Verification_Unfurnished_Room='Onboarding Verification Unfurnished Room';
    public static String CHECKLIST_RT_Onboarding_Verification_Unfurnished_Studio='Onboarding Verification Unfurnished Studio';
    public static String CHECKLIST_RT_TYPE_MOVE_IN_CHECK = 'Move in Check';
    public static String CHECKLIST_RT_TYPE_MOVE_OUT_CHECK = 'Move Out Check';
    public static String CHECKLIST_RT_TYPE_MOVE_IN_CHECK_READ_ONLY = 'Move in Check Read Only';
    public static String CHECKLIST_RT_TYPE_MOVE_OUT_CHECK_READ_ONLY = 'Move Out Check Read Only';
    public static String CHECKLIST_RT_TYPE_MIMO_CHECK = 'MIMO HIC';
    public static String CHECKLIST_RT_TYPE_OFFBOARDING_UNFURNISHED_CHEKCLIST = 'Offboarding Unfurnishing Checklist';

      // type_of_HIC_approval stats picklist values 
    public static String CHECKLIST_Approval_Status='Approved by Docs Reviewer';
    public static String CHECKLIST_APPROVAL_STATUS_APPROVAL_SKIP='Approved Skip Request';
    public static String CHECKLIST_APPROVAL_STATUS_HOUSE_INSPECT_COMP='House Inspection Completed';
}