/*
* Description: This class is used to check phone number validation
*/

Global class PhoneValidationCheck{
    //@future(CallOut=True)
    Global Static boolean isErrorInPhone;
    Global Static String strInfo;
    public Static Void numberValidation(Id LeadId, String PhoneNumber, String strEmail,String CountryCode ,Boolean isError){ 
    isErrorInPhone=FALSE;
        Boolean isValidPhone;    
        HttpRequest req = new HttpRequest();
        List<NestAway_End_Point__c > phoneURL = NestAway_End_Point__c.getall().values();
        req.setEndPoint(phoneURL[0].Phone_URL__c+'/api/v3/sf_integration/phone_validator/validate?auth=16f20b9a1913b16ca99e020e04dfbd04035a12e7294fc08a29595e1241038756&email='+strEmail+'&phone='+PhoneNumber+'&country_code='+CountryCode);      
        req.setMethod('GET');
        Http http = new Http();
        HttpResponse res;
        //if(!Test.isRunningTest()){
            String strSuccess='';
            if(!Test.isRunningTest()){
            res = http.send(req); System.debug('==Res.body()=='+res.getBody());
            }else{
                /* create sample data for test method */
                String resString = '{"success":true,"info":null}';
                resString +='</geometry> </result> </GeocodeResponse>';
                res = new HttpResponse();
                res.setBody(resString);
                res.setStatusCode(200);
            }
            if(res.getStatusCode()==200 ||(res.getStatusCode()==500 && res.getBody()!=null)){ //if phone number is invalid then status code will be 500 
                String str = res.getBody();
                Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(str);
                System.debug('==m=='+m);
                strSuccess=String.ValueOf(m.get('success'));
                String phoneValMsg = '';
                if(strSuccess=='False'){
                    isValidPhone=FALSE;
                    //isError=TRUE;  
                    isErrorInPhone=TRUE;
                    strInfo=String.ValueOf(m.get('info'));
                    System.debug('*************strInfo error******************** '+strInfo);
                    List<String> strInfoList = strInfo.split('\\[');
                    phoneValMsg = strInfoList[0];
                    strInfo = phoneValMsg;
                }else if(strSuccess=='True'){
                    isValidPhone=TRUE;
                    //isError=FALSE;  
                    isErrorInPhone=FALSE;
                    phoneValMsg = 'Phone number validated successfully';   
                }
                System.debug('==isError=='+isError);  
                System.debug('==isErrorInPhone in phonecheck class=='+isErrorInPhone);
                Lead objLead = new Lead(Id=LeadId,IsValidPrimaryContact__c=isValidPhone, Phone_Validation_Message__c = phoneValMsg);
                try{
                    Update objLead;
                }catch(Exception ex){
                    System.debug('==exception whiele Validation Lead Detail==='+ex);
                }     
            }
        /*}else{*/
        /* create sample data for test method */
        /*String resString = '<GeocodeResponse><status>OK</status><result><geometry><location><lat>37.4217550</lat> <lng>-122.0846330</lng></location>';
        resString +='</geometry> </result> </GeocodeResponse>';
        res = new HttpResponse();
        res.setBody(resString);
        res.setStatusCode(200);
        
        }*/
        
    }
    // created by chandu
    // To call this method form lead trigger.
    @future(CallOut=True)
    public Static Void numberValidationFuture(Id LeadId, String PhoneNumber, String strEmail,String CountryCode){ 
        boolean isErrorInPhoneFut=FALSE;
        string strInfoFut='';
        Boolean isValidPhone;    
        HttpRequest req = new HttpRequest();
        List<NestAway_End_Point__c> phoneURL = NestAway_End_Point__c.getall().values();
        req.setEndPoint(phoneURL[0].Phone_URL__c+'/api/v3/sf_integration/phone_validator/validate?auth=16f20b9a1913b16ca99e020e04dfbd04035a12e7294fc08a29595e1241038756&email='+strEmail+'&phone='+PhoneNumber+'&country_code='+CountryCode);        
        req.setMethod('GET');
        Http http = new Http();
        HttpResponse res;
        //if(!Test.isRunningTest()){
            String strSuccess='';
            if(!Test.isRunningTest()){
            res = http.send(req); System.debug('==Res.body()=='+res.getBody());
            }else{
                /* create sample data for test method */
                String resString = '{"success":true,"info":null}';
                resString +='</geometry> </result> </GeocodeResponse>';
                res = new HttpResponse();
                res.setBody(resString);
                res.setStatusCode(200);
            }
            if(res.getStatusCode()==200 ||(res.getStatusCode()==500 && res.getBody()!=null)){ //if phone number is invalid then status code will be 500 
                String str = res.getBody();
                Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(str);
                System.debug('==m=='+m);
                strSuccess=String.ValueOf(m.get('success'));
                String phoneValMsg = '';
                if(strSuccess=='False'){
                    isValidPhone=FALSE;
                    //isError=TRUE;  
                    isErrorInPhoneFut=TRUE;
                    strInfoFut=String.ValueOf(m.get('info'));
                    System.debug('*************strInfoFut error******************** '+strInfoFut);
                    List<String> strInfoList = strInfoFut.split('\\[');
                    phoneValMsg = strInfoList[0];
                    strInfoFut = phoneValMsg;
                }else if(strSuccess=='True'){
                    isValidPhone=TRUE;
                    //isError=FALSE;  
                    isErrorInPhoneFut=FALSE;
                    phoneValMsg = 'Phone number validated successfully';   
                }
                //System.debug('==isError=='+isError);  
                System.debug('==isErrorInPhoneFut in phonecheck class=='+isErrorInPhoneFut);
                boolean changeLeadRecType=false;
                Id ownerLeadIdWithPhoneOption = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Owner Lead With Phone Option').getRecordTypeId(); 
                if(isValidPhone){
                    
                    List<account> accLst=[SELECT Id, PersonEmail, Phone FROM Account WHERE PersonEmail=:strEmail and IsPersonAccount =: TRUE];
                    if(accLst.size()>0){
                         account acc=accLst.get(0);
                         if(acc.Phone!=PhoneNumber){
                             
                             changeLeadRecType=true;
                         }
                    }
                }
                    Lead objLead;
                    if(!changeLeadRecType){
                        objLead = new Lead(Id=LeadId,IsValidPrimaryContact__c=isValidPhone,isAPISuccess__c=true,Phone_Validation_Message__c = phoneValMsg);
                    }
                    else{
                        objLead = new Lead(Id=LeadId,IsValidPrimaryContact__c=isValidPhone,isAPISuccess__c=true,Phone_Validation_Message__c = phoneValMsg,recordtypeId=ownerLeadIdWithPhoneOption);
                    }
                
                try{
                    Update objLead;
                }catch(Exception ex){
                    System.debug('==exception whiele Validation Lead Detail==='+ex);
                }     
            }
        /*}else{*/
        /* create sample data for test method */
        /*String resString = '<GeocodeResponse><status>OK</status><result><geometry><location><lat>37.4217550</lat> <lng>-122.0846330</lng></location>';
        resString +='</geometry> </result> </GeocodeResponse>';
        res = new HttpResponse();
        res.setBody(resString);
        res.setStatusCode(200);
        
        }*/
        
    }
}