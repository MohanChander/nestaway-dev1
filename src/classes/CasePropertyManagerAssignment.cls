/**********************************************
Created By : Deepak
Purpose    : Property Manager Assignment Logic for MIMO, Onboarding, Offboarding Cases
Invocation : During the Onboarding, Offboarding, MIMO Case Creation
**********************************************/
public class CasePropertyManagerAssignment {
    Public Static Id caseServiceRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_SERVICE_REQUEST).getRecordTypeId();
    public static id caseOccupiedFurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
    public static id caseOccupiedUnfurnishedRecordtype = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_OCCUPIED_UNFURNISHED_OFFBOARDING).getRecordTypeId();
    public static id caseUnoccupiedFurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_UNOCCUPIED_FURNISHED_OFFBOARDING).getRecordTypeId();
    public static id caseUnoccupiedUnfurnishedRecordtype =  Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_UNOCCUPIED_UNFURNISHED_OFFBOARDING).getRecordTypeId();
    public static Id MoveIn_RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEIN).getRecordTypeId();
    public static Id MoveOutRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEOUT).getRecordTypeId();
    public static Id IT_RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_INTERNAL_TRANSFER).getRecordTypeId();     
    
    public static void assignmentOfOwners(List<Case> caseList,string typeOfAssignment){
        
        system.debug('**assignmentOfOwners');
        System.debug('**caseList: ' + caseList + '\n Size of caseList: ' + caseList.size());
        
        Set<Id> SetofOnboardingHouse = new Set<Id>();
        Set<Id> SetofHouse = new Set<Id>();
        Set<ID> SetofInternalHouse = new Set<Id>();
        Set<ID> SetofInternalHouse1 = new Set<Id>();
        Set<Id> SetofOnboardingHouse1 = new Set<Id>();
        Set<Id> SetofHouse1 = new Set<Id>();
        Set<Id> setOfcase = new Set<Id>();
        Set<id> houseSetToBeUpdated = new Set<id>();
        Map<Id, Integer> openworkOrderMap = new Map<Id, Integer>();
        Map<Id,Id> mapOfHouseProperotyManager = new  Map<Id,Id>();
        Map<Id,String> mapOfHOuseandZone = new  Map<Id,String>();
        Map<Id,Id> mapOfHouseProperotyManagerOffBoarding = new  Map<Id,Id>();
        Map<Id,House__C> mapOfHouse = new Map<ID,House__c>();
        Map<Id,House__C> mapOfHouseInternal = new Map<ID,House__c>();
        Map<String,Id> cityToHeadId = new Map<String,Id>();
        Map<Id,String> mapOfHOuseandZoneOffBoarding  = new  Map<Id,String>();
        List<House__C> houseListToBeUpdated = new  List<House__C>();
        List<Case> caseListToUPdated = new List<Case>();
        Map<Id,House__C>  mapOfHouseOffBoarding = new Map<ID,House__c>();
        string moveinQueueId;
        string moveoutQueueId;
        string zoneRrecordTypeName;
        string zoneMappingRrecordTypeName;
        string caseRecordTypeName;
        try{
            for(Case each: caseList){
                if(each.house__C != null){
                    SetofHouse.add(each.House__C);
                }
                if(each.HouseForOffboarding__c != null){
                    SetofOnboardingHouse.add(each.HouseForOffboarding__c);
                }
                if(each.From_House__c  != null){
                    SetofInternalHouse.add(each.From_House__c );
                }
            }
            
            system.debug('SetofHouse'+SetofHouse);
            // Set of onboarding // mimo case house
            if(!SetofHouse.isEmpty()){
                List<House__C> houseList = [Select id,Property_Manager__c,PM_Zone_Code__c,city__c from House__C where id in:SetofHouse];
                for(House__c each:HouseList){
                    mapOfHouse.put(each.id,each);
                }
            }
            
            // City Detail with Operation head
            List<City__c> cityList= [ Select id,name, Operation_Head__c from City__c where Operation_Head__c!=null];
            for(City__c c: cityList){
                cityToHeadId.put(c.name,c.Operation_Head__c);
            }
            
            
            // for off boarding cases 
            if(!SetofOnboardingHouse.isEmpty()){
                List<House__C> houseList = [Select id,Property_Manager__c,PM_Zone_Code__c,city__c from House__C where id in:SetofOnboardingHouse];
                for(House__c each:HouseList){
                    mapOfHouseOffBoarding.put(each.id,each);
                    
                }
            }
            
            // Set of Internal house cases
            if(!SetofInternalHouse.isEmpty()){
                List<House__C> houseList = [Select id,Property_Manager__c,PM_Zone_Code__c,city__c from House__C where id in:SetofInternalHouse];
                for(House__c each:HouseList){
                    mapOfHouseInternal.put(each.id,each);
                    
                }
            }
            
            
            
            
            Set<String> queueNames = new Set<string>();
            Org_Param__c param = Org_Param__c.getInstance();
            List<String> closedStatus= new List<string>();
            if(param!=null && param.Move_In_Case_Closed_Status__c!=null){
                
                closedStatus=param.Move_In_Case_Closed_Status__c.split(';');
            }
            
            if(typeOfAssignment=='Move In'){
                
                zoneRrecordTypeName=Constants.ZONE_RECORD_TYPE_MOVE_IN;
                zoneMappingRrecordTypeName=Constants.ZONE_MAPPING_RECORD_TYPE_MOVE_IN;
                caseRecordTypeName=Constants.CASE_RT_MOVEIN;
                
                if(param.Default_Movein_Case_Queue__c!=null){
                    queueNames.add(param.Default_Movein_Case_Queue__c);
                }
            }
            else if(typeOfAssignment=='Move out'){
                
                zoneRrecordTypeName=Constants.ZONE_RECORD_TYPE_MOVE_OUT;
                zoneMappingRrecordTypeName=Constants.ZONE_MAPPING_RECORD_TYPE_MOVE_OUT;
                caseRecordTypeName=Constants.CASE_RT_MOVEOUT;
                
                if(param.Default_Moveout_Case_Queue__c!=null){
                    queueNames.add(param.Default_Moveout_Case_Queue__c);
                }
            }
            
            if(queueNames.size()>0){
                
                List<Group> groupLst=[select Id from Group where Name IN:queueNames and Type = 'Queue'];
                
                if(groupLst.size()>0){
                    
                    if(typeOfAssignment=='Move In'){
                        moveinQueueId=groupLst.get(0).id;
                    }
                    else if(typeOfAssignment=='Move out'){
                        moveoutQueueId=groupLst.get(0).id;
                    } 
                    else{
                        
                    }   
                    
                } 
                
            }
            
            Id unassignedHouseOffboardingCaseQueueId = GroupAndGroupMemberSelector.getIdfromName(Constants.QUEUE_UNASSIGNED_HOUSE_OFFBOARDING_CASE_QUEUE);
            
            
            
            
            
            for(Case each:CaseList){
                if(each.recordtypeid !=caseServiceRTId){
                    
                    if(each.recordtypeId ==caseOccupiedFurnishedRecordtype || each.recordtypeId ==caseOccupiedUnfurnishedRecordtype || each.recordtypeId == caseUnoccupiedFurnishedRecordtype || each.recordtypeId ==caseUnoccupiedUnfurnishedRecordtype){
                        if(mapOfHouseOffBoarding.containsKey(each.HouseForOffboarding__c) && mapOfHouseOffBoarding.get(each.HouseForOffboarding__c).Property_Manager__c !=null ){
                            each.OwnerId  = mapOfHouseOffBoarding.get(each.HouseForOffboarding__c).Property_Manager__c;
                        }
                        else{
                            caseListToUPdated.add(each);
                            houseSetToBeUpdated.add(each.HouseForOffboarding__c);
                        }
                    }
                    else if(each.recordtypeId  == IT_RecordTypeId){
                        if(mapOfHouseInternal.containsKey(each.From_House__c) && mapOfHouseInternal.get(each.From_House__c).Property_Manager__c !=null ){
                            each.OwnerId  = mapOfHouseInternal.get(each.From_House__c).Property_Manager__c;
                        }
                        else{
                            caseListToUPdated.add(each);
                            houseSetToBeUpdated.add(each.From_House__c);
                        }
                    }
                    else{
                        
                        System.debug('notoffboaring');
                        if(mapOfHouse.containsKey(each.House__c) &&  mapOfHouse.get(each.House__C).Property_Manager__c !=null ){
                            each.OwnerId  = mapOfHouse.get(each.House__c).Property_Manager__c;
                            System.debug(' each.OwnerId'+each.OwnerId);
                        }
                        else{
                            
                            caseListToUPdated.add(each);
                            houseSetToBeUpdated.add(each.House__C);
                        }
                    }
                }
            }
            system.debug('caseListToUPdated'+caseListToUPdated);
            system.debug('houseSetToBeUpdated'+houseSetToBeUpdated);
            
            if(!houseSetToBeUpdated.isEmpty()){
                houseListToBeUpdated= [Select id,Onboarding_Zone_Code__c,Property_Manager__c,APM__C  from house__c where id in:houseSetToBeUpdated];
            }
            
            if(!houseListToBeUpdated.isEmpty()){
                HouseTriggerHelper.populatePmOnHouse(houseListToBeUpdated, false);
            }
            
            for(Case each: caseListToUPdated){
                if(each.house__C != null){
                    SetofHouse1.add(each.House__C);
                }
                if(each.HouseForOffboarding__c != null){
                    SetofOnboardingHouse1.add(each.HouseForOffboarding__c);
                }
                if(each.From_House__c != null){
                    SetofInternalHouse1.add(each.From_House__c);
                }
            }
            
            if(!SetofHouse1.isEmpty()){
                List<House__C> houseList1 = [Select id,Property_Manager__c,PM_Zone_Code__c,city__c from House__C where id in:SetofHouse1];
                for(House__c each:HouseList1){
                    mapOfHouse.put(each.id,each);
                }
            }
            
            if(!SetofOnboardingHouse1.isEmpty()){
                List<House__C> houseList1 = [Select id,Property_Manager__c,PM_Zone_Code__c,city__c from House__C where id in:SetofOnboardingHouse1];
                for(House__c each:HouseList1){
                    mapOfHouseOffBoarding.put(each.id,each);
                }
            }
            
            if(!SetofInternalHouse1.isEmpty()){
                List<House__C> houseList1 = [Select id,Property_Manager__c,PM_Zone_Code__c,city__c from House__C where id in:SetofInternalHouse1];
                for(House__c each:HouseList1){
                    mapOfHouseInternal.put(each.id,each);
                }
            }
            
            system.debug('houseListToBeUpdated'+houseListToBeUpdated);
            
            if(!caseListToUPdated.isEmpty()){
                for(Case each:caseListToUPdated){
                    System.debug('caseListToUPdated');
                    if(each.recordtypeid !=caseServiceRTId){
                        if(each.recordtypeId ==caseOccupiedFurnishedRecordtype || each.recordtypeId ==caseOccupiedUnfurnishedRecordtype || each.recordtypeId == caseUnoccupiedFurnishedRecordtype || each.recordtypeId ==caseUnoccupiedUnfurnishedRecordtype){
                            if(mapOfHouseOffBoarding.containsKey(each.HouseForOffboarding__c) && mapOfHouseOffBoarding.get(each.HouseForOffboarding__c).Property_Manager__c !=null ){
                                each.OwnerId  = mapOfHouseOffBoarding.get(each.HouseForOffboarding__c).Property_Manager__c;
                            }
                            else if(mapOfHouseOffBoarding.containsKey(each.HouseForOffboarding__c) && mapOfHouseOffBoarding.get(each.HouseForOffboarding__c).city__c != null && cityToHeadId.containsKey(mapOfHouseOffBoarding.get(each.HouseForOffboarding__c).city__c)){
                                each.ownerId=cityToHeadId.get(mapOfHouseOffBoarding.get(each.HouseForOffboarding__c).city__c);
                            }
                            else{
                                each.OwnerId = unassignedHouseOffboardingCaseQueueId;  
                            }
                        }
                        else if(each.recordtypeId  == IT_RecordTypeId){
                            if(mapOfHouseInternal.containsKey(each.From_House__c) && mapOfHouseInternal.get(each.From_House__c).Property_Manager__c !=null ){
                                each.OwnerId  = mapOfHouseInternal.get(each.From_House__c).Property_Manager__c;
                            }
                            else if(mapOfHouseInternal.containsKey(each.From_House__c) && mapOfHouseInternal.get(each.From_House__c).city__c != null && cityToHeadId.containsKey(mapOfHouseOffBoarding.get(each.HouseForOffboarding__c).city__c)){
                                each.ownerId=cityToHeadId.get(mapOfHouseInternal.get(each.From_House__c).city__c);
                            }
                            else{
                                
                            }
                        }
                        else{
                            System.debug('notoffboaring');
                            if(mapOfHouse.containsKey(each.House__c) &&  mapOfHouse.get(each.House__C).Property_Manager__c !=null ){
                                each.OwnerId  = mapOfHouse.get(each.House__c).Property_Manager__c;
                                System.debug(' each.OwnerId'+each.OwnerId);
                            }
                            else if(mapOfHouse.containsKey(each.House__c) &&  mapOfHouse.get(each.House__C).city__c != null && cityToHeadId.containsKey(mapOfHouse.get(each.house__c).city__c)){
                                each.ownerId=cityToHeadId.get(mapOfHouse.get(each.House__c).city__c);
                            }
                            else{
                                if(typeOfAssignment=='Move In' && moveinQueueId!=null){
                                    each.ownerId=moveinQueueId;
                                    System.debug('****4th assignment movein queue'+ each.ownerId);
                                }
                                else if(typeOfAssignment=='Move out' && moveoutQueueId!=null){
                                    each.ownerId=moveoutQueueId;
                                    System.debug('****4th assignment moveout queue'+ each.ownerId);
                                } 
                            }
                        }
                    }
                }
            }
        }
        Catch (Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e);              
        }
    }
    
}