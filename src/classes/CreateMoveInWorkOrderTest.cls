@IsTest
public class CreateMoveInWorkOrderTest {
    public static   Id MoveIn_RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEIN).getRecordTypeId();
    public static Id MoveInWorkOrderRTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(Constants.WORKORDER_RECORD_TYPE_MOVE_IN_CHECK_READ_ONLY).getRecordTypeId();
    Public Static TestMethod Void doTest(){
        NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
        cusSet.name = 'nestaway';
        cusSet.Nestaway_URL__c = 'www.test.com';
        insert cusSet;
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        Map<ID,Account> mapAccount = new Map<Id,Account>();
        mapAccount.put(accObj.id,accObj);
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        hos.House_Owner__c=accObj.id;
        hos.OwnerId=newuser.id;        
        insert hos;
        Map<Id,House__C> houseMap = new Map<Id,House__C>();
        houseMap.put(hos.id,hos);
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        test.starttest();
        Case c=  new Case();
        c.Status='new';
        c.House__c=hos.id;
        c.ownerId=newuser.Id;
        c.AccountId=accobj.id;
        c.MoveIn_Slot_End_Time__c=system.today();
        c.MoveIn_Slot_Start_Time__c=system.today();
        c.Booked_Object_ID__c='162';
        c.Booked_Object_Type__c='House';
        c.MoveIn_Executive__c=newuser.Id;
        c.RecordTypeId= MoveIn_RecordTypeId;
        c.Settle_Amount_To_Be_deducted__c=3762;
        c.Booking_Id__c='389769';
        c.Type='MOvein';
        c.Room_Term__c=rt.id;
        c.Tenant__c=accObj.id;
        c.Contract_start_Date__c=system.today();
        c.Move_in_date__c=system.today();
        c.License_Start_Date__c=system.today();
        c.Requires_Owner_Approval__c=true;
        c.Owner_approval_status__c='Pending';
        c.Contract_End_Date__c=system.today();
        c.Documents_Verified__c=true;
        c.Tenant_Profile_Verified__c=true;
        c.Documents_Uploaded__c=true;
        insert c;
        Map<Id,Case> newMap = new  Map<Id,Case>();
        newMap.put(c.id,c);
        CreateMoveInWorkOrder.createWorkOrder(newMap,mapAccount,houseMap);
    }
    
    
}