public class OperationalProcessSelector{    
    
    public static List<Operational_Process__c> getContractVersioningFieldsLst(){

        Id operationalProcessRecTypeId = getRecordTypeIdByName(Operational_ProcessConstants.OPERATIONALPROCESS_RT_CONTRACT_VER_FIELDS);
        return [select id,Field_API_Name__c from Operational_Process__c where recordtypeid=:operationalProcessRecTypeId and Field_API_Name__c!=null];
    }       
    
    public static Id getRecordTypeIdByName(string recordTypeName){
        
        return Schema.SObjectType.Operational_Process__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
    }
}