public class CreateCasesForHouse {
    Public House__C hs{get;set;}  
    public Boolean Plumbing{get;set;}
    public Boolean Carpentry{get;set;}
    public Boolean Electrical{get;set;}
    public Boolean Others{get;set;}
    public String HouseOwner;
    public String HouseID;
    public Integer count1;
    public Integer count2;
    public Integer count3;
    public Integer count4;
    public static Id caseServiceRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_SERVICE_REQUEST).getRecordTypeId();
    public CreateCasesForHouse (ApexPages.StandardController sController) {
        Plumbing = false;
        Carpentry = false;
        Electrical = false;
        Others = false;
        count1=0;
        count2=0;
        count3=0;
        count4=0;
        hs= [Select Id,House_Owner__c,Plumbing__c,Carpentry__c,Electrical__c,Others__c from House__C  WHERE id =: ApexPages.currentPage().getParameters().get('id')];
        if(hs.id != null){
            HouseID= hs.id;
        }
        if(hs.House_Owner__c != null){
            HouseOwner = hs.House_Owner__c;
        }
        if(hs.Plumbing__c == true){
            count1++;
            Plumbing = true;
        }
        else{
            Plumbing = false;
        }
        
        if(hs.Carpentry__c == true){
            Carpentry = true;
            count2++;
        }
        else{
            Carpentry = false;
            
        }
        if(hs.Electrical__c == true){
            Electrical = true;
            count3++;
        }
        else{
            Electrical= false;
        }
        if(hs.Others__c == true){
            Others = true;
            count4++;
        }
        else{
            Others= false;
        }
    }
    public  void CreateCase(){
        system.debug('Plumbing'+Plumbing);
        system.debug('Carpentry'+Carpentry);
        system.debug('Electrical'+Electrical);
        system.debug('Others'+Others);
        system.debug('Plumbing'+count1);
        if(Plumbing == true && count1 ==0){
            Case c = new Case();
            c.RecordTypeId = caseServiceRTId;
            c.Status='open';
            c.Origin='Web';
            c.accountId= HouseOwner;
            c.House1__c =HouseID;
            c.Problem__c='a0Q0l000000LS4g';
            insert c;
            hs.Plumbing__c =true;
            update hs;
        }
        if(Carpentry == true && count2 ==0){
            Case c = new Case();
            c.RecordTypeId = caseServiceRTId;
            c.Status='open';
            c.Origin='Web';
            c.House1__c =HouseID;
            c.accountId= HouseOwner;
            c.Problem__c='a0Q0l000000LRzE';
            insert c;
            hs.Carpentry__c  =true;
            update hs;
        }
        if(Electrical == true && count3 ==0){
            Case c = new Case();
            c.RecordTypeId = caseServiceRTId;
            c.Status='open';
            c.Origin='Web';
            c.House1__c =HouseID;
            c.accountId= HouseOwner;
            c.Problem__c='a0Q0l000000LRzg';
            insert c;
            hs.Electrical__c   =true;
            update hs;
        }
        if(Others == true && count4==0){
            Case c = new Case();
            c.RecordTypeId = caseServiceRTId;
            c.Status='open';
            c.Origin='Web';
            c.House1__c =HouseID;
            c.accountId= HouseOwner;
            c.Problem__c='a0Q0l000000LRwh';
            insert c;
            hs.Others__c    =true;
            update hs;
        }
        
        
    }
}