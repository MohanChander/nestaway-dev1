@isTest
Public Class LeadTriggerHandlerTest{
    
    Public Static TestMethod void doTest(){
      Test.StartTest();
        
            City__c city = new City__c();
           city.name='Bangalore';
           insert city;
        
            Zing_API_URL__c zing = new Zing_API_URL__c();
            zing.Name          = 'Zing URL';
            zing.Auth_Token__c = 'ftLKMp_z_e3xX9Yiax-q';
            zing.Email__c      = 'unknownadmin@nestaway.com';
            zing.Tag__c        = 'OwnerAcquisition';
            zing.URL__c        = 'http://40.122.207.71/admin/zinc/lat_long?';
            
            insert zing;
            
            
            Lead objLead = new Lead();
            objLead.FirstName                  = 'Test';
            objLead.LastName                   = 'Test';
            objLead.Phone                      = '9066955369';
            objLead.MobilePhone                = '9406695578';      
            objLead.Email                      = 'Test@test.com';
            objLead.Source_Type__c             = 'Assisted';
            objLead.Area_Code__c               = '7';         
            objLead.LeadSource                 = 'City Marketing';
            objLead.City_Marketing_Activity__c = 'Roadshow';
            objLead.Street                     = 'Test';
            objLead.City                       = 'Bangalore';
            objLead.State                      = 'Karnataka';
            objLead.Status                     = 'New';
            objLead.PostalCode                 = '560078';
            objLead.Country                    = 'India';
            objLead.Company                    = 'NestAway';
            objLead.IsValidPrimaryContact__c           = True;        // Added by Poornapriya
            insert objLead;
            
            objLead.Status                     = 'House Visit Scheduled';
            objLead.House_Visit_Scheduled_Date__c = System.Now().AddDays(2);
            Update objLead;
            
        Test.StopTest();   
    } 
    
    @isTest static void testCreateEventOnLeadCreation(){
        Test.startTest();
        
        City__c city = new City__c();
        city.name='Bangalore';
        insert city;
        
        Zing_API_URL__c zing = Test_library.createZingAPIURL();
        
        Test_library.createNestAwayCustomSetting();
        
        Lead lead = Test_library.createLead();
        insert lead;
        lead = [SELECT FirstName,Phone ,LastName ,Email, OwnerId,House_Visit_Scheduled_Date__c, Lead_Owner__c,Street,City,State,PostalCode,Country FROM Lead Where Id = :lead.Id];
        
        Event newEvent =  [SELECT ID, Subject, Location,StartDateTime, EndDateTime, WhoId, OwnerId, WhatId, Type, Description, Event_Type__c
                                          		FROM Event 
                                          		WHERE WhoId  = :lead.Id];//Event_Type__c='House Visit Scheduled';
        
        String houseOwnerFullName = lead.FirstName + ' '+ lead.LastName;
        System.assertEquals('Nestaway: Meeting with Owner - '+houseOwnerFullName, newEvent.Subject);
        System.assertEquals('Meeting', newEvent.Type);
        
        List<String> descriptionArgs = new List<String>();
        descriptionArgs.add(houseOwnerFullName); // {0} - House Owner  Name
        descriptionArgs.add(lead.Phone); // {1} - Phone number
        String propertyAddress = 
            lead.Street+ '' + lead.City + ' ' +lead.State + ' '+ lead.Country + ' '+lead.PostalCode;
        descriptionArgs.add(propertyAddress); // {2} - Property Address
        descriptionArgs.add(lead.Lead_Owner__c); // {3} - Nestaway Executive: Name
        
        String description = String.format('Owner Details : \n{0}\n{1}\n{2}\n\nNestaway Executive: {3}', descriptionArgs);
        
        System.assertEquals(description, newEvent.Description);
        System.assertEquals(propertyAddress, newEvent.Location );
        System.assertEquals('House Visit Scheduled', newEvent.Event_Type__c); 
        
        System.assertEquals(lead.id,newEvent.WhoId); // Lead ID or a Contact ID
        System.assertEquals(lead.OwnerId, newEvent.OwnerId);//user id
        //newEvent.WhatId = lead.id;//Account ID or an Opportunity ID
        
        //Update only time ??? about other values
        System.assertEquals(lead.House_Visit_Scheduled_Date__c, newEvent.StartDateTime);
        Datetime startTime = Datetime.newInstance(newEvent.StartDateTime.getTime());
        DateTime newEndDateTime = startTime.addHours(1); // Add 1 Hour
        System.assertEquals(newEndDateTime, newEvent.EndDateTime);
        
        Test.stopTest();
        
    }
    
    
    @isTest static void testCreateEventOnLeadReSchedule(){
        Test.startTest();
        
        City__c city = new City__c();
        city.name='Bangalore';
        insert city;
        
        Zing_API_URL__c zing = Test_library.createZingAPIURL();
        
        Test_library.createNestAwayCustomSetting();
        
        Lead lead = Test_library.createLead();
        insert lead;
        //update lead;
        lead.House_Visit_Scheduled_Date__c = System.now().addHours(1);
        update lead;
        
        lead = [SELECT FirstName,Phone ,LastName ,Email, OwnerId,House_Visit_Scheduled_Date__c, Lead_Owner__c,Street,City,State,PostalCode,Country FROM Lead Where Id = :lead.Id];
       
        Event newEvent =  [SELECT ID, Subject, Location,StartDateTime, EndDateTime, WhoId, OwnerId, WhatId, Type, Description, Event_Type__c
                                          		FROM Event 
                                          		WHERE WhoId  = :lead.Id];//Event_Type__c='House Visit Scheduled';
        
      
        String houseOwnerFullName = lead.FirstName + ' '+ lead.LastName;
        System.assertEquals('Nestaway: Meeting with Owner - '+houseOwnerFullName, newEvent.Subject);
        System.assertEquals('Meeting', newEvent.Type);
        
        List<String> descriptionArgs = new List<String>();
        descriptionArgs.add(houseOwnerFullName); // {0} - House Owner  Name
        descriptionArgs.add(lead.Phone); // {1} - Phone number
        String propertyAddress = 
            lead.Street+ '' + lead.City + ' ' +lead.State + ' '+ lead.Country + ' '+lead.PostalCode;
        descriptionArgs.add(propertyAddress); // {2} - Property Address
        descriptionArgs.add(lead.Lead_Owner__c); // {3} - Nestaway Executive: Name
        
        String description = String.format('Owner Details : \n{0}\n{1}\n{2}\n\nNestaway Executive: {3}', descriptionArgs);
        
        System.assertEquals(description, newEvent.Description);
        System.assertEquals(propertyAddress, newEvent.Location );
        System.assertEquals('House Visit Scheduled', newEvent.Event_Type__c); 
        
        System.assertEquals(lead.id,newEvent.WhoId); // Lead ID or a Contact ID
        System.assertEquals(lead.OwnerId, newEvent.OwnerId);//user id
        //newEvent.WhatId = lead.id;//Account ID or an Opportunity ID
        
        //Update only time ??? about other values
        System.assertEquals(lead.House_Visit_Scheduled_Date__c, newEvent.StartDateTime);
        Datetime startTime = Datetime.newInstance(newEvent.StartDateTime.getTime());
        DateTime newEndDateTime = startTime.addHours(1); // Add 1 Hour
        System.assertEquals(newEndDateTime, newEvent.EndDateTime);
        
        Test.stopTest();
        
    }
}