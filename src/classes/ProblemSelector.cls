public class ProblemSelector {
     //Query for all the Problem
    public static List<Problem__c > getProblemList(){
    return [select Id,Queue__c, Scheduled_Visit_Required__c, Priority__c, Name,
            SLA_1__c, SLA_2__c, SLA_3__c, SLA_4__c, Work_Type__c, Work_Type__r.EstimatedDuration
            from Problem__c];
  }

  //Query for Problems from input Set
  public static List<Problem__c> getProblemListFromSet(Set<Id> caseIdSet){
    return [select Id,Queue__c, Scheduled_Visit_Required__c, Priority__c, Name,
            SLA_1__c, SLA_2__c, SLA_3__c, SLA_4__c
            from Problem__c where Id =: caseIdSet];
  }
}