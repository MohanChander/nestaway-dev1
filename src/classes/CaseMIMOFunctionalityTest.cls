@Istest
public class CaseMIMOFunctionalityTest {
    public static   Id MoveIn_RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEIN).getRecordTypeId();
    public static  Id RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Furnished House Onboarding').getRecordTypeId();
    public static  Id internaltRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_INTERNAL_TRANSFER ).getRecordTypeId();
    public static Id MoveInWorkOrderRTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(Constants.WORKORDER_RECORD_TYPE_MOVE_IN_CHECK_READ_ONLY).getRecordTypeId();
    public static    Id MoveOutWorkOrderRTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(Constants.WORKORDER_RECORD_TYPE_MOVE_Out_CHECK_READ_ONLY).getRecordTypeId();
    public static   Id mimohicTypeId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get(Constants.CHECKLIST_RT_TYPE_MIMO_CHECK ).getRecordTypeId();
    public static  Id mimoricRTId = Schema.SObjectType.Room_Inspection__c.getRecordTypeInfosByName().get(Constants.ROOM_INSP_RT_MOVE_OUT_CHECK).getRecordTypeId();
    public static Id mimobicRTId = Schema.SObjectType.Bathroom__c.getRecordTypeInfosByName().get(Constants.BATHROOM_INSP_RT_TYPE_MIMO_CHECK ).getRecordTypeId();
    public static   Id MoveOutRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEOUT).getRecordTypeId();
    public static    Id caseSettlement_RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Case_Settlement).getRecordTypeId();
    
    public static  Id contractRecordTypeId = Schema.SObjectType.Contract.getRecordTypeInfosByName().get(Constants.tenantContract_RecordType).getRecordTypeId();
   Public Static TestMethod Void doTest123(){
        Case c=  createmoveout();
        case c1 = creatSettlement();
        c1.ParentId = c.id;
        update c1;
        c.Settle_Amount_To_Be_deducted__c = 10;
        c.status = Constants.CASE_STATUS_MOVEDOUTWITHISSUE;
        c.Bed_Released_API_Message__c = 'sdgdy';
        c.Bed_Released_API_Status__c = 'kdjkd';
        c.Bed_Released_API_Status__c ='True';
        c.Move_Out_Type__c = Constants.CASE_MOVEOUT_CANCELLATION;
        c.Kotak_API_Response__c = 'Payment Initiated';
        update c;
       Map<id,Case> oldmap =  new Map<Id,Case>();
	   oldmap.put(c.id,c);
      c.Kotak_API_Response__c = 'Payment Successful';
       update c;
        Map<id,Case> newmap =  new Map<Id,Case>();
        newmap.put(c.id,c);
        Set<id> caseid= new Set<id>();
        caseid.add(c.id);
              
        
    }
    
     Public Static TestMethod Void doTest1234(){
        Case c=  createmoveout1();
        case c1 = creatSettlement();
        c1.ParentId = c.id;
        update c1;
        c.Settle_Amount_To_Be_deducted__c = 10;
        c.status = Constants.CASE_STATUS_MOVEDOUTWITHISSUE;
        c.Bed_Released_API_Message__c = 'sdgdy';
        c.Bed_Released_API_Status__c = 'kdjkd';
        c.Bed_Released_API_Status__c ='True';
        update c;
        Map<id,Case> newmap =  new Map<Id,Case>();
        newmap.put(c.id,c);
        Set<id> caseid= new Set<id>();
        caseid.add(c.id);
             
        
    }
    
    Public Static TestMethod Void doTest(){
        NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
        cusSet.name = 'nestaway';
        cusSet.Nestaway_URL__c = 'www.test.com';
        insert cusSet;
        Case c=  createmoveincase();
         Map<id,Case> oldmap =  new Map<Id,Case>();
	       oldmap.put(c.id,c);
        c.Stage__c = 'Document Verification';
         update c;
         Map<id,Case> newmap =  new Map<Id,Case>();
        newmap.put(c.id,c);
         CaseMIMOFunctionality.afterUpdateMIMO(newMap,oldMap);
        
    }

    
    
    Public Static TestMethod Void doTest1(){
        NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
        cusSet.name = 'nestaway';
        cusSet.Nestaway_URL__c = 'www.test.com';
        insert cusSet;
        Case c=  createmoveout();
       
        Map<Id,Case> newMap = new  Map<Id,Case> ();
        newMap.put(c.id,c);
        set <id> massClosedParentIds= new Set<id>();
        massClosedParentIds.add(c.id);
        Map<Id,Id> parentToCaseIdMap = new  Map<Id,Id>();
        parentToCaseIdMap.put(c.id,c.id);
        CaseMIMOFunctionality.closeParentAndDeleteRelated(massClosedParentIds);
        CaseMIMOFunctionality.tenantCancelledMoveOut(c.id,'new');
        CaseMIMOFunctionality.bedReleaseOnInspectionCOmplete(c.id,'new');
        CaseMIMOFunctionality.createMoveOutWorkOrderFOrCAllCentre(newmap);
        CaseMIMOFunctionality.checkMoveInDateValidation(newmap,parentToCaseIdMap,MoveOutRecordTypeId);
        CaseMIMOFunctionality.createTaskMoveOutSchedule(newMap);
    }
    Public Static TestMethod Void doTest2(){
        NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
        cusSet.name = 'nestaway';
        cusSet.Nestaway_URL__c = 'www.test.com';
        insert cusSet;
        Case c=  creatSettlement();
    }
     Public Static TestMethod Void doTes(){
     
                Id caseSettlement_RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Case_Settlement).getRecordTypeId();

        Id MoveIn_RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEIN).getRecordTypeId();
        Id RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Furnished House Onboarding').getRecordTypeId();
        Id MoveOutRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEOUT).getRecordTypeId();
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        insert hos;
         map<id,house__c> houseMap = new map<id,house__c>();
         houseMap.put(hos.id,hos);
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
           Bank_Detail__c bc = new Bank_Detail__c ();
        bc.IFSC_Code__c ='KKBK0000045';
        bc.Account_Number__c ='98765431245678';
        bc.Related_Account__c= accObj.id;
        insert bc;
         MAp<Id,Account> mapofAcount = new MAp<id,Account>();
         mapofAcount.put(accObj.id,accObj);
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        /*
        Tenancy__c b= new Tenancy__c();
        b.Tenant__c=accObj.Id;
        insert b;
        */
        Tenancy__c b = new Tenancy__c();
        b.Tenant__c=accObj.Id;
        b.house__c=hos.id;
        insert b;
           Test.startTest();
        Case c=  new Case();
        c.Status='New';
        c.House__c=hos.Id;
        c.ownerId=newuser.Id;
        c.MoveOut_Slot_End_Time__c=System.today();
        c.Moveout_Slot_Start_Time__c=System.today();
        c.Move_Out_Date__c=System.today();
        c.Booked_Object_ID__c='162';
        c.Move_Out_Status__c='In Progress';
        c.Move_Out_Type__c='Rent Default';
        c.Booked_Object_Type__c='Room';
        c.MoveOut_Executive__c=newuser.Id;
        c.RecordTypeId= MoveOutRecordTypeId;
        c.Booking_Id__c='389769';
        c.Type='MOveOUt';
        c.Room_Term__c=rt.id;
        c.Tenant__c=accObj.id;
        c.Contract_End_Date__c=system.today();
        insert c;
        List<Case>  cList =  new List<Case>();
        cList.add(c);
        Map<id,Case> newmap =  new Map<Id,Case>();
        newmap.put(c.id,c);
      c.Status='Move Out Inspection Pending';
        c.MoveIn_Slot_Start_Time__c=System.today()+1;
        update c;
         c.Status='Moved-Out all Ok';
       // update c;
          c.Status='Moved Out with Issues';
       // update c;
        Map<id,Case> oldmap =  new Map<Id,Case>();
        newmap.put(c.id,c);
        Set<id> caseid= new Set<id>();
        caseid.add(c.id);
          Org_Param__c nestURL = Org_Param__c.getOrgDefaults();
        insert nesturl;
      CaseMIMOFunctionality.createTasks(newMap,mapofAcount,houseMap);
         CaseMIMOFunctionality.triggerAutoOwnerApprovalConfirmationInWebApp(c.id);
        Test.stopTest();
    }
    
    public static CAse createmoveincase()
    {    
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        accObj.Ekyc_Verified__c = true;
        insert accObj;
         Bank_Detail__c bc = new Bank_Detail__c ();
        bc.IFSC_Code__c ='KKBK0000045';
        bc.Account_Number__c ='98765431245678';
        bc.Related_Account__c= accObj.id;
        insert bc;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        hos.House_Owner__c=accObj.id;
        hos.OwnerId=newuser.id;        
        insert hos;
        
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        test.starttest();
        Case c=  new Case();
        c.Status='new';
        c.stage__c ='new';
        c.Booking_Cancelled_reason__c = 'this is test';
        c.House__c=hos.id;
        c.ownerId=newuser.Id;
        c.AccountId=accobj.id;
        c.Documents_Verified__c= true;
        c.Documents_Uploaded__c= true;
        //c.Documents_Complete__c = true;
        c.MoveIn_Slot_End_Time__c=system.today();
        c.MoveIn_Slot_Start_Time__c=system.today();
        c.Booked_Object_ID__c='162';
        c.Booked_Object_Type__c='House';
        c.MoveIn_Executive__c=newuser.Id;
        c.RecordTypeId= MoveIn_RecordTypeId;
        c.Settle_Amount_To_Be_deducted__c=3762;
        c.Booking_Id__c='389769';
        c.Type='MOvein';
        c.Room_Term__c=rt.id;
        c.Tenant__c=accObj.id;
        c.Contract_start_Date__c=system.today();
        c.Move_in_date__c=system.today();
        c.License_Start_Date__c=system.today();
        c.Requires_Owner_Approval__c=true;
        c.Owner_approval_status__c='Pending';
        c.Contract_End_Date__c=system.today();
        c.Documents_Verified__c=true;
        c.Tenant_Profile_Verified__c=true;
        c.Documents_Uploaded__c=true;
        insert c;
        return c;
        
    }
    public static CAse createmoveout()
    {    
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
         Bank_Detail__c bc = new Bank_Detail__c ();
        bc.IFSC_Code__c ='KKBK0000045';
        bc.Account_Number__c ='98765431245678';
        bc.Related_Account__c= accObj.id;
        insert bc;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        hos.OwnerId=newuser.id;        
        insert hos;
        
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        
        case cint= new case();
        cint.RecordTypeId=internaltRecordTypeId;
        cint.Tenant__c=accobj.id;
        cint.AccountId=accobj.id;
        cint.status='Progress';
        cint.type='Internal Transfer';
        cint.Tenant_Type__c='boys';
        cint.SD_Status__c='pending';
        cint.From_House__c=hos.id;
        cint.Origin='web';
        insert cint;
        
        test.starttest();
        Case c=  new Case();
        c.parentid=cint.id;
        c.Status='new';
        c.House__c=hos.Id;
        c.ownerId=newuser.Id;
        c.Move_in_date__c=System.today()-2;
        c.MoveOut_Slot_End_Time__c=System.today();
        c.Moveout_Slot_Start_Time__c=System.today();
        c.Move_Out_Date__c=System.today();
        c.Booked_Object_ID__c='162';
        c.Move_Out_Status__c='In Progress';
        c.Move_Out_Type__c='SD Default';
        c.Booked_Object_Type__c='House';
        c.MoveOut_Executive__c=newuser.Id;
        c.RecordTypeId= MoveOutRecordTypeId;
        c.Booking_Id__c='389769';
        c.Type='MoveOut';
        
        c.Tenant__c=accObj.id;
        
        c.Contract_End_Date__c=system.today();
        insert c;
        return c;
        
    }
    
    public static CAse creatSettlement()
    {    
        NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
        cusSet.name = 'nestaway';
        cusSet.Nestaway_URL__c = 'www.test.com';
        insert cusSet;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        insert hos;
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
         Bank_Detail__c bc = new Bank_Detail__c ();
        bc.IFSC_Code__c ='KKBK0000045';
        bc.Account_Number__c ='98765431245678';
        bc.Related_Account__c= accObj.id;
        insert bc;
        Set<id> setoftennant = new Set<id>();
        setoftennant.add(accObj.id);
        Photo__c ph= new Photo__c();
        insert ph;
        
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        
        Tenancy__c b = new Tenancy__c();
        b.Tenant__c=accObj.Id;
        b.house__c=hos.id;
        insert b;
        Case c=  new Case();
        c.Status='new';
        c.House__c=hos.Id;
        c.MoveOut_Slot_End_Time__c=System.today();
        c.Moveout_Slot_Start_Time__c=System.today();
        c.Move_Out_Date__c=System.today();
        c.Booked_Object_ID__c='162';
        c.Move_Out_Status__c='In Progress';
        c.Move_Out_Type__c='Rent Default';
        c.Booked_Object_Type__c='Room';
        c.RecordTypeId= caseSettlement_RecordTypeId;
        c.Booking_Id__c='389769';
        c.Dashboard_Update_Required__c='Yes';
        c.Dashboard_Update_Status__c='Updated Successfully';
        c.Room_Term__c=rt.id;        
        c.Tenant__c=accObj.id;
        c.Net_Amount_to_Be_Refunded__c=82;
        c.Contract_End_Date__c=system.today();
        insert c;
        return c;        
    }
         public static CAse createmoveout1()
    {    
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        Bank_Detail__c bc = new Bank_Detail__c ();
        bc.IFSC_Code__c ='KKBK0000045';
        bc.Account_Number__c ='98765431245678';
        bc.Related_Account__c= accObj.id;
        insert bc;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        hos.OwnerId=newuser.id;        
        insert hos;
        
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        
        
        
        test.starttest();
        Case c=  new Case();
        c.Status='new';
        c.House__c=hos.Id;
        c.ownerId=newuser.Id;
        c.Move_in_date__c=System.today()-2;
        c.MoveOut_Slot_End_Time__c=System.today();
        c.Moveout_Slot_Start_Time__c=System.today();
        c.Move_Out_Date__c=System.today();
        c.Booked_Object_ID__c='162';
        c.Move_Out_Status__c='In Progress';
        c.Move_Out_Type__c='SD Default';
        c.Booked_Object_Type__c='House';
        c.MoveOut_Executive__c=newuser.Id;
        c.RecordTypeId= MoveOutRecordTypeId;
        c.Booking_Id__c='389769';
        c.Type='MoveOut';
        
        c.Tenant__c=accObj.id;
        
        c.Contract_End_Date__c=system.today();
        insert c;
        return c;
        
    }
    
}