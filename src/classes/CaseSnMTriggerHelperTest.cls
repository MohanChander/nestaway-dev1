@isTest
public class CaseSnMTriggerHelperTest {

	@isTest
	public static void ownerInclusionCaseCommentTest(){

		Case c = Test_library.createServiceRequestCase();

		//update the Owner Inclusion of the Case
		c.Owner_Inclusion__c = 'true';
		c.Owner_Inclusion_comment__c = 'Onwer need to bare the charges';
		c.Owner_Share_approx__c = 1000;
		update c;

		//update the Owner Approval Status of the Case
		c.Owner_approval_status__c = 'Approved';
		update c;		

	}	


	@isTest
	public static void ownerApprovalStatusChangeCaseCommentTest(){

		Case c = Test_library.createServiceRequestCase();

		//update the Owner Approval Status of the Case
		c.Owner_approval_status__c = 'Approved';
		update c;

		//change the Owner Approval Status of the Case
		c.Owner_approval_status__c = 'Rejected';
		update c;

	}


	@isTest
	public static void updateRequestorExpectedResolutionTimeTest(){

		Case c = Test_library.createServiceRequestCase();

		//change the Owner Approval Status of the Case
		c.Requestor_Expected_Resolution_Time__c = System.now();
		c.Requestor_Esclation_Level__c = '1';
		update c;

	}	


	@isTest
	public static void CreateSettlementCaseTest(){

		Case c = Test_library.createServiceRequestCase();

		//change the Owner Approval Status of the Case
		c.Status = 'Closed';
		update c;

	}	


	@isTest
	public static void caseResolutionValidatorTest(){

		Case c = Test_library.createServiceRequestCase();

		//change the Owner Approval Status of the Case
		c.Status = 'Resolved';
		update c;

	}		


	@isTest
	public static void HrmTaskForService(){

		Case c = Test_library.createServiceRequestCase();

		//change the Owner Approval Status of the Case
		c.Status = 'Waiting on Owner';
		update c;

	}		

}