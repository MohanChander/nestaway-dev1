public class ContractTriggerHelper
{
    public static boolean CheckOppStage = false;
    //Added by baibhav to Upadte  Contract Billing addres and shipping addres from Account
    public static void ContarctAddressFormAccount(List<contract> contlist)
    {
        try{
            // Chandu: adding condition to update only in owner contract.
            
            Id ownerConRecordTypeId=Schema.SObjectType.Contract.getRecordTypeInfosByName().get(Constants.CONTRACT_RT_OWNER).getRecordTypeId();
            
            //End
            
            
            Set<id> accIdSet=new Set<id>();
            Map<id,Account> accMap=new Map<id,Account>();
            for(contract ct:contlist)
            {
                accIdSet.add(ct.Accountid);
            }
            if(!accIdSet.isEmpty())
            {
                accMap=AccountSelector.getAccountMapfromAccId(accIdSet);
            }
            for(contract ct:contlist)
            {
                // chandu : adding condition to update only in owner contract.
                
                if(ct.recordtypeId==null || ct.recordtypeId==ownerConRecordTypeId){       
                    if(accMap.containsKey(ct.accountid))
                    {
                        if(accMap.get(ct.accountid).BillingCity!=null)
                        {
                            ct.BillingCity=accMap.get(ct.accountid).BillingCity;
                        }
                        if(accMap.get(ct.accountid).BillingCountry!=null)
                        {
                            ct.BillingCountry=accMap.get(ct.accountid).BillingCountry;
                        }
                        if(accMap.get(ct.accountid).BillingCountryCode!=null)
                        {
                            ct.BillingCountryCode=accMap.get(ct.accountid).BillingCountryCode;
                        }
                        if(accMap.get(ct.accountid).BillingPostalCode!=null)
                        {
                            ct.BillingPostalCode=accMap.get(ct.accountid).BillingPostalCode;
                        }
                        if(accMap.get(ct.accountid).BillingState!=null)
                        {
                            ct.BillingState=accMap.get(ct.accountid).BillingState;
                        }
                        if(accMap.get(ct.accountid).BillingStateCode!=null)
                        {
                            ct.BillingStateCode=accMap.get(ct.accountid).BillingStateCode;
                        }
                        if(accMap.get(ct.accountid).BillingStreet!=null)
                        {
                            ct.BillingStreet=accMap.get(ct.accountid).BillingStreet;
                        }
                        if(accMap.get(ct.accountid).BillingGeocodeAccuracy!=null)
                        {
                            ct.BillingGeocodeAccuracy=accMap.get(ct.accountid).BillingGeocodeAccuracy;
                        }
                        if(accMap.get(ct.accountid).ShippingCity!=null)
                        {
                            ct.ShippingCity=accMap.get(ct.accountid).ShippingCity;
                        }
                        if(accMap.get(ct.accountid).ShippingCountry!=null)
                        {
                            ct.ShippingCountry=accMap.get(ct.accountid).ShippingCountry;
                        }
                        if(accMap.get(ct.accountid).ShippingCountryCode!=null)
                        {
                            ct.ShippingCountryCode=accMap.get(ct.accountid).ShippingCountryCode;
                        }
                        if(accMap.get(ct.accountid).ShippingGeocodeAccuracy!=null)
                        {
                            ct.ShippingGeocodeAccuracy=accMap.get(ct.accountid).ShippingGeocodeAccuracy;
                        }
                        if(accMap.get(ct.accountid).ShippingPostalCode!=null)
                        {
                            ct.ShippingPostalCode=accMap.get(ct.accountid).ShippingPostalCode;
                        }
                        if(accMap.get(ct.accountid).ShippingState!=null)
                        {
                            ct.ShippingState=accMap.get(ct.accountid).ShippingState;
                        }
                        if(accMap.get(ct.accountid).ShippingStateCode!=null)
                        {
                            ct.ShippingStateCode=accMap.get(ct.accountid).ShippingStateCode;
                        }
                        if(accMap.get(ct.accountid).ShippingStreet!=null)
                        {
                            ct.ShippingStreet=accMap.get(ct.accountid).ShippingStreet;
                        }
                        
                    }
                } 
            }
        } Catch(Exception e) {
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e,'ContarctAddressFormAccount');
        }
        
    }
public static void ContratStatusTime(List<Contract> contList, Map<id,contract> oldMap){
        try{
            for(Contract con:contList){
                
                if(con.status==Constants.CONTRACT_STAGE_DRAFT && oldMap.get(con.id).status!=con.status){
                    con.Draft_Time__c=System.Now();
                }
                else if(con.status==Constants.CONTRACT_STAGE_SMAPLE && oldMap.get(con.id).status!=con.status){
                    con.Sample_Contract_Time__c=System.Now();   
                }
                else if(con.status==Constants.CONTRACT_STAGE_FINAL && oldMap.get(con.id).status!=con.status){
                    con.Final_Contract_Time__c=System.Now();
                }
                else if(con.status==Constants.CONTRACT_STAGE_FINAL_DOWN && oldMap.get(con.id).status!=con.status){
                    con.Final_Contract_Downloaded_Time__c=System.Now();
                }
                else if(con.status==Constants.CONTRACT_STAGE_FINAL_PRINT && oldMap.get(con.id).status!=con.status){
                    con.Final_Contract_Printed_time__c=System.Now();
                }
                else if(con.status==Constants.CONTRACT_STAGE_FINAL_HAND && oldMap.get(con.id).status!=con.status){
                    con.Final_Contract_handed_to_Sales_Time__c=System.Now();
                }

                if(con.Approval_Status__c==Constants.CONTRACT_APP_STATUS_AW_ZAM_APP && oldMap.get(con.id).Approval_Status__c!=con.Approval_Status__c){
                    con.Awaiting_ZM_Approval_Time__c=System.Now();   
                }
                else if(con.Approval_Status__c==Constants.CONTRACT_APP_STATUS_APP_ZM && oldMap.get(con.id).Approval_Status__c!=con.Approval_Status__c){
                    con.Approved_by_ZM_Time__c=System.Now();
                }
                else if(con.Approval_Status__c==Constants.CONTRACT_APP_STATUS_REJ_ZM && oldMap.get(con.id).Approval_Status__c!=con.Approval_Status__c){
                    con.Rejected_by_ZM_Time__c=System.Now();
                }
                else if(con.Approval_Status__c==Constants.CONTRACT_APP_STATUS_SAM_CONT_APP_OWNER && oldMap.get(con.id).Approval_Status__c!=con.Approval_Status__c){
                    con.Sample_Contract_Approved_by_Owner_Time__c=System.Now();
                }
                else if(con.Approval_Status__c==Constants.CONTRACT_APP_STATUS_SAM_CONT_REJ_OWNER && oldMap.get(con.id).Approval_Status__c!=con.Approval_Status__c){
                    con.Sample_Contract_Rejected_by_Owner_Time__c=System.Now();
                }
                else if(con.Approval_Status__c==Constants.CONTRACT_APP_STATUS_MANULLY_APP_ZM && oldMap.get(con.id).Approval_Status__c!=con.Approval_Status__c){
                    con.Manually_Approved_by_ZM_Time__c=System.Now();
                }
                else if(con.Approval_Status__c==Constants.CONTRACT_APP_STATUS_AW_HRM_APP && oldMap.get(con.id).Approval_Status__c!=con.Approval_Status__c){
                    con.Awaiting_HRM_Approval_Time__c=System.Now();
                }
                else if(con.Approval_Status__c==Constants.CONTRACT_APP_STATUS_HRM_APP && oldMap.get(con.id).Approval_Status__c!=con.Approval_Status__c){
                    con.Approved_by_HRM_Time__c=System.Now();
                }
                else if(con.Approval_Status__c==Constants.CONTRACT_APP_STATUS_HRM_REJ && oldMap.get(con.id).Approval_Status__c!=con.Approval_Status__c){
                    con.Rejected_by_HRM_Time__c=System.Now();
                }
                else if(con.Approval_Status__c==Constants.CONTRACT_APP_STATUS_APP_CENTAL && oldMap.get(con.id).Approval_Status__c!=con.Approval_Status__c){
                    con.Approved_by_Central_Team_Time__c=System.Now();
                }
                else if(con.Approval_Status__c==Constants.CONTRACT_APP_STATUS_REJ_CENTAL && oldMap.get(con.id).Approval_Status__c!=con.Approval_Status__c){
                    con.Rejected_by_Central_Team_Time__c=System.Now();
                }
                else if(con.Approval_Status__c==Constants.CONTRACT_APP_STATUS_OWNER_APP && oldMap.get(con.id).Approval_Status__c!=con.Approval_Status__c){
                    con.Approved_by_Owner_Time__c=System.Now();
                }
                else if(con.Approval_Status__c==Constants.CONTRACT_APP_STATUS_OWNER_REJ && oldMap.get(con.id).Approval_Status__c!=con.Approval_Status__c){
                    con.Rejected_by_Owner_Time__c=System.Now();
                }
            } 
            
        } Catch(Exception e) {
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e,'Contract stage time');
        }
    }
    // added by deepak 
    public static void chnageOpportunityFurnishingType(List<Contract> contList){
        system.debug('chnageOpportunityFurnishingType');
        Set<Id> SetofCOnt = new Set<Id>();
        Set<Id> setOppId = new Set<ID>();
        List<Opportunity> oppList = new List<Opportunity>();
        List<Contract> conList = new List<Contract>();
        Map<String,String> mapOfopp = new Map<String,String>();
        List<Opportunity> oppListUpdate = new List<Opportunity>();
        for(Contract each: contList){
            SetofCOnt.add(each.id);
            setOppId.add(Each.Opportunity__c);
        }
        system.debug('setOppId'+setOppId);
        
        try{
            if(!SetofCOnt.isEmpty()){
                conList = [Select id,Furnishing_Type__c,Opportunity__c from Contract where id in: SetofCOnt];
            }
            system.debug('conList'+conList);
            
            if(!conList.isEmpty()){
                for(Contract each : conList){
                    mapOfopp.put(each.Opportunity__c,each.Furnishing_Type__c); 
                }
            }
            system.debug('mapOfopp'+mapOfopp);
            
            if(!setOppId.isEmpty()){
                oppList = [Select id,Furnishing_Type__c from Opportunity where id in :setOppId];
            }
            if(CheckOppStage == false){
                if(!oppList.isEmpty()){
                    for(Opportunity each: oppList){
                        each.Furnishing_Type__c= mapOfopp.get(each.id);
                        oppListUpdate.add(each);
                    }
                }
                if(!oppListUpdate.isEmpty()){
                    CheckOppStage = true;
                    update oppListUpdate;
                    CheckOppStage = false;
                }
            }
            system.debug('oppListUpdate'+oppListUpdate);
        }
        Catch(Exception e) {
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e,'Contract stage time');
        }
    }
    // Added by deepak 
    // To update insurance to send a api
    public static void sendApiCallForInsurance(List<Contract> contList){
        Set<ID> setofHouse = new Set<ID>();
        List<House__c> houseList = new List<House__C>();
        List<Insurance__C> insList = new List<Insurance__C>();
        try{
            for(Contract each : ContList){
                if(each.house__c != null){
                    setofHouse.add(each.House__c);
                }
            }
            if(!setofHouse.isEmpty()){
                houseList = [Select id,(Select id,status__c from Insurances__r where status__C ='new' and LOI__c =  false ) from House__C where id in:setofHouse];
                if(!houseList.isEmpty()){
                    for(House__C each : houseList){
                        for(Insurance__c  ins :  each.Insurances__r){
                            ins.status__c = Constants.INSURANCE_STATUS_TYPE_TO_BE_QUEUED; 
                            ins.LOI__c = true;
                            insList.add(ins);
                        }
                    }
                }
                if(!insList.isEmpty()){
                    update insList;
                }
            }
        }Catch(Exception e) {
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e,'insurance from contract');
        }
    }
    /****************************************************************************************
Added by Baibhav Kumar
Purpose : for Populating Hrm on contract as Approver for Final  contract Approving
Created on: 11/7/2017

*****************************************************************************************/
    public static void HrmforApproval(list<Contract> newList){
        try{
            List<GroupMember> grpmbList=new List<GroupMember>();
            
            Map<id,Integer> usrMapWithCount=new Map<id,Integer>();
            
            grpmbList =[SELECT GroupId,Id,UserOrGroupId,group.name FROM GroupMember where group.name=:label.Contract_HRM_Approval_Queue_name];
            
            if(!grpmbList.isEmpty()){
                for(GroupMember grp:grpmbList){
                    usrMapWithCount.put(grp.UserOrGroupId,0);
                }
            }
            
            System.debug('***User'+usrMapWithCount);
            List<Contract> contlist=new List<Contract>(); 
            List<AggregateResult>  agrresult=new List<AggregateResult>();
            
            if(!usrMapWithCount.keySet().isEmpty()){
                agrresult=[Select hrm__c, Count(id) from contract where HRM__c=:usrMapWithCount.keySet() and  Status='Final Contract' 
                           and (Approval_Status__c=:ContractConstants.CONTRACT_APP_STATUS_AW_HRM_APP or Approval_Status__c=:ContractConstants.CONTRACT_APP_STATUS_MANULLY_APP_ZM 
                                or Approval_Status__c=:ContractConstants.CONTRACT_APP_STATUS_SAM_CONT_APP_OWNER) and RecordType.name='Owner Contract' group by HRM__c];
            }
            
            System.debug('***aggreatmap'+agrresult);
            
            for(AggregateResult ar:agrresult){
                Integer noOfCont = 0;
                if((Integer)ar.get('expr0') == null){
                    noOfCont = 0;
                } else {
                    noOfCont = (Integer)ar.get('expr0');
                }
                usrMapWithCount.put((Id)ar.get('HRM__c'),noOfCont);
            }
            
            System.debug('***User1map'+usrMapWithCount);
            
            
            for(Contract ct:newList){
                
                if(!usrMapWithCount.keySet().isEmpty()){
                    
                    List<AssignmentWrapper> awList = new List<AssignmentWrapper>();
                    for(Id userId: usrMapWithCount.keySet()){
                        awList.add(new AssignmentWrapper(userId, usrMapWithCount.get(userId)));
                    }
                    
                    awList.sort();
                    ct.HRM__c  = awList[0].UserId; 
                    
                    Integer noOfCont = usrMapWithCount.get(awList[0].UserId);
                    usrMapWithCount.put(ct.HRM__c, ++noOfCont);
                }
                
            }
        } Catch(Exception e) {
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e,'HRM Assiignment for contract Approval');
        }
        
        
    }
    /* Wrapper Class for sorting the least number of cases assigned to the Agent */
    public class AssignmentWrapper implements Comparable{
        public Id userId{set; get;}
        public Integer noOfCases {set; get;}
        
        public AssignmentWrapper(Id UserId, Integer noOfCases){
            this.userId = userId;
            this.noOfCases = noOfCases;
        }
        
        // Implement the compareTo() method
        public Integer compareTo(Object compareTo) {
            AssignmentWrapper compareToObj = (AssignmentWrapper)compareTo;
            if (noOfCases == compareToObj.noOfCases) return 0;
            if (noOfCases > compareToObj.noOfCases) return 1;
            return -1;        
        }       
    } 
    // added by deepak
    // 
    public static void createWorkOrderForOwner(List<Contract> contList){
        Id eSignedRecordTypeId=Schema.SObjectType.WorKOrder.getRecordTypeInfosByName().get(WorkOrder_Constants.WORKORDER_RT_E_SIGNED).getRecordTypeId();
        List<WorkOrder> woList = new List<workOrder>();
        Map<Id,Case> mapOfCase = new Map<Id,Case>();
        List<Case> caseList  = new List<Case>();
        Set<Id> setOfCase = new Set<Id>();
        for(Contract each : contList){
            if(each.MoveIn_Case__c != null){
                setOfCase.add(each.MoveIn_Case__c);
            }
        }
        if(!setOfCase.isEmpty()){
            caseList  = [Select id,OwnerId from Case where id in:setOfCase];
            if(!caseList.isEmpty()){
                for(Case each : caseList){
                    mapOfCase.put(each.id,each);
                }
            }
        }
        
        for(Contract each : contList){
            WorKOrder wo = new WorkOrder();
            wo.RecordtypeId  = eSignedRecordTypeId;
            wo.caseId =  each.MoveIn_Case__c;
            if(mapOfCase.containsKey(each.MoveIn_Case__c) && mapofCase.get(each.MoveIn_Case__c).OwnerId != null){
                wo.OwnerId = mapofCase.get(each.MoveIn_Case__c).OwnerId ;
            }
            wo.AccountId = each.AccountId;
            wo.house__c = each.house__c;
            wo.Type_of_WO__c = WorkOrder_Constants.CONTRACT_SENT_FOR_OWNER_SIGNATURE;
            wo.StartDate  = system.now();
			wo.Contract__c  = each.id;
            wo.Subject= 'Contract should be signed this contract';
            wo.Description  = 'Contract should be signed this contract';
            woList.add(wo);
        }
        if(!woList.isEmpty()){
            insert woList;
        }
    }
    public static void createWorkOrderForTenant(List<Contract> contList){
        Id  eSignedRecordTypeId=Schema.SObjectType.WorKOrder.getRecordTypeInfosByName().get(WorkOrder_Constants.WORKORDER_RT_E_SIGNED).getRecordTypeId();
        List<WorkOrder> woList = new List<workOrder>();
        Map<Id,Case> mapOfCase = new Map<Id,Case>();
        List<Case> caseList  = new List<Case>();
        Set<Id> setOfCase = new Set<Id>();
        for(Contract each : contList){
            if(each.MoveIn_Case__c != null){
                setOfCase.add(each.MoveIn_Case__c);
            }
        }
        if(!setOfCase.isEmpty()){
            caseList  = [Select id,OwnerId from Case where id in:setOfCase];
            if(!caseList.isEmpty()){
                for(Case each : caseList){
                    mapOfCase.put(each.id,each);
                }
            }
        }
        for(Contract each : contList){
            WorKOrder wo = new WorkOrder();
            wo.RecordtypeId  = eSignedRecordTypeId;
            if(mapOfCase.containsKey(each.MoveIn_Case__c) && mapofCase.get(each.MoveIn_Case__c).OwnerId != null){
                wo.OwnerId = mapofCase.get(each.MoveIn_Case__c).OwnerId ;
            }
            wo.caseId =  each.MoveIn_Case__c;
            wo.AccountId = each.AccountId;
            wo.Tenant__c = each.Tenant__c;
            wo.house__c = each.house__c;
            wo.Contract__c  = each.id;
            wo.Type_of_WO__c = WorkOrder_Constants.CONTRACT_SENT_TO_TENANT;
            wo.StartDate  = system.now();
            wo.Subject = 'Contract sent to Tenant';
            wo.Description = 'Contract sent to Tenant';
            woList.add(wo);
        }
        if(!woList.isEmpty()){
            insert woList;
        }
    }
     // Added by deepak
    public static void UpdateTenantUrlSignature(List<Contract> contList){
        Set<Id> setOfContract = new Set<Id>();
        List<Contract> updateCon = new List<Contract>();
        List<Contract> contListToBeUpdated  = new List<Contract>();
        try{
        for(Contract each : contList){
            setOfContract.add(each.id);
        }
            
        if(!setOfContract.isEmpty()){
            contListToBeUpdated=[Select id,(Select id from Attachments order by CreatedDate desc limit 1) from Contract where id in:setOfContract];
            if(!contListToBeUpdated.isEmpty()){
                for(Contract each : contListToBeUpdated){
                    each.Tenant_Contract_URL__c	 = Label.sDoc_URL+'/servlet/servlet.FileDownload?file='+each.Attachments[0].id;
                    each.IsEstamped__c = true;
                    updateCon.add(each);
                }
            }
            if(!updateCon.isEmpty()){
                update updateCon;
            }
        }
        }Catch(Exception e) {
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e,'UpdateTenantUrlSignature');
        }
        
        
    }
     public static void UpdateTenantUrlAgreement(List<Contract> contList){
        Set<Id> setOfContract = new Set<Id>();
        List<Contract> updateCon = new List<Contract>();
        List<Contract> contListToBeUpdated  = new List<Contract>();
         try{
        for(Contract each : contList){
            setOfContract.add(each.id);
        }
        if(!setOfContract.isEmpty()){
            contListToBeUpdated=[Select id,(Select id from Attachments order by CreatedDate desc limit 1) from Contract where id in:setOfContract];
            if(!contListToBeUpdated.isEmpty()){
                for(Contract each : contListToBeUpdated){
                    each.Tenant_Contract_URL__c	 = Label.sDoc_URL+'/servlet/servlet.FileDownload?file='+each.Attachments[0].id;
                    each.Is_Signed_By_Both_Parties__c = true;
                    updateCon.add(each);
                }
            }
            if(!updateCon.isEmpty()){
                update updateCon;
            }
        }
     }Catch(Exception e) {
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e,'UpdateTenantUrlAgreement');
        }
    }
    //
    //
    
    public static Void sendEmail(List<Contract> zmApprovedContracts, List<String> toAddresses){
        EmailTemplate emailTemplate = [SELECT Id, Name, Body, Subject, HtmlValue
                    FROM EmailTemplate WHERE NAME = 'Contract-Owner Approval Email V1'];

		System.debug('emailTemplate '+emailTemplate);
        
        Messaging.SingleEmailMessage emailMessage = new Messaging.SingleEmailMessage();
        emailMessage.setToAddresses(toAddresses);
        
        //if(orgEmails.size()>0){
          //  emailMessage.setOrgWideEmailAddressId(orgEmails.get(0).Id);
        //}  
        emailMessage.setSubject(emailTemplate.Subject);
        emailMessage.setHtmlBody(emailTemplate.HtmlValue);
        //emailMessage.setPlainTextBody(emailTemplate.Body);
        
        emailMessage.setTargetObjectId(Userinfo.getUserId());
        emailMessage.setTemplateId(emailTemplate.Id);
        
        emailMessage.setWhatId(zmApprovedContracts.get(0).ID);
        emailMessage.setSaveAsActivity(false);

        
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {emailMessage};
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        
        // Call a helper method to inspect the returned results
        inspectResults(results);
        
    }
    
    // Helper method mail inspectResults
    private static Boolean inspectResults(Messaging.SendEmailResult[] results) {
        Boolean sendResult = true;
        
        // sendEmail returns an array of result objects.
        // Iterate through the list to inspect results. 
        // In this class, the methods send only one email, 
        // so we should have only one result.
        for (Messaging.SendEmailResult res : results) {
            if (res.isSuccess()) {
                System.debug('Email sent successfully');
            }
            else {
                sendResult = false;
                System.debug('@inspectResults() The following errors occurred: ' + res.getErrors());                 
            }
        }
        
        return sendResult;
    }
    
}