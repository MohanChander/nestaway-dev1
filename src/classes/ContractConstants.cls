public class ContractConstants {
    
  // Contract agreement type piclist values
   public static String CONTRACT_AGREEMENT_TYPE_E_STAMPED='E-Stamped';
   public static String CONTRACT_AGREEMENT_TYPE_E_LOI='LOI';
   public static String CONTRACT_AGREEMENT_TYPE_E_NOTARIZED='Notarized';     
   // END    
 
    // Contract Stage picklist values
    public static String CONTACT_STAGE_FINAL_CONTACT = 'Final Contract Downloaded';  
    public static String CONTACT_STAGE_CONTRACT_CANCELLED = 'Cancelled';
    public static String CONTACT_STAGE_CONTRACT_GENERATED = 'Contract Generated';
    public static String CONTACT_STAGE_CONTRACT_PRINTED  = 'Contract Printed';
    public static String CONTACT_STAGE_QUEUED_FOR_ESTAMPING  = 'Queued For Estamping';
    public static String CONTACT_STAGE_CONTRACT_SENT_FOR_ESTAMPING  = 'Contract Sent For Estamping';
    public static String CONTACT_STAGE_CONTRACT_ESTAMPED  = 'Contract Estamped';
    public static String CONTACT_STAGE_QUEUED_FOR_ESIGNING  = 'Queued for E - Signing';
    public static String CONTACT_STAGE_CONTRACT_SIGNED_BY_TENANT_CONTRACT_SIGNED_BY_NESTAWAY  = 'Contract Signed by Tenant/Contract Signed by NestAway ';
    public static String CONTACT_STAGE_CONTRACT_SIGNED_BY_BOTH_PARTIES    = 'Contract Signed By Both Parties';
    public static String CONTACT_STAGE_SIGNED_COPY_UPLOADED   = 'Signed Copy Uploaded';
    public static String CONTRACT_STAGE_SENT_TO_TENANT    = 'Contract Sent to Tenant';
    public static String CONTRACT_STAGE_SENT_FOR_OWNER_SIGNATURE    = 'Contract Sent for Owner Signature';
    public static String CONTRACT_STAGE_DELIVERED_TO_TENANT = 'Contract Delivered to Tenant';
    public static String CONTACT_STAGE_FINAL_CONTACT_DOWN = 'Final Contract Downloaded';
    public static String CONTACT_STAGE_CONTRACT_UPLOADED = 'Contract Uploaded';
    public static String CONTRACT_STAGE_SMAPLE = 'Sample Contract';
    public static String CONTRACT_STAGE_DRAFT = 'Draft';
    public static String CONTRACT_STAGE_FINAL = 'Final Contract';
    public static String CONTRACT_STAGE_FINAL_DOWN = 'Final Contract Downloaded';
    public static String CONTRACT_STAGE_FINAL_PRINT = 'Final Contract Printed';
    public static String CONTRACT_STAGE_FINAL_HAND = 'Final Contract handed to Sales';    
    // END

    // Contract approval status field  
    public static String CONTRACT_APP_STATUS_AW_ZAM_APP = 'Awaiting ZM Approval';
    public static String CONTRACT_APP_STATUS_APP_ZM = 'Approved by ZM';
    public static String CONTRACT_APP_STATUS_REJ_ZM = 'Rejected by ZM'; 
    public static String CONTRACT_APP_STATUS_SAM_CONT_APP_OWNER = 'Sample Contract Approved by Owner';
    public static String CONTRACT_APP_STATUS_SAM_CONT_REJ_OWNER = 'Sample Contract Rejected by Owner';
    public static String CONTRACT_APP_STATUS_MANULLY_APP_ZM = 'Sample Contract Manually Approved by ZM';
    public static String CONTRACT_APP_STATUS_AW_HRM_APP = 'Awaiting HRM Approval';
    public static String CONTRACT_APP_STATUS_HRM_APP = 'Approved by HRM';
    public static String CONTRACT_APP_STATUS_HRM_REJ = 'Rejected by HRM';
    public static String CONTRACT_APP_STATUS_APP_CENTAL = 'Approved by Central Team';
    public static String CONTRACT_APP_STATUS_REJ_CENTAL = 'Rejected by Central Team';
    public static String CONTRACT_APP_STATUS_OWNER_APP = 'Approved by Owner';
    public static String CONTRACT_APP_STATUS_OWNER_REJ = 'Rejected by Owner';
    // END



    
    
    //Contract record type
    
    public static String CONTRACT_RT_CLONE_CONTRACT='Clone Contract';
    
    public static String tenantContract_RecordType {
        get {
            return 'Tenant Contract';
        }
    }
    
    public static String tenantContractReadonly_RecordType {
        get {
            return 'Tenant Contract Read Only';
        }
    }
     //contract
    public static String CONTRATC_WHO_PAY_DEPOSIT_NESTAWAY='NestAway';
    
    
    // Contract status values
    
    public static String CONTRATC_APPROVAL_STATUS_APPROVED_BY_OWNER='Approved by Owner';
    public static String CONTRACT_RT_OWNER='Owner Contract';
    
    public static Id getRecordTypeIdByName(string recordTypeName){
        
        return Schema.SObjectType.Contract.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
    }
    
    
}