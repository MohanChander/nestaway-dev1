public class ContractHelper {
    // Added BY deepak
    // To renew Contarct to move in case after 11 months
    public static Void CreateContract(List<WBTenantAgreementContractClass.TenantAgreementContractWrapper> TenantAgreementList,Map<id,Account> tenantAccountMap,Map<id,House__c> houseMap, Map<id,Tenancy__c> mapOfTenancy){
        list<contract> contractlistToInsert = new list <contract> ();
        Id 	contractRecordTypeId = ContractConstants.getRecordTypeIdByName(ContractConstants.tenantContract_RecordType);
        for(WBTenantAgreementContractClass.TenantAgreementContractWrapper each : tenantAgreementList){
            Contract c = new Contract();
            String TenantName = '' ;
            c.Tenant__c = each.Tenantid; 
            c.House__c = each.houseId;
            if(each.Rent != null )
            c.Fixed_Monthly_Rent__c = each.Rent;
            c.StartDate = each.Contract_start_Date;
            c.Contract_End_Date__c = each.Contract_End_Date;
            c.Status = 'Draft' ;
            c.MoveIn_Case__c = each.MoveIn_Case;
            if(mapOfTenancy.ContainsKey(each.tenantId)){
            c.Tenancy__c	 = mapOfTenancy.get(each.tenantId).id;
            }
            if(each.tenantId!=null && tenantAccountMap.containsKey(each.tenantId)){
                Account acc = tenantAccountMap.get(each.tenantid);
                if(acc.firstname != null)
                    TenantName = TenantName+' '+acc.firstname ;
                if(acc.lastname != null)
                    TenantName = TenantName+' '+ acc.lastname;
                if(acc.Relation_with_Guardian__c != null) {
                    c.Tenant_Relation_with_Guardian__c = acc.Relation_with_Guardian__c;
                }
                if(acc.Birth_Date__c != null)
                    c.Date_Of_Birth__c = acc.Birth_Date__c;
                if(acc.ShippingCity != null)
                    c.BillingCity = acc.ShippingCity ;
                if(acc.ShippingCountry != null)
                    c.BillingCountry = acc.ShippingCountry ;
                if(acc.ShippingCountryCode != null)
                    c.BillingCountryCode = acc.ShippingCountryCode ;
                if(acc.ShippingPostalCode != null)
                    c.BillingPostalCode = acc.ShippingPostalCode ;
                if(acc.ShippingState != null)
                    c.BillingState = acc.ShippingState ;
                if(acc.ShippingStreet != null)
                    c.BillingStreet = acc.ShippingStreet ;
                if(acc.PersonEmail != null)
                    c.Tenant_s_Email__c = acc.PersonEmail ;
                if(acc.phone != null){
                    c.Primary_Phone__c = acc.phone ;
                    c.Tenant_phone__c =  acc.phone ;
                }
            }
            
            if(houseMap.get(each.houseId).contract__r.Who_pays_Society_Maintenance__c != null)
                c.Who_pays_Society_Maintenance__c = houseMap.get(each.houseId).contract__r.Who_pays_Society_Maintenance__c  ;
            if(houseMap.get(each.houseId).House_Owner__c != null)
                c.accountId = houseMap.get(each.houseId).House_Owner__c  ;  
            if(houseMap.get(each.houseId).Booking_Type__c != null)
                c.Booking_Type__c  = houseMap.get(each.houseId).Booking_Type__c; 
            contractlistToInsert.add(c);
        }
        if(!contractlistToInsert.isEmpty()){
            insert contractlistToInsert;
        }
    }
    
}