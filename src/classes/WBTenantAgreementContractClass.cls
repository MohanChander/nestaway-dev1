@RestResource(urlMapping='/TenantAgreementContractClass/*')
global with sharing  class WBTenantAgreementContractClass {
    // Response wrapper
    // 
    global class TenantAgreementContractWrapperArray {
        public List<TenantAgreementContractWrapper> tenantAgreementContractWrapper;
    }  
    
    global class TenantAgreementContractWrapper {
        public String tenantId;
        public String houseId;
        public Date Contract_start_Date;
        public Date Contract_End_Date;
        public String MoveIn_Case;
        public Decimal Rent;
        public Boolean is_Tenant_Contract_Active; 
    }   
    
    @HttpPost   
    global static TenantAgreementContractWrapperArray getTenantContractDetails() {
        Set<Id> setOfTenant = new Set<Id>();
        Set<Id> setOfHouse = new Set<Id>();
        List<Tenancy__c> tenancyListToBeInserted = new   List<Tenancy__c>();
        List<Tenancy__c> tenancyList = new   List<Tenancy__c>();
        List<TenantAgreementContractWrapper> tenantAgreementList = new  List<TenantAgreementContractWrapper>();
        List<TenantAgreementContractWrapper> tenantAgreementListfirst = new  List<TenantAgreementContractWrapper>();
        List<TenantAgreementContractWrapper> tenantAgreementListUPdated = new  List<TenantAgreementContractWrapper>(); 
        
        
        RestRequest req = RestContext.request;
        TenantAgreementContractWrapperArray resp = new TenantAgreementContractWrapperArray();         
        try{   
            
            String bodyOfReq = RestContext.request.requestBody.toString(); 
            system.debug('****bodyOfReq' + bodyOfReq);      
            TenantAgreementContractWrapperArray params = new TenantAgreementContractWrapperArray();
            params = (TenantAgreementContractWrapperArray)JSON.deserialize(bodyOfReq,TenantAgreementContractWrapperArray.Class);
            if(params.TenantAgreementContractWrapper != null && params.TenantAgreementContractWrapper.size() > 0){
                for(TenantAgreementContractWrapper each : params.TenantAgreementContractWrapper ) {
                    if(each.tenantId == null){
                    }
                    else if(each.houseId == null){
                    }
                    else if(each.houseId == null  && each.tenantId == null){
                    }
                    else if(each.houseId != null && each.tenantId != null){
                        setOfTenant.add(each.tenantId);
                        setOfHouse.add(each.houseid);
                        tenantAgreementList.add(each);
                    }
                }
            }
            Map<id,Account> tenantAccountMap = AccountSelector.getAccountMapfromTenantAccId(setOfTenant);
            Map<id,House__c> houseMap = HouseSelector.getHouseMapForContract(setOfHouse);
            Map<id,Tenancy__c> mapOfTenancy = TenancySelector.getTenancyMap(setOfTenant,setOfHouse);
            if(!tenantAgreementList.isEmpty()){
                for(TenantAgreementContractWrapper each : tenantAgreementList){
                    if(!mapOfTenancy.containsKey(each.tenantId)){
                        Tenancy__c newTen = TenancyHelper.createNewTenancyRecord(each.tenantId,each.houseId);
                        tenancyListToBeInserted.add(newTen);
                        tenantAgreementListUPdated.add(each);
                    }
                    else{
                        tenantAgreementListfirst.add(each);
                    }
                }
            }
            if(!tenantAgreementListfirst.isEmpty()){
                ContractHelper.CreateContract(tenantAgreementListfirst,tenantAccountMap,houseMap,mapOfTenancy);
            }            
            if(!tenancyListToBeInserted.isEmpty()){
                insert tenancyListToBeInserted;
            }
            
            Map<id,Tenancy__c> mapOfTenancyUpdated = TenancySelector.getTenancyMap(setOfTenant,setOfHouse);
            if(!tenantAgreementListUPdated.isEmpty()){
                ContractHelper.CreateContract(tenantAgreementListUPdated,tenantAccountMap,houseMap,mapOfTenancyUpdated);
            }
            
        }catch(Exception e){    
            //// resp.success=false;
            // resp.hasException=true;
            system.debug('****exception in API: -'+e.getMessage()+' at line :-'+e.getLineNumber());             
            
        }
        return resp;
    }
}