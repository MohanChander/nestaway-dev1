public class HouseSyncStatusController {
    Public List<House__c> houseList{get;set;}
    Public String Message{get;set;}
    public HouseSyncStatusController (ApexPages.StandardController sController) {
        houseList=[select ID,Description_Message__c  from House__c WHERE id =: ApexPages.currentPage().getParameters().get('id')];
		Message = houselist[0].Description_Message__c;
    }
}