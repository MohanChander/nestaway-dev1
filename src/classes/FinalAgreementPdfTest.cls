@isTest
Public Class FinalAgreementPdfTest {
    
    Public Static TestMethod Void doTest(){
          Test.StartTest();
          
          Account Acc = new Account(Name='Test');
          insert Acc;
          
          Opportunity objOpp = new Opportunity();
          objOpp.AccountId = Acc.Id;
          objOpp.Name ='Test Opp';
          objOpp.CloseDate = System.Today();
          objOpp.StageName = 'House Inspection';
          
          insert objOpp;
          
          Contract objContract         = new Contract();
          objContract.Opportunity__c   = objOpp.Id;
          objContract.AccountId        = objOpp.AccountId;
          objContract.StartDate        = objOpp.CloseDate;
          objContract.Status           = 'Draft';
          objContract.ContractTerm     = 11;
          objContract.Contact_Email__c = objOpp.Contact_Email__c;
          objContract.Furnishing_Package__c = 'Package A';
          objContract.Approval_Status__c  = 'Approved by Central Team';
          insert objContract;
          
          objContract.Status           = 'Final Contract';
          Update objContract;
          
          
          PageReference pageRef1 = Page.FinalAgreementPdf;
          pageRef1.getParameters().put('id', String.valueOf(objContract.Id));
          System.debug('==Id=='+pageRef1.getParameters().get('id'));
          Test.setCurrentPage(pageRef1);

          System.Debug('====objContract===='+objContract);
          ApexPages.StandardController sc = new ApexPages.StandardController(objContract);
          FinalAgreementPdf testAccPlan = new FinalAgreementPdf(sc);
          
          testAccPlan.saveAttachement();
          testAccPlan.redirectToParentPage();          
          Test.StopTest();
          
          
    }
  /*  Public Static TestMethod Void doTest1(){
          Test.StartTest();
          Id recordTypeId = [Select Id from RecordType where Name=:'House Inspection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id;
        Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        Account vendoracc = new account(name ='TestVendor',Active__c =true,Contact_Email__c='ameed@warpdrivetech.in',TIN_No__c='tin12345',BillingStreet='mayur vihar -1',BillingCity='New Delhi',BillingState='Delhi',BillingPostalCode='110091',recordtypeid=devRecordTypeId) ;
        vendoracc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        insert(vendoracc);
          Account Acc = new Account(Name='Test');
          insert Acc;
          
          Opportunity objOpp = new Opportunity();
          objOpp.AccountId = Acc.Id;
          objOpp.Name ='Test Opp';
          objOpp.CloseDate = System.Today();
          objOpp.StageName = 'House Inspection';
          
          insert objOpp;
          
          Contract objContract         = new Contract();
          objContract.Opportunity__c   = objOpp.Id;
          objContract.AccountId        = objOpp.AccountId;
          objContract.StartDate        = objOpp.CloseDate;
          objContract.Status           = 'Draft';
          objContract.ContractTerm     = 11;
          objContract.Contact_Email__c = objOpp.Contact_Email__c;
          objContract.Furnishing_Package__c = 'Package A';
          objContract.Approval_Status__c  = 'Approved by Central Team';
          insert objContract;
          
          objContract.Status           = 'Final Contract';
          Update objContract;
          
           House_Inspection_Checklist__c houseIC = new House_Inspection_Checklist__c();
            houseIC.RecordTypeId = recordTypeId;
            houseIC.Opportunity__c = objOpp.Id;
            houseIC.Status__c = 'House Inspection In Progress';  
            houseIC.House_layout__c =  '1 BH';    
            insert houseIC;
            system.debug('===houseInspec==='+houseIC);
            system.debug('===objOpp.AccountId=='+objOpp.AccountId);
            
            Order oRec = new Order();
            oRec.OpportunityId = objOpp.id;
            oRec.Vendor__c= vendoracc.Id;
            oRec.AccountId = objOpp.AccountID;
            oRec.Status = 'Submitted';
            oRec.EffectiveDate = System.today();
            oRec.Name__c = objOpp.Name__c;
            oRec.Do_you_want_to_add_a_POA__c = objOpp.Do_You_Want_To_Add_A_POA__c;
            oRec.Marital_Status__c = objOpp.Marital_Status__c;
            oRec.POA_Name__c = objOpp.POA_Name__c;
            oRec.Date_of_Birth__c = objOpp.Date_Of_Birth__c;
            oRec.POA_Marital_Status__c = objOpp.POA_Marital_Status__c;
            oRec.Age__c = objOpp.Age__c;
            oRec.Father_Husband_name__c = objOpp.Father_Husband_Name__c;
            oRec.Relation_with_Guardian__c = objOpp.Relation_with_Guardian__c;
            oRec.Primary_Phone__c = objOpp.Primary_Phone__c;
            oRec.Secondary_Phone__c = objOpp.Secondary_Phone__c;
            oRec.POA_Father_Husband_Name__c = objOpp.POA_Father_Husband_Name__c;
            oRec.POA_Relation_with_Guardian__c = objOpp.POA_Relation_with_Guardian__c;
            oRec.POA_Date__c = objOpp.POA_Date__c;
            oRec.POA_Permanent_Address__c = objOpp.POA_Permanent_Address__c;
            oRec.POA_Age__c = objOpp.POA_Age__c;
            oRec.Pricebook2Id = objOpp.Pricebook2Id;
            oRec.POA_Witness_Name__c = objOpp.POA_Witness_Name__c; 
            
            insert oRec;
          
          PageReference pageRef1 = Page.FinalAgreementBachelors;
          pageRef1.getParameters().put('contractId', String.valueOf(objContract.Id));
          System.debug('==Id=='+pageRef1.getParameters().get('contractId'));
          Test.setCurrentPage(pageRef1);

          System.Debug('====objContract===='+objContract);
          ApexPages.StandardController sc = new ApexPages.StandardController(objContract);
          FinalAgreementBachelors testAccPlan = new FinalAgreementBachelors(sc);
          
                    
          Test.StopTest();
          
          
    } */
}