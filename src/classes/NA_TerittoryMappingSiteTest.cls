@isTest
global class NA_TerittoryMappingSiteTest {
    
    @isTest Static void doTerittoryTest(){
    
        Test.StartTest();
         City__c city = new City__c();
           city.name='Bangalore';
           insert city;
            set<Id> LeadIdSet = new set<Id>();
            Id LeadRecId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Owner Lead').getRecordTypeId();
             
            Zing_API_URL__c zing = new Zing_API_URL__c();
            zing.Name          = 'Zing URL';
            zing.Auth_Token__c = 'ftLKMp_z_e3xX9Yiax-q';
            zing.Email__c      = 'unknownadmin@nestaway.com';
            zing.Tag__c        = 'OwnerAcquisition';
            zing.URL__c        = 'http://40.122.207.71/admin/zinc/lat_long?';
            
            insert zing;
            
            Lead objLead = new Lead();
            objLead.FirstName                  = 'Test';
            objLead.RecordTypeId               =  LeadRecId;
            objLead.LastName                   = 'Test';
            objLead.Phone                      = '9066955369';
            objLead.MobilePhone                = '9406695578';      
            objLead.Email                      = 'rohan.j@nestaway.com';
            objLead.Source_Type__c             = 'Direct';
           // objLead.Area_Code__c               = '389';         
            objLead.LeadSource                 = 'City Marketing';
            objLead.City_Marketing_Activity__c = 'Roadshow';
            objLead.Street                     = 'Test';
            objLead.City                       = 'Bangalore';
            objLead.State                      = 'Karnataka';
            objLead.Status                     = 'New';
            objLead.PostalCode                 = '560078';
            objLead.Country                    = 'India';
            objLead.Company                    = 'NestAway';
            objLead.Latitude = null;
            objLead.longitude = null;
            insert objLead;
            LeadIdSet.add(objLead.Id);
            
            Lead objLead1 = new Lead();
            objLead1.FirstName                  = 'Test One';
            objLead1.RecordTypeId               =  LeadRecId;
            objLead1.LastName                   = 'Test One';
            objLead1.Phone                      = '9066955119';
            objLead1.MobilePhone                = '9406695118';      
            objLead1.Email                      = 'rohan.j@nestaway.com';
            objLead1.Source_Type__c             = 'Direct';
            objLead1.Area_Code__c               = '389';         
            objLead1.LeadSource                 = 'City Marketing';
            objLead1.City_Marketing_Activity__c = 'Roadshow';
            objLead1.Street                     = 'Test';
            objLead1.City                       = 'Bangalore';
            objLead1.State                      = 'Karnataka';
            objLead1.Status                     = 'New';
            objLead1.PostalCode                 = '560078';
            objLead1.Country                    = 'India';
            objLead1.Company                    = 'NestAway';
                        
            insert objLead1;
            LeadIdSet.add(objLead1.Id);
            
            Round_Robin__c rouRob = new Round_Robin__c();
            rouRob.Queue_Name__c ='Bangalore HSR-BTM';
            rouRob.Total_Assigned_Percentage__c = 100;
            insert rouRob;
            
            Lead_Distribution__c leadDist = new Lead_Distribution__c();
            leadDist.Assigned__c = 50;
            leadDist.Round_Robin__c = rouRob.Id;
            insert leadDist;
            
           // NA_TerittoryMappingSite naTerMap = new NA_TerittoryMappingSite();
            NA_TerittoryMappingSite.handleMappingSite(LeadIdSet);
            
        Test.StopTest();
    }
}