global class LeadEscalationBatchClass implements Database.Batchable<sObject>,Schedulable
{   
	public DateTime runForDate;
    
    public LeadEscalationBatchClass(DateTime runForDate){
    
        this.runForDate=runForDate;
    }

    global void execute(SchedulableContext sc) {
      LeadEscalationBatchClass batch = new LeadEscalationBatchClass(System.now()); 
      database.executebatch(batch);
   }

	global Database.QueryLocator start(Database.BatchableContext BC) {
		   /* Querring for lead which are in new and Open Stage and whose escalation time/drop time is less than present time and 
		   more then half hours less then present time and also more whose folllow up time is more than present time and less than  
		   half hour more than present time */
      
    dateTime dt=System.now();
    if(this.runForDate!=null){
       dt=this.runForDate;
    } 
			
	
	return database.getQueryLocator([SELECT Id,OwnerID,Drop_Date__c,lastname,Escalation_Time__c,Follow_Up_Time__c,New_Stage_Escalation_Level__c, 
							 Open_Stage_Escalation_Level__c,Status FROM Lead where (Status='New' or Status='Open')
							 and ((Follow_Up_Time__c >:dt and Follow_Up_Time__c < :dt.addMinutes(30)) 
							 or (Drop_Date__c < :dt and Drop_Date__c > :dt.addMinutes(-30)) 
							 or (Escalation_Time__c < :dt and Escalation_Time__c >:dt.addMinutes(-30))) and (Recordtype.name='Owner Lead' or Recordtype.name='Owner Lead With Phone Option')]);
	}


	global void execute(Database.BatchableContext BC, List<Lead> ldList) {
		Lead_Escalation__mdt le=[Select New_Stage_RM_to_CT_SLA_Hrs__c,New_Stage_ZAM_to_RM_SLA_Hrs__c,Open_Stage_RM_to_CT_SLA_Hrs__c,
		                         Open_Stage_ZAM_to_RM_SLA_Hrs__c,Label from Lead_Escalation__mdt where QualifiedApiName='Escalation'];
        Id  generalTask = Schema.SObjectType.Task.getRecordTypeInfosByName().get(Constants.TASK_RT_GENERAL_TASK).getRecordTypeId();
        List<task> instask=new List<task>();	
        List<Lead> updlead=new List<Lead>();		                         

		for(Lead ld:ldList){
			// Checking status
			if(ld.Status==LeadConstants.LEAD_STATUS_NEW){
				// checking if new Stage Escaltion is null then escalate to ZAM
				if(ld.New_Stage_Escalation_Level__c ==null && 
               	  ld.Escalation_Time__c < runForDate && ld.Escalation_Time__c > runForDate.addMinutes(-30)){

                   ld.New_Stage_Escalation_Level__c = LeadConstants.ZAM_ESCALATION;
                   ld.Escalation_Time__c=runForDate.addHours((Integer)le.New_Stage_ZAM_to_RM_SLA_Hrs__c);
                   System.debug('****ZAM'+ld.New_Stage_Escalation_Level__c);
                   updlead.add(ld);
               }
               // checking if new Stage Escaltion is ZAM then escalate to RM
               else if(ld.New_Stage_Escalation_Level__c == LeadConstants.ZAM_ESCALATION && 
               	  ld.Escalation_Time__c < runForDate && ld.Escalation_Time__c > runForDate.addMinutes(-30)){

                   ld.New_Stage_Escalation_Level__c = LeadConstants.RM_ESCALATION;
                   ld.Escalation_Time__c=runForDate.addHours((Integer)le.New_Stage_RM_to_CT_SLA_Hrs__c);
                   updlead.add(ld);
               }
               // checking if new Stage Escaltion is RM then escalate to CT
               else if(ld.New_Stage_Escalation_Level__c==LeadConstants.RM_ESCALATION && 
               		   ld.Escalation_Time__c <runForDate && ld.Escalation_Time__c > runForDate.addMinutes(-30)){
               	
                   ld.New_Stage_Escalation_Level__c = LeadConstants.CT_ESCALATION;
                   updlead.add(ld);
               }				
			}
			else if(ld.Status==LeadConstants.LEAD_STATUS_OPEN){
                // checking if Open Stage Escaltion is null then escalate to ZAM
				if(ld.Open_Stage_Escalation_Level__c==null && 
	               	   ld.Escalation_Time__c <runForDate && ld.Escalation_Time__c > runForDate.addMinutes(-30)){

	                   ld.Open_Stage_Escalation_Level__c=LeadConstants.ZAM_ESCALATION;
	                   ld.Escalation_Time__c=runForDate.addHours((Integer)le.Open_Stage_ZAM_to_RM_SLA_Hrs__c);
	                   ld.Time_Spend__c=null;
                       ld.SLA_Start_Time__c=runForDate;
	                   updlead.add(ld);
	               }
	               // checking if Open Stage Escaltion is ZAM then escalate to RM
	               else if(ld.Open_Stage_Escalation_Level__c==LeadConstants.ZAM_ESCALATION && 
	               	   ld.Escalation_Time__c <runForDate && ld.Escalation_Time__c > runForDate.addMinutes(-30)){

	                   ld.Open_Stage_Escalation_Level__c=LeadConstants.RM_ESCALATION;
	                   ld.Escalation_Time__c=runForDate.addHours((Integer)le.Open_Stage_RM_to_CT_SLA_Hrs__c);
	                   ld.Time_Spend__c=null;
                       ld.SLA_Start_Time__c=runForDate;
	                   updlead.add(ld);
	               }
	                // checking if Open Stage Escaltion is RM then escalate to CT
	               else if(ld.Open_Stage_Escalation_Level__c==LeadConstants.RM_ESCALATION && 
	               	       ld.Escalation_Time__c <runForDate && ld.Escalation_Time__c > runForDate.addMinutes(-30)){

	                   ld.Open_Stage_Escalation_Level__c=LeadConstants.CT_ESCALATION;
	                   ld.Time_Spend__c=null;
                       ld.SLA_Start_Time__c=runForDate;
	                   updlead.add(ld);
	               }	
                   // creating task for FollowUp 
	               if( ld.Follow_Up_Time__c > runForDate && ld.Follow_Up_Time__c < runForDate.addMinutes(30)){
	               	  System.debug('*****follow task');
                       Task tk=new Task();
				       tk.OwnerId=ld.OwnerId;
				       tk.Subject='FollowUp reminder :'+ld.lastname; 
				       tk.WhoID=ld.id;
				       tk.RecordtypeID=generalTask;
				       DateTime dT=ld.Follow_Up_Time__c;
				       tk.ActivityDate=date.newinstance(dT.year(), dT.month(), dT.day());
				      // tk.ReminderDateTime=System.now().addMinutes(15);
				       instask.add(tk);
	               }


			}
			// Dropping the Lead 
			if(ld.Drop_Date__c < runForDate && ld.Drop_Date__c > runForDate.addMinutes(-30)){
	           	ld.status=LeadConstants.LEAD_STATUS_DROPPED;
	           	updlead.add(ld);
	          }
		}
		if(!instask.isEmpty()){
			insert instask;
		}
		if(!updlead.isEmpty()){
			Update updlead;
		}
	 
	}
	global void finish(Database.BatchableContext BC) {
	       System.debug('lead baych class finsihed');     
    }
}