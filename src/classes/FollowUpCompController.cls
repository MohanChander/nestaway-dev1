public class FollowUpCompController {

    @AuraEnabled
    public static List<String> getPicklistValues(String sObjName){
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult;
        
        if(sObjName == 'Opportunity'){
            fieldResult = Opportunity.Follow_Up_Reason__c.getDescribe();
        } else {
            fieldResult = Lead.Follow_Up_Reason__c.getDescribe();
        }
        
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry f: ple) {
            options.add(f.getLabel());
        }
        return options;
    }
    
    @AuraEnabled
    public static String getFollowUp(String sObjName, String sObjRecordId){
		DateTime followUpTime;
        String queryString = 'select Follow_Up_Time__c from ' + sObjName + 
            				 ' where Id = \'' + sObjRecordId + '\'';
        
        
        if(sObjName == 'Lead'){
            List<Lead> leadList = (List<Lead>)Database.query(queryString);
            followUpTime = leadList[0].Follow_Up_Time__c;
        } else {
            List<Opportunity> optyList = (List<Opportunity>)Database.query(queryString);
            followUpTime = optyList[0].Follow_Up_Time__c;            
        }

        return String.valueof(followUpTime);
    }
    
    @AuraEnabled
    public static Boolean saveFollowUp(String sObjName, String sObjRecordId, DateTime followUp){
        
        System.debug('**saveFollowUp');
		DateTime followUpTime;
        String queryString = 'select Id, Follow_Up_Time__c from ' + sObjName + 
            				 ' where Id = \'' + sObjRecordId + '\'';
        
        try{
                if(sObjName == 'Lead'){
                    List<Lead> leadList = (List<Lead>)Database.query(queryString);
                    leadList[0].Follow_Up_Time__c = followUp;
                    update leadList[0];
                } else {
                    List<Opportunity> optyList = (List<Opportunity>)Database.query(queryString);
                    optyList[0].Follow_Up_Time__c = followUp;  
                    update optyList[0];
                }   
            
            	return true;
        } catch (exception e){
                    System.debug('**Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + 
                                 '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + 
                                 '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'saveFollowUp Aura Method');                  
            return false;
        }
    }    
    

}