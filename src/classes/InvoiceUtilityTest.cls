@istest
public class InvoiceUtilityTest {
    static testMethod void RoomTriggerTest() {
        //Create Dummy contract
        Map<id,List<OrderItem>> mapoforder = new   Map<id,List<OrderItem>>();
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Final Contract';
        
        insert opp;
        
        
        PriceBook2 priceBk = new PriceBook2();
        priceBk.Name = 'TestPriceBook';
        // priceBk.isStandard = FALSE;
        insert priceBk;
        
        Product2 prod = new Product2();
        prod.Name = 'testProd';
        prod.isActive = TRUE;
        insert prod;
        
        Contract cont = new Contract();
        cont.Name = 'TestContract';
        cont.AccountId = accObj.id;
        cont.Opportunity__c = opp.id;
        cont.PriceBook2Id = priceBk.Id;
        cont.status = 'Draft';
        cont.Maximum_number_of_tenants_allowed__c = '1';
        cont.Booking_Type__c = 'Shared House';
        insert cont;
        
        
        Order order = new Order(Name = 'test', Status = 'Draft',
                                AccountId = accObj.id,
                                EffectiveDate = Date.TODAY(),
                                ContractId = cont.Id,
                                OpportunityId = opp.id);
         OrderItem newOi = new OrderItem();
        newOi.OrderId = order.Id;
        insert newOi;
        List<orderitem> listof = new List<Orderitem>();
        Mapoforder.put(newOi.Id,listof);
        
        
    } 
    
}