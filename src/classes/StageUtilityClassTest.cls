//Added by baibhav
@IsTest
public with sharing class StageUtilityClassTest {
    public Static TestMethod Void doTest1(){

        Zing_API_URL__c zn=new Zing_API_URL__c();
        zn.Email__c='unknownadmin@nestaway.com';
        zn.URL__c='https://www.staging.nestaway.xyz/admin/zinc/lat_long?';
        zn.Auth_Token__c='ftLKMp_z_e3xX9Yiax-q';
        zn.Tag__c='OwnerAcquisition';
        zn.name='Zing url';
        insert zn;

        NestAway_End_Point__c ne=new NestAway_End_Point__c();
        ne.Phone_URL__c='http://www.google.com';
        ne.name='nest';
        insert ne;

         Profile prof=[Select id from profile where name='System Administrator'];

            User newUser = new User( email='firstlast@nestaway.com'
                    , ProfileId = prof.Id
                    , UserName= 'tuser@nestaway.com'
                    , Alias='cspu' 
                    , CommunityNickName='cspu'
                    , TimeZoneSidKey='America/Los_Angeles'
                    , LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1' 
                    , LanguageLocaleKey='en_US'
                    , FirstName = 'Test_'
                    , LastName = 'Test' 
                    , Phone = '9620600507'
                   );
            insert newUser;

            User newUser1 = new User( email='firstlast@nestaway.com'
                    , ProfileId = prof.Id
                    , UserName= 'tuse323r@nestaway.com'
                    , Alias='cs1pu' 
                    , CommunityNickName='cs1pu'
                    , TimeZoneSidKey='America/Los_Angeles'
                    , LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1' 
                    , LanguageLocaleKey='en_US'
                    , FirstName = 'Test_'
                    , LastName = 'Test12' 
                    , Phone = '9620600507'
                   );
            insert newUser1;

    	Id ownerldRecId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Owner Lead').getRecordTypeId();
	   Lead objLead = new Lead();
            objLead.FirstName                  = 'Test';
            objLead.LastName                   = 'Test';
            objLead.Phone                      = '9620600507';
            objLead.MobilePhone                = '9620600507';      
            objLead.Email                      = 'Test@test.com';
            objLead.Source_Type__c             = 'Assisted';
            //objLead.Area_Code__c               = '375';         
            objLead.LeadSource                 = 'City Marketing';
            objLead.City_Marketing_Activity__c = 'Roadshow';
            objLead.Street                     = 'Test';
            objLead.City                       = 'Bangalore';
            objLead.State                      = 'Karnataka';
            objLead.Status                     = 'New';
            objLead.PostalCode                 = '560078';
            objLead.Country                    = 'India';
            objLead.recordtypeID        	   =ownerldRecId;
            objLead.IsValidPrimaryContact__c   = True;  
            objLead.Tenancy_Type__c='Singles';     
            objLead.House_Visit_Scheduled_Date__c=System.now().addhours(5);
            insert objLead;

            objLead.Escalation_User__c=newUser1.id;
            update objLead;

            objLead.New_Stage_Escalation_Level__c='ZAM Escalation';
            update objLead;

            objLead.Status                     = 'Open';
            update objLead;

            objLead.New_Stage_Escalation_Level__c='RM Escalation';
            update objLead;
            
            objLead.Follow_Up_Time__c=System.now().addHours(146);
            objLead.Follow_Up_Reason__c='Owner requested a call back';
            update objLead;



           

            objLead.ownerid=newUser.id;
            update objLead;


	}
	   public Static TestMethod Void doTest2(){

        Zing_API_URL__c zn=new Zing_API_URL__c();
        zn.Email__c='unknownadmin@nestaway.com';
        zn.URL__c='https://www.staging.nestaway.xyz/admin/zinc/lat_long?';
        zn.Auth_Token__c='ftLKMp_z_e3xX9Yiax-q';
        zn.Tag__c='OwnerAcquisition';
        zn.name='Zing url';
        insert zn;

        NestAway_End_Point__c ne=new NestAway_End_Point__c();
        ne.Phone_URL__c='http://www.google.com';
        ne.name='nest';
        insert ne;
	   Lead objLead = new Lead();
            objLead.FirstName                  = 'Test';
            objLead.LastName                   = 'Test';
            objLead.Phone                      = '9066955369';
            objLead.MobilePhone                = '9406695578';      
            objLead.Email                      = 'Test@test.com';
            objLead.Source_Type__c             = 'Assisted';
            objLead.Area_Code__c               = '7';         
            objLead.LeadSource                 = 'City Marketing';
            objLead.City_Marketing_Activity__c = 'Roadshow';
            objLead.Street                     = 'Test';
            objLead.City                       = 'Bangalore';
            objLead.State                      = 'Karnataka';
            objLead.Status                     = 'New';
            objLead.PostalCode                 = '560078';
            objLead.Country                    = 'India';
            objLead.Company                    = 'NestAway';
            objLead.IsValidPrimaryContact__c   = True;       
            objLead.House_Visit_Scheduled_Date__c=System.now().addhours(5);
            insert objLead;

            objLead.New_Stage_Escalation_Level__c='RM Escalation';
            update objLead;

             objLead.New_Stage_Escalation_Level__c='City Escalation';
            update objLead;
        }
        public Static TestMethod Void doTest3(){
        Account Acc = new Account(Name='Test',PAN_Number__c='CAVPK1234V');
        insert Acc;

        Opportunity objOpp = new Opportunity();
        objOpp.AccountId = Acc.Id;
        objOpp.Name ='Test Opp';
        objOpp.CloseDate = System.Today();
        objOpp.StageName = 'House Inspection';

        insert objOpp;

         Profile prof=[Select id from profile where name='System Administrator'];

            User newUser = new User( email='firstlast@nestaway.com'
                    , ProfileId = prof.Id
                    , UserName= 'tuser@nestaway.com'
                    , Alias='cspu' 
                    , CommunityNickName='cspu'
                    , TimeZoneSidKey='America/Los_Angeles'
                    , LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1' 
                    , LanguageLocaleKey='en_US'
                    , FirstName = 'Test_'
                    , LastName = 'Test' 
                    , Phone = '9620600507'
                   );
            insert newUser;

             User newUser1 = new User( email='firstlast@nestaway.com'
                    , ProfileId = prof.Id
                    , UserName= 'tuser123@nestaway.com'
                    , Alias='csertpu' 
                    , CommunityNickName='cswepu'
                    , TimeZoneSidKey='America/Los_Angeles'
                    , LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1' 
                    , LanguageLocaleKey='en_US'
                    , FirstName = 'Test_'
                    , LastName = 'Test2' 
                    , Phone = '9620600507'
                   );
            insert newUser1;

             User newUser2 = new User( email='firstlast@nestaway.com'
                    , ProfileId = prof.Id
                    , UserName= 'tuser1243@nestaway.com'
                    , Alias='cseertpu' 
                    , CommunityNickName='csweepu'
                    , TimeZoneSidKey='America/Los_Angeles'
                    , LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1' 
                    , LanguageLocaleKey='en_US'
                    , FirstName = 'Test_'
                    , LastName = 'Test22' 
                    , Phone = '9620600507'
                   );
            insert newUser2;

            System.runAs(newUser){

                 objOpp.Follow_Up_Time__c=System.now().addHours(146);
                 objOpp.Follow_Up_Reason__c='Owner requested a call back';
                update objOpp;

                objOpp.ownerid=newUser1.id;
                update objOpp;

                objOpp.House_Inspection_Escalation_level__c='ZAM Escalation';
                update objOpp;

               
                objOpp.StageName = 'Sample Contract';
                update objOpp;

                objOpp.Sample_Contract_Escalation_level__c='RM Escalation';
                update objOpp;

                 objOpp.ownerid=newUser2.id;
                update objOpp;


                objOpp.StageName = 'Final Contract';
                update objOpp;

                objOpp.Final_Contract_Escalation_level__c='City Escalation';
                update objOpp;

                 objOpp.ownerid=newUser1.id;
                update objOpp;


                objOpp.StageName = 'Sales Order';
                update objOpp;

                objOpp.Sales_Order_Escalation_level__c='City Escalation';
                update objOpp;

                 objOpp.ownerid=newUser2.id;
                update objOpp;


                objOpp.StageName = 'Quote Creation';
                update objOpp;

                objOpp.Quote_Creation_Escalation_level__c='City Escalation';
                update objOpp;

                objOpp.StageName = 'Docs Collected';
                update objOpp;

                objOpp.Docs_Collected_Escalation_level__c='City Escalation';
                update objOpp;

                objOpp.StageName = 'House';
                update objOpp;

                objOpp.House_Escalation_level__c='City Escalation';
                update objOpp;

            }

        }
         public Static TestMethod Void doTest4(){
        Account Acc = new Account(Name='Test',PAN_Number__c='CAVPK1234V');
        insert Acc;

         Profile prof=[Select id from profile where name='System Administrator'];

            User newUser = new User( email='firstlast@nestaway.com'
                    , ProfileId = prof.Id
                    , UserName= 'tuser@nestaway.com'
                    , Alias='cspu' 
                    , CommunityNickName='cspu'
                    , TimeZoneSidKey='America/Los_Angeles'
                    , LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1' 
                    , LanguageLocaleKey='en_US'
                    , FirstName = 'Test_'
                    , LastName = 'Test' 
                    , Phone = '9620600507'
                   );
            insert newUser;

             User newUser1 = new User( email='firstlast@nestaway.com'
                    , ProfileId = prof.Id
                    , UserName= 'tuser123@nestaway.com'
                    , Alias='csertpu' 
                    , CommunityNickName='cswepu'
                    , TimeZoneSidKey='America/Los_Angeles'
                    , LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1' 
                    , LanguageLocaleKey='en_US'
                    , FirstName = 'Test_'
                    , LastName = 'Test2' 
                    , Phone = '9620600507'
                   );
            insert newUser1;

            System.runAs(newUser){

                  Opportunity objOpp = new Opportunity();
                objOpp.AccountId = Acc.Id;
                objOpp.Name ='Test Opp';
                objOpp.CloseDate = System.Today();
                objOpp.StageName = 'Sample Contract';

                 insert objOpp;
                objOpp.ownerid=newUser1.id;
                update objOpp;

                objOpp.Follow_Up_Time__c=System.now().addHours(146);
                objOpp.Follow_Up_Reason__c='Owner requested a call back';
                update objOpp;

                objOpp.Sample_Contract_Escalation_level__c='RM Escalation';
                objOpp.Escalation_User__c=newUser1.id;
                update objOpp;

                objOpp.StageName = 'Final Contract';
                update objOpp;
            }

        }
    public Static TestMethod Void doTest5(){
        Account Acc = new Account(Name='Test',PAN_Number__c='CAVPK1234V');
        insert Acc;

         Profile prof=[Select id from profile where name='System Administrator'];

            User newUser = new User( email='firstlast@nestaway.com'
                    , ProfileId = prof.Id
                    , UserName= 'tuser@nestaway.com'
                    , Alias='cspu' 
                    , CommunityNickName='cspu'
                    , TimeZoneSidKey='America/Los_Angeles'
                    , LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1' 
                    , LanguageLocaleKey='en_US'
                    , FirstName = 'Test_'
                    , LastName = 'Test' 
                    , Phone = '9620600507'
                   );
            insert newUser;

             User newUser1 = new User( email='firstlast@nestaway.com'
                    , ProfileId = prof.Id
                    , UserName= 'tuser123@nestaway.com'
                    , Alias='csertpu' 
                    , CommunityNickName='cswepu'
                    , TimeZoneSidKey='America/Los_Angeles'
                    , LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1' 
                    , LanguageLocaleKey='en_US'
                    , FirstName = 'Test_'
                    , LastName = 'Test2' 
                    , Phone = '9620600507'
                   );
            insert newUser1;

            System.runAs(newUser){

                  Opportunity objOpp = new Opportunity();
                objOpp.AccountId = Acc.Id;
                objOpp.Name ='Test Opp';
                objOpp.CloseDate = System.Today();
                objOpp.StageName = 'Final Contract';

                 insert objOpp;
                objOpp.ownerid=newUser1.id;
                update objOpp;

                objOpp.Final_Contract_Escalation_level__c='City Escalation';
                update objOpp;

                objOpp.StageName = 'Quote Creation';
                update objOpp;
            }

        }
        public Static TestMethod Void doTest6(){
        Account Acc = new Account(Name='Test',PAN_Number__c='CAVPK1234V');
        insert Acc;

         Profile prof=[Select id from profile where name='System Administrator'];

            User newUser = new User( email='firstlast@nestaway.com'
                    , ProfileId = prof.Id
                    , UserName= 'tuser@nestaway.com'
                    , Alias='cspu' 
                    , CommunityNickName='cspu'
                    , TimeZoneSidKey='America/Los_Angeles'
                    , LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1' 
                    , LanguageLocaleKey='en_US'
                    , FirstName = 'Test_'
                    , LastName = 'Test' 
                    , Phone = '9620600507'
                   );
            insert newUser;

             User newUser1 = new User( email='firstlast@nestaway.com'
                    , ProfileId = prof.Id
                    , UserName= 'tuser123@nestaway.com'
                    , Alias='csertpu' 
                    , CommunityNickName='cswepu'
                    , TimeZoneSidKey='America/Los_Angeles'
                    , LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1' 
                    , LanguageLocaleKey='en_US'
                    , FirstName = 'Test_'
                    , LastName = 'Test2' 
                    , Phone = '9620600507'
                   );
            insert newUser1;

            System.runAs(newUser){

                  Opportunity objOpp = new Opportunity();
                objOpp.AccountId = Acc.Id;
                objOpp.Name ='Test Opp';
                objOpp.CloseDate = System.Today();
                objOpp.StageName = 'Quote Creation';

                 insert objOpp;
                objOpp.ownerid=newUser1.id;
                update objOpp;

                objOpp.Quote_Creation_Escalation_level__c='City Escalation';
                update objOpp;

                objOpp.StageName = 'Sales Order';
                update objOpp;
            }

        } 
   public Static TestMethod Void doTest7(){
        Account Acc = new Account(Name='Test',PAN_Number__c='CAVPK1234V');
        insert Acc;

         Profile prof=[Select id from profile where name='System Administrator'];

            User newUser = new User( email='firstlast@nestaway.com'
                    , ProfileId = prof.Id
                    , UserName= 'tuser@nestaway.com'
                    , Alias='cspu' 
                    , CommunityNickName='cspu'
                    , TimeZoneSidKey='America/Los_Angeles'
                    , LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1' 
                    , LanguageLocaleKey='en_US'
                    , FirstName = 'Test_'
                    , LastName = 'Test' 
                    , Phone = '9620600507'
                   );
            insert newUser;

             User newUser1 = new User( email='firstlast@nestaway.com'
                    , ProfileId = prof.Id
                    , UserName= 'tuser123@nestaway.com'
                    , Alias='csertpu' 
                    , CommunityNickName='cswepu'
                    , TimeZoneSidKey='America/Los_Angeles'
                    , LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1' 
                    , LanguageLocaleKey='en_US'
                    , FirstName = 'Test_'
                    , LastName = 'Test2' 
                    , Phone = '9620600507'
                   );
            insert newUser1;

            System.runAs(newUser){

                  Opportunity objOpp = new Opportunity();
                objOpp.AccountId = Acc.Id;
                objOpp.Name ='Test Opp';
                objOpp.CloseDate = System.Today();
                objOpp.StageName = 'Sales Order';

                 insert objOpp;
                objOpp.ownerid=newUser1.id;
                update objOpp;

                objOpp.Sales_Order_Escalation_level__c='City Escalation';
                update objOpp;

                objOpp.StageName = 'Docs Collected';
                update objOpp;
            }

        }
           public Static TestMethod Void doTest8(){
        Account Acc = new Account(Name='Test',PAN_Number__c='CAVPK1234V');
        insert Acc;

         Profile prof=[Select id from profile where name='System Administrator'];

            User newUser = new User( email='firstlast@nestaway.com'
                    , ProfileId = prof.Id
                    , UserName= 'tuser@nestaway.com'
                    , Alias='cspu' 
                    , CommunityNickName='cspu'
                    , TimeZoneSidKey='America/Los_Angeles'
                    , LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1' 
                    , LanguageLocaleKey='en_US'
                    , FirstName = 'Test_'
                    , LastName = 'Test' 
                    , Phone = '9620600507'
                   );
            insert newUser;

             User newUser1 = new User( email='firstlast@nestaway.com'
                    , ProfileId = prof.Id
                    , UserName= 'tuser123@nestaway.com'
                    , Alias='csertpu' 
                    , CommunityNickName='cswepu'
                    , TimeZoneSidKey='America/Los_Angeles'
                    , LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1' 
                    , LanguageLocaleKey='en_US'
                    , FirstName = 'Test_'
                    , LastName = 'Test2' 
                    , Phone = '9620600507'
                   );
            insert newUser1;

            System.runAs(newUser){

                  Opportunity objOpp = new Opportunity();
                objOpp.AccountId = Acc.Id;
                objOpp.Name ='Test Opp';
                objOpp.CloseDate = System.Today();
                objOpp.StageName = 'Docs Collected';

                 insert objOpp;
                objOpp.ownerid=newUser1.id;
                update objOpp;

                objOpp.Docs_Collected_Escalation_level__c='City Escalation';
                update objOpp;

                objOpp.StageName = 'House';
                update objOpp;
            }

        }  
     public Static TestMethod Void doTest9(){
        Account Acc = new Account(Name='Test',PAN_Number__c='CAVPK1234V');
        insert Acc;

         Profile prof=[Select id from profile where name='System Administrator'];

            User newUser = new User( email='firstlast@nestaway.com'
                    , ProfileId = prof.Id
                    , UserName= 'tuser@nestaway.com'
                    , Alias='cspu' 
                    , CommunityNickName='cspu'
                    , TimeZoneSidKey='America/Los_Angeles'
                    , LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1' 
                    , LanguageLocaleKey='en_US'
                    , FirstName = 'Test_'
                    , LastName = 'Test' 
                    , Phone = '9620600507'
                   );
            insert newUser;

             User newUser1 = new User( email='firstlast@nestaway.com'
                    , ProfileId = prof.Id
                    , UserName= 'tuser123@nestaway.com'
                    , Alias='csertpu' 
                    , CommunityNickName='cswepu'
                    , TimeZoneSidKey='America/Los_Angeles'
                    , LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1' 
                    , LanguageLocaleKey='en_US'
                    , FirstName = 'Test_'
                    , LastName = 'Test2' 
                    , Phone = '9620600507'
                   );
            insert newUser1;

            System.runAs(newUser){

                  Opportunity objOpp = new Opportunity();
                objOpp.AccountId = Acc.Id;
                objOpp.Name ='Test Opp';
                objOpp.CloseDate = System.Today();
                objOpp.StageName = 'House';

                 insert objOpp;
                objOpp.ownerid=newUser1.id;
                update objOpp;

                objOpp.House_Escalation_level__c='City Escalation';
                update objOpp;

                objOpp.StageName = 'Keys Collected';
                update objOpp;
            }

        }  
 /***************************************************************/
   public Static TestMethod Void doTest10(){
    
    id furnishCaseOnboarding=Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Furnished_House_Onboarding).getRecordTypeId();

     House__c hou=Test_library.createHouse('134','Service Request Zone');
     insert hou;
     
     House_Inspection_Checklist__c hic=Test_library.createHIC();
      hic.House__c=hou.Id;
      hic.No_of_Non_Functional_Fans__c=0;
      hic.Washing_Machine__c='Yes and Not Working';
      insert hic;

      Case ca=new Case();
      ca.recordtypeid=furnishCaseOnboarding;
      ca.House__c=hou.id;
      ca.Checklist__c=hic.id;
      insert ca;

      Operational_Process__c op=new Operational_Process__c();
      op.Field_API_Name__c='No_of_Non_Functional_Fans__c';
      op.Field_Data_Type__c='Integer';
      op.Type_of_Object__c='House_Inspection_Checklist__c';
      insert op;

      Operational_Process__c op1=new Operational_Process__c();
      op1.Field_API_Name__c='Washing_Machine__c';
      op1.New_Field_Value__c='Yes and Not Working';
      op1.Closing_Value__c='Yes and Working';
      op1.Field_Data_Type__c='Picklist';
      op1.Type_of_Object__c='House_Inspection_Checklist__c';
      insert op1;

      ca.status='Maintenance';
      update ca;
      


   }  
}