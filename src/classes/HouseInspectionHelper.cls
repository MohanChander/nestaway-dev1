/*  Created By deeepak 
*  Purpose : To help functionality*/

public class HouseInspectionHelper {
    
    public static Set<Id> returnHicOFTypeHouseInspectionChecklist(Set<Id> setOfHouse){
        Set<Id> setOfHic= new Set<Id>();
        List<House_Inspection_Checklist__c> hicList = new List<House_Inspection_Checklist__c>();
        hicList = [Select id from House_Inspection_Checklist__c where house__C in :setOfHouse and Type_Of_HIC__c = :Constants.CHECKLIST_RT_House_Inspection_Checklist];
        if(!hicList.isEmpty()){
            for(House_Inspection_Checklist__c each : hicList){
                setOfHic.add(each.id);
            }
        }
        return setOfHic;
    }
}