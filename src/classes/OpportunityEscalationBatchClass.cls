global  class OpportunityEscalationBatchClass  implements Database.Batchable<sObject>,Schedulable
 {
 	public DateTime runForDate;
    
    public OpportunityEscalationBatchClass(DateTime runForDate){
  
        this.runForDate=runForDate;
    }

     global void execute(SchedulableContext sc) {
      OpportunityEscalationBatchClass batch = new OpportunityEscalationBatchClass(System.now()); 
      database.executebatch(batch);
   }

	global Database.QueryLocator start(Database.BatchableContext BC) {
		   /* Querring for lead which are in new and Open Stage and whose escalation time/drop time is less than present time and 
		   more then half hours less then present time and also more whose folllow up time is more than present time and less than  
		   half hour more than present time */
    
     dateTime dt=System.now();
    if(this.runForDate!=null){
       dt=this.runForDate;
    } 
			
	return database.getQueryLocator([SELECT Id,OwnerID,Escalation_Time__c,name,Follow_Up_Time__c,Escalation_User__c,House_Inspection_Escalation_level__c,
		Sample_Contract_Escalation_level__c,Final_Contract_Escalation_level__c,Quote_Creation_Escalation_level__c,Sales_Order_Escalation_level__c,
		Docs_Collected_Escalation_level__c,House_Escalation_level__c,StageName FROM Opportunity where (StageName!=:OpportunityConstants.OPPORTUNITY_STAGE_KEY_REC 
		and StageName!=:OpportunityConstants.OPPORTUNITY_STAGE_DROPPED and StageName!=:OpportunityConstants.OPPORTUNITY_STAGE_LOST)
		 and ((Follow_Up_Time__c > :dt and Follow_Up_Time__c < :dt.addMinutes(30)) or (Escalation_Time__c <:dt and Escalation_Time__c >:dt.addMinutes(-30)))]);
	}

	global void execute(Database.BatchableContext BC, List<Opportunity> oppList) {

			Lead_Escalation__mdt le=[Select HIC_Stage_RM_to_CT_Hrs__c,HIC_Stage_ZAM_to_RM_Hrs__c,Sample_Stage_RM_to_CT_Hrs__c,
		                         Sample_Stage_ZAM_to_RM_Hrs__c,Final_Stage_RM_to_CT_Hrs__c,Final_Stage_ZAM_to_RM_Hrs__c,Quote_Stage_RM_to_CT_Hrs__c,
		                         Quote_Stage_ZAM_to_RM_Hrs__c,Sales_Stage_ZAM_to_RM_Hrs__c,Sales_Stage_RM_to_CT_Hrs__c,Docs_Stage_RM_to_CT_Hrs__c,	Docs_Stage_ZAM_to_RM_Hrs__c,
		                         House_Stage_RM_to_CT_Hrs__c,House_Stage_ZAM_to_RM_Hrs__c,Label from Lead_Escalation__mdt where QualifiedApiName='Escalation'];

        Id  generalTask = Schema.SObjectType.Task.getRecordTypeInfosByName().get(Constants.TASK_RT_GENERAL_TASK).getRecordTypeId();
        List<task> instask=new List<task>();	
        List<Opportunity> updOpp=new List<Opportunity>(); 

        for(Opportunity opp:oppList){
        		if(opp.StageName==OpportunityConstants.OPPORTUNITY_STAGE_HOUSE_INSPECTION){
				// checking if new Stage Escaltion is null then escalate to ZAM
					if(opp.House_Inspection_Escalation_level__c ==null && 
	               	  opp.Escalation_Time__c <runForDate && opp.Escalation_Time__c > runForDate.addMinutes(-30)){

	                   opp.House_Inspection_Escalation_level__c = OpportunityConstants.ZAM_ESCALATION;
	                   opp.Escalation_Time__c=runForDate.addHours((Integer)le.HIC_Stage_ZAM_to_RM_Hrs__c);
	                    opp.SLA_Start_Time__c=runForDate;
	                    opp.Time_Spend__c=null;
	                   updOpp.add(opp);
	               }
	               else if(opp.House_Inspection_Escalation_level__c ==OpportunityConstants.ZAM_ESCALATION && 
	               	  opp.Escalation_Time__c <runForDate && opp.Escalation_Time__c > runForDate.addMinutes(-30)){

	                   opp.House_Inspection_Escalation_level__c = OpportunityConstants.RM_ESCALATION;
	                   opp.Escalation_Time__c=runForDate.addHours((Integer)le.HIC_Stage_RM_to_CT_Hrs__c);
	                   opp.SLA_Start_Time__c=runForDate;
	                   opp.Time_Spend__c=null;
  	                   updOpp.add(opp);
	               }
	               else if(opp.House_Inspection_Escalation_level__c ==OpportunityConstants.RM_ESCALATION && 
	               	  opp.Escalation_Time__c <runForDate && opp.Escalation_Time__c > runForDate.addMinutes(-30)){

	                   opp.House_Inspection_Escalation_level__c = OpportunityConstants.CT_ESCALATION;
	                   opp.SLA_Start_Time__c=runForDate; 
	                   opp.Time_Spend__c=null;  
	                   updOpp.add(opp);
	               }
	                 // creating task for FollowUp 
	               if( opp.Follow_Up_Time__c > runForDate && opp.Follow_Up_Time__c < runForDate.addMinutes(30)){
                       Task tk=new Task();
				       tk.OwnerId=opp.OwnerId;
				       tk.Subject='FollowUp reminder :'+opp.Name; 
				       tk.WhatId=opp.id;
				       tk.RecordtypeID=generalTask;
				       DateTime dT=opp.Follow_Up_Time__c;
				       tk.ActivityDate=date.newinstance(dT.year(), dT.month(), dT.day());
				      // tk.ReminderDateTime=runForDate.addMinutes(15);
				       instask.add(tk);
	               }
	           }
	           else if(opp.StageName==OpportunityConstants.OPPORTUNITY_STAGE_SAMPLE_CONTACT){
				// checking if new Stage Escaltion is null then escalate to ZAM
					if(opp.Sample_Contract_Escalation_level__c ==null && 
	               	  opp.Escalation_Time__c <runForDate && opp.Escalation_Time__c > runForDate.addMinutes(-30)){

	                   opp.Sample_Contract_Escalation_level__c = OpportunityConstants.ZAM_ESCALATION;
	                   opp.Escalation_Time__c=runForDate.addHours((Integer)le.Sample_Stage_ZAM_to_RM_Hrs__c);
	                   opp.SLA_Start_Time__c=runForDate;
	                   opp.Time_Spend__c=null;
	                   updOpp.add(opp);
	               }
	               else if(opp.Sample_Contract_Escalation_level__c ==OpportunityConstants.ZAM_ESCALATION && 
	               	  opp.Escalation_Time__c <runForDate && opp.Escalation_Time__c > runForDate.addMinutes(-30)){
	                   opp.Sample_Contract_Escalation_level__c = OpportunityConstants.RM_ESCALATION;
	                   opp.Escalation_Time__c=runForDate.addHours((Integer)le.Sample_Stage_RM_to_CT_Hrs__c);
	                   opp.SLA_Start_Time__c=runForDate;   
	                   opp.Time_Spend__c=null;   
	                   updOpp.add(opp);
	               }
	               else if(opp.Sample_Contract_Escalation_level__c ==OpportunityConstants.RM_ESCALATION && 
	               	  opp.Escalation_Time__c <runForDate && opp.Escalation_Time__c > runForDate.addMinutes(-30)){
	                   opp.Sample_Contract_Escalation_level__c = OpportunityConstants.CT_ESCALATION;
	                   opp.SLA_Start_Time__c=runForDate;
	                   opp.Time_Spend__c=null;
	                   updOpp.add(opp);
	               }
	                if( opp.Follow_Up_Time__c > runForDate && opp.Follow_Up_Time__c < runForDate.addMinutes(30)){
                       Task tk=new Task();
				       tk.OwnerId=opp.OwnerId;
				       tk.Subject='FollowUp reminder :'+opp.Name; 
				       tk.WhatId=opp.id;
				       tk.RecordtypeID=generalTask;
				       DateTime dT=opp.Follow_Up_Time__c;
				       tk.ActivityDate=date.newinstance(dT.year(), dT.month(), dT.day());
				      // tk.ReminderDateTime=runForDate.addMinutes(15);
				       instask.add(tk);
	               }
	           }
	            else if(opp.StageName==OpportunityConstants.OPPORTUNITY_STAGE_FINAL_CONTACT){
				// checking if new Stage Escaltion is null then escalate to ZAM
					if(opp.Final_Contract_Escalation_level__c ==null && 
	               	  opp.Escalation_Time__c <runForDate && opp.Escalation_Time__c > runForDate.addMinutes(-30)){
	                   opp.Final_Contract_Escalation_level__c = OpportunityConstants.ZAM_ESCALATION;
	                   opp.Escalation_Time__c=runForDate.addHours((Integer)le.Final_Stage_ZAM_to_RM_Hrs__c);
	                   updOpp.add(opp);
	               }
	               else if(opp.Final_Contract_Escalation_level__c ==OpportunityConstants.ZAM_ESCALATION && 
	               	  opp.Escalation_Time__c <runForDate && opp.Escalation_Time__c > runForDate.addMinutes(-30)){
	                   opp.Final_Contract_Escalation_level__c = OpportunityConstants.RM_ESCALATION;
	                   opp.Escalation_Time__c=runForDate.addHours((Integer)le.Final_Stage_RM_to_CT_Hrs__c);
	                   updOpp.add(opp);
	               }
	               else if(opp.Final_Contract_Escalation_level__c ==OpportunityConstants.RM_ESCALATION && 
	               	  opp.Escalation_Time__c <runForDate && opp.Escalation_Time__c > runForDate.addMinutes(-30)){
	                   opp.Final_Contract_Escalation_level__c = OpportunityConstants.CT_ESCALATION;
	                   updOpp.add(opp);
	               }
	               
	           }
	           else if(opp.StageName==OpportunityConstants.OPPORTUNITY_STAGE_QUOTE){
				// checking if new Stage Escaltion is null then escalate to ZAM
					if(opp.Quote_Creation_Escalation_level__c ==null && 
	               	  opp.Escalation_Time__c <runForDate && opp.Escalation_Time__c > runForDate.addMinutes(-30)){
	                   opp.Quote_Creation_Escalation_level__c = OpportunityConstants.ZAM_ESCALATION;
	                   opp.Escalation_Time__c=runForDate.addHours((Integer)le.Quote_Stage_ZAM_to_RM_Hrs__c);
	                   updOpp.add(opp);
	               }
	               else if(opp.Quote_Creation_Escalation_level__c ==OpportunityConstants.ZAM_ESCALATION && 
	               	  opp.Escalation_Time__c <runForDate && opp.Escalation_Time__c > runForDate.addMinutes(-30)){
	                   opp.Quote_Creation_Escalation_level__c = OpportunityConstants.RM_ESCALATION;
	                   opp.Escalation_Time__c=runForDate.addHours((Integer)le.Quote_Stage_RM_to_CT_Hrs__c);
	                   updOpp.add(opp);
	               }
	               else if(opp.Quote_Creation_Escalation_level__c ==OpportunityConstants.RM_ESCALATION && 
	               	  opp.Escalation_Time__c <runForDate && opp.Escalation_Time__c > runForDate.addMinutes(-30)){
	                   opp.Quote_Creation_Escalation_level__c = OpportunityConstants.CT_ESCALATION;
	                   updOpp.add(opp);
	               }
	               
	           }
	           else if(opp.StageName==OpportunityConstants.OPPORTUNITY_STAGE_SALES_ORDER){
				// checking if new Stage Escaltion is null then escalate to ZAM
					if(opp.Sales_Order_Escalation_level__c ==null && 
	               	  opp.Escalation_Time__c <runForDate && opp.Escalation_Time__c > runForDate.addMinutes(-30)){
	                   opp.Sales_Order_Escalation_level__c = OpportunityConstants.ZAM_ESCALATION;
	                   opp.Escalation_Time__c=runForDate.addHours((Integer)le.Sales_Stage_ZAM_to_RM_Hrs__c);
	                   updOpp.add(opp);
	               }
	               else if(opp.Sales_Order_Escalation_level__c ==OpportunityConstants.ZAM_ESCALATION && 
	               	  opp.Escalation_Time__c <runForDate && opp.Escalation_Time__c > runForDate.addMinutes(-30)){
	                   opp.Sales_Order_Escalation_level__c = OpportunityConstants.RM_ESCALATION;
	                   opp.Escalation_Time__c=runForDate.addHours((Integer)le.Sales_Stage_RM_to_CT_Hrs__c);
	                   updOpp.add(opp);
	               }
	               else if(opp.Sales_Order_Escalation_level__c ==OpportunityConstants.RM_ESCALATION && 
	               	  opp.Escalation_Time__c <runForDate && opp.Escalation_Time__c > runForDate.addMinutes(-30)){
	                   opp.Sales_Order_Escalation_level__c = OpportunityConstants.CT_ESCALATION;
	                   updOpp.add(opp);
	               }
	               
	           }
	            else if(opp.StageName==OpportunityConstants.OPPORTUNITY_STAGE_DOCS_COLL){
				// checking if new Stage Escaltion is null then escalate to ZAM
					if(opp.Docs_Collected_Escalation_level__c ==null && 
	               	  opp.Escalation_Time__c <runForDate && opp.Escalation_Time__c > runForDate.addMinutes(-30)){
	                   opp.Docs_Collected_Escalation_level__c = OpportunityConstants.ZAM_ESCALATION;
	                   opp.Escalation_Time__c=runForDate.addHours((Integer)le.Docs_Stage_ZAM_to_RM_Hrs__c);
	                   updOpp.add(opp);
	               }
	               else if(opp.Docs_Collected_Escalation_level__c ==OpportunityConstants.ZAM_ESCALATION && 
	               	  opp.Escalation_Time__c <runForDate && opp.Escalation_Time__c > runForDate.addMinutes(-30)){
	                   opp.Docs_Collected_Escalation_level__c = OpportunityConstants.RM_ESCALATION;
	                   opp.Escalation_Time__c=runForDate.addHours((Integer)le.Docs_Stage_RM_to_CT_Hrs__c);
	                   updOpp.add(opp);
	               }
	               else if(opp.Docs_Collected_Escalation_level__c ==OpportunityConstants.RM_ESCALATION && 
	               	  opp.Escalation_Time__c <runForDate && opp.Escalation_Time__c > runForDate.addMinutes(-30)){
	                   opp.Docs_Collected_Escalation_level__c = OpportunityConstants.CT_ESCALATION;
	                   updOpp.add(opp);
	               }
	               
	           }
	            else if(opp.StageName==OpportunityConstants.OPPORTUNITY_STAGE_HOUSE){
				// checking if new Stage Escaltion is null then escalate to ZAM
					if(opp.House_Escalation_level__c ==null && 
	               	  opp.Escalation_Time__c <runForDate && opp.Escalation_Time__c > runForDate.addMinutes(-30)){
	                   opp.House_Escalation_level__c = OpportunityConstants.ZAM_ESCALATION;
	                   opp.Escalation_Time__c=runForDate.addHours((Integer)le.House_Stage_ZAM_to_RM_Hrs__c);
	                   updOpp.add(opp);
	               }
	               else if(opp.House_Escalation_level__c ==OpportunityConstants.ZAM_ESCALATION && 
	               	  opp.Escalation_Time__c <runForDate && opp.Escalation_Time__c > runForDate.addMinutes(-30)){
	                   opp.House_Escalation_level__c = OpportunityConstants.RM_ESCALATION;
	                   opp.Escalation_Time__c=runForDate.addHours((Integer)le.House_Stage_RM_to_CT_Hrs__c);
	                   updOpp.add(opp);
	               }
	               else if(opp.House_Escalation_level__c ==OpportunityConstants.RM_ESCALATION && 
	               	  opp.Escalation_Time__c <runForDate && opp.Escalation_Time__c > runForDate.addMinutes(-30)){
	                   opp.House_Escalation_level__c = OpportunityConstants.CT_ESCALATION;
	                   updOpp.add(opp);
	               }
	               
	           }
        }
        if(!updOpp.isEmpty()){
        	update updOpp;
        }
        if(!instask.isEmpty()){
          insert instask;
        }	


	}

	global void finish(Database.BatchableContext BC) {
	       System.debug('Opp baych class finsihed'); 
	        
    }
}