/**********************************************************************************************************************************************
    Added by baibhav
    Purpose: To Add Agent history records 
**********************************************************************************************************************************************/
public class StageUtilityClass {
	
	public static void InsertAgentHistory(List<SObject> sobjList,Map<id,Sobject> oldmap,Boolean followUp){
        
        try{
        		List<Agent_History__c> insertHistory=new List<Agent_History__c>();
        		List<Agent_History__c> updateHistory=new List<Agent_History__c>();
        		Set<id> idSet =new Set<id>();
        
        		for(SObject obj:sobjList){
                    idSet.add(obj.id);
        		}
        		List<Agent_History__c> ahlist=[Select id,lead__c,User__c,opportunity__c,End_Time__c,Follow_Up__c,Esclation_Level__c,No_of_FollowUp_Days__c,
        		                               Stage__c,Start_Time__c from Agent_History__c where (lead__c=:idSet or opportunity__c=:idSet) and End_Time__c=null];
        		for(SObject obj:sobjList){
        
        				Agent_History__c ah=new Agent_History__c();
        	            Map<String,Object> valueMap=obj.getPopulatedFieldsAsMap();

        				if(String.valueOf(obj.getSObjectType())=='Lead'){
        					ah.Lead__c=(Id)valueMap.get('Id');
        					ah.Stage__c=(String)valueMap.get('Status');

        				}

        				else if(String.valueOf(obj.getSObjectType())=='Opportunity'){
        					ah.Opportunity__c=(Id)valueMap.get('Id');
        					ah.Stage__c=(String)valueMap.get('StageName');
        				}

        				
        				ah.Start_Time__c=System.now();
        				ah.user__c=(Id)valueMap.get('OwnerId');

        				if(!ahlist.isEmpty()){

        					for(Agent_History__c agh:ahlist){

        						if((agh.lead__c==obj.id || agh.Opportunity__c==obj.id) && followUp==false){
        							agh.End_Time__c=System.now();
                                    if(oldmap.containsKey(obj.id))
                                    {
                                    	Map<String,Object> olDvalueMap=oldmap.get(obj.id).getPopulatedFieldsAsMap();
            							if((string)olDvalueMap.get('Status')=='New'){
    		                             agh.Esclation_Level__c=(string)olDvalueMap.get('New_Stage_Escalation_Level__c');
    		        					}
    		        					else if((string)olDvalueMap.get('Status')=='Open'){
    		                             agh.Esclation_Level__c=(string)olDvalueMap.get('Open_Stage_Escalation_Level__c');        						
                    		        	}
                    		        	else if((string)olDvalueMap.get('StageName')=='House Inspection'){
    		                             agh.Esclation_Level__c=(string)olDvalueMap.get('House_Inspection_Escalation_level__c');        						
                    		        	}
                    		        	else if((string)olDvalueMap.get('StageName')=='Sample Contract'){
    		                             agh.Esclation_Level__c=(string)olDvalueMap.get('Sample_Contract_Escalation_level__c');        						
                    		        	}
                    		        	else if((string)olDvalueMap.get('StageName')=='Final Contract'){
    		                             agh.Esclation_Level__c=(string)olDvalueMap.get('Final_Contract_Escalation_level__c');        						
                    		        	}
                    		        	else if((string)olDvalueMap.get('StageName')=='Quote Creation'){
    		                             agh.Esclation_Level__c=(string)olDvalueMap.get('Quote_Creation_Escalation_level__c');        						
                    		        	}
                    		        	else if((string)olDvalueMap.get('StageName')=='Sales Order'){
    		                             agh.Esclation_Level__c=(string)olDvalueMap.get('Sales_Order_Escalation_level__c');        						
                    		        	}
                    		        	else if((string)olDvalueMap.get('StageName')=='Docs Collected'){
    		                             agh.Esclation_Level__c=(string)olDvalueMap.get('Docs_Collected_Escalation_level__c');        						
                    		        	}
                    		        	else if((string)olDvalueMap.get('StageName')=='House'){
    		                             agh.Esclation_Level__c=(string)olDvalueMap.get('House_Escalation_level__c');        						
                    		        	}
                     				}

        							updateHistory.add(agh);
        						}

        						else if(followUp==true && (agh.lead__c==obj.id || agh.Opportunity__c==obj.id)){
        							agh.Follow_Up__c=true;
        							agh.No_of_FollowUp_Days__c=(Decimal)valueMap.get('Total_No_of_FollwUp_Days__c');
        							updateHistory.add(agh);
        						}
        	                                               
        					}
        				}
        				
        
                        insertHistory.add(ah);
        		}
        		if(!updateHistory.isEmpty()){
        			update updateHistory;
        		}
        		if(!insertHistory.isEmpty() && followUp!=true){
        			insert insertHistory;
        		}
        } catch (Exception e){
          System.debug('**Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + 
                       '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + 
                       '\nStack Trace ' + e.getStackTraceString());
          UtilityClass.insertGenericErrorLog(e, 'Agent History');  
        }  
	}
/*********************************************************************************************************
Added by Baibhav
Purpose : to create service case on  Object field value
created on: 08/12/2017
**********************************************************************************************************/
    public static void forCreatingCaseFromObjectFields(List<Sobject> sobjList, Set<String> objSet,  Map<id,id> mapHicTocase, Map<id,id> mapHicToOwner,Map<id,id> mapHicToAPM){

        try{
            List<Operational_Process__c> oplist = new List<Operational_Process__c>(); 
            Map<String,id> fieldmapToProb=new Map<String,id>();
            Map<String,id> fieldmapToOprss=new Map<String,id>(); 
            List<case> caseInsrt=new List<case>();
            Map<String,String> fieldMapToType=new Map<String,String>();

            oplist=[select id,Field_API_Name__c,Type_of_Object__c,Field_Data_Type__c,New_Field_Value__c,Problem__c,Problem__r.id from Operational_Process__c where Type_of_Object__c=:objSet];
            
            if(!oplist.isEmpty()){
                for(Operational_Process__c op:oplist){
                   fieldMapToType.put(op.Field_API_Name__c.toLowerCase(),op.Field_Data_Type__c);
                }
            }
      
            System.debug('*****Start****');
      
            for(Operational_Process__c op:oplist){
              if(op.Field_Data_Type__c==Operational_ProcessConstants.FIELD_DATA_TYPE_INTEGER){
        
                      fieldmapToProb.put(op.Field_API_Name__c.toUpperCase(),op.Problem__r.id);
                      fieldmapToOprss.put(op.Field_API_Name__c.toUpperCase(),op.id); 
              }
              else if(op.Field_Data_Type__c==Operational_ProcessConstants.FIELD_DATA_TYPE_PICKLIST){
                     
                      fieldmapToProb.put(op.Field_API_Name__c.toUpperCase()+'-'+op.New_Field_Value__c,op.Problem__r.id);
                      fieldmapToOprss.put(op.Field_API_Name__c.toUpperCase()+'-'+op.New_Field_Value__c,op.id);
                  }
            }
      
            Account acc=[Select id,name from Account where name='Nestaway'];
      
            System.debug('**PB*'+fieldmapToProb);
            System.debug('**op*'+fieldmapToOprss);
      /**********************************************************************/
       Map<String, Schema.SObjectType> hicschemaMap = Schema.getGlobalDescribe();
       Schema.SObjectType hicSchema = hicschemaMap.get('House_Inspection_Checklist__c');
       Map<String, Schema.SObjectField> hicfieldMap = hicSchema.getDescribe().fields.getMap();

       Map<String, Schema.SObjectType> ricschemaMap = Schema.getGlobalDescribe();
       Schema.SObjectType ricSchema = ricschemaMap.get('Room_Inspection__c');
       Map<String, Schema.SObjectField> ricfieldMap = ricSchema.getDescribe().fields.getMap();

       Map<String, Schema.SObjectType> bicschemaMap = Schema.getGlobalDescribe();
       Schema.SObjectType bicSchema = bicschemaMap.get('Bathroom__c');
       Map<String, Schema.SObjectField> bicfieldMap = bicSchema.getDescribe().fields.getMap();

      /**************************************************************************/
      
             for(Sobject sob:sobjlist){
      
               Map<String,Object> valueMap=sob.getPopulatedFieldsAsMap();
               Map<String,Object> neWvalueMap=new Map<String,Object>();
      
               for(string st:valueMap.keySet()){
                  neWvalueMap.put(st.toUpperCase(),valueMap.get(st));
               }
      
                System.debug('**neWvalueMap*'+neWvalueMap);
            
               for (String fieldName: fieldMapToType.keySet()){
      
                  
                  if(neWvalueMap.containsKey(fieldName.toUpperCase())){
      
                      // Added to see Integer, double or Deciaml type field
                      if(fieldMapToType.get(fieldName)==Operational_ProcessConstants.FIELD_DATA_TYPE_INTEGER
                          && fieldmapToProb.containsKey(fieldName.toUpperCase()) && fieldmapToOprss.containsKey(fieldName.toUpperCase()) ){
      
                          if((Decimal)neWvalueMap.get(fieldName.toUpperCase())>0){
                               
                                System.debug('***2**'+(Decimal)neWvalueMap.get(fieldName.toUpperCase()));
                               
                               id prob = fieldmapToProb.get(fieldName.toUpperCase());
                               id oproc = fieldmapToOprss.get(fieldName.toUpperCase());
      
                               if(sob.id.getSobjectType() ==  Room_Inspection__c.SobjectType){

                                    String fieldLablename=ricfieldMap.get(fieldName).getDescribe().getLabel();
                                    id hic=(Id)neWvalueMap.get('House_Inspection_Checklist__c'.toUpperCase());
                                    caseInsrt.add(CaseCreationService.CaseCreation(prob,oproc,sob.id,hic,mapHicToOwner.get(hic),(Id)neWvalueMap.get('House__c'.toUpperCase()),mapHicTocase.get(hic),fieldLablename,mapHicToAPM.get(hic) ));
                                }
                                else if(sob.id.getSobjectType() ==  Bathroom__c.SobjectType){
                                    String fieldLablename=bicfieldMap.get(fieldName).getDescribe().getLabel();
                                  if(neWvalueMap.get('Checklist__c'.toUpperCase())!=null){
                                        id hic=(Id)neWvalueMap.get('Checklist__c'.toUpperCase());                      
                                        caseInsrt.add(CaseCreationService.CaseCreation(prob,oproc,sob.id,hic,mapHicToOwner.get(hic),(Id)neWvalueMap.get('House__c'.toUpperCase()),mapHicTocase.get(hic),fieldLablename,mapHicToAPM.get(hic) ));
                                    }
                                  else if(neWvalueMap.get('AttachedBathroom__c'.toUpperCase())!=null){
                                        id hic=(Id)neWvalueMap.get('AttachedBathroom__c'.toUpperCase());                      
                                        caseInsrt.add(CaseCreationService.CaseCreation(prob,oproc,sob.id,hic,mapHicToOwner.get(hic),(Id)neWvalueMap.get('House__c'.toUpperCase()),mapHicTocase.get(hic),fieldLablename,mapHicToAPM.get(hic) ));
                                    }
                                }
                               else {
                                     String fieldLablename=hicfieldMap.get(fieldName).getDescribe().getLabel();
                                     caseInsrt.add(CaseCreationService.CaseCreation(prob,oproc,sob.id,null,mapHicToOwner.get(sob.id),(Id)neWvalueMap.get('House__c'.toUpperCase()),mapHicTocase.get(sob.id),fieldLablename,mapHicToAPM.get(sob.id) ));
                                }
      
                          }
      
                      }
      
                      // Added to see Picklist type field
                       if(fieldMapToType.get(fieldName)==Operational_ProcessConstants.FIELD_DATA_TYPE_PICKLIST 
                          && fieldmapToProb.containsKey(fieldName.toUpperCase()+'-'+neWvalueMap.get(fieldName.toUpperCase()))
                          && fieldmapToOprss.containsKey(fieldName.toUpperCase()+'-'+neWvalueMap.get(fieldName.toUpperCase()))){
                            
                            id prob = fieldmapToProb.get(fieldName.toUpperCase()+'-'+neWvalueMap.get(fieldName.toUpperCase()));
                            id oproc = fieldmapToOprss.get(fieldName.toUpperCase()+'-'+neWvalueMap.get(fieldName.toUpperCase()));
                         
                           if(sob.id.getSobjectType() ==  Room_Inspection__c.SobjectType){
                                    id hic=(Id)neWvalueMap.get('House_Inspection_Checklist__c'.toUpperCase());  
                                    String fieldLablename=ricfieldMap.get(fieldName).getDescribe().getLabel();
                                    caseInsrt.add(CaseCreationService.CaseCreation(prob,oproc,sob.id,hic,mapHicToOwner.get(hic),(Id)neWvalueMap.get('House__c'.toUpperCase()),mapHicTocase.get(hic),fieldLablename,mapHicToAPM.get(hic) ));
                                }
                            else if(sob.id.getSobjectType() ==  Bathroom__c.SobjectType){
                                   String fieldLablename=bicfieldMap.get(fieldName).getDescribe().getLabel();
                              if(neWvalueMap.get('Checklist__c'.toUpperCase())!=null){
                                    id hic=(Id)neWvalueMap.get('Checklist__c'.toUpperCase());                      
                                    caseInsrt.add(CaseCreationService.CaseCreation(prob,oproc,sob.id,hic,mapHicToOwner.get(hic),(Id)neWvalueMap.get('House__c'.toUpperCase()),mapHicTocase.get(hic),fieldLablename,mapHicToAPM.get(hic) ));
                                }
                              else if(neWvalueMap.get('AttachedBathroom__c'.toUpperCase())!=null){
                                    id hic=(Id)neWvalueMap.get('AttachedBathroom__c'.toUpperCase());                      
                                    caseInsrt.add(CaseCreationService.CaseCreation(prob,oproc,sob.id,hic,mapHicToOwner.get(hic),(Id)neWvalueMap.get('House__c'.toUpperCase()),mapHicTocase.get(hic),fieldLablename,mapHicToAPM.get(hic) ));
                                }
                            }
                           else {
                                 String fieldLablename=hicfieldMap.get(fieldName).getDescribe().getLabel();
                                 caseInsrt.add(CaseCreationService.CaseCreation(prob,oproc,sob.id,null,mapHicToOwner.get(sob.id),(Id)neWvalueMap.get('House__c'.toUpperCase()),mapHicTocase.get(sob.id),fieldLablename,mapHicToAPM.get(sob.id) ));
                            }
                      }
      
                  }
      
               }
             }
      
             if(!caseInsrt.isEmpty()){
              System.debug('***Size'+caseInsrt.size());
              insert caseInsrt;
             }
        }catch (Exception e){
          System.debug('**Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + 
                       '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + 
                       '\nStack Trace ' + e.getStackTraceString());
          UtilityClass.insertGenericErrorLog(e, 'Stage Utllity for CaseCreation');  
        }  
      

    }

}