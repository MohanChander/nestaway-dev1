public class OpportunityTriggerHelper 
{
/**********************************************************************************************************************************************
    Added by baibhav
    Purpose to Add error when Address on Opportunity isChanged when house is there 
********************************************************************************************************************************************/
    public static void OpportunityAddressErroe(Map<id,Opportunity> oppMap)
    {
        try
        {
            List<House__c> houList=HouseSelector.getHouseListFromOppidSet(oppMap.keySet());
            Map<id,List<House__c>> houMapOpp =new Map<id,List<House__c>>();
            for(House__c ho:houList)
            {
                List<House__c> hlist =new List<House__c>();
                if(houMapOpp.containsKey(ho.Opportunity__c))
                {
                    hlist=houMapOpp.get(ho.Opportunity__c);
                } 
                hlist.add(ho);
                houMapOpp.put(ho.Opportunity__c,hlist);
            }
            for(Opportunity op:oppMap.values())
            {
                if(houMapOpp.containskey(op.id) && houMapOpp.get(op.id).size()>0)
                op.adderror('Address cannot be changed on Opportunity');
            }
            } Catch(Exception e) {
                 System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                 UtilityClass.insertGenericErrorLog(e);
            }
     
    }
/**********************************************************************************************************************************************
    Added by baibhav
    Purpose To Catch time of opportunity satge change
            1) invokes on stage change
**********************************************************************************************************************************************/
    public static void OpportunityStagetrack(List<Opportunity> oppList){
       try{
            Lead_Escalation__mdt leMdt=[SELECT DeveloperName,Id,Label,Language,Max_Opp_No_of_FollowUp__c,NamespacePrefix,
                Open_Stage_RM_to_CT_SLA_Hrs__c,Open_Stage_ZAM_to_RM_SLA_Hrs__c,Opportunity_Doc_Escalation_Hrs__c,
                Opportunity_Final_Escalation_Hrs__c,Opportunity_HI_Escalation_Hrs__c,Opportunity_House_Escalation_Hrs__c,
                Opportunity_Quote_Escalation_Hrs__c,Opportunity_Sales_Escalation_Hrs__c,Opportunity_Sample_Escalation_Hrs__c,
                Opp_Owner_req_callBack_FollowUp_dys__c,QualifiedApiName FROM Lead_Escalation__mdt where QualifiedApiName='Escalation'];

              for(Opportunity opp:oppList){

                 if(opp.StageName==Constants.OPPORTUNITY_STAGE_HOUSE){
                  opp.House_Time__c=System.now();
                   opp.Escalation_Time__c=System.Now().addHours((Integer)leMdt.Opportunity_House_Escalation_Hrs__c);
                   opp.House_Escalation_level__c=null;
                }
                else if(opp.StageName==Constants.OPPORTUNITY_STAGE_DOCS_COLL){
                  opp.Docs_Collected_Time__c=System.now();
                  opp.Escalation_Time__c=System.Now().addHours((Integer)leMdt.Opportunity_Doc_Escalation_Hrs__c);
                  opp.Docs_Collected_Escalation_level__c=null;
                }
                else if(opp.StageName==Constants.OPPORTUNITY_STAGE_SAMPLE_CONTACT){
                  opp.Sample_Contract_Time__c=System.now();              
                 opp.Escalation_Time__c=System.Now().addHours((Integer)leMdt.Opportunity_Sample_Escalation_Hrs__c);
                 opp.Sample_Contract_Escalation_level__c=null;
                }
                else if(opp.StageName==Constants.OPPORTUNITY_STAGE_FINAL_CONTACT){
                  opp.Final_Contract_Time__c=System.now();
                   opp.Escalation_Time__c=System.Now().addHours((Integer)leMdt.Opportunity_Final_Escalation_Hrs__c);
                   opp.Final_Contract_Escalation_level__c=null;
                }
                else if(opp.StageName==Constants.OPPORTUNITY_STAGE_QUOTE){
                  opp.Quote_Creation_Time__c=System.now();
                  opp.Escalation_Time__c=System.Now().addHours((Integer)leMdt.Opportunity_Quote_Escalation_Hrs__c);
                  opp.Quote_Creation_Escalation_level__c=null;
                }
                else if(opp.StageName==Constants.OPPORTUNITY_STAGE_SALES_ORDER){
                  opp.Sales_Order_Time__c=System.now();
                   opp.Escalation_Time__c=System.Now().addHours((Integer)leMdt.Opportunity_Sales_Escalation_Hrs__c);
                   opp.Sales_Order_Escalation_level__c=null;
                }
               else if(opp.StageName==Constants.OPPORTUNITY_STAGE_KEY_REC){
                  opp.Keys_Collected_Time__c=System.now();
                }
                else if(opp.StageName==Constants.OPPORTUNITY_STAGE_HOUSE_INSPECTION){
                  opp.House_Inspection_Time__c=System.now();                  
                   opp.Escalation_Time__c=System.Now().addHours((Integer)leMdt.Opportunity_HI_Escalation_Hrs__c);
                   opp.House_Inspection_Escalation_level__c=null;
                }
                else if(opp.StageName=='Dropped'){
                  opp.Dropped_Time__c=System.now();
                }
                else if(opp.StageName=='Lost'){
                  opp.Lost_Time__c=System.now();
                }
              }
          }  Catch(Exception e) {
         System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                 UtilityClass.insertGenericErrorLog(e);
      }       
  }
/**********************************************************************************************************************************************
    Added by baibhav
    Purpose To handle the FollowUp Hours ligic 
**********************************************************************************************************************************************/
    public static void OpportunityFollowUp(List<Opportunity> oppList,Map<id,Opportunity> oldMap){
      
      try{

          Lead_Escalation__mdt leMdt=[SELECT DeveloperName,Id,Label,Language, Max_Opp_No_of_FollowUp__c,NamespacePrefix,Sample_Stage_ZAM_to_RM_Hrs__c,
            Opportunity_Doc_Escalation_Hrs__c,Opportunity_Final_Escalation_Hrs__c,Opportunity_HI_Escalation_Hrs__c,
            Opportunity_House_Escalation_Hrs__c,Opportunity_Quote_Escalation_Hrs__c,Opportunity_Sales_Escalation_Hrs__c,Sample_Stage_RM_to_CT_Hrs__c,
            Opportunity_Sample_Escalation_Hrs__c,Opp_Owner_req_callBack_FollowUp_dys__c,QualifiedApiName,HIC_Stage_ZAM_to_RM_Hrs__c,HIC_Stage_RM_to_CT_Hrs__c
            FROM Lead_Escalation__mdt where QualifiedApiName='Escalation'];

            for(Opportunity opp:oppList){

                    if(opp.Opportunity_followUp_Count__c==null){
                     opp.Opportunity_followUp_Count__c=0;
                   }
                    if(opp.Opportunity_followUp_Count__c>=(Integer)leMdt.Max_Opp_No_of_FollowUp__c){
                        opp.addError('Max Opportunity NO of Follow allow is: '+leMdt.Max_Opp_No_of_FollowUp__c);
                    }
                   else{
                       opp.Opportunity_followUp_Count__c=opp.Opportunity_followUp_Count__c+1;
                      }
/*******************************************************************************/
                    
                    if(opp.Follow_Up_Time__c < System.now()){
                        opp.addError('Follow up should be greater then present time');

                    }
                    else if(oldMap.get(opp.id).Follow_Up_Time__c==null || opp.Sample_Contract_Escalation_level__c!=oldMap.get(opp.id).Sample_Contract_Escalation_level__c 
                      || opp.House_Inspection_Escalation_level__c!=oldMap.get(opp.id).House_Inspection_Escalation_level__c){

                        Long newdt1Long = opp.SLA_Start_Time__c.getTime();
                        Long newdt2Long = System.now().getTime();
                        Long newmilliseconds = newdt2Long - newdt1Long;
                        Long newseconds = newmilliseconds / 1000;
                        long newminutes = newseconds / 60;
                       

                        opp.Time_Spend__c=newminutes;  
                        System.debug('******fo'+opp.Time_Spend__c);                 
                    }
                    else if(oldMap.get(opp.id).Follow_Up_Time__c < System.now()){

                        Long newdt1Long = oldMap.get(opp.id).Follow_Up_Time__c.getTime();
                        Long newdt2Long = System.now().getTime();
                        Long newmilliseconds = newdt2Long - newdt1Long;
                        Long newseconds = newmilliseconds / 1000;
                        long newminutes = newseconds / 60;
                         if(opp.Time_Spend__c==null){
                          opp.Time_Spend__c=0 ;   
                         }
                        opp.Time_Spend__c=opp.Time_Spend__c+newminutes; 
                        System.debug('******fo1'+opp.Time_Spend__c);                 
                        
                    }

                     if(opp.Time_Spend__c==null){
                          opp.Time_Spend__c=0 ;   
                      }
                    
                    if(opp.StageName==Constants.OPPORTUNITY_STAGE_HOUSE_INSPECTION){ 
                          if(opp.House_Inspection_Escalation_level__c==null){
                             Integer addmin=(Integer)((leMdt.Opportunity_HI_Escalation_Hrs__c*60)-opp.Time_Spend__c);
                             opp.Escalation_Time__c=opp.Follow_Up_Time__c.addMinutes(addmin);
                        System.debug('******fo2'+opp.Escalation_Time__c);                 

                          }
                          else if(opp.House_Inspection_Escalation_level__c==LeadConstants.ZAM_ESCALATION){
                             Integer addmin=(Integer)((leMdt.HIC_Stage_ZAM_to_RM_Hrs__c*60)-opp.Time_Spend__c);
                             opp.Escalation_Time__c=opp.Follow_Up_Time__c.addMinutes(addmin);
                           }
                          else if(opp.House_Inspection_Escalation_level__c==LeadConstants.RM_ESCALATION){
                              Integer addmin=(Integer)((leMdt.HIC_Stage_RM_to_CT_Hrs__c*60)-opp.Time_Spend__c);
                              opp.Escalation_Time__c=opp.Follow_Up_Time__c.addMinutes(addmin);
                          }
                        }
                         else if(opp.StageName==Constants.OPPORTUNITY_STAGE_SAMPLE_CONTACT){ 
                          if(opp.Sample_Contract_Escalation_level__c==null){
                             Integer addmin=(Integer)((leMdt.Opportunity_Sample_Escalation_Hrs__c*60)-opp.Time_Spend__c);
                             opp.Escalation_Time__c=opp.Follow_Up_Time__c.addMinutes(addmin);
                          }
                          else if(opp.Sample_Contract_Escalation_level__c==LeadConstants.ZAM_ESCALATION){
                             Integer addmin=(Integer)((leMdt.Sample_Stage_ZAM_to_RM_Hrs__c*60)-opp.Time_Spend__c);
                             opp.Escalation_Time__c=opp.Follow_Up_Time__c.addMinutes(addmin);
                           }
                          else if(opp.Sample_Contract_Escalation_level__c==LeadConstants.RM_ESCALATION){
                              Integer addmin=(Integer)((leMdt.Sample_Stage_RM_to_CT_Hrs__c*60)-opp.Time_Spend__c);
                              opp.Escalation_Time__c=opp.Follow_Up_Time__c.addMinutes(addmin);
                          }
                        }
                        
/**********************************************************************************/


                       if(oldMap.get(opp.id).Follow_Up_Time__c==null){
                        Long newdt1Long = opp.Stage_Time__c.getTime();
                        Long newdt2Long = opp.Follow_Up_Time__c.getTime();
                        Long newmilliseconds = newdt2Long - newdt1Long;
                        Long newseconds = newmilliseconds / 1000;
                        long newminutes = newseconds / 60;
                        long hours=newminutes/60;
                        System.debug('**12***');
                       opp.Total_No_of_FollwUp_Hours__c=(Integer)hours;
                    }
                     else if(oldMap.get(opp.id).Follow_Up_Time__c> opp.Follow_Up_Time__c){
                        Long newdt1Long = opp.Follow_Up_Time__c.getTime();
                        Long newdt2Long = oldMap.get(opp.id).Follow_Up_Time__c.getTime();
                        Long newmilliseconds = newdt2Long - newdt1Long;
                        Long newseconds = newmilliseconds / 1000;
                        long newminutes = newseconds / 60;
                        long hours=newminutes/60;
                        if(opp.Total_No_of_FollwUp_Hours__c==null){
                          opp.Total_No_of_FollwUp_Hours__c=0 ;   
                        }
                        System.debug('**13***');

                       opp.Total_No_of_FollwUp_Hours__c=opp.Total_No_of_FollwUp_Hours__c-hours;
                       
                   }
                    else if(oldMap.get(opp.id).Follow_Up_Time__c!=null){
                         if(System.now()>oldMap.get(opp.id).Follow_Up_Time__c){
                             Long newdt1Long = System.now().getTime();
                            Long newdt2Long =opp.Follow_Up_Time__c.getTime();
                            Long newmilliseconds = newdt2Long - newdt1Long;
                            Long newseconds = newmilliseconds / 1000;
                            long newminutes = newseconds / 60;
                            long hours=newminutes/60;
                              if(opp.Total_No_of_FollwUp_Hours__c==null){
                              opp.Total_No_of_FollwUp_Hours__c=0 ;   
                              }
                            System.debug('**14***');

                             opp.Total_No_of_FollwUp_Hours__c=opp.Total_No_of_FollwUp_Hours__c+hours;
                        }
                        else {
                            Long newdt1Long = oldMap.get(opp.id).Follow_Up_Time__c.getTime();
                            Long newdt2Long =opp.Follow_Up_Time__c.getTime();
                            Long newmilliseconds = newdt2Long - newdt1Long;
                            Long newseconds = newmilliseconds / 1000;
                            long newminutes = newseconds / 60;
                            long hours=newminutes/60;
                            System.debug('***adss*');
                              if(opp.Total_No_of_FollwUp_Hours__c==null){
                               opp.Total_No_of_FollwUp_Hours__c=0 ;   
                              }
                            System.debug('**15***');

                            opp.Total_No_of_FollwUp_Hours__c=opp.Total_No_of_FollwUp_Hours__c+hours;
                        }  
                       
                   } 

                  

                         
                    
                     if(opp.Follow_Up_Reason__c==OpportunityConstants.OPPORTUNITY_OWNER_REQ_FOLLOWUP && opp.Stage_Time__c.Date().daysBetween(opp.Follow_Up_Time__c.Date())>(Integer)leMdt.Opp_Owner_req_callBack_FollowUp_dys__c){
                      opp.addError('Total Followup cannot be more then '+(Integer)leMdt.Opp_Owner_req_callBack_FollowUp_dys__c+' days');
                     }
                 
      
            }
          StageUtilityClass.InsertAgentHistory(oppList,null,true);
        } Catch(Exception e) {
                 System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                 UtilityClass.insertGenericErrorLog(e);
            }
    }
     // Added by Deepak 
    // To get the recordtypeid basis on house layout and furnishing type
    public static string getRecordTypeId(String FurnishingType,String HouseLayout ){
        String recordtypeId;
        Id  onboardingPrecheckFurnishedBHK = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get(Constants.CHECKLIST_RT_Onboarding_Precheck_Furnished_BHK).getRecordTypeId();
        Id  onboardingPrecheckFurnishedRoom =Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get(Constants.CHECKLIST_RT_Onboarding_Precheck_Furnished_Room).getRecordTypeId();
        Id  onboardingPrecheckFurnishedStudio = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get(Constants.CHECKLIST_RT_Onboarding_Precheck_Furnished_Studio).getRecordTypeId();
        Id  onboardingPrecheckUnfurnishedBHK = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get(Constants.CHECKLIST_RT_Onboarding_Precheck_Unfurnished_BHK).getRecordTypeId();
        Id  onboardingPrecheckUnfurnishedRoom = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get(Constants.CHECKLIST_RT_Onboarding_Precheck_Unfurnished_Room).getRecordTypeId();
        Id  onboardingPrecheckUnfurnishedStudio = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get(Constants.CHECKLIST_RT_Onboarding_Precheck_Unfurnished_Studio).getRecordTypeId();
   
         System.debug('FurnishingType'+FurnishingType);
        System.debug('HouseLayout'+HouseLayout);
        if(FurnishingType == 'Fully Furnished' || FurnishingType == 'Semi-Furnished' ){
            if(HouseLayout == 'Studio'){
                recordtypeId=onboardingPrecheckFurnishedStudio;
            }
            else if(HouseLayout == '1 RK' ||  HouseLayout =='1 R'){
                recordtypeId=onboardingPrecheckFurnishedRoom;
            }
            else{
                recordtypeId=onboardingPrecheckFurnishedBHK;
            }
        }
        else{
            if(HouseLayout == 'Studio'){
                recordtypeId=onboardingPrecheckUnfurnishedStudio;
            }
            else if(HouseLayout == '1 RK' ||  HouseLayout =='1 R'){
                recordtypeId=onboardingPrecheckUnfurnishedRoom;
            }
            else{
                recordtypeId=onboardingPrecheckUnfurnishedBHK;
            }
        }
        
        return recordtypeId;
    }
    
/**********************************************
    Created By : Baibhav
    Purpose    : Update the Next Escalation Email on Insert or On change of Opportunity Owner
**********************************************/
  public static void populateOwnerEmailId(List<Opportunity> oppList){

        System.debug('**populateOwnerEmailId');
        
        try{  
            Set<Id> ownerIdSet = new Set<Id>();

            for(Opportunity opp: oppList){
              ownerIdSet.add(opp.OwnerId);
            }

            Map<Id, User> userMap = new Map<Id, User>(UserSelector.getUserDetails(ownerIdSet));

            for(Opportunity opp: oppList){
                    if(string.valueOf(opp.OwnerId).startsWith('005')){

                        opp.Next_Escalation_Email__c = userMap.get(opp.OwnerId).Email;
                        opp.Escalation_User__c = opp.OwnerId;
                    }
            }

            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'Opp - populating Owner Email Id on Opportunity');      
            }       
  } 
/**********************************************
    Created By : Baibhav
    Purpose    : 1) on change of Stage Escalation Level  populate the Next Escalation Email
                    and Next Escalation User
                 2) To populate leve1,level2,leve3 Excalation email reciptent                 
**********************************************/
  public static void populateNextEscalationEmail(List<Opportunity> oppList,Map<id,Opportunity> oldmap){

        System.debug('**populateNextEscalationEmail');

        try{  
                Set<Id> ownerIdSet = new Set<Id>();
            Set<Id> userIdSet = new Set<Id>();
                Map<Id, User> userMap = new Map<Id, User>();

            for(Opportunity opp: oppList){
              if(opp.Escalation_User__c != null){
                userIdSet.add(opp.Escalation_User__c);
              } else if(string.valueOf(opp.OwnerId).startsWith('005')){
                userIdSet.add(opp.OwnerId);
              }
            }

                if(!userIdSet.isEmpty()){
                    userMap = new Map<Id, User>(UserSelector.getUserDetails(userIdSet));
                }    

            for(Opportunity opp: oppList){

                    if(opp.Escalation_User__c != null && userMap.containsKey(opp.Escalation_User__c)){
                        if(userMap.get(opp.Escalation_User__c).ManagerId != null){
                            opp.Next_Escalation_Email__c = userMap.get(opp.Escalation_User__c).Manager.Email;
                            opp.Escalation_User__c = userMap.get(opp.Escalation_User__c).ManagerId;
                        }
                    }
                     else if(string.valueOf(opp.OwnerId).startsWith('005') && userMap.containsKey(opp.OwnerId)){
                        if(userMap.get(opp.OwnerId).ManagerId != null){
                            opp.Next_Escalation_Email__c = userMap.get(opp.OwnerId).Manager.Email;
                            opp.Escalation_User__c = userMap.get(opp.OwnerId).ManagerId;
                        }
                   } 

                    if(opp.Level_1_Escalation__c==null && 
                        ((opp.House_Inspection_Escalation_level__c==OpportunityConstants.ZAM_ESCALATION && oldmap.get(opp.id).House_Inspection_Escalation_level__c!=OpportunityConstants.ZAM_ESCALATION) 
                            || (opp.Sample_Contract_Escalation_level__c==OpportunityConstants.ZAM_ESCALATION && oldmap.get(opp.id).Sample_Contract_Escalation_level__c!=OpportunityConstants.ZAM_ESCALATION)
                            || (opp.Final_Contract_Escalation_level__c==OpportunityConstants.ZAM_ESCALATION && oldmap.get(opp.id).Final_Contract_Escalation_level__c!=OpportunityConstants.ZAM_ESCALATION)
                            || (opp.Quote_Creation_Escalation_level__c==OpportunityConstants.ZAM_ESCALATION && oldmap.get(opp.id).Quote_Creation_Escalation_level__c!=OpportunityConstants.ZAM_ESCALATION)
                            || (opp.Sales_Order_Escalation_level__c==OpportunityConstants.ZAM_ESCALATION && oldmap.get(opp.id).Sales_Order_Escalation_level__c!=OpportunityConstants.ZAM_ESCALATION)
                            || (opp.Docs_Collected_Escalation_level__c==OpportunityConstants.ZAM_ESCALATION && oldmap.get(opp.id).Docs_Collected_Escalation_level__c!=OpportunityConstants.ZAM_ESCALATION)
                            || (opp.House_Escalation_level__c==OpportunityConstants.ZAM_ESCALATION && oldmap.get(opp.id).House_Escalation_level__c!=OpportunityConstants.ZAM_ESCALATION))){
                           opp.Level_1_Escalation__c= opp.Next_Escalation_Email__c;
                        
                    }

                    if(opp.Level_2_Escalation__c==null && 
                        ((opp.House_Inspection_Escalation_level__c==OpportunityConstants.RM_ESCALATION && oldmap.get(opp.id).House_Inspection_Escalation_level__c!=OpportunityConstants.RM_ESCALATION) 
                            || (opp.Sample_Contract_Escalation_level__c==OpportunityConstants.RM_ESCALATION && oldmap.get(opp.id).Sample_Contract_Escalation_level__c!=OpportunityConstants.RM_ESCALATION)
                            || (opp.Final_Contract_Escalation_level__c==OpportunityConstants.RM_ESCALATION && oldmap.get(opp.id).Final_Contract_Escalation_level__c!=OpportunityConstants.RM_ESCALATION)
                            || (opp.Quote_Creation_Escalation_level__c==OpportunityConstants.RM_ESCALATION && oldmap.get(opp.id).Quote_Creation_Escalation_level__c!=OpportunityConstants.RM_ESCALATION)
                            || (opp.Sales_Order_Escalation_level__c==OpportunityConstants.RM_ESCALATION && oldmap.get(opp.id).Sales_Order_Escalation_level__c!=OpportunityConstants.RM_ESCALATION)
                            || (opp.Docs_Collected_Escalation_level__c==OpportunityConstants.RM_ESCALATION && oldmap.get(opp.id).Docs_Collected_Escalation_level__c!=OpportunityConstants.RM_ESCALATION)
                            || (opp.House_Escalation_level__c==OpportunityConstants.RM_ESCALATION && oldmap.get(opp.id).House_Escalation_level__c!=OpportunityConstants.RM_ESCALATION))){
                          opp.Level_2_Escalation__c = opp.Next_Escalation_Email__c;
                    }

                    if(opp.Level_3_Escalation__c==null && 
                        ((opp.House_Inspection_Escalation_level__c==OpportunityConstants.CT_ESCALATION && oldmap.get(opp.id).House_Inspection_Escalation_level__c!=OpportunityConstants.CT_ESCALATION) 
                             || (opp.Sample_Contract_Escalation_level__c==OpportunityConstants.CT_ESCALATION && oldmap.get(opp.id).Sample_Contract_Escalation_level__c!=OpportunityConstants.CT_ESCALATION)
                             || (opp.Final_Contract_Escalation_level__c==OpportunityConstants.CT_ESCALATION && oldmap.get(opp.id).Final_Contract_Escalation_level__c!=OpportunityConstants.CT_ESCALATION)
                             || (opp.Quote_Creation_Escalation_level__c==OpportunityConstants.CT_ESCALATION && oldmap.get(opp.id).Quote_Creation_Escalation_level__c!=OpportunityConstants.CT_ESCALATION)
                             || (opp.Sales_Order_Escalation_level__c==OpportunityConstants.CT_ESCALATION && oldmap.get(opp.id).Sales_Order_Escalation_level__c!=OpportunityConstants.CT_ESCALATION)
                             || (opp.Docs_Collected_Escalation_level__c==OpportunityConstants.CT_ESCALATION && oldmap.get(opp.id).Docs_Collected_Escalation_level__c!=OpportunityConstants.CT_ESCALATION)
                             || (opp.House_Escalation_level__c==OpportunityConstants.CT_ESCALATION && oldmap.get(opp.id).House_Escalation_level__c!= OpportunityConstants.CT_ESCALATION))){
                          opp.Level_3_Escalation__c = opp.Next_Escalation_Email__c;
                    }
                    
                     
            }

            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'Opp - populateNextEscalationEmail');      
            }       
  } 
/**********************************************
    Created By : Baibhav
    Purpose    : 1) To reset Esclation level on owner change
                                
**********************************************/

  public static void ClearEscaltionlevelOnownerChange(List<Opportunity> opplist){
    try{
        for(Opportunity opp:opplist){
          System.debug('*******test1'+opp);
           if(opp.StageName==Constants.OPPORTUNITY_STAGE_HOUSE_INSPECTION){
              opp.House_Inspection_Escalation_level__c=null;
           }
           else if(opp.StageName==Constants.OPPORTUNITY_STAGE_SAMPLE_CONTACT){
              opp.Sample_Contract_Escalation_level__c=null;
           }
            else if(opp.StageName==Constants.OPPORTUNITY_STAGE_FINAL_CONTACT){
              opp.Final_Contract_Escalation_level__c=null;
           }
            else if(opp.StageName==Constants.OPPORTUNITY_STAGE_QUOTE){
              opp.Quote_Creation_Escalation_level__c=null;
           }
            else if(opp.StageName==Constants.OPPORTUNITY_STAGE_SALES_ORDER){
              opp.Sales_Order_Escalation_level__c=null;
           }
            else if(opp.StageName==Constants.OPPORTUNITY_STAGE_DOCS_COLL){
              opp.Docs_Collected_Escalation_level__c=null;
           }
            else if(opp.StageName==Constants.OPPORTUNITY_STAGE_HOUSE){
              opp.House_Escalation_level__c=null;
           }
          System.debug('*******test2'+opp);
    
        }
      }  catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'Opportunity - EscaltionOwnerChange');      
            } 
  } 
/**********************************************
    Created By : Baibhav
    Purpose    : 1) To Create Escalation task
                                
**********************************************/ 
  public static void CreateEscalationtask(List<opportunity> opplist){
    try{
      Id  generalTask = Schema.SObjectType.Task.getRecordTypeInfosByName().get(Constants.TASK_RT_GENERAL_TASK).getRecordTypeId();
        List<task> inTaskList=new List<Task>();
        for(Opportunity opp:opplist){
           Task tk=new Task();
           tk.OwnerId=opp.Escalation_User__c;
           tk.Subject='Escaltion :'+opp.name; 
           tk.WhatID=opp.id;
           tk.RecordtypeID=generalTask;
           tk.ActivityDate=System.Today();
           inTaskList.add(tk);
        }
        if(!inTaskList.isEmpty()){
          insert inTaskList;
        }
      }catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'Opportunity - EscaltionTaskCreation');      
            } 
  }
    // Added by Deepak
    public static void chnageHicRecordTYpeId(List<Opportunity> oppList){
        Set<Id> setOppId = new Set<Id>();
        List<House_Inspection_Checklist__c > hicList = new List<House_Inspection_Checklist__c >();
        List<Opportunity> opportList = new List<Opportunity>();
        for(Opportunity each : oppList){
            setOppId.add(each.id);
        }
        if(!setOppId.isEmpty()){
            opportList = [Select id,Furnishing_Type__c,House_Layout__c,(Select id,Furnishing_condition__c,RecordTypeId from  House_Inspection_Checklist__R) from Opportunity where id in:setOppId]; 
        }
        system.debug('opportList'+opportList);
        if(!opportList.isEmpty()){
            for(opportunity opp : opportList){
                for(House_Inspection_Checklist__c  hic : opp.House_Inspection_Checklist__R){
                    hic.Furnishing_condition__c  = opp.Furnishing_Type__c;
                    hic.RecordTypeId = getRecordTypeId(opp.Furnishing_Type__c,opp.House_Layout__c);
                    system.debug('hicrecordtype'+getRecordTypeId(opp.Furnishing_Type__c,opp.House_Layout__c));
                    hicList.add(hic);
                }
            }
        }
        system.debug('hicList'+hicList);
        if(!hicList.isEmpty()){
            update hicList;
        }
    }
}