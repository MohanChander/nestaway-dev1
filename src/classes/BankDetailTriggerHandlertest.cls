@IsTest
public with sharing class BankDetailTriggerHandlertest {
	public static TestMethod void tes1(){
		Account acc=Test_library.createAccount();
		insert acc;
		Bank_Detail__c bk=new Bank_Detail__c();
		bk.Bank_Name__c='SBI';
		bk.Related_Account__c=acc.id;
		bk.Branch_Name__c='Golmuri';
		bk.Account_Number__c='123456789000034';
		bk.IFSC_Code__c='SBIN0010508';
		insert bk; 

		
		id sdpaymentRecordType = Schema.SObjectType.Workorder.getRecordTypeInfosByName().get(Constants.WORKORDER_RECORD_SD_PAYMENT).getRecordTypeId();
		Workorder wrk=new Workorder();
		wrk.AccountId=acc.id;
		wrk.RecordtypeId=sdpaymentRecordType;
		wrk.Bank_Detail__c=bk.id;
		wrk.Account_Number__c=bk.Account_Number__c;
		wrk.Bank_Name__c=bk.Bank_Name__c;
		wrk.IFSC_Code__c=bk.IFSC_Code__c;
		wrk.Branch_Name__c=bk.Branch_Name__c;
		insert wrk;

		bk.Bank_Name__c='HDFC';
		update bk;

	}
}