/*  Created By : Mohan
    Purpose    : Queries related to Group and GroupMember Object */
public class GroupAndGroupMemberSelector {

    //get Queue Id from Queue Name
    public static Id getIdfromName(String queueName){
        return [select Id from Group where Name =: queueName].Id;
    }

    //get QueueMap for all the Queues
    public static Map<String, Id> getQueueMap(){
        Map<String, Id> queueMap = new Map<String, Id>();

        for(Group grp: [select Id, Name from Group]){
            queueMap.put(grp.Name, grp.Id);
        }

        return queueMap;
    }


    //get Queue Members related to Queue
    public Static List<GroupMember> getQueueMembers(Set<String> queues, String sObjectType){
        return [SELECT UserOrGroupId, Group.Name from GroupMember where 
                GroupId in (SELECT QueueId from QueueSobject where SobjectType =: sObjectType) and
                Group.Name =: queues];
    }
}