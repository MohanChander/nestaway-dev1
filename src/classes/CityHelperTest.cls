@Istest
public with sharing class CityHelperTest {
	/*public Static TestMethod void doTest(){
            Test.StartTest();
		City__c ct=new City__c();
		ct.name='Bangalore';
		insert ct;

		City__c ct1=new City__c();
		ct1.name='Banglaore';
		ct1.NewCity__c=ct.id;
		insert ct1;

		 Lead objLead = new Lead();
            objLead.FirstName                  = 'Test';
            objLead.LastName                   = 'Test';
            objLead.Phone                      = '9066955369';
            objLead.MobilePhone                = '9406695578';      
            objLead.Email                      = 'Test@test.com';
            objLead.Source_Type__c             = 'Assisted';
            objLead.Area_Code__c               = '7';         
            objLead.LeadSource                 = 'City Marketing';
            objLead.City_Marketing_Activity__c = 'Roadshow';
            objLead.Street                     = 'Test';
            objLead.City                       = 'Bangalore';
            objLead.State                      = 'Karnataka';
            objLead.Status                     = 'New';
            objLead.PostalCode                 = '560078';
            objLead.Country                    = 'India';
            objLead.Company                    = 'NestAway';
            objLead.IsValidPrimaryContact__c           = True;        
            insert objLead;

            objLead.City    = 'Banglaore';
            update objLead;
            Test.StopTest();

	}*/
      public Static TestMethod void doTest1(){
            Test.StartTest();
            City__c ct=new City__c();
            ct.name='Bangalore';
            insert ct;

            City__c ct1=new City__c();
            ct1.name='Banglaore';
            ct1.NewCity__c=ct.id;
            insert ct1;

             Opportunity opp = new Opportunity();
            opp.Name ='Test Opp'; 
            opp.CloseDate = System.Today();
            opp.StageName = 'Quote Creation';
            opp.State__c = 'Karnataka';
            opp.City__c='Bangalore';
            insert opp;

            opp.City__c  = 'Banglaore';
            update opp;

            Test.StopTest();
      }
       public Static TestMethod void doTest2(){
            Test.StartTest();
            City__c ct=new City__c();
            ct.name='Bangalore';
            insert ct;

            City__c ct1=new City__c();
            ct1.name='Banglaore';
            ct1.NewCity__c=ct.id;
            insert ct1;

            NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
            cusSet.name = 'nestaway';
            cusSet.Nestaway_URL__c = 'www.test.com';
            insert cusSet;
            User newUser = Test_library.createStandardUser(1);
            insert newuser;
            Account accObj = new Account();
            accObj.Name = 'TestAcc';
            insert accObj;
       
        
       Opportunity opp = new Opportunity();
       opp.Name ='Test Opp'; 
       opp.CloseDate = System.Today();
       opp.StageName = 'Quote Creation';
       opp.State__c = 'Karnataka';
       insert opp;

        contract c= new Contract();
        c.accountid=accobj.id;
        c.Opportunity__c=opp.id;
        c.Furnishing_Type__c = Constants.CONTRACT_FURNISHING_CONDITION_FURNISHED;
        insert c;
        
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        Room_Terms__c rc= new Room_Terms__c();
        insert rc;
       
        House__c hos= new House__c();
        hos.name='house1';
        hos.Contract__c=c.id;
        hos.City__c='Bangalore';
        hos.Locality_city__c='Bangalore';
        hos.House_Owner__c=accObj.id;
        hos.Assets_Created__c=false;
        hos.ZAM__c= newUser.id;  
        hos.Stage__c =  Constants.HOUSE_STAGE_HOUSE_DRAFT;
        hos.Opportunity__c=opp.id;
        hos.Initiate_Offboarding__c='Yes';
        hos.OwnerId=newuser.id;
        insert hos;

       hos.City__c  = 'Banglaore';
       update opp;

       hos.Locality_city__c  = 'Banglaore';
       update opp;

       Test.StopTest();
      }
}