@isTest
public Class  ContractSelectorTest {
    public Static TestMethod void testMethod1(){
        Set<id> setOfHouse = new Set<Id>();
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        insert hos;
        setOfHouse.add(hos.Id);
        Set<Id> setOfOppid = new Set<Id>();
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp'; 
        opp.CloseDate = System.Today();
        opp.StageName = 'Quote Creation';
        opp.State__c = 'Karnataka';
        insert opp;
        setOfOppid.add(opp.Id);
        ContractSelector.getContractListFromHouseIdSet(setOfHouse);
        ContractSelector.getContractListFromOpportunities(setOfOppid);
    }
}