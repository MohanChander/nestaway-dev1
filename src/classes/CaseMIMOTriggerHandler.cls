/*  Created By : Mohan 
    Purpose    : All the Trigger invocations for MIMO functionality are handled from this class */

public class CaseMIMOTriggerHandler {

    public static Id moveInRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEIN).getRecordTypeId();
    public static Id moveOutRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_MOVEOUT).getRecordTypeId();

    /*  Created By : Mohan 
        Purpose    : All the afterUpdate operations for MIMO are handled here */
    public static void afterUpdate(Map<Id, Case> newMap, Map<Id, Case> oldMap){

            System.debug('***************************CaseMIMOTriggerHandler-afterUpdate');
            
        try{
                List<Case> moveInScheduledCaseList = new List<Case>();
                List<Case> moveOutScheduledCaseList = new List<Case>();

                for(Case c: newMap.values()){
                    Case oldCase = oldMap.get(c.Id);
                    
                  //chandu: adding the stage for creating workorders   

                 //   if(c.RecordTypeId == moveInRecordTypeId && c.Status == Constants.CASE_STATUS_MOVE_IN_SCHEDULED && c.Status != oldCase.Status){
                   if(c.RecordTypeId == moveInRecordTypeId && c.stage__c== Constants.CASE_STATUS_MOVE_IN_WIP && c.stage__c!= oldCase.stage__c){
                        moveInScheduledCaseList.add(c);
                    }

                    if(c.RecordTypeId == moveOutRecordTypeId && c.Status == Constants.CASE_STATUS_MOVE_OUT_INSPECTION_PENDING && c.Status != oldCase.Status){
                        moveOutScheduledCaseList.add(c);
                    }                   
                }

                if(!moveInScheduledCaseList.isEmpty()){
                    CaseMoveInHelper.createMoveInOrMoveOutCheckWorkOrder(moveInScheduledCaseList);
                }

                if(!moveOutScheduledCaseList.isEmpty()){
                    CaseMoveInHelper.createMoveInOrMoveOutCheckWorkOrder(moveOutScheduledCaseList);
                }
            } catch(Exception e){
                System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                UtilityClass.insertGenericErrorLog(e, 'MIMO Functionality');                
            }
    }  
}