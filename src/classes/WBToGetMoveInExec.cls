@RestResource(urlMapping='/GetExecutiveForZone/*')
global with sharing class WBToGetMoveInExec {
    global class HouseIdsWrapper {
        public String houseId;
        public string caseType;
    }
    @HttpPost   
    global static WBMIMOJSONWrapperClasses.SendFEDetailsToWA getMoveInExecs() {
        String ExecutiveJson;
        RestRequest request = RestContext.request;
        String jSONRequestBody = request.requestBody.toString().trim();
        //fromJSONWrapper JSONDeserialized = (fromJSONWrapper) JSON.deserializeStrict(jSONRequestBody, fromJSONWrapper.class);
        //System.debug('JSONDeserialized '+JSONDeserialized);
        HouseIdsWrapper JSONDeserialized = (HouseIdsWrapper) JSON.deserializeStrict(jSONRequestBody, HouseIdsWrapper.class);
        System.debug('****request '+jSONRequestBody + '\nJSONDeserialized -> '+JSONDeserialized);
        System.debug('**** input request HouseIdsList '+JSONDeserialized);
        set<id> houseIds_MoveIn = new set<id> ();
        set<id> houseIds_MoveOut = new set<id> ();

        if(JSONDeserialized != null && JSONDeserialized.houseId != null && JSONDeserialized.caseType != null) {
            if(JSONDeserialized.caseType == 'movein') {
                houseIds_MoveIn.add(Id.valueOf(JSONDeserialized.houseId));
            }
            else {
                houseIds_MoveOut.add(Id.valueOf(JSONDeserialized.houseId));
            }
        }
        /*for(HouseIdsWrapper each : JSONDeserialized) {
            houseIds.add(Id.valueOf(each.houseId));
        }*/
         
        /*Houses for movein Exex */

        list<house__C> houses_movein = new list<house__C> ();
        houses_movein = [select id,Onboarding_Zone_Code__c  from house__c where id in : houseIds_MoveIn];
        map<id,house__c> HouseAndZoneCodeMap_movein = new map<id,house__c>();

        map<String,list<user>> returnedMap_ZoneAndUser = new map<String,list<user>>();


        for(house__c each : houses_movein) {
            if(each.Onboarding_Zone_Code__c != null) {
                System.debug('**HouseAndZoneCodeMap_movein'+HouseAndZoneCodeMap_movein);
                HouseAndZoneCodeMap_movein.put(each.id,each);
            }
        }

        if(HouseAndZoneCodeMap_movein != null && HouseAndZoneCodeMap_movein.size() > 0 ) {
            returnedMap_ZoneAndUser = UtilityClass.GetMoveInExecutivesFromZoneCode(HouseAndZoneCodeMap_movein);
            System.debug('***returnedMap_ZoneAndUser'+returnedMap_ZoneAndUser);
            if(returnedMap_ZoneAndUser == null || returnedMap_ZoneAndUser.size()< 1 ){
                dateTime todaysdate = System.now();
                UtilityClass.insertStringInLog('No users for selected zone. Logged at '+ todaysdate +'moveout type '+JSONDeserialized.caseType+'map to get users '+HouseAndZoneCodeMap_movein +'\n' +returnedMap_ZoneAndUser);
            }
        }

        /*Houses for moveout Exex */

        list<house__C> houses_moveout = new list<house__C> ();
        houses_moveout = [select id,Onboarding_Zone_Code__c  from house__c where id in : houseIds_MoveOut];
        map<id,house__c> HouseAndZoneCodeMap_moveout = new map<id,house__c>();

        for(house__c each : houses_moveout) {
            if(each.Onboarding_Zone_Code__c != null) {
                System.debug('**HouseAndZoneCodeMap_moveout'+HouseAndZoneCodeMap_moveout);
                HouseAndZoneCodeMap_moveout.put(each.id,each);
            }
        }
        
        if( HouseAndZoneCodeMap_moveout != null && HouseAndZoneCodeMap_moveout.size() > 0  ) {
            returnedMap_ZoneAndUser = UtilityClass.GetMoveOutExecutivesFromZoneCode(HouseAndZoneCodeMap_moveout);
            System.debug('***returnedMap_ZoneAndUser'+returnedMap_ZoneAndUser);
            if(returnedMap_ZoneAndUser == null || returnedMap_ZoneAndUser.size()<1 ){
                dateTime todaysdate = System.now();
                UtilityClass.insertStringInLog('No users for selected zone. Logged at '+ todaysdate +'moveout type '+JSONDeserialized.caseType +'map to get users '+HouseAndZoneCodeMap_moveout +'\n' +returnedMap_ZoneAndUser);
            }
        }
        
        WBMIMOJSONWrapperClasses.SendFEDetailsToWA SendFEDetailsToWAList = new WBMIMOJSONWrapperClasses.SendFEDetailsToWA();
        for(String each : returnedMap_ZoneAndUser.keySet()) {
            WBMIMOJSONWrapperClasses.SendFEDetailsToWA objRec = new WBMIMOJSONWrapperClasses.SendFEDetailsToWA();
            objRec.zone_code = each ;
            list<WBMIMOJSONWrapperClasses.WA_Field_Executives> feList = new list<WBMIMOJSONWrapperClasses.WA_Field_Executives>();
            for(user eachuser : returnedMap_ZoneAndUser.get(each)){
                WBMIMOJSONWrapperClasses.WA_Field_Executives obj = new WBMIMOJSONWrapperClasses.WA_Field_Executives();
                obj.executive_id = eachuser.id;
                obj.executive_email_id = eachuser.Email;
                feList.add(obj);
            }
            objRec.field_executives = new list<WBMIMOJSONWrapperClasses.WA_Field_Executives> ();
            objRec.field_executives = feList;
            SendFEDetailsToWAList = (objRec);
        }
        System.debug('***** SendFEDetailsToWAList '+SendFEDetailsToWAList);
        return SendFEDetailsToWAList;
        
       // return returnedMap_ZoneAndUser;
        
    }

}