global class batchToUpdateAccount implements Database.Batchable<sObject>{
    
    public final string query;
    
    public batchToUpdateAccount(string query){
        
        this.query=query;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
       
       /* String query = 'Select id,Onboarding_Zone_code__c,House_Lat_Long_details__Latitude__s,'+
                        'House_Lat_Long_details__Longitude__s , Acquisition_Zone_code__c, House__c'+
                        'FROM House_Inspection_Checklist__c'+
                        'where (Acquisition_Zone_code__c = NULL OR Onboarding_Zone_code__c = NULL)'+
                    'AND (House_Lat_Long_details__Longitude__s != NULL AND House_Lat_Long_details__Latitude__s != NULL )';
        */
       // String query = 'Select id,Acquisition_Zone_Code__c,Onboarding_Zone_Code__c,House_Lattitude__c,House_Longitude__c,city__c,HRM__c,ZAM__c from house__c';
        System.debug('***query'+query);
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Account> accLst) {
      
         try{
             
               update accLst;
             
         }
         catch(exception e){
             
             
         }
         
    }   
    
    global void finish(Database.BatchableContext BC) {
        System.debug('***One batch Executed -- Ameed');
    }
}