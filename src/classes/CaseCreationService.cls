/**********************************************************************************
Added by Baibhav
      
**********************************************************************************/
public  class CaseCreationService {
	//Added for creation service case
	public static Case CaseCreation(Id proId,Id opId,id objId,id hicId,id accid,id houId,id casId,string lable,id apm){
        try{
                Id serviceCaseRecordtypeId=Schema.SobjectType.case.getRecordTypeInfosByName().get(CaseConstants.CASE_RT_SERVICE_REQUEST).getRecordTypeId();
                System.debug('****case');
        		Case ca=new Case();
        		ca.Problem__c=proId;
        		ca.Operational_Process__c=opId;
        		ca.recordtypeID=serviceCaseRecordtypeId;
        		if(objId.getSobjectType() ==  House_Inspection_Checklist__c.SobjectType){
        			System.Debug('****H');
        			ca.Checklist__c=objId;
        		}
        		if(objId.getSobjectType() ==  Room_Inspection__c.SobjectType){
        			System.Debug('****R');
        			ca.Checklist__c=hicId;
        			ca.MimoRic__c=objId;
        		}
        		if(objId.getSobjectType() ==  Bathroom__c.SobjectType){
        			System.Debug('****B');
        			ca.Checklist__c=hicId;
        			ca.MimoBic__c=objId;
        		}
        		if(accid!=null){
        			ca.Accountid=accid;
        		}
        		if(houId!=null){
        			ca.House1__c=houId;
        		}
        		if(casId!=null){
        			ca.Parentid=casId;
        		}
                if(lable!=null){
                    ca.Case_Creation_for__c=lable;
                }
                if(apm!=null){
                    ca.APM__c=apm;
                }

                ca.Type=CaseConstants.CASE_TYPE_ONBOARDING;
                ca.Origin=CaseConstants.CASE_ORIGN_SYSTEM;
        		return ca;
        	}catch (Exception e){
          System.debug('**Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + 
                       '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + 
                       '\nStack Trace ' + e.getStackTraceString());
          UtilityClass.insertGenericErrorLog(e, 'Service CaseCreation for HIC');
          return null;  
        }  
	}  
}