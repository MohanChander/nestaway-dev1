public with sharing class TestclassonCase {
    public CAse oP {get; set;}
    Public string acctname{get;set;}
     Public string accid{get;set;}
   public string pMarkets{get;set;}
     public string startdate{get;set;}
    Public List<Case> caseList{get;set;}
    public TestclassonCase (ApexPages.StandardController sController) {
         oP = (case)sController.getRecord();
        caseList=[select ClosedDate,CreatedDate from Case ];
    }
    public List<SelectOption> getStageOptions() 
    {
        List<SelectOption> options = new List<SelectOption>();
        //options.add(new SelectOption('Select','--Select--'));
        //options.add(new SelectOption('None','--None--'));
        Schema.DescribeFieldResult fieldResult = Opportunity.StageName.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for(Schema.PicklistEntry p : ple)
        {
            if(p.getValue() == '9:00' || p.getValue() == '9:30' || p.getValue() == '10:00' || p.getValue() == '10:30' || p.getValue() == '' || p.getValue() == 'Negotiation/Review' || p.getValue() == 'Closed Won' || p.getValue() == 'Closed Lost')
            {
                
            }
            else
            {
                options.add(new SelectOption(p.getValue(), p.getValue()));
            }
        }        
        return options;
    }
    
    
}