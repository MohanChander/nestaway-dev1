@isTest
public class RoomTermsTriggerHandlerTest {
    static testMethod void RoomTriggerTest() {
        //Create Dummy contract
        
        NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
        cusSet.name = 'nestaway';
        cusSet.Nestaway_URL__c = 'www.test.com';
        insert cusSet;
        
        Id recordTypeDccId = [Select Id from RecordType where Name=:'Document Collection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id;
        List<House_Inspection_Checklist__c> houseList = new List<House_Inspection_Checklist__c>();
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Final Contract';
        
        insert opp;
        
        PriceBook2 priceBk = new PriceBook2();
        priceBk.Name = 'TestPriceBook';
        // priceBk.isStandard = FALSE;
        insert priceBk;
        
        Product2 prod = new Product2();
        prod.Name = 'testProd';
        prod.isActive = TRUE;
        insert prod;
        
        Contract cont = new Contract();
        cont.Name = 'TestContract';
        cont.AccountId = accObj.id;
        cont.Opportunity__c = opp.id;
        cont.PriceBook2Id = priceBk.Id;
        cont.status = 'Draft';
        cont.Maximum_number_of_tenants_allowed__c = '1';
        cont.Booking_Type__c = 'Shared House';
        insert cont;
        
        cont.Status = 'Final Contract';
        cont.Approval_Status__c = 'Approved by Central Team';
        cont.Furnishing_Plan__c = 'Nestaway furnished';
        update cont;
        system.debug('****Contract***'+cont);
        
        Contract cont2 = new Contract();
        cont2.Name = 'TestContract2';
        cont2.AccountId = accObj.id;
        cont2.Opportunity__c = opp.id;
        cont2.PriceBook2Id = priceBk.Id;
        cont2.status = 'Draft';
        cont2.Maximum_number_of_tenants_allowed__c = '1';
        cont2.Booking_Type__c = 'Full House';
        insert cont2;
        
        cont2.Status = 'Final Contract';
        cont2.Approval_Status__c = 'Approved by Central Team';
        cont2.Furnishing_Plan__c = 'Nestaway furnished';
        update cont2;
        system.debug('****Contract***'+cont2);
        
        
        House_Inspection_Checklist__c houseIC1 = new House_Inspection_Checklist__c();
        houseIC1.RecordTypeId = recordTypeDccId ;
        houseIC1.Opportunity__c = opp.Id;
        houseIC1.Status__c = 'Draft'; 
        houseIC1.Doc_Reviewer_Approved__c = TRUE;       
        houseList.add(houseIC1);
        
        insert houseList;
        
        City__c city = new City__c ();
        city.name = 'Bengaluru';
        city.Rent_Min_Limit__c  = 90;
        
        Insert city;
        
        
        
        City__c city2 = new City__c ();
        city2.name = 'Bengaluru';
        //city2.Rent_Min_Limit__c  = null;
        
        Insert city2;
        Set<id> setofhome= new Set<Id>();
        House__c houseObj2 = new House__c();
        houseObj2.Name = 'TestHouse';
        houseObj2.Initiate_Offboarding__c='No';
        houseObj2.Stage__c = 'House Draft';
        houseObj2.Opportunity__c = opp.Id;
        houseObj2.City_Master__c = city.id;
        houseObj2.listing__c='On';
        insert houseObj2;
        
        
      setofhome.add(houseObj2.ID);
        list<Room_Terms__c> RoomTermList = new list<Room_Terms__c>();
        List<Room_Terms__c> RoomTermListupdate = new list<Room_Terms__c>();
        
        Room_Terms__c roomTermRec2 = new Room_Terms__c();
        roomTermRec2.Name = 'Test Room2';
        roomTermRec2.Actual_Room_Rent__c = 10000;
        roomTermRec2.Monthly_Base_Rent_Per_Bed__c = 5000;
        roomTermRec2.Monthly_Base_Rent_Per_Room__c = 10000;
        // added by baibhav
        roomTermRec2.Number_of_beds__c  =  String.valueOf(2);
        roomTermRec2.House__c = houseObj2.id;
        roomTermRec2.Contract__c = cont.id;  
        insert roomTermRec2;
        Bed__c  bd = new Bed__c();
        bd.House__c = houseObj2.id;
        // added by baibhav
        bd.Room_Terms__c=roomTermRec2.Id;
        insert bd;
        RoomTermList.add(roomTermRec2);
        roomTermRec2.Actual_Room_Rent__c = 15000;
        // added by baibhav
        roomTermRec2.Number_of_beds__c  = String.valueOf(4);
        update roomTermRec2;
        // added by baibhav
        houseObj2.Stage__c = 'House Live';
        update houseObj2 ;
        try{
        roomTermRec2.listing__c = 'Off';
        update roomTermRec2;
         } catch (Exception e){}


        RoomTermListupdate.add(roomTermRec2);
        
        Map<id,Room_Terms__c> MapofRoom =  new Map<id,Room_Terms__c>();
        Map<id,Room_Terms__c> MapofRoomold =  new Map<id,Room_Terms__c>();
        for(Room_Terms__c r:RoomTermList){
            MapofRoom.put(r.id,r);
        }
        for(Room_Terms__c r:RoomTermListupdate){
            MapofRoomold.put(r.id,r);
        }
        Test.startTest();
        RoomTermsTriggerHandler.afterInsertMethod(MapofRoom);
        RoomTermsTriggerHandler.afterInsert(RoomTermList,MapofRoom,MapofRoomold);
        RoomTermsTriggerHandler.sendWebEntityHouseJson(MapofRoom);
        RoomTermsTriggerHandler.checkRoomTermValidationBeforeUpdate(MapofRoom,MapofRoomold);
        RoomTermsTriggerHandler.updateHouseWhenAllRoomHavePaymentasON(setofhome,'Off',MapofRoom,MapofRoomold);
        // Added by baibhav
        RoomTermsTriggerHandler.updateHouseWhenAllRoomHavePaymentasON(setofhome,'On',MapofRoom,MapofRoomold);

        RoomTermsTriggerHandler.updateHouseWhenAllRoomHaveListingAsOF(setofhome,'Off',MapofRoom,MapofRoomold);
        Test.stopTest();
    } 
    
    static testMethod void RoomTriggerTest1() {
        //Create Dummy contract
        Test.startTest();
        NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
        cusSet.name = 'nestaway';
        cusSet.Nestaway_URL__c = 'www.test.com';
        insert cusSet;
        
        Id recordTypeDccId = [Select Id from RecordType where Name=:'Document Collection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id;
        List<House_Inspection_Checklist__c> houseList = new List<House_Inspection_Checklist__c>();
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Final Contract';
        
        insert opp;
        
        PriceBook2 priceBk = new PriceBook2();
        priceBk.Name = 'TestPriceBook';
        // priceBk.isStandard = FALSE;
        insert priceBk;
        
        Product2 prod = new Product2();
        prod.Name = 'testProd';
        prod.isActive = TRUE;
        insert prod;
        
        Contract cont = new Contract();
        cont.Name = 'TestContract';
        cont.AccountId = accObj.id;
        cont.Opportunity__c = opp.id;
        cont.PriceBook2Id = priceBk.Id;
        cont.status = 'Draft';
        cont.Maximum_number_of_tenants_allowed__c = '1';
        cont.Booking_Type__c = 'Shared House';
        insert cont;
        
        cont.Status = 'Final Contract';
        cont.Approval_Status__c = 'Approved by Central Team';
        cont.Furnishing_Plan__c = 'Nestaway furnished';
        update cont;
        system.debug('****Contract***'+cont);
        
        Contract cont2 = new Contract();
        cont2.Name = 'TestContract2';
        cont2.AccountId = accObj.id;
        cont2.Opportunity__c = opp.id;
        cont2.PriceBook2Id = priceBk.Id;
        cont2.status = 'Draft';
        cont2.Maximum_number_of_tenants_allowed__c = '1';
        cont2.Booking_Type__c = 'Full House';
        insert cont2;
        
        cont2.Status = 'Final Contract';
        cont2.Approval_Status__c = 'Approved by Central Team';
        cont2.Furnishing_Plan__c = 'Nestaway furnished';
        update cont2;
        system.debug('****Contract***'+cont2);
        
        
        House_Inspection_Checklist__c houseIC1 = new House_Inspection_Checklist__c();
        houseIC1.RecordTypeId = recordTypeDccId ;
        houseIC1.Opportunity__c = opp.Id;
        houseIC1.Status__c = 'Draft'; 
        houseIC1.Doc_Reviewer_Approved__c = TRUE;       
        houseList.add(houseIC1);
        
        insert houseList;
        
        City__c city = new City__c ();
        city.name = 'Bengaluru';
        city.Rent_Min_Limit__c  = 90;
        
        Insert city;
        
        
        
        City__c city2 = new City__c ();
        city2.name = 'Bengaluru';
        //city2.Rent_Min_Limit__c  = null;
        
        Insert city2;
        Set<id> setofhome= new Set<Id>();
        House__c houseObj2 = new House__c();
        houseObj2.Name = 'TestHouse';
        houseObj2.Stage__c = 'House Draft';
        houseObj2.Opportunity__c = opp.Id;
        houseObj2.City_Master__c = city.id;
      houseObj2.Initiate_Offboarding__c='No';
        insert houseObj2;
        
    
        setofhome.add(houseObj2.Id);
        list<Room_Terms__c> RoomTermList = new list<Room_Terms__c>();
        List<Room_Terms__c> RoomTermListupdate = new list<Room_Terms__c>();
        
        Room_Terms__c roomTermRec2 = new Room_Terms__c();
        roomTermRec2.Name = 'Test Room2';
        roomTermRec2.Actual_Room_Rent__c = 10000;
        roomTermRec2.Monthly_Base_Rent_Per_Bed__c = 5000;
        roomTermRec2.Monthly_Base_Rent_Per_Room__c = 10000;
        roomTermRec2.Number_of_beds__c  = String.valueOf(2);
        roomTermRec2.House__c = houseObj2.id;
        roomTermRec2.Contract__c = cont.id;  
        insert roomTermRec2;
         Bed__c  bd = new Bed__c();
         bd.House__c = houseObj2.id;
        bd.Room_Terms__c=roomTermRec2.ID;
        insert bd;
        RoomTermList.add(roomTermRec2);
        roomTermRec2.Actual_Room_Rent__c = 15000;
        roomTermRec2.Number_of_beds__c  = String.valueOf(4);
        update roomTermRec2;
        RoomTermListupdate.add(roomTermRec2);
        
        Map<id,Room_Terms__c> MapofRoom =  new Map<id,Room_Terms__c>();
        Map<id,Room_Terms__c> MapofRoomold =  new Map<id,Room_Terms__c>();
        for(Room_Terms__c r:RoomTermList){
            MapofRoom.put(r.id,r);
        }
        for(Room_Terms__c r:RoomTermListupdate){
            MapofRoomold.put(r.id,r);
        }
        RoomTermsTriggerHandler.afterInsert(RoomTermList,MapofRoom,MapofRoomold);
        RoomTermsTriggerHandler.sendWebEntityHouseJson(MapofRoom);
        RoomTermsTriggerHandler.checkRoomTermValidationBeforeUpdate(MapofRoom,MapofRoomold);
        //RoomTermsTriggerHandler.updateHouseWhenAllRoomHavePaymentasON(setofhome,'On',MapofRoom,MapofRoomold);
        //RoomTermsTriggerHandler.updateHouseWhenAllRoomHaveListingAsOF(setofhome,'On',MapofRoom,MapofRoomold);
        Test.stopTest();
    } 
    
    static testMethod void RoomTriggerTest3() {
        //Create Dummy contract
        Test.startTest();
        NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
        cusSet.name = 'nestaway';
        cusSet.Nestaway_URL__c = 'www.test.com';
        insert cusSet;
        
        Id recordTypeDccId = [Select Id from RecordType where Name=:'Document Collection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id;
        List<House_Inspection_Checklist__c> houseList = new List<House_Inspection_Checklist__c>();
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Final Contract';
        
        insert opp;
        
        PriceBook2 priceBk = new PriceBook2();
        priceBk.Name = 'TestPriceBook';
        // priceBk.isStandard = FALSE;
        insert priceBk;
        
        Product2 prod = new Product2();
        prod.Name = 'testProd';
        prod.isActive = TRUE;
        insert prod;
        
        Contract cont = new Contract();
        cont.Name = 'TestContract';
        cont.AccountId = accObj.id;
        cont.Opportunity__c = opp.id;
        cont.PriceBook2Id = priceBk.Id;
        cont.status = 'Draft';
        cont.Maximum_number_of_tenants_allowed__c = '1';
        cont.Booking_Type__c = 'Shared House';
        insert cont;
        
        cont.Status = 'Final Contract';
        cont.Approval_Status__c = 'Approved by Central Team';
        cont.Furnishing_Plan__c = 'Nestaway furnished';
        update cont;
        system.debug('****Contract***'+cont);
        
        Contract cont2 = new Contract();
        cont2.Name = 'TestContract2';
        cont2.AccountId = accObj.id;
        cont2.Opportunity__c = opp.id;
        cont2.PriceBook2Id = priceBk.Id;
        cont2.status = 'Draft';
        cont2.Maximum_number_of_tenants_allowed__c = '1';
        cont2.Booking_Type__c = 'Full House';
        insert cont2;
        
        cont2.Status = 'Final Contract';
        cont2.Approval_Status__c = 'Approved by Central Team';
        cont2.Furnishing_Plan__c = 'Nestaway furnished';
        update cont2;
        system.debug('****Contract***'+cont2);
        
        
        House_Inspection_Checklist__c houseIC1 = new House_Inspection_Checklist__c();
        houseIC1.RecordTypeId = recordTypeDccId ;
        houseIC1.Opportunity__c = opp.Id;
        houseIC1.Status__c = 'Draft'; 
        houseIC1.Doc_Reviewer_Approved__c = TRUE;       
        houseList.add(houseIC1);
        
        insert houseList;
        
        City__c city = new City__c ();
        city.name = 'Bengaluru';
        city.Rent_Min_Limit__c  = 90;
        
        Insert city;
        
        
        
        City__c city2 = new City__c ();
        city2.name = 'Bengaluru';
        //city2.Rent_Min_Limit__c  = null;
        
        Insert city2;
        Set<id> setofhome= new Set<Id>();
        House__c houseObj2 = new House__c();
        houseObj2.Name = 'TestHouse';
        houseObj2.Stage__c = 'House Draft';
        houseObj2.Opportunity__c = opp.Id;
     houseObj2.Initiate_Offboarding__c='No';
        houseObj2.City_Master__c = city.id;
        insert houseObj2;
        
        
        setofhome.add(houseObj2.Id);
        list<Room_Terms__c> RoomTermList = new list<Room_Terms__c>();
        List<Room_Terms__c> RoomTermListupdate = new list<Room_Terms__c>();
        
        Room_Terms__c roomTermRec2 = new Room_Terms__c();
        roomTermRec2.Name = 'Test Room2';
        roomTermRec2.Actual_Room_Rent__c = 10000;
        roomTermRec2.Monthly_Base_Rent_Per_Bed__c = 5000;
        roomTermRec2.Monthly_Base_Rent_Per_Room__c = 10000;
        roomTermRec2.Number_of_beds__c  =String.valueOf(2);
        roomTermRec2.House__c = houseObj2.id;
        roomTermRec2.Contract__c = cont.id;  
        insert roomTermRec2;
         Bed__c  bd = new Bed__c();
         bd.House__c = houseObj2.id;
        bd.Room_Terms__c=roomTermRec2.ID;
        insert bd;
        RoomTermList.add(roomTermRec2);
        roomTermRec2.Actual_Room_Rent__c = 15000;
       
        roomTermRec2.Number_of_beds__c  = String.valueOf(2);
        update roomTermRec2;
        RoomTermListupdate.add(roomTermRec2);
        
        Map<id,Room_Terms__c> MapofRoom =  new Map<id,Room_Terms__c>();
        Map<id,Room_Terms__c> MapofRoomold =  new Map<id,Room_Terms__c>();
        for(Room_Terms__c r:RoomTermList){
            MapofRoom.put(r.id,r);
        }
        for(Room_Terms__c r:RoomTermListupdate){
            MapofRoomold.put(r.id,r);
        }
        RoomTermsTriggerHandler.afterInsert(RoomTermList,MapofRoom,MapofRoomold);
        RoomTermsTriggerHandler.sendWebEntityHouseJson(MapofRoom);
        RoomTermsTriggerHandler.checkRoomTermValidationBeforeUpdate(MapofRoom,MapofRoomold);
        //RoomTermsTriggerHandler.updateHouseWhenAllRoomHavePaymentasON(setofhome,'On',MapofRoom,MapofRoomold);
        //RoomTermsTriggerHandler.updateHouseWhenAllRoomHaveListingAsOF(setofhome,'On',MapofRoom,MapofRoomold);
        Test.stopTest();
    } 
    
     static testMethod void RoomTriggerTest4() {
        //Create Dummy contract
        Test.startTest();
        NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
        cusSet.name = 'nestaway';
        cusSet.Nestaway_URL__c = 'www.test.com';
        insert cusSet;
        
        Id recordTypeDccId = [Select Id from RecordType where Name=:'Document Collection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id;
        List<House_Inspection_Checklist__c> houseList = new List<House_Inspection_Checklist__c>();
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Final Contract';
        
        insert opp;
        
        PriceBook2 priceBk = new PriceBook2();
        priceBk.Name = 'TestPriceBook';
        // priceBk.isStandard = FALSE;
        insert priceBk;
        
        Product2 prod = new Product2();
        prod.Name = 'testProd';
        prod.isActive = TRUE;
        insert prod;
        
        Contract cont = new Contract();
        cont.Name = 'TestContract';
        cont.AccountId = accObj.id;
        cont.Opportunity__c = opp.id;
        cont.PriceBook2Id = priceBk.Id;
        cont.status = 'Draft';
        cont.Maximum_number_of_tenants_allowed__c = '1';
        cont.Booking_Type__c = 'Shared House';
        insert cont;
        
        cont.Status = 'Final Contract';
        cont.Approval_Status__c = 'Approved by Central Team';
        cont.Furnishing_Plan__c = 'Nestaway furnished';
        update cont;
        system.debug('****Contract***'+cont);
        
        Contract cont2 = new Contract();
        cont2.Name = 'TestContract2';
        cont2.AccountId = accObj.id;
        cont2.Opportunity__c = opp.id;
        cont2.PriceBook2Id = priceBk.Id;
        cont2.status = 'Draft';
        cont2.Maximum_number_of_tenants_allowed__c = '1';
        cont2.Booking_Type__c = 'Full House';
        insert cont2;
        
        cont2.Status = 'Final Contract';
        cont2.Approval_Status__c = 'Approved by Central Team';
        cont2.Furnishing_Plan__c = 'Nestaway furnished';
        update cont2;
        system.debug('****Contract***'+cont2);
        
        
        House_Inspection_Checklist__c houseIC1 = new House_Inspection_Checklist__c();
        houseIC1.RecordTypeId = recordTypeDccId ;
        houseIC1.Opportunity__c = opp.Id;
        houseIC1.Status__c = 'Draft'; 
        houseIC1.Doc_Reviewer_Approved__c = TRUE;       
        houseList.add(houseIC1);
        
        insert houseList;
        
        City__c city = new City__c ();
        city.name = 'Bengaluru';
        city.Rent_Min_Limit__c  = 90;
        
        Insert city;
        
        
        
        City__c city2 = new City__c ();
        city2.name = 'Bengaluru';
        //city2.Rent_Min_Limit__c  = null;
        
        Insert city2;
        Set<id> setofhome= new Set<Id>();
        House__c houseObj2 = new House__c();
        houseObj2.Name = 'TestHouse';
        houseObj2.Stage__c = 'House Draft';
        houseObj2.Opportunity__c = opp.Id;
        houseObj2.City_Master__c = city.id;
      houseObj2.Initiate_Offboarding__c='No';
        insert houseObj2;
        
        
        setofhome.add(houseObj2.Id);
        list<Room_Terms__c> RoomTermList = new list<Room_Terms__c>();
        List<Room_Terms__c> RoomTermListupdate = new list<Room_Terms__c>();
        
        Room_Terms__c roomTermRec2 = new Room_Terms__c();
        roomTermRec2.Name = 'Test Room2';
        roomTermRec2.Actual_Room_Rent__c =0;
        roomTermRec2.Monthly_Base_Rent_Per_Bed__c = 5000;
        roomTermRec2.Monthly_Base_Rent_Per_Room__c = 10000;
        roomTermRec2.Number_of_beds__c  =null;
        roomTermRec2.House__c = houseObj2.id;
        roomTermRec2.Contract__c = cont.id;  
        insert roomTermRec2;
         Bed__c  bd = new Bed__c();
         bd.House__c = houseObj2.id;
        bd.Room_Terms__c=roomTermRec2.ID;
        insert bd;
        RoomTermList.add(roomTermRec2);
        roomTermRec2.Actual_Room_Rent__c = 15000;
       
        roomTermRec2.Number_of_beds__c  = null;
        update roomTermRec2;
        RoomTermListupdate.add(roomTermRec2);
        
        Map<id,Room_Terms__c> MapofRoom =  new Map<id,Room_Terms__c>();
        Map<id,Room_Terms__c> MapofRoomold =  new Map<id,Room_Terms__c>();
        for(Room_Terms__c r:RoomTermList){
            MapofRoom.put(r.id,r);
        }
        for(Room_Terms__c r:RoomTermListupdate){
            MapofRoomold.put(r.id,r);
        }
        RoomTermsTriggerHandler.afterInsert(RoomTermList,MapofRoom,MapofRoomold);
        RoomTermsTriggerHandler.sendWebEntityHouseJson(MapofRoom);
        RoomTermsTriggerHandler.checkRoomTermValidationBeforeUpdate(MapofRoom,MapofRoomold);
        //RoomTermsTriggerHandler.updateHouseWhenAllRoomHavePaymentasON(setofhome,'On',MapofRoom,MapofRoomold);
        //RoomTermsTriggerHandler.updateHouseWhenAllRoomHaveListingAsOF(setofhome,'On',MapofRoom,MapofRoomold);
        Test.stopTest();
    } 
   // Added by baibhav 
     public static testMethod void test1() {
           Test.startTest();
        NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
        cusSet.name = 'nestaway';
        cusSet.Nestaway_URL__c = 'www.test.com';
        insert cusSet;
        
        Id recordTypeDccId = [Select Id from RecordType where Name=:'Document Collection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id;
        List<House_Inspection_Checklist__c> houseList = new List<House_Inspection_Checklist__c>();
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Final Contract';
        
        insert opp;
        

        Contract cont = new Contract();
        cont.Name = 'TestContract';
        cont.AccountId = accObj.id;
        cont.Opportunity__c = opp.id;
        //cont.PriceBook2Id = priceBk.Id;
        cont.status = 'Draft';
        cont.Maximum_number_of_tenants_allowed__c = '1';
        cont.Booking_Type__c = 'Shared House';
        insert cont;

         City__c city2 = new City__c ();
        city2.name = 'Bengaluru';
        
        Insert city2;

        Set<id> setofhome= new Set<Id>();
        House__c houseObj2 = new House__c();
        houseObj2.Name = 'TestHouse';
        houseObj2.Stage__c = 'House Draft';
        houseObj2.Opportunity__c = opp.Id;
        houseObj2.City_Master__c = city2.id;
        houseObj2.Initiate_Offboarding__c='No';
        insert houseObj2;

        Room_Terms__c roomTermRec2 = new Room_Terms__c();
        roomTermRec2.Name = 'Test Room2';
        roomTermRec2.Actual_Room_Rent__c =0;
        roomTermRec2.Monthly_Base_Rent_Per_Bed__c = 5000;
        roomTermRec2.Monthly_Base_Rent_Per_Room__c = 10000;
        roomTermRec2.Number_of_beds__c  ='2';
        roomTermRec2.House__c = houseObj2.id;
        roomTermRec2.Contract__c = cont.id;  
        insert roomTermRec2;
     }
        public static testMethod void test2() {
           Test.startTest();
        NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
        cusSet.name = 'nestaway';
        cusSet.Nestaway_URL__c = 'www.test.com';
        insert cusSet;
        
        Id recordTypeDccId = [Select Id from RecordType where Name=:'Document Collection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id;
        List<House_Inspection_Checklist__c> houseList = new List<House_Inspection_Checklist__c>();
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Final Contract';
        
        insert opp;
        

        Contract cont = new Contract();
        cont.Name = 'TestContract';
        cont.AccountId = accObj.id;
        cont.Opportunity__c = opp.id;
        //cont.PriceBook2Id = priceBk.Id;
        cont.status = 'Draft';
        cont.Maximum_number_of_tenants_allowed__c = '1';
        cont.Booking_Type__c = 'Shared House';
        insert cont;

         City__c city2 = new City__c ();
        city2.name = 'Bengaluru';
        
        Insert city2;

        Set<id> setofhome= new Set<Id>();
        House__c houseObj2 = new House__c();
        houseObj2.Name = 'TestHouse';
        houseObj2.Stage__c = 'House Draft';
        houseObj2.Opportunity__c = opp.Id;
        houseObj2.City_Master__c = city2.id;
        houseObj2.Initiate_Offboarding__c='No';
        insert houseObj2;

        Room_Terms__c roomTermRec2 = new Room_Terms__c();
        roomTermRec2.Name = 'Test Room2';
        roomTermRec2.Actual_Room_Rent__c =0;
        roomTermRec2.Monthly_Base_Rent_Per_Bed__c = 5000;
        roomTermRec2.Monthly_Base_Rent_Per_Room__c = 10000;
        roomTermRec2.Number_of_beds__c  ='2';
        roomTermRec2.House__c = houseObj2.id;
        roomTermRec2.Contract__c = cont.id;  
        insert roomTermRec2;

         Bed__c  bd = new Bed__c();
         bd.House__c = houseObj2.id;
        bd.Room_Terms__c=roomTermRec2.ID;
        insert bd;

         Bed__c  bd1 = new Bed__c();
         bd1.House__c = houseObj2.id;
        bd1.Room_Terms__c=roomTermRec2.ID;
        bd1.Status__c = 'Sold Out';
        insert bd1;
        try{
                delete roomTermRec2;
                } catch(Exception e){}
     }
      public static testMethod void test3() {
           Test.startTest();
        NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
        cusSet.name = 'nestaway';
        cusSet.Nestaway_URL__c = 'www.test.com';
        insert cusSet;
        
        Id recordTypeDccId = [Select Id from RecordType where Name=:'Document Collection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id;
        List<House_Inspection_Checklist__c> houseList = new List<House_Inspection_Checklist__c>();
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Final Contract';
        
        insert opp;
        

        Contract cont = new Contract();
        cont.Name = 'TestContract';
        cont.AccountId = accObj.id;
        cont.Opportunity__c = opp.id;
        //cont.PriceBook2Id = priceBk.Id;
        cont.status = 'Draft';
        cont.Maximum_number_of_tenants_allowed__c = '1';
        cont.Booking_Type__c = 'Shared House';
        insert cont;

         City__c city2 = new City__c ();
        city2.name = 'Bengaluru';
        
        Insert city2;

        Set<id> setofhome= new Set<Id>();
        House__c houseObj2 = new House__c();
        houseObj2.Name = 'TestHouse';
        houseObj2.Stage__c = 'House Draft';
        houseObj2.Opportunity__c = opp.Id;
        houseObj2.City_Master__c = city2.id;
        houseObj2.Initiate_Offboarding__c='No';
        insert houseObj2;

        Room_Terms__c roomTermRec2 = new Room_Terms__c();
        roomTermRec2.Name = 'Test Room2';
        roomTermRec2.Actual_Room_Rent__c =13000;
        roomTermRec2.Monthly_Base_Rent_Per_Bed__c = 5000;
        roomTermRec2.Monthly_Base_Rent_Per_Room__c = 10000;
        roomTermRec2.Number_of_beds__c  ='2';
        roomTermRec2.House__c = houseObj2.id;
        roomTermRec2.Contract__c = cont.id;  
        insert roomTermRec2;

         Bed__c  bd = new Bed__c();
         bd.House__c = houseObj2.id;
        bd.Room_Terms__c=roomTermRec2.ID;
        insert bd;

         Bed__c  bd1 = new Bed__c();
         bd1.House__c = houseObj2.id;
        bd1.Room_Terms__c=roomTermRec2.ID;
        insert bd1;

       roomTermRec2.Number_of_beds__c  ='3';
       roomTermRec2.Monthly_Base_Rent_Per_Bed__c = 3500;
       roomTermRec2.Monthly_Base_Rent_Per_Room__c = 10500;
       update roomTermRec2;

     }
     public static testMethod void test4() {
           Test.startTest();
        NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
        cusSet.name = 'nestaway';
        cusSet.Nestaway_URL__c = 'www.test.com';
        insert cusSet;
        
        Id recordTypeDccId = [Select Id from RecordType where Name=:'Document Collection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id;
        List<House_Inspection_Checklist__c> houseList = new List<House_Inspection_Checklist__c>();
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Final Contract';
        
        insert opp;
        

        Contract cont = new Contract();
        cont.Name = 'TestContract';
        cont.AccountId = accObj.id;
        cont.Opportunity__c = opp.id;
        //cont.PriceBook2Id = priceBk.Id;
        cont.status = 'Draft';
        cont.Maximum_number_of_tenants_allowed__c = '1';
        cont.Booking_Type__c = 'Shared House';
        insert cont;

         City__c city2 = new City__c ();
        city2.name = 'Bengaluru';
        
        Insert city2;

        Set<id> setofhome= new Set<Id>();
        House__c houseObj2 = new House__c();
        houseObj2.Name = 'TestHouse';
        houseObj2.Stage__c = 'House Draft';
        houseObj2.Opportunity__c = opp.Id;
        houseObj2.City_Master__c = city2.id;
        houseObj2.Initiate_Offboarding__c='No';
        insert houseObj2;

        Room_Terms__c roomTermRec2 = new Room_Terms__c();
        roomTermRec2.Name = 'Test Room2';
        roomTermRec2.Actual_Room_Rent__c =13000;
        roomTermRec2.Monthly_Base_Rent_Per_Bed__c = 5000;
        roomTermRec2.Monthly_Base_Rent_Per_Room__c = 10000;
        roomTermRec2.Number_of_beds__c  ='2';
        roomTermRec2.House__c = houseObj2.id;
        roomTermRec2.Contract__c = cont.id;
        roomTermRec2.Listing__c = null;  
        insert roomTermRec2;

        roomTermRec2.Listing__c = 'On';
        Update roomTermRec2;
    }
     public static testMethod void test5() {
           Test.startTest();
        NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
        cusSet.name = 'nestaway';
        cusSet.Nestaway_URL__c = 'www.test.com';
        insert cusSet;
        
        Id recordTypeDccId = [Select Id from RecordType where Name=:'Document Collection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id;
        List<House_Inspection_Checklist__c> houseList = new List<House_Inspection_Checklist__c>();
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Final Contract';
        
        insert opp;
        

        Contract cont = new Contract();
        cont.Name = 'TestContract';
        cont.AccountId = accObj.id;
        cont.Opportunity__c = opp.id;
        //cont.PriceBook2Id = priceBk.Id;
        cont.status = 'Draft';
        cont.Maximum_number_of_tenants_allowed__c = '1';
        cont.Booking_Type__c = 'Shared House';
        insert cont;

         City__c city2 = new City__c ();
        city2.name = 'Bengaluru';
        
        Insert city2;

        Set<id> setofhome= new Set<Id>();
        House__c houseObj2 = new House__c();
        houseObj2.Name = 'TestHouse';
        houseObj2.Stage__c = 'House Draft';
        houseObj2.Opportunity__c = opp.Id;
        houseObj2.City_Master__c = city2.id;
        houseObj2.Initiate_Offboarding__c='No';
        insert houseObj2;

        Room_Terms__c roomTermRec2 = new Room_Terms__c();
        roomTermRec2.Name = 'Test Room2';
        roomTermRec2.Actual_Room_Rent__c =13000;
        roomTermRec2.Monthly_Base_Rent_Per_Bed__c = 5000;
        roomTermRec2.Monthly_Base_Rent_Per_Room__c = 10000;
        roomTermRec2.Number_of_beds__c  ='2';
        roomTermRec2.House__c = houseObj2.id;
        roomTermRec2.Contract__c = cont.id;
      
        insert roomTermRec2;

         roomTermRec2.Actual_Room_Rent__c =null;
          roomTermRec2.Monthly_Base_Rent_Per_Room__c =null;
        try{  
                Update roomTermRec2;
                } catch(Exception e){}
    }
     public static testMethod void test6() {
           Test.startTest();
        NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
        cusSet.name = 'nestaway';
        cusSet.Nestaway_URL__c = 'www.test.com';
        insert cusSet;
        
        Id recordTypeDccId = [Select Id from RecordType where Name=:'Document Collection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id;
        List<House_Inspection_Checklist__c> houseList = new List<House_Inspection_Checklist__c>();
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Final Contract';
        
        insert opp;
        

        Contract cont = new Contract();
        cont.Name = 'TestContract';
        cont.AccountId = accObj.id;
        cont.Opportunity__c = opp.id;
        //cont.PriceBook2Id = priceBk.Id;
        cont.status = 'Draft';
        cont.Maximum_number_of_tenants_allowed__c = '1';
        cont.Booking_Type__c = 'Shared House';
        insert cont;

         City__c city2 = new City__c ();
        city2.name = 'Bengaluru';
        
        Insert city2;

        Set<id> setofhome= new Set<Id>();
        House__c houseObj2 = new House__c();
        houseObj2.Name = 'TestHouse';
        houseObj2.Stage__c = 'House Draft';
        houseObj2.Opportunity__c = opp.Id;
        houseObj2.City_Master__c = city2.id;
        houseObj2.Initiate_Offboarding__c='No';
        insert houseObj2;

        Room_Terms__c roomTermRec2 = new Room_Terms__c();
        roomTermRec2.Name = 'Test Room2';
        roomTermRec2.Actual_Room_Rent__c =13000;
        roomTermRec2.Monthly_Base_Rent_Per_Bed__c = 5000;
        roomTermRec2.Monthly_Base_Rent_Per_Room__c = 10000;
        roomTermRec2.Number_of_beds__c  ='2';
        roomTermRec2.House__c = houseObj2.id;
        roomTermRec2.Contract__c = cont.id;
        roomTermRec2.Payment__c='On';
      
        insert roomTermRec2;

        roomTermRec2.Payment__c='Off';
        Update roomTermRec2;
    }

    
}