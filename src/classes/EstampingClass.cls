global without sharing class EstampingClass {
    public EstampingClass(ApexPages.StandardController controller) {
    }
    public  pageReference Estamp(){
        try{
            String sourceId= ApexPages.currentPage().getParameters().get('sourceId');
            Contract con = [Select id,E_Stamp__c,MoveIn_Case__r.SD_Status__c,MoveIn_Case__r.Tenant_Profile_Verified__c,MoveIn_Case__r.Owner_approval_status__c 
                            from Contract where id =: sourceID];
            system.debug('con'+con.MoveIn_Case__r.SD_Status__c);
           if( con.MoveIn_Case__r.Owner_approval_status__c != 'Confirmed' ){
                con.adderror('Owner Approval is not confirmed.');
                return null;
            }
            if( con.MoveIn_Case__r.Tenant_Profile_Verified__c != true ){
                con.adderror('Profile is not verified.');
                return null;
            }
          else  if(con.MoveIn_Case__r.SD_Status__c !=  CaseConstants.CASE_STATUS_IN_PAID){
                con.adderror('Sd is not paid.');
                return null;
                
            }
            else {
                SendAPIForContract.sendContractDetailsToWebAppFuture(Con.id);
            }
            
            pageReference ref = new PageReference('/'+Con.id); 
            ref.setRedirect(true); 
            return ref; 
        }
        catch(Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e);
            return null;
        }
    }
}