/**    
  *   Class Name     :  RestUtilities
  *
  *   Description    :  RestUtilities class for sending request to endpoint.   
  *
  *    Created By    :  Chandraprakash Jangid   
  *
  *    Created Date  :  25/07/2017
  *
  *    Version       :  V1.0 Created
  
**/

public class RestUtilities {


  // Method to send http request to the endpoint. Pass request type,endpoint,body and time.

   public static HttpResponse httpRequest(String requestType,String requestUrl, String requestBody,Integer timeout){
        Http h = new Http();   
        HttpRequest req = new HttpRequest(); 
        req.setMethod(requestType);       
        req.setEndpoint(requestUrl);
        req.setTimeout(120000);
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept', 'application/json');
        req.setBody(requestBody);
        if(timeout!=null){
         req.setTimeout(timeout);
         }
        HttpResponse res = h.send(req); 
        return res; 
    } 

}