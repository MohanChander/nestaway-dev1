public class WorkOrderScheduleController {
    Public WorkOrder wo{get;set;}  
    public String stDate;
    public String endDate;
    private date stDate1;
    private date enddate1;
    public string  CityFromHouse;
    public String selectedUsers{get;set;}
    public List<SelectOption> usersoption{get;set;}
    public map<Id,User> users{get;set;}
    public map<id,house__c> HouseAndzoneCodeMap{get;set;}
    public map<string,list<user>> HouseAndusers{get;set;}
    public String selectedAPM {get; set;}
    public List<SelectOption> APMList {get; set;}
    public String selectedScheduleDuration {get; set;}
    public String SelectedSlot {get;set;}
    public List<SelectOption> slotoption {get; set;}
    public String selectedDate {get;set;}
    public List<SelectOption> dateOptions {get; set;}
    public List<WBMIMOJSONWrapperClasses.holidays> holidays {get; set;}
    public House__C  houseRecord;
    public String ZoneCode;
    public List<SelectOption> Fe_user{get;set;}
    public boolean makeresvisiblefes{get;set;}
    public boolean makeZoneFeVisible{get;set;}
    public boolean makeCityFeVisible{get;set;}
    //Constructor
    public WorkOrderScheduleController (ApexPages.StandardController sController) {
       makeZoneFeVisible=true;
        makeCityFeVisible=false;
        usersoption = new List<SelectOption>();
        Fe_user = new List<SelectOption>();
        wo=[select Id,House__c  from WorkOrder  WHERE id =: ApexPages.currentPage().getParameters().get('id')];
        if(wo.House__c != null){
        houseRecord =[select id,PM_Zone_Code__c,City_Master__c,
                      City__c from house__c where id  =: wo.House__C];
        }
        
        if(houseRecord != null){
        ZoneCode = houseRecord.PM_Zone_Code__c;
        CityFromHouse=houseRecord.City__c;
        }
        string respBody=getDatesFromNAApp(wo.Id);
          WBMIMOJSONWrapperClasses.WorkOrderDateJsonWrapper JSONDeserialized;
        try{

              JSONDeserialized = (WBMIMOJSONWrapperClasses.WorkOrderDateJsonWrapper) JSON.deserialize(respBody, WBMIMOJSONWrapperClasses.WorkOrderDateJsonWrapper.class);
        }
        catch(exception e){
            
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error in getting dates from webapp.'));
        }
         if(JSONDeserialized!=null && JSONDeserialized.data != null && JSONDeserialized.data.start_date!=null && JSONDeserialized.data.end_date!=null) {
            
            System.debug('****APi response JSONDeserialized'+JSONDeserialized);
            stDate = JSONDeserialized.data.start_date;
            endDate = JSONDeserialized.data.end_date;
            holidays = JSONDeserialized.data.holidays;
            showDateOptions();
        }
        else{
            
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error in getting dates from webapp.'));
        }
        dateOptions = new List<SelectOption>();
        dateOptions.add(new SelectOption('abc','abc'));
        usersoption=allUsersoption();
        system.debug('CityFromHouse'+CityFromHouse);
    }
    
    //populate the dateOptions to display on the page
    public void showDateOptions(){      
        dateOptions  = new List<SelectOption>();
        List<Date> datesBetweenStartEnd = new List<Date>();
        List<Date> datetoremoved = new List<Date>();
        stDate1=date.valueOf(stDate);
        endDate1=date.valueOf(endDate);
        for(WBMIMOJSONWrapperClasses.holidays holiday: holidays){
            
            datetoremoved.add(date.valueOf(holiday.date_value));
        }
        while(stDate1 <=endDate1) {
            datesBetweenStartEnd.add(stDate1);
            stDate1=stDate1.adddays(1);
        }
        
        Set<date> newDateList = new set<date> ();
        for(date each :datesBetweenStartEnd) {
            
            if(datetoremoved.size()>0){
                
                for(date eachInternal :datetoremoved) {
                    if(!(each == eachInternal)) {
                        newDateList.add(each);
                    }
                }
            }  
            else{
                
                newDateList.add(each);
            }
            
        }
        system.debug('****newDateList'+newDateList);
        dateOptions.add(new SelectOption('-None-','-None-'));
        for(Date each1:newDateList){
            dateOptions.add(new SelectOption(String.valueof(each1),String.valueof(each1)));
        }
        
        dateOptions.add(new SelectOption('abc','abc'));
    }
    
    public List<SelectOption> getscheduleDurationList() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('-None-','-None-'));
        options.add(new SelectOption('.5 hrs','.5 hrs'));
        options.add(new SelectOption('1  hrs','1  hrs'));
        options.add(new SelectOption('1.5 hrs','1.5 hrs'));
        options.add(new SelectOption('2  hrs','2  hrs'));
        options.add(new SelectOption('2.5 hrs','2.5 hrs'));
        options.add(new SelectOption('3  hrs','3  hrs'));
        return options;
    }
    
    
    public List<SelectOption> allUsersoption(){
        List<SelectOption> usersoption = new List<SelectOption>();
        List<Zone_and_OM_Mapping__c> zoneUserList=[Select Id, User__c, User__r.Name,User__r.email from Zone_and_OM_Mapping__c where Property_Manager__r.Zone__r.Zone_Code__c = :ZoneCode];
        System.debug('zoneUserList'+zoneUserList);
        usersoption.add(new SelectOption('','-None-'));
        
        if(zoneUserList != null && zoneUserList.size() > 0) {
            for(Zone_and_OM_Mapping__c each : zoneUserList) {
                usersoption.add(new SelectOption(each.user__C,each.user__r.email));
            } 
        } 
        system.debug('list of user'+usersoption);
        return usersoption;
    }
    
      public pagereference showCityUsers(){
       
       makeZoneFeVisible=false;
       makeCityFeVisible=true;     
       Fe_user=new List<SelectOption>();
       Fe_user.add(new SelectOption('','-None-'));   
       Fe_user=allFe_user();
        
     
       return null;
   }
    
    public List<SelectOption> allFe_user()
    {
        Set<Id> setofzones= new  Set<Id>();
        Set<Id> setofUsers= new  Set<Id>();
        List<string> cityNameLst = new List<string>();
        /*if(CityFromHouse!=null){
            
            
            if(CityFromHouse=='Bengaluru' || CityFromHouse=='Bangalore'){
                cityNameLst.add('Bengaluru');
                cityNameLst.add('Bangalore');
            }
            if(CityFromHouse=='New Delhi' || CityFromHouse=='Delhi'){       
                cityNameLst.add('New Delhi');
                cityNameLst.add('Delhi');
            }
            if(CityFromHouse=='Greater Noida' || CityFromHouse=='Noida'){       
                cityNameLst.add('Greater Noida');
                cityNameLst.add('Noida');
            }
            if(CityFromHouse=='Gurgaon' || CityFromHouse=='Gurugram'){      
                cityNameLst.add('Gurgaon');
                cityNameLst.add('Gurugram');
            }
            if(CityFromHouse=='Mumbai' || CityFromHouse=='Navi Mumbai'){        
                cityNameLst.add('Mumbai');
                cityNameLst.add('Navi Mumbai');
            }
            
            cityNameLst.add(CityFromHouse);
        }*/
        List<Zone_and_OM_Mapping__c> zoneUserList=[Select Id, User__c, User__r.Name,User__r.email from Zone_and_OM_Mapping__c where Property_Manager__r.Zone__r.City__r.name  = :CityFromHouse];
        for(Zone_and_OM_Mapping__c z:zoneUserList){
            setofUsers.add(z.User__c);
        }
        List<SelectOption> Fe_user = new List<SelectOption>();
        List<User> userlist=[Select id,email,Name from User where id in:setofUsers and  IsActive=true];
       
        Fe_user.add(new SelectOption('','-None-'));
        for(user eachUser : userlist){
            Fe_user.add(new SelectOption(eachUser.Id,eachUser.email));
//users.put(eachUser.Id,eachUser);
        }
        return Fe_user;
    }
     public pagereference showZoneUsers(){
       
       makeZoneFeVisible=true;
       makeCityFeVisible=false; 
       usersoption=new List<SelectOption>();
       usersoption.add(new SelectOption('','-None-'));
       //HouseAndusers = UtilityClass.GetMoveInExecutivesFromZoneCode(HouseAndzoneCodeMap);
       usersoption=allUsersoption();       
     
       return null;
   }
    
   public pagereference cancel(){
       
        PageReference parentPage;
        if(wo!=null){
            parentPage = new PageReference('/' + wo.Id);
            parentPage.setRedirect(true);
            return parentPage;
        }
        
        return null;

    }
    
    
     public string getDatesFromNAApp(Id woId){
        String  House_sf_id;
        workOrder  wo = [select id,API_Success__c,API_Info__c,Last_API_Sync_Time__c,house__c  from workOrder    where id=:woId];  
         House_sf_id = wo.house__c;
        /*API Callout to get dates*/
        HttpRequest req = new HttpRequest();
        House_sf_id=wo.house__c;
        List<NestAway_End_Point__c > nestURL = NestAway_End_Point__c.getall().values();       
        String authToken=nestURL[0].Webapp_Auth__c;
        String strEndPoint;
        strEndPoint= nestURL[0].Moveout_Calendar_Date_API__c+'&auth='+authToken;
        string jsonBody='';
        WBMIMOJSONWrapperClasses.HOuseSfID HOuseSf = new WBMIMOJSONWrapperClasses.HOuseSfID();
        HOuseSf.Houseid=House_sf_id;
        jsonBody=JSON.serialize(HOuseSf);
        HttpResponse res;
        res=RestUtilities.httpRequest('POST',strEndPoint,jsonBody,null);
        String respBody = res.getBody();
        return respBody;
    }
   
}