@isTest
Public Class CaseTriggerhandlerTest {
    
    /*
    Public Static TestMethod Void doTest(){
        Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Furnished House Onboarding').getRecordTypeId();
        Id ZoneRecordTypeId = Schema.SObjectType.Zone__c.getRecordTypeInfosByName().get(Constants.ZONE_RECORD_TYPE_HO_ZONE ).getRecordTypeId();
        List<Case>  cList =  new List<Case>();
        List<Case>  updatedcList =  new List<Case>();
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        zone__c zc= new zone__c();
        zc.Zone_code__c ='123';
        zc.Name='Test';
        insert zc;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone__c=zc.id;
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Furnishing_Type__c='Fully Furnished';
        insert hos;
        
        House_Inspection_Checklist__c hic= new House_Inspection_Checklist__c ();
        hic.name='test';
        hic.TV__c='Yes and Working';
        hic.House__c=hos.Id;
        hic.Fridge__c='Yes and Working';
        hic.Washing_Machine__c='Yes and Working';
        hic.Type_Of_HIC__c='House Inspection Checklist';
        hic.Kitchen_Package__c='Completely Present';
        insert hic;
        List<House__c> hicList= new List<House__c>();
        hicList.add(hos);
        
        Case c=  new Case();
        c.Checklist__c=hic.Id;
        c.HouseForOffboarding__c=hos.id;
        c.RecordTypeId=devRecordTypeId;
        c.ownerId=newuser.Id;
        c.HouseForOffboarding__c=hos.id;
        c.Status='new';
        c.House__c=hos.Id;
        insert c;
        Map<id,case> oldmap = new Map<id,case>();
         oldmap.put(c.id,c);
        
        
        case c1= new case(id=c.id);
        c1.Status='Maintenance';
        c1.House__c=hos.id;
        c1.HouseForOffboarding__c=hos.id;
        update c1;
         Map<id,case> newmap = new Map<id,case>();
         newmap.put(c1.id,c1);
        
        CaseTriggerhandler.beforeUpdate(newmap,newmap);
        CaseTriggerhandler.afterUpdate(newmap,newmap);
        CaseTriggerhandler.CaseWrapper cth = new   CaseTriggerhandler.CaseWrapper('Test',newuser.id,2);
        cth.compareTo(cth);
    } */
        Public Static TestMethod Case doTest1(){
       Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Furnished House Onboarding').getRecordTypeId();
        Id ZoneRecordTypeId = Schema.SObjectType.Zone__c.getRecordTypeInfosByName().get(Constants.ZONE_RECORD_TYPE_HO_ZONE ).getRecordTypeId();
        Id opRecordTypeId = Schema.SObjectType.Operational_Process__c.getRecordTypeInfosByName().get('House Onboarding').getRecordTypeId();
        Id zuRecordTypeId = Schema.SObjectType.Zone_and_OM_Mapping__c.getRecordTypeInfosByName().get('Onboarding').getRecordTypeId();
        
        List<Operational_Process__c> opList = new List<Operational_Process__c>();
        List<Zone_and_OM_Mapping__c> zuList = new List<Zone_and_OM_Mapping__c>();
        List<Case>  cList =  new List<Case>();
        List<Case>  updatedcList =  new List<Case>();
        User newUser = Test_library.createStandardUser(1);
        insert newuser;

        //create Operation Process
        Operational_Process__c op = new Operational_Process__c();
        op.Task_Type__c = Constants.CASE_STATUS_FUR_DTH_WIFI_CONNECT;
        opList.add(op);

        Operational_Process__c op2 = new Operational_Process__c();
        op2.Task_Type__c = 'Photography';
        opList.add(op2);

        Operational_Process__c op3 = new Operational_Process__c();
        op3.Task_Type__c = 'Key Handling';
        opList.add(op3);  

        insert opList;              

        zone__c zc= new zone__c();
        zc.Zone_code__c ='123';
        zc.ZOM__c = newuser.Id;
        zc.ZOM_as_Onboarding_Manager__c = true;
        zc.Name='Test';
        zc.RecordTypeId = ZoneRecordTypeId;
        insert zc;

        Zone_and_OM_Mapping__c zu1 = new Zone_and_OM_Mapping__c();
        zu1.User__c = newuser.Id;
        zu1.FE__c = true;
        zu1.Zone__c = zc.Id;
        zu1.RecordTypeId = zuRecordTypeId;
        zuList.add(zu1);

        Zone_and_OM_Mapping__c zu2 = new Zone_and_OM_Mapping__c();
        zu2.User__c = newuser.Id;
        zu2.Zone__c = zc.Id;
        zu2.FE__c = true;
        zu2.RecordTypeId = zuRecordTypeId;
        zuList.add(zu2); 

        insert zuList;       

        House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone__c=zc.id;
        hos.Onboarding_Zone_Code__c = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.Furnishing_Type__c = Constants.FURNISHING_CONDITION_SEMI_FURNISHED;
        insert hos;
        
        House_Inspection_Checklist__c hic = TestDataFactory.createHouseWithChecklists(hos);

        /*
        hic.name='test';
        hic.TV__c='Yes and Working';
        hic.House__c=hos.Id;
        hic.Fridge__c='Yes and Working';
        hic.Washing_Machine__c='Yes and Working';
        hic.Type_Of_HIC__c='House Inspection Checklist';
        hic.Kitchen_Package__c='Completely Present';
        insert hic;  */

        List<House__c> hicList= new List<House__c>();
        hicList.add(hos);

        Case c=  new Case();
        c.OwnerId = newuser.Id;
        c.Checklist__c=hic.Id;
        c.HouseForOffboarding__c=hos.id;
        c.RecordTypeId=devRecordTypeId;
        c.ownerId=newuser.Id;
        c.HouseForOffboarding__c=hos.id;
        c.Status='new';
        c.House__c = hos.Id;
        c.Offboarding_Reason__c = 'Owner has sold the Property';
        insert c;
        Map<id,case> oldmap = new Map<id,case>();
         oldmap.put(c.id,c);

        
/*
        c.Status = Constants.CASE_STATUS_KEY_HANDLING;
        update c;

        c.Status = Constants.CASE_STATUS_MAINTENANCE;
        update c;

        c.Status = Constants.CASE_STATUS_FUR_DTH_WIFI_CONNECT;
        update c;

        c.Status = Constants.CASE_STATUS_VERIFICATION;
        update c;

        c.Status = Constants.CASE_STATUS_PHOTOGRAPHY;
        update c;

        c.Status = Constants.CASE_STATUS_MAKE_HOUSE_LIVE;
        update c;        

        c.Status = Constants.CASE_STATUS_CLOSED;
        update c;
*/

        case c1= new case(id=c.id);
        /*
        c1.Status=Constants.CASE_STATUS_MAKE_HOUSE_LIVE;
        c1.House__c=hos.id;
        c1.HouseForOffboarding__c=hos.id;
        update c1; */


         Map<id,case> newmap = new Map<id,case>();
         newmap.put(c1.id,c1);
        
        CaseTriggerhandler.beforeUpdate(newmap,newmap);
        CaseTriggerhandler.afterUpdate(newmap,newmap);
        CaseTriggerhandler.CaseWrapper cth = new   CaseTriggerhandler.CaseWrapper('Test',newuser.id,2);
        cth.compareTo(cth);
        return c;
    }

    @isTest
    public static void caseKeyHandlingStatusTest(){
        Case c = doTest1();

        Test.startTest();
        c.Status = Constants.CASE_STATUS_KEY_HANDLING;
        update c;
        Test.stopTest();
    }

    @isTest
    public static void caseMaintenanceStatusTest(){
        Case c = doTest1();

        Test.startTest();
        c.Status = Constants.CASE_STATUS_MAINTENANCE;
        update c;
        Test.stopTest();
    }

    @isTest
    public static void caseFurnishingStatusTest(){
        Case c = doTest1();

        Test.startTest();
        c.Status = Constants.CASE_STATUS_FURNISHING;
        update c;
        Test.stopTest();
    }    

    @isTest
    public static void caseDthWifiStatusTest(){
        Case c = doTest1();

        Test.startTest();
        c.Status = Constants.CASE_STATUS_FUR_DTH_WIFI_CONNECT;
        update c;
        Test.stopTest();
    }

    @isTest
    public static void caseVerificationStatusTest(){
        Case c = doTest1();

        Test.startTest();
        c.Status = Constants.CASE_STATUS_VERIFICATION;
        update c;
        Test.stopTest();
    }    

    @isTest
    public static void casePhotographyStatusTest(){
        Case c = doTest1();

        Test.startTest();
        c.Status = Constants.CASE_STATUS_PHOTOGRAPHY;
        update c;
        Test.stopTest();
    }  

    @isTest
    public static void caseMakeHouseLiveStatusTest(){
        Case c = doTest1();

        Test.startTest();
        c.Status = Constants.CASE_STATUS_MAKE_HOUSE_LIVE;
        update c;
        Test.stopTest();
    } 

    @isTest
    public static void caseDroppedStatusTest(){
        Case c = doTest1();

        Test.startTest();
        c.Offboarding_Reason__c = 'Owner has sold the Property';
        c.Status = 'Dropped';        
        //update c;
        Test.stopTest();
    }       
    
    @isTest
    public static void caseClosedStatusTest(){
        Case c = doTest1();

        Test.startTest();
        c.Status = Constants.CASE_STATUS_CLOSED;
        //update c;
        Test.stopTest();
    }               
    
   
    
}