@isTest
public class ContractHelperTest {
    Public Static TestMethod Void doTest(){
        User newUser = Test_library.createStandardUser(1);
        
        insert newuser;
        Account accObj = new Account();
        accObj.Name = 'TestAcc tsts';
        accobj.Relation_with_Guardian__c='son';
        accObj.ShippingCity = 'Bangalore';
        accObj.ShippingState = 'Karnataka';
        accObj.ShippingCountry = 'India';
        insert accObj;
        contract c= new Contract();
        c.accountid=accobj.id;
        c.Agreement_Type__c ='loi';
        c.Who_pays_Society_Maintenance__c= 'nestaway';
        c.Who_pays_Deposit__c=Constants.CONTRATC_WHO_PAY_DEPOSIT_NESTAWAY ;
        c.SD_Upfront_Amount__c = 76.00;
        c.Furnishing_Type__c = Constants.CONTRACT_FURNISHING_CONDITION_FURNISHED;
        insert c;
        Map<id,Account> tenantAccountMap = new   Map<id,Account>();
        tenantAccountMap.put(accObj.id,accObj);
        Bank_Detail__c bc = new Bank_Detail__c ();
        bc.IFSC_Code__c ='KKBK0000045';
        bc.Account_Number__c ='98765431245678';
        bc.Related_Account__c= accObj.id;
        insert bc;
        House__c hos= new House__c();
        hos.name='house1';
        hos.contract__c = c.id;
        hos.Assets_Created__c=false;
        hos.House_Owner__c=accObj.id;
        hos.Booking_Type__c ='Full House';
        hos.OwnerId=newuser.id;        
        insert hos;
        Map<id,House__c> houseMap  = new  Map<id,House__c> ();
        houseMap.put(hos.id,hos);
        Tenancy__c newTen = new Tenancy__c();
        newTen.Tenant__c = accObj.id;
        newTen.House__c = hos.id;
        insert newTen;
        Map<id,Tenancy__c> mapOfTenancy  = new Map<id,Tenancy__c> (); 
        mapOfTenancy.put(newTen.id,newTen);
        WBTenantAgreementContractClass.TenantAgreementContractWrapper WbNew = new WBTenantAgreementContractClass.TenantAgreementContractWrapper();
        wbNew.houseId = hos.id;
        wbNew.tenantId = accObj.id;
        wbNew.rent = 111;
        List<WBTenantAgreementContractClass.TenantAgreementContractWrapper> wbList = new   List<WBTenantAgreementContractClass.TenantAgreementContractWrapper>();
        wbList.add(wbNew);
        ContractHelper.CreateContract(wbList,tenantAccountMap,houseMap,mapOfTenancy);
    }
    
}