public class CreateMoveInWorkOrder {
    public static Id moveInWorkOrderRTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Move In  WorkOrder').getRecordTypeId();
    public static void createWorkOrder(Map<Id,CAse>  newMAp, map<id,account> tenantAccountMap,map<id,house__c> houseMap){
        
        List<WorkOrder> woList = new List<WorkOrder>();
        for(Case each : newMap.values()){
            // Tenant Profile Verification workorder
            WorkOrder woProfile = new WorkOrder();
            woProfile.RecordTypeId = moveInWorkOrderRTId;
            woProfile.caseid= each.Id;
            woProfile.House__c= each.House__c;
            woProfile.AccountId =each.Tenant__c;   
            woProfile.Queue__c = 'Profile_Completion_Reminder_Queue';
            if(tenantAccountMap.containsKey(each.tenant__c)) {
                if(tenantAccountMap.get(each.tenant__c).Profile_Verified__c == true){
                    woProfile.status = 'Closed' ;
                }
            }
            else{
                woProfile.Status  = 'new';
            }
            if(houseMap.containsKey(each.House__c) &&  houseMap.get(each.House__c).HouseId__c != null ) 
                woProfile.HouseID__c  = houseMap.get(each.House__c).HouseId__c ;
            woProfile.Type_of_WO__c='Tenant Verification';
            woProfile.Description = 'Profile Completion  Reminder';
            woProfile.Subject = 'Profile Completion  Reminder';
            woList.add(woProfile);
            
            // Tenant Document Complete Verification 
            WorkOrder woDocument = new WorkOrder();
            woDocument.RecordTypeId = moveInWorkOrderRTId;
            woDocument.caseid= each.Id;
            woDocument.House__c= each.House__c;
            woDocument.AccountId =each.Tenant__c; 
            woDocument.Queue__c = 'Document_Upload_Reminder_Queue';
            if(tenantAccountMap.containsKey(each.tenant__c)){
                if(tenantAccountMap.get(each.tenant__c).Documents_Complete__c == true || tenantAccountMap.get(each.tenant__c).Ekyc_Verified__c==true){                      
                    woDocument.status = 'Closed';                       
                }
            }
            else{
                woDocument.Status  = 'new';
            }
            if(houseMap.containsKey(each.House__c) &&  houseMap.get(each.House__c).HouseId__c != null ) 
                woDocument.HouseID__c  = houseMap.get(each.House__c).HouseId__c ;
            woDocument.Type_of_WO__c='Tenant Document Complete';
            woDocument.Description ='Document Complete Reminder';
            woDocument.Subject = 'Document Completetion Reminder';
            woList.add(woDocument);
            
            
            // Tenant SD Payment Verification            
            WorkOrder woSD = new WorkOrder();
            woSD.RecordTypeId = moveInWorkOrderRTId;
            woSD.caseid= each.Id;
            woSD.House__c= each.House__c;
            woSD.Tenant__c=each.Tenant__c; 
            if(each.SD_Status__c != null && each.SD_Status__c == 'Paid') {
                woSD.status = 'Closed' ;
            }
            else{
                woSD.Status  = 'New';
            }
            woSD.Type_of_WO__c='SD Verification';
            woSD.Queue__c = 'SD_Payment_Reminder_Queue';
            if(houseMap.containsKey(each.House__c) &&  houseMap.get(each.House__c).HouseId__c != null ) 
                woSD.HouseID__c  = houseMap.get(each.House__c).HouseId__c ;
            woSD.Description ='SD Payment Reminder';
            woSD.Subject = 'SD Payment Reminder';
            woList.add(woSD);            
        }
        if(!woList.isEmpty()){
            insert woList;
        }
    }
    public static void workOrderAssignment(List<WorkOrder> WorkList){
        system.debug('workOrderAssignment');
        Map<Id, Integer> openCaseMap = new Map<Id, Integer>();       
        Map<String, Id> queueMap = new Map<String, Id>();             //Map of global Queues
        Map<String, Map<Id, Integer>> queueLoadMap = new Map<String, Map<Id, Integer>>();
        
        for(AggregateResult ar: getNoOfOpenWokrOrderPerUser()){
            Integer noOfCases;
            if((Integer)ar.get('expr0') == null){
                noOfCases = 0;
            } else {
                noOfCases = (Integer)ar.get('expr0');
            }
            openCaseMap.put((Id)ar.get('OwnerId'), noOfCases);
        }
        system.debug('openCaseMap'+openCaseMap);
        set<String> globalQueues = new Set<String>(Label.Global_WorkOrder_Queue.split(';'));
        system.debug('globalQueues'+globalQueues);
        
        for(Group grp: [select Id, Name from Group where Name =: globalQueues]){
            queueMap.put(grp.Name, grp.id);
            queueLoadMap.put(grp.Name, new Map<Id, Integer>{});
        }
        system.debug('queueMap'+queueMap);
        system.debug('queueLoadMap'+queueLoadMap);
        for(GroupMember member: [Select UserOrGroupId, Group.Name, GroupId From GroupMember where Group.Name =: globalQueues
                                 and UserOrGroupId in (select Id from User where isActive = true and isAvailableForAssignment__c = true)]){
                                     if(queueLoadMap.containsKey(member.Group.Name)){
                                         queueLoadMap.get(member.Group.Name).put(member.UserOrGroupId, openCaseMap.get(member.UserOrGroupId) == null ? 0 : openCaseMap.get(member.UserOrGroupId));
                                     } else{
                                         queueLoadMap.put(member.Group.Name, new Map<Id, Integer>{member.UserOrGroupId => openCaseMap.get(member.UserOrGroupId) == null ? 0 : openCaseMap.get(member.UserOrGroupId)});
                                     }  
                                 }  
        for(WorkOrder each : WorkList){
            String queueName;
            if(each.Queue__c != null){
                queueName = each.Queue__c;
                if(!queueLoadMap.isEmpty() && queueLoadMap.containsKey(queueName) ){
                    List<AssignmentWrapper> awList = new List<AssignmentWrapper>();
                    for(Id userId: queueLoadMap.get(queueName).keySet()){
                        awList.add(new AssignmentWrapper(userId, queueLoadMap.get(queueName).get(userId)));
                    }  
                    system.debug('awList'+awList);
                   if(awList.size()>0){  
                   system.debug('awList'+awList);
                   awList.sort();
                   each.OwnerId = awList[0].UserId;
                   Integer noOfCases = queueLoadMap.get(queueName).get(awList[0].UserId);
                   queueLoadMap.get(queueName).put(awList[0].UserId, noOfCases+1); 
                 }
                 else{
                   if(queueMap.containsKey(queueName))
                    each.OwnerId=queueMap.get(queueName);
                  }
            }
        }
    }
    }
    public static List<AggregateResult> getNoOfOpenWokrOrderPerUser(){
        return [select OwnerId, count(Id) from WorkOrder where status != 'Closed' and 
                RecordTypeId =: moveInWorkOrderRTId group by OwnerId ];
    }
    /* Wrapper Class for sorting the least number of cases assigned to the Agent */
    public class AssignmentWrapper implements Comparable{
        public Id userId{set; get;}
        public Integer noOfCases {set; get;}
        
        public AssignmentWrapper(Id UserId, Integer noOfCases){
            this.userId = userId;
            this.noOfCases = noOfCases;
        }
        
        // Implement the compareTo() method
        public Integer compareTo(Object compareTo) {
            AssignmentWrapper compareToObj = (AssignmentWrapper)compareTo;
            if (noOfCases == compareToObj.noOfCases) return 0;
            if (noOfCases > compareToObj.noOfCases) return 1;
            return -1;        
        }       
    }   
    
}