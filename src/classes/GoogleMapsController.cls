/**************************************************************************************
Apex Class Name : GoogleMapsController 
Version         : 1.0 
Function        : Handler Class for Property Google Maps
Modification Log :
-----------------------------------------------------------------------------
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
*Mohan Chander           18 Nov 2017         Original Version
*************************************************************************************/
public class GoogleMapsController {

    /*
    Method Name     : getAddressAutoComplete
    Purpose         : To get the autocomplete suggestions from google api
    */
    @AuraEnabled
    public static string getAddressAutoComplete(String input, String types) {        

        Http http = new Http();
        String url = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?input='
        + EncodingUtil.urlEncode(input, 'UTF-8')
                    //    + '&types=' + 'establishment'
                    + '&key=AIzaSyD-9wy2X9VSpr-5N5WDzKBydr-6z9eV-Ac';
        HttpRequest req = new HttpRequest();
        HttpResponse res = null;
        req.setEndPoint(url);
        req.setMethod('GET');
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept','application/json');
        res = http.send(req);
        System.debug(res.getBody());
        return res.getBody();
    }

    /*
    Method Name     : getGeoCodeDetails
    Purpose         : To get the GeoCode (Latitude and Longitude) based on placeid
    */
    @AuraEnabled
    public static String getGeoCodeDetails(String strPlaceId){
        Http http = new Http();
        String url = 'https://maps.googleapis.com/maps/api/place/details/json?placeid='+strPlaceId+
        + '&key=AIzaSyCbHqLlNtWBRhztLE0SdbyY249lI8wt7-w';
        HttpRequest req = new HttpRequest();
        HttpResponse res = null;
        req.setEndPoint(url);
        req.setMethod('GET');
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept','application/json');
        res = http.send(req);
        System.debug(res.getBody());
        return res.getBody();
    }

    /*
    Method Name     : updateProperty
    Purpose         : To update the property with user selection
    */
    @AuraEnabled
    public static void updateProperty(String latlng, Id propertyId){
        System.debug(latlng);

        House__c house = [SELECT House_Longitude__c, House_Lattitude__c, Id from House__c where id=: propertyId];
        if(String.isNotBlank(latlng)){
            List<String> latlngArray = latlng.split(' ');
            house.House_Lattitude__c = Decimal.valueOf(latlngArray[0]);
            house.House_Longitude__c = Decimal.valueOf(latlngArray[1]);
            update house;
        }
    }

    /*
    Method Name     : getGeoCodeOfProperty
    Purpose         : To get the details of property if force:recordData is not returned
    */
    @AuraEnabled
    public static House__c getGeoCodeOfProperty (Id propertyId){
        return [SELECT ID, House_Lattitude__c, House_Longitude__c FROM House__c WHERE ID=: propertyId];
    }
}