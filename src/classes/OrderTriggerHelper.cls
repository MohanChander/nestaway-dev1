/**********************************************
    Created By : Mohan
    Purpose    : Helper Class for Order Trigger
**********************************************/
public class OrderTriggerHelper {

/**********************************************
    Created By : Mohan
    Purpose    : before Insert Handler for Order Object
**********************************************/
	public static void purchaseOrderValidator(List<Order> orderList){

		System.debug('**purchaseOrderValidator OrderTriggerHelper');

        try{
        		Set<Id> vendorIdSet = new Set<Id>();
        		Map<Id, Bank_Detail__c> vendorIdToBankDetailMap = new Map<Id, Bank_Detail__c>();
                Set<Id> approverIdSet = new Set<Id>();
                Map<Id, User> approverMap;
                Set<String> profilesSet = new Set<String>(Label.PO_Approver.split(';'));
                Map<Id, Profile> profileMap = new Map<Id, Profile>([select Id, Name from Profile where Name =: profilesSet]);                

        		for(Order ord: orderList){
        			vendorIdSet.add(ord.Vendor__c);

                    if(ord.Approver__c != null){
                        approverIdSet.add(ord.Approver__c);
                    }
        		}

                if(!approverIdSet.isEmpty()){
                    approverMap = new Map<Id, User>([select Id, ProfileId from User where Id =: approverIdSet]);
                }
        		List<Bank_Detail__c> bankDetailList = [select Id, Related_Account__c from Bank_Detail__c
        											   where Related_Account__c =: vendorIdSet];

				for(Bank_Detail__c bd: bankDetailList){
					vendorIdToBankDetailMap.put(bd.Related_Account__c, bd);
				}    

                System.debug('***************approverMap: ' + approverMap + '\n Size of approverMap: ' + approverMap.size());
                System.debug('***************profileMap: ' + profileMap + '\n Size of profileMap: ' + profileMap.size());

				for(Order ord: orderList){
					if(!vendorIdToBankDetailMap.containsKey(ord.Vendor__c)){
						ord.addError('Please ensure that vendor has Bank Details before raising the PO');
					}

                    //if the Approver does not belong to the mentioned profiles throw error
                    if(ord.Approver__c != null && approverMap.containsKey(ord.Approver__c) &&
                        !profileMap.containsKey(approverMap.get(ord.Approver__c).ProfileId)){
                            ord.addError('Please select an appropriate user as the approver');
                    }
				}    											   
            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'Order - purchaseOrderValidator');      
            }   

	}
}