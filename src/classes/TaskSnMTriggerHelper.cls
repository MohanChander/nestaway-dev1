/******************************************************************************************************************************************
Added by baibhav
Purpose: To handle SnM task
*******************************************************************************************************************************************/
public with sharing class TaskSnMTriggerHelper {

/******************************************************************************************************************************************
Added by baibhav
Purpose: to update Service case on Owner task getting Approved
******************************************************************************************************************************************/
	public static void updateSnMServiceCase(Set<id> casid)
	{	  
        Map<id,case> casMap = new Map<id,case>([Select id,status,Owner_approval_status__c from case where id=:casId]);

        List<Case> casList=new List<Case>();
        for(Case c:casMap.values())
        {     
                c.status=Constants.CASE_STATUS_WORK_IN_PROGRESS;
                c.Owner_approval_status__c='Approved';
                casList.add(c);
        }

        if(!casList.isEmpty())
        update casList;
	}
		
	
}