@isTest 
public class UtilityClassTest {
    
    
    public Static TestMethod void testMethod2(){
        User u=Test_library.createStandardUser(1);
        insert u;
        House__c hos= new House__c();
        hos.name='house1';
        hos.House_Layout__c='Studio';
        insert hos;
        
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        Bathroom__c bac= new Bathroom__c ();
        bac.Room__c=rt.Id;
        bac.House__c=hos.Id;
        insert bac;
        
        List<room_terms__c> rtlist= new List<room_terms__c>();
        rtlist.add(rt);
        Photo__c ph= new Photo__c ();
        ph.Room_Term__c=rt.Id;
        ph.House__c=hos.ID;
        ph.Is_Visible__c=true;
        ph.Image_For__c='House';
        ph.Bathroom__c=bac.ID;
        ph.Image_Tag__c='Living';
        insert ph;
        Photo__c ph1= new Photo__c ();
        ph1.Room_Term__c=rt.Id;
        ph1.Image_Tag__c='Living';
        ph1.House__c=hos.ID;
        ph1.Is_Visible__c=true;
        ph1.Image_For__c='House';
        ph1.Bathroom__c=bac.ID;
        insert ph1;
        Photo__c ph4= new Photo__c ();
        ph4.Room_Term__c=rt.Id;
        ph4.Image_Tag__c='Living';
        ph4.House__c=hos.ID;
        ph4.Is_Visible__c=true;
        ph4.Image_For__c='House';
        ph4.Bathroom__c=bac.ID;
        insert ph4;
        Photo__c ph2= new Photo__c ();
        ph2.Room_Term__c=rt.Id;
        ph2.House__c=hos.ID;
        ph2.Image_Tag__c='Kitchen';
        ph2.Is_Visible__c=true;
        ph2.Image_For__c='House';
        ph2.Bathroom__c=bac.ID;
        insert ph2;
        
        Set<Id> setofuser= new Set<id>();
        setOfuser.add(u.id);
        map<id,house__c> HouseAndzoneCodeMap = new map<id,house__c> ();
        HouseAndzoneCodeMap.put(hos.id,hos);
          UtilityClass utc= new   UtilityClass();
        UtilityClass.GetMoveInExecutivesFromZoneCode(HouseAndzoneCodeMap);
        UtilityClass.SendEmailToUser(u.id,'Test');
        UtilityClass.ValidateAllPhotosPresentForHouse(hos.ID);
        UtilityClass.getUserFromZone(zc.Zone_code__c,zc.Name);
        UtilityClass.validateWithinRadius(2.3,2.3,2.3,2.3,2.3);
        UtilityClass.getSubordinateUsers(u.id);
        UtilityClass.getCitySet();
        
    }
    public Static TestMethod void testMethod5(){
        User u=Test_library.createStandardUser(1);
        insert u;
        House__c hos= new House__c();
        hos.name='house1';
        hos.House_Layout__c='Studio';
        insert hos;
        
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        Bathroom__c bac= new Bathroom__c ();
        bac.Room__c=rt.Id;
        bac.House__c=hos.Id;
        insert bac;
        
        List<room_terms__c> rtlist= new List<room_terms__c>();
        rtlist.add(rt);
        Photo__c ph= new Photo__c ();
        ph.Room_Term__c=rt.Id;
        ph.House__c=hos.ID;
        ph.Is_Visible__c=true;
        ph.Image_For__c='House';
        ph.Bathroom__c=bac.ID;
        ph.Image_Tag__c='Living';
        insert ph;
        Photo__c ph1= new Photo__c ();
        ph1.Room_Term__c=rt.Id;
        ph1.Image_Tag__c='Kitchen';
        ph1.House__c=hos.ID;
        ph1.Is_Visible__c=true;
        ph1.Image_For__c='House';
        ph1.Bathroom__c=bac.ID;
        insert ph1;
        Photo__c ph4= new Photo__c ();
        ph4.Room_Term__c=rt.Id;
        ph4.Image_Tag__c='Kitchen';
        ph4.House__c=hos.ID;
        ph4.Is_Visible__c=true;
        ph4.Image_For__c='House';
        ph4.Bathroom__c=bac.ID;
        insert ph4;
        Photo__c ph2= new Photo__c ();
        ph2.Room_Term__c=rt.Id;
        ph2.House__c=hos.ID;
        ph2.Image_Tag__c='Kitchen';
        ph2.Is_Visible__c=true;
        ph2.Image_For__c='House';
        ph2.Bathroom__c=bac.ID;
        insert ph2;
        
        Set<Id> setofuser= new Set<id>();
        setOfuser.add(u.id);
        UtilityClass.SendEmailToUser(u.id,'Test');
        UtilityClass.ValidateAllPhotosPresentForHouse(hos.ID);
        UtilityClass.getUserFromZone(zc.Zone_code__c,zc.Name);
        UtilityClass.validateWithinRadius(2.3,2.3,2.3,2.3,2.3);
        UtilityClass.getSubordinateUsers(u.id);
        
    }
    public Static TestMethod void testMethod3(){
        User u=Test_library.createStandardUser(1);
        insert u;
        House__c hos= new House__c();
        hos.name='house1';
        hos.House_Layout__c='1 BHK';
        insert hos;
        
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        Bathroom__c bac= new Bathroom__c ();
        bac.Room__c=rt.Id;
        bac.House__c=hos.Id;
        insert bac;
        
        List<room_terms__c> rtlist= new List<room_terms__c>();
        rtlist.add(rt);
        Photo__c ph= new Photo__c ();
        ph.Room_Term__c=rt.Id;
        ph.House__c=hos.ID;
        ph.Is_Visible__c=true;
        ph.Image_For__c='House';
        ph.Bathroom__c=bac.ID;
        ph.Image_Tag__c='Kitchen';
        insert ph;
        Photo__c ph1= new Photo__c ();
        ph1.Room_Term__c=rt.Id;
        ph1.Image_Tag__c='Dining';
        ph1.House__c=hos.ID;
        ph1.Is_Visible__c=true;
        ph1.Image_For__c='House';
        ph1.Bathroom__c=bac.ID;
        insert ph1;
        Photo__c ph2= new Photo__c ();
        ph2.Room_Term__c=rt.Id;
        ph2.House__c=hos.ID;
        ph2.Image_Tag__c='Kitchen';
        ph2.Is_Visible__c=true;
        ph2.Image_For__c='House';
        ph2.Bathroom__c=bac.ID;
        insert ph2;
        
        Set<Id> setofuser= new Set<id>();
        setOfuser.add(u.id);
        UtilityClass.SendEmailToUser(u.id,'Test');
        UtilityClass.ValidateAllPhotosPresentForHouse(hos.ID);
        UtilityClass.getUserFromZone(zc.Zone_code__c,zc.Name);
        UtilityClass.validateWithinRadius(2.3,2.3,2.3,2.3,2.3);
        UtilityClass.getSubordinateUsers(u.id);
        UtilityClass.insertErrorLog('test','test');
        
    }
    
    public Static TestMethod void testMethod4(){
        User u=Test_library.createStandardUser(1);
        insert u;
        House__c hos= new House__c();
        hos.name='house1';
        hos.House_Layout__c='1 BHK';
        insert hos;
        
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        Bathroom__c bac= new Bathroom__c ();
        bac.Room__c=rt.Id;
        bac.House__c=hos.Id;
        insert bac;
        
        List<room_terms__c> rtlist= new List<room_terms__c>();
        rtlist.add(rt);
        Photo__c ph= new Photo__c ();
        ph.Room_Term__c=rt.Id;
        ph.House__c=hos.ID;
        ph.Is_Visible__c=true;
        ph.Image_For__c='House';
        ph.Bathroom__c=bac.ID;
        ph.Image_Tag__c='Living';
        insert ph;
        Photo__c ph1= new Photo__c ();
        ph1.Room_Term__c=rt.Id;
        ph1.Image_Tag__c='Living';
        ph1.House__c=hos.ID;
        ph1.Is_Visible__c=true;
        ph1.Image_For__c='House';
        ph1.Bathroom__c=bac.ID;
        insert ph1;
        Photo__c ph2= new Photo__c ();
        ph2.Room_Term__c=rt.Id;
        ph2.House__c=hos.ID;
        ph2.Image_Tag__c='Kitchen';
        ph2.Is_Visible__c=true;
        ph2.Image_For__c='House';
        ph2.Bathroom__c=bac.ID;
        insert ph2;

        Set<Id> setofuser= new Set<id>();
        setOfuser.add(u.id);
        UtilityClass.SendEmailToUser(u.id,'Test');
        UtilityClass.ValidateAllPhotosPresentForHouse(hos.ID);
        UtilityClass.getUserFromZone(zc.Zone_code__c,zc.Name);
        UtilityClass.validateWithinRadius(2.3,2.3,2.3,2.3,2.3);
        UtilityClass.getSubordinateUsers(u.id);
        UtilityClass.insertErrorLog('test','test');
        UtilityClass.insertStringInLog('test');
        
    }
    
    
}