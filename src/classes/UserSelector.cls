/*  Created By : Mohan
    Purpose    : User Object related queries   */

public class UserSelector {

    //get user details
    public static List<User> getUserDetails(Set<Id> userIdSet){
        return [select Id, ManagerId, AccountID, email, Manager.Email, Manager.FirstName
                from User where Id =: userIdSet];
    }

     public static Map<id,User> getUserDetailsMap(Set<Id> userIdSet){
        return new Map<id,User>([select Id, ManagerId,AccountID,ContactID,email, Vendor_Type__c, Manager.Email from User where Id =: userIdSet]);
    }

    public static List<User> getAllUserDetails(){
        return [select Id, Manager.Email, ManagerId from User where isActive = true];
    }

    //get user List from Set of User Email Id's
    public static List<User> getUserListFromUserEmailIds(Set<String> emailSet){
        return [select Id, Email, ManagerId, Manager.Email, Manager.FirstName
                 from User where Email =: emailSet and isActive = true];
    }
}