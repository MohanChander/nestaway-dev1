/*  Created By : Deepak
    Purpose    : All the Queries related to Tenancy are accessed from here */
public class TenancySelector {
        //get map of Tenancy
    public static Map<id,Tenancy__c> getTenancyMap(Set<id> setOfTenant , Set<Id> setOfHouse)
    {
     return new Map<id,Tenancy__c>([Select Id,House__c,Tenant__c  from Tenancy__c  
                                where House__c in :setOfHouse and Tenant__c in :setOfTenant and House__c != null and Tenant__c != null]);
    }


}