/*  Created By   : Mohan - WarpDrive Tech Works
	Created Date : 21/05/2017
	Purpose      : All the queries related to the Pricebook2 Object are here */

public with sharing class Pricebook2Selector {

	//query for the NestAway Standard Price Book
	public static Pricebook2 getNestAwayStandardPricebook(){
		return [select Id, Name from Pricebook2 Where Name = 'NestAway Standard Price Book'];
	}
}