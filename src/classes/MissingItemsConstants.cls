/*  Created By   : Deepak - WarpDrive Tech Works
Created Date : 22/09/2017
Purpose      : All the constants, Picklist values, RecordType Name are accessed from this class */

public class MissingItemsConstants {

public static String OPERATIONAL_PROCESS_RT_MIMO_SERVICE= 'MIMO and Service Request Configuration';   

  // Category of Cases 
    
    public static String CASE_CATEGORY_AGREEMENT='Agreement';
    public static String CASE_CATEGORY_CLEANING='Cleaning';
    public static String CASE_CATEGORY_CONNECTIVITY='Connectivity';
    public static String CASE_CATEGORY_ELECTRICAL='Electrical/Plumbing';
    public static String CASE_CATEGORY_ELECTRONICS='Electronics';
    public static String CASE_CATEGORY_FURNISHING='Furnishing';
    public static String CASE_CATEGORY_KEYS='Keys';
    
    // Sub Category of Cases 
    public static String CASE_SUB_CATEGORY_AGREEMENT='Agreement';
    public static String CASE_SUB_CATEGORY_LIVING_ROOM='Living Room';
    public static String CASE_SUB_CATEGORY_DINING_ROOM='Dining Room';
    public static String CASE_SUB_CATEGORY_TENANT_ROOM_CLEANED='Tenant Room Cleaned';
    public static String CASE_SUB_CATEGORY_TENANT_BATHROOM_CLEANED='Tenant Bathroom Cleaned';
    public static String CASE_SUB_CATEGORY_DTH='DTH';
    public static String CASE_SUB_CATEGORY_INTERNET='Internet';
    public static String CASE_SUB_CATEGORY_TUBELIGHT='Tube Light';
    public static String CASE_SUB_CATEGORY_CEILING_FAN='Ceiling Fan';
    public static String CASE_SUB_CATEGORY_WATER_HEATER='Water Heater';
    public static String CASE_SUB_CATEGORY_TV='TV';
    public static String CASE_SUB_CATEGORY_FRIDGE='Fridge';
    public static String CASE_SUB_CATEGORY_WASHING_MACHINE='Washing Machine';
    public static String CASE_SUB_CATEGORY_SOFA='Sofa';
    public static String CASE_SUB_CATEGORY_CENTER_TABLE='Center Table';
    public static String CASE_SUB_CATEGORY_WINDOW_CURTAIN='Window Curtain';
    public static String CASE_SUB_CATEGORY_DINING_TABLE='Dining Table';
    public static String CASE_SUB_CATEGORY_DINING_CHAIRS='Dining Chairs';
    public static String CASE_SUB_CATEGORY_GAS_PIPES='Gas Regulator & Pipe';
    public static String CASE_SUB_CATEGORY_STOVE='Stove';
    public static String CASE_SUB_CATEGORY_CROCKERY_SET='Crockery Set';
    public static String CASE_SUB_CATEGORY_PILLOW='Pillow';
    public static String CASE_SUB_CATEGORY_NIGHT_LAMP='Night lamp';
    public static String CASE_SUB_CATEGORY_BED_SHEET='Bed Sheet';
    public static String CASE_SUB_CATEGORY_WINDOW_CURTAINS='Window Curtain';
    public static String CASE_SUB_CATEGORY_COT='Cot';
    public static String CASE_SUB_CATEGORY_MATTRESS='Mattress';
    public static String CASE_SUB_CATEGORY_AC='AC';
    public static String CASE_SUB_CATEGORY_CHAIR='Chair';
    public static String CASE_SUB_CATEGORY_SIDE_TABLE='Side Table';
    public static String CASE_SUB_CATEGORY_STUDY_TABLE='Study Table';
    public static String CASE_SUB_CATEGORY_TOILET_BRUSH='Toilet Brush';
    public static String CASE_SUB_CATEGORY_BUCKET_MUG='Bucket & Mug';
    public static String CASE_SUB_CATEGORY_MIRROR='Mirror';
    public static String CASE_SUB_CATEGORY_WARDROBE_KEYS='Wardrobe Keys';
    public static String CASE_SUB_CATEGORY_ROOM_KEYS='Room Keys';
    public static String CASE_SUB_CATEGORY_MAIN_DOOR_KEYS='Main Door Keys';
    public static String CASE_SUB_CATEGORY_NO_OF_NON_FUNCTIONING_ROOM_KEYS='No of Non Functioning Room Keys';
    public static String CASE_SUB_CATEGORY_NO_OF_NON_FUNCTIONING_WARDROBE_KEYS='No of Non Functioning Wardrobe Keys';
    public static String CASE_SUB_CATEGORY_NO_OF_NON_FUNCTIONING_Fans='No of Fans';


       public static String AGREEMENT_NOT_PROVIDED='Not Provided';
    
    public static String LIVING_ROOM_NOT_PROVIDED='Not Provided';
    
    public static String DINING_ROOM_NOT_PROVIDED='Not Provided';
    
    public static String TENANT_ROOM_CLEANED_NO='NO';
    
    public static String TENANT_BATHROOM_CLEANED_NO='No';
    
    public static String DTH_INSTALLED='Yes';
    public static String DTH_NOT_INSTALLED='No';
    
    public static String INTERNET_NOT_INSTALLED='No';
    
    public static String TUBELIGHT_NOT_PROVIDED='Not Provided';
    
    public static String CEILING_FAN_NOT_PROVIDED='Not Provided';
    
    public static String WATER_HEATER_PRESENT_AND_WORKING='Present & Working';
    public static String WATER_HEATER_PRESENT_AND_NOT_WORKING='Present but Not Working';
    public static String WATER_HEATER_NOT_WORKING='Not Present';
    
    public static String TV_PRESENT_AND_WORKING='Yes and Working';
    public static String TV_PRESENT_AND_NOT_WORKING='Yes and Not Working'; 
    public static String TV_NOT='No';
    
    public static String FRIDGE_PRESENT_AND_WORKING='Yes and Working';
    public static String FRIDGE_PRESENT_AND_NOT_WORKING='Yes and Not Working'; 
    public static String FRIDGE_NOT='No';
    
    public static String WASHING_MACHINE_PRESENT_AND_WORKING='Yes and Working';
    public static String WASHING_MACHINE_PRESENT_AND_NOT_WORKING='Yes and Not Working'; 
    public static String WASHING_MACHINE_NOT='No'; 
       
    public static String SOFA_YES='Yes';
    public static String SOFA_NO='No';
    
    public static String CENTER_TABLE_PRESENT_AND_IN_GOOD_CONDITION='Present & in Good Condition';
    public static String CENTER_TABLE_PRESENT_AND_NOT_IN_GOOD_CONDITION='Present but not in Good Condition';
    public static String CENTER_TABLE_NOT_PRESENT='Not Present';
    
    public static String WINDOW_CURTAIN_NOT='Not Provided';
    
    public static String DINING_TABLE_YES='Yes';
    public static String DINING_TABLE_NO='No';
    
    public static String DINING_CHAIRS_PRESENT_AND_IN_GOOD_CONDITION='All Present & In Good Condition';
    public static String DINING_CHAIRS_SOME_CHAIR_MISSING='Some Chairs Missing';


    public static String GAS_PIPES_PRESENT_AND_WORKING='Yes and Working';
    public static String GAS_PIPES_PRESENT_AND_NOT_WORKING='Yes and Not Working';
    public static String GAS_PIPES_NOT_WORKING='No';

 
    public static String STOVE_PIPES_PRESENT_AND_WORKING='Yes and Working';
    public static String STOVE_PIPES_PRESENT_AND_NOT_WORKING='Yes and Not Working';
    public static String STOVE_PIPES_NOT_WORKING='No';
    
    public static String CROCKERY_SET_YES_AND_COMPLETE='Yes and Complete';
    public static String CROCKERY_SET_YES_AND_INCOMPLETE='Yes But Incomplete';
    public static String CROCKERY_SET_NO='No';

    public static String PILLOW_NOT_PROVIDED='Not Provided';
    
    public static String NIGHT_LAMP_PROVIDED='Provided';
    public static String NIGHT_LAMP_NOT_PROVIDED='Not Provided';
    
    public static String BED_SHEET_NOT_PROVIDED='Not Provided';
    
    public static String WINDOW_CURTAINS_NOT_PROVIDED='Not Provided';
    
    public static String COT_NO='No';
    
    public static String MATTRESS_No='No';
    
     public static String AC_YES_AND_WORKING='Yes and Working';
     public static String AC_YES_AND_NOT_WORKING='Yes and Not Working';
     public static String AC_NOT_WORKING='Not Working';
     public static String AC_PRESENT_AND_WORKING='Present & Working';
     public static String AC_PRESENT_AND_NOT_WORKING='Present But Not Working';
     public static String AC_NO='No';
     
    
    public static String CHAIR_YES='Yes';
    public static String CHAIR_NO='No';

    public static String SIDE_TABLE_PRESENT_AND_NOT_IN_GOOD_CONDITION='Present but In Good Condition';
    public static String SIDE_TABLE_NOT_PRESENT='Not Present';

    
    public static String STUDY_TABLE_YES='Yes';
    public static String STUDY_TABLE_NO='No';
    
    public static String TOILET_BRUSH_PRESENT='Present';
    public static String TOILET_BRUSH_NOT_PRESENT='Not Present';

    public static String BUCKET_MUG_PRESENT='Present';
    public static String BUCKET_MUG_NOT_PRESENT='Not Present';

    public static String MIRROR_PRESENT_AND_IN_GOOD_CONDITION='Present & In Good Condition';
    public static String MIRROR_PRESENT_AND_NOT_IN_GOOD_CONDITION='Present but broken';
    public static String MIRROR_NOT_PRESENT='Not Present';

    public static String WARDROBE_KEYS_NOT_PROVIDED='Not Provided';

    public static String ROOM_KEYS_NOT_PROVIDED='Not Provided';
    public static String MAIN_DOOR_KEYS_NOT_PROVIDED='Not Provided';


    
    
    }