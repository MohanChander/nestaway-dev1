//  Created By: Deepak
//   Purpose   : Create WorkOrder for the Move out Case Whose Status is Move out Scheduled
public class CaseMoveOutHelper {
    public static void createMoveOutCheckWorkOrder(Map<Id, Case> caseMap, Map<Id, Case> newMap){
        
        Id moveoutWorkOrderRTId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(Constants.WORKORDER_RECORD_TYPE_MOVE_Out_CHECK).getRecordTypeId();
        
        List<WorkOrder> woList = new List<WorkOrder>();
        
        for(Case c: caseMap.values()){
            WorkOrder wo = new WorkOrder();
            wo.CaseId = c.Id;
            wo.Accountid=c.AccountId;
            wo.RecordTypeId = moveoutWorkOrderRTId;
            wo.Description='Move out Check task is created to Check House Room Bathroom and verify all things';
            wo.Subject = 'Move out Check for ' + c.Booked_Object_Type__c;
            if(c.House__c != null)
                wo.House__c = c.House__c;
            else
                newMap.get(c.Id).addError('The Move out Case is associated with House. Please ensure House is Present on Case to schedule Move out');
            woList.add(wo);
        }
        if(!woList.isEmpty())
            insert woList;
    }
}