@isTest
public class CaseLegalHelperTest {
    public static Id legalRecordTypeId  = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Legal').getRecordTypeId();  
    public Static TestMethod void test1(){
        NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
        cusSet.name = 'nestaway';
        cusSet.Nestaway_URL__c = 'www.test.com';
        insert cusSet;
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Assets_Created__c=false;
        insert hos;
        Account accObj = new Account();
        accObj.Name = 'NestAway';
        insert accObj;
        Bank_Detail__c bc = new Bank_Detail__c ();
        bc.IFSC_Code__c ='KKBK0000045';
        bc.Account_Number__c ='98765431245678';
        bc.Related_Account__c= accObj.id;
        insert bc;
        Case c = new CAse();
        c.Defendant__c =accObj.id;
        c.Petitioner_Plaintiff__c =accObj.id;
        insert c;
        List<Case> caseList = new List<Case>();
        caseList.add(c);
        Map<Id,case> newMap = new Map<Id,Case>();
        newMap.put(c.id,c);
        CaseLegalHelper.beforeUpdate(newMap);
        CaseLegalHelper.beforeInsert(caseList);
        
    }
    
    
    
}