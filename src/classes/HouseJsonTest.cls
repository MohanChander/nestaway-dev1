@isTest
public Class HouseJsonTest{
     public Static TestMethod void jsonTest(){
        Test.StartTest();
            
            NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
            cusSet.name = 'nestaway';
            cusSet.Nestaway_URL__c = 'www.test.com';
            insert cusSet;
            
            Id recordTypeId = [Select Id from RecordType where Name=:'House Inspection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id; 
            Account accObj = new Account();
            accObj.Name = 'TestAcc';
            insert accObj;
        
            Opportunity opp = new Opportunity();
            opp.Name ='Test Opp';
            opp.AccountId = accObj.Id;
            opp.CloseDate = System.Today();
            opp.StageName = 'Final Contract';
            opp.House_Layout__c = '1 BHK';
            insert opp;
            
            House_Inspection_Checklist__c houseIC = new House_Inspection_Checklist__c();
            houseIC.RecordTypeId = recordTypeId;
            houseIC.Opportunity__c = opp.Id;
            houseIC.House_Layout__c = '1 BHK';
            houseIC.Status__c = 'House Inspection In Progress';     
            insert houseIC;
            
            Quote quoObj = new Quote();
            quoObj.Name = 'Quote1';
            quoObj.Status = 'Draft';
            quoObj.OpportunityId = opp.Id;
            quoObj.approval_status__c = 'Awaiting Items Manager Approval';
            insert quoObj;
            
            PriceBook2 priceBk = new PriceBook2();
            priceBk.Name = 'TestPriceBook';
            // priceBk.isStandard = FALSE;
            insert priceBk;
            
            Product2 prod = new Product2();
            prod.Name = 'testProd';
            prod.isActive = TRUE;
            insert prod;
            
            Contract cont = new Contract();
            cont.Name = 'TestContract';
            cont.AccountId = accObj.id;
            cont.Opportunity__c = opp.id;
            cont.PriceBook2Id = priceBk.Id;
            cont.status = 'Draft';
            cont.Maximum_number_of_tenants_allowed__c = '1';
            insert cont;
            
            House__c houseObj = new House__c();
            houseObj.Name = 'TestHouse';
            houseObj.Stage__c = 'House Draft';
            houseObj.Opportunity__c = opp.Id;
            houseObj.House_Owner__c = accObj.Id;
            houseObj.Contract__c = cont.id;
            
            insert houseObj;
            
            HouseJson.createJSON(houseObj.Id);
            HouseJson.createJSONContract(cont.id);
        
        Test.StopTest();
    }
}