global class EmailToLead implements Messaging.InboundEmailHandler {
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail emailToLead,Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();      
   
         string EmailPlainText= '';
        string EmailFirstName;
        string EmailLastName;
        string EmailAddress;
        string EmailCountrycode;
        string EmailPhone;
        string EmailStreet;
        string EmailCity;
        string EmailState;
        String EmailPostalCode;     
        String emailToReply=emailToLead.ReplyTo;
        string firstName;
        List<OrgWideEmailAddress>  orgEmails=[SELECT Address,Id FROM OrgWideEmailAddress where Address='no-reply@nestaway.com'];
        
        try{
            
            EmailPlainText = emailToLead.plainTextBody;
            system.debug('EmailPlainText'+EmailPlainText);
            String[] emailbody=emailToLead.plainTextBody.split('\n',0);
            system.debug('emailbody'+emailbody);
           for(string s: emailbody){
               
                if(s.contains(LABEL.EMAIL_TO_LEAD_FN)){
                     system.debug('firstNameLst'+s.split(':'));
                    List<String> firstNameLst=s.split(':');
                    if(firstNameLst.size()>=1){ 
                        system.debug('firstNameLst'+firstNameLst);
                        EmailFirstName=firstNameLst[1]; 
                    }
                }
               
                if(s.contains(LABEL.EMAIL_TO_LEAD_LN)){
                    List<String> LastNameLst=s.split(':');
                    if(LastNameLst.size()>=1){                      
                        EmailLastName=LastNameLst[1];   
                    }
                }
               
                if(s.contains(LABEL.EMAIL_TO_LEAD_EMAIL)){
                    List<String> EmailLst=s.split(':');
                    if(EmailLst.size()>=1){                     
                        EmailAddress=EmailLst[1]; 
                    }
                }
               
                if(s.contains(LABEL.EMAIL_TO_LEAD_CNCODE)){
                    List<String> CountryLst=s.split(':');
                    if(CountryLst.size()>=1){                       
                        EmailCountrycode=CountryLst[1]; 
                    }
                }
               
                if(s.contains(LABEL.EMAIL_TO_LEAD_PHNUM)){
                    List<String> PhoneLst=s.split(':');
                    if(PhoneLst.size()>=1){                     
                        EmailPhone=PhoneLst[1]; 
                    }
                }
               
                if(s.contains(LABEL.EMAIL_TO_LEAD_STREET)){
                    List<String> StreetLst=s.split(':');
                    if(StreetLst.size()>=1){                        
                        EmailStreet=StreetLst[1];   
                    }
                }
               
                if(s.contains(LABEL.EMAIL_TO_LEAD_CITY)){
                    List<String> CityLst=s.split(':');
                    if(CityLst.size() >=1){                     
                        EmailCity=CityLst[1];   
                    }
                }
               
                if(s.contains(LABEL.EMAIL_TO_LEAD_STATE)){
                    List<String> StateLst=s.split(':');
                    if(StateLst.size() >= 1){                       
                        EmailState=StateLst[1]; 
                    }
                }
                if(s.contains(LABEL.EMAIL_TO_LEAD_POSTALCODE)){
                    List<String> PostalCodeLst=s.split(':');
                    if(PostalCodeLst.size() >=1){                       
                        EmailPostalCode=PostalCodeLst[1];   
                    }
                }
           }   
       
            Lead leadToInsert = new lead(
                FirstName = EmailFirstName,
                LastName = EmailLastName,
                email = EmailAddress,
                Phone = EmailPhone,
                Country_Code__c = EmailCountrycode,
                Street = EmailStreet,
                City = EmailCity,
                State = EmailState,
                Status ='New',
                PostalCode=EmailPostalCode,
                LeadSource='Email',
                EmailToLead__c=true
            );
            insert(leadToInsert);
            
            
            
            EmailTemplate templateId = [Select id,body,subject from EmailTemplate where name =:LABEL.EMAIL_TO_LEAD_SUCCESS_TEMPLATE];
            String htmlBody = templateId.body;
            list<String> Str= new list<String>();
            Str.add(emailToReply);
            Messaging.Singleemailmessage mail = new Messaging.Singleemailmessage();
            mail.setToAddresses(Str);
            if(orgEmails.size()>0){
                 mail.setOrgWideEmailAddressId(orgEmails.get(0).Id);
            }           
            mail.setBccSender(false);
            mail.setSubject(LABEL.EMAIL_TO_LEAD_EMAIL_SUBJECT);
            mail.setPlainTextBody(htmlBody );
           // mail.setTemplateId(templateId.id);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        catch (exception e) {
            /* Log the exception in log object */
            
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
             UtilityClass.insertErrorLog('Email To Lead','Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());  
            UtilityClass.insertErrorLog('Email To Lead',emailToLead.plainTextBody);            
            EmailTemplate templateId = [Select id,body,subject from EmailTemplate where name =:LABEL.EMAIL_TO_LEAD_FORMAT_TEMPLATE];
            String htmlBody = templateId.body;
            list<String> Str= new list<String>();
            Str.add(emailToReply);
            Messaging.Singleemailmessage mail = new Messaging.Singleemailmessage();
            mail.setToAddresses(Str);
            mail.setBccSender(false);
            if(orgEmails.size()>0){
                 mail.setOrgWideEmailAddressId(orgEmails.get(0).Id);
            }  
            mail.setSubject(LABEL.EMAIL_TO_LEAD_FAIL_SUBJECT);
            mail.setPlainTextBody(htmlBody);
           // mail.setTemplateId(templateId.id);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        return result;
    }
    
}