@RestResource(urlMapping='/OwnerContractApprovalClass/*')
global with sharing class WBOwnerContractApprovalClass {
    
    // Response wrapper
    global class OwnerContractApprovalWrapper {
        public String status;
        public String action;
        public string agreementApprovalToken;
        public string url;       
        public boolean hasException;        
        public boolean success;
    }   
    
    
    @HttpPost   
    global static OwnerContractApprovalWrapper getOwnerContractDetails() {
        
        
         RestRequest req = RestContext.request;
         OwnerContractApprovalWrapper resp = new OwnerContractApprovalWrapper();         
          try{   
          
            String bodyOfReq = RestContext.request.requestBody.toString(); 
            system.debug('****bodyOfReq' + bodyOfReq);      
            OwnerContractApprovalWrapper params = new OwnerContractApprovalWrapper();
            params = (OwnerContractApprovalWrapper)JSON.deserialize(bodyOfReq,OwnerContractApprovalWrapper.Class);
            if(params!=null && params.agreementApprovalToken!=null && params.action!=null){
                
                List<contract> conLst=[select id,Manually_Approved_By_ZM__c,isCloneContract__c,Agreement_Type__c,Tenancy_Type__c,Owner_Approval_Status__c,Agreement_Approval_Code__c,Owner_Approval_API_Msg__c,Status,Approval_Status__c,Owner_Approved_By_API__c from contract where Agreement_Approval_Code__c=:params.agreementApprovalToken];
                if(conLst.size()>0){
                    
                     if(conLst.size()==1){
                         
                         contract con=conLst.get(0);
                          List<NestAway_End_Point__c > nestURL = NestAway_End_Point__c.getall().values();
                          resp.url=nestURL[0].Owner_Contract_Site_URL__c;
                          
                         if(con.isCloneContract__c){
                             
                              resp.url+='/ContractTermChanges?Id='+params.agreementApprovalToken;
                         } 
                         else{
                          
                          if(con.Agreement_Type__c!='LOI'){
                              
                             if(con.Tenancy_Type__c== 'Family'){
                                 
                                 resp.url+='/SampleFamilyAgremeent?Id='+con.id;
                             }
                             else{
                                 
                                 resp.url+='/SampleBachelorAgremeent?Id='+con.id;
                             }
                              
                          }
                          else{
                              resp.url+='/SampleLOIPDF?Id='+con.id;
                          }  
                         } 
                          resp.action=params.action;
                          resp.agreementApprovalToken=params.agreementApprovalToken;
                          
                        if(params.action=='initiate'){
                            
                              if(con.Owner_Approval_Status__c==null || con.Owner_Approval_Status__c==''){
                                  
                                  resp.status='PENDING';
                                  
                                  if(con.isCloneContract__c && con.Manually_Approved_By_ZM__c){
                                      
                                      resp.status='APPROVED';
                                  }
                              }
                              else if(con.Owner_Approval_Status__c=='APPROVED'){
                                  resp.status='APPROVED';
                              }
                              else if(con.Owner_Approval_Status__c=='REJECTED'){
                                  resp.status='REJECTED';
                              }
                              else{
                                  
                                   resp.status='PENDING';
                              }
                              
                              resp.success=true;
                              resp.hasException=false;
                               
                          
                        }                         
                         
                        else if(params.action=='approved' || params.action=='rejected'){
                            
                             if(con.isCloneContract__c){
                                 
                                  if(params.action=='approved'){ 
                                        con.Approval_Status__c = 'Approved by Owner';                                       
                                        con.Owner_Approved_By_API__c=true;
                                        con.Owner_Approval_Status__c='APPROVED';
                                  }
                                  else {
                                      
                                       con.Approval_Status__c = 'Rejected by Owner';
                                       con.Owner_Approved_By_API__c=true;
                                       con.Owner_Approval_Status__c='REJECTED';
                                  }                          
                                 
                             }
                             else{
                              
                                  if(con.Approval_Status__c == 'Approved by ZM' || con.Approval_Status__c == 'Sample Contract Manually Approved by ZM'){
                                      
                                      if(params.action=='approved'){ 
                                        con.Approval_Status__c = 'Sample Contract Approved by Owner';
                                        con.status = 'Final Contract';
                                        con.Owner_Approved_By_API__c=true;
                                        con.Owner_Approval_Status__c='APPROVED';
                                      }
                                      else {
                                          
                                           con.Approval_Status__c = 'Sample Contract Rejected by Owner';
                                           con.Owner_Approved_By_API__c=true;
                                           con.Owner_Approval_Status__c='REJECTED';
                                      }
                                        
                                  }
                                  else{
                                      
                                      con.Owner_Approval_API_Msg__c='Owner approval received from API but status moved from the Approved by ZM or Sample Contract Manually Approved by ZM';
                                      con.Owner_Approved_By_API__c=true;
                                      if(params.action=='approved'){ 
                                           con.Owner_Approval_Status__c='APPROVED';
                                      }
                                      else{
                                           con.Owner_Approval_Status__c='REJECTED';
                                      }
                                  } 
                                  
                             }
                             
                              update con;
                              resp.status=con.Owner_Approval_Status__c;
                              resp.success=true;
                              resp.hasException=false;
                              
                          }
                          else{
                         
                           resp.success=false;
                        }
                         
                     }
                     else{
                         
                        resp.success=false;
                     }
                    
                }
                else{
                    
                   resp.success=false;
                }
                
                
            }
            else{
                resp.success=false;
            }
            
          }
          catch(Exception e){    
              resp.success=false;
              resp.hasException=true;
              system.debug('****exception in API: -'+e.getMessage()+' at line :-'+e.getLineNumber());             
            
        }
        
        return resp; 
        
    }

}