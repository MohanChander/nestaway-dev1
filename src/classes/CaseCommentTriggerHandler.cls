/*  Created By : Mohan
    Purpose    : Trigger Handler for Case Comment Custom Object */
public class CaseCommentTriggerHandler {

    /*  Created By : Mohan
        Purpose    : Trigger Handler for Case Comment Custom Object */
    public static void updateNoOfCommentsOnCase(List<Case_Comment__c> caseCommentList) {

            System.debug('*********************updateNoOfCommentsOnCase');

        try{
                Set<Id> caseIdSet = new Set<Id>();
                Map<Id, Map<String, Integer>> commentCountMap = new Map<Id, Map<String, Integer>>();
                List<Case> caseList = new List<Case>();

                Account nestAwayAccount = AccountSelector.getNestAwayAccount();

                for(Case_Comment__c cc: caseCommentList){
                    caseIdSet.add(cc.Case__c);
                }

                for(AggregateResult ar: CaseSelector.getNoOfCaseComments(caseIdSet)){

                    Id caseId = (Id)ar.get('Case__c');
                    Id accountId = (Id)ar.get('Account__c');
                    Integer commentCount = (Integer)ar.get('expr0');

                    if(commentCountMap.containsKey(caseId)){
                        if(accountId == null)
                            commentCountMap.get(caseId).put('Agent', commentCount);
                        else if(accountId == nestAwayAccount.Id)
                            commentCountMap.get(caseId).put('App', commentCount);
                        else
                            commentCountMap.get(caseId).put('Customer', commentCount);

                    } else{
                        if(accountId == null)
                            commentCountMap.put(caseId, new Map<String, Integer>{'Agent' => commentCount});   
                        else if(accountId == nestAwayAccount.Id)
                            commentCountMap.put(caseId, new Map<String, Integer>{'App' => commentCount});
                        else
                            commentCountMap.put(caseId, new Map<String, Integer>{'Customer' => commentCount});
                    }
                }

                for(Id caseId: commentCountMap.keySet()){
                    Case c = new Case();
                    c.Id = caseId;
                    c.No_of_Agent_Comments__c = commentCountMap.get(caseId).get('Agent');
                    c.No_of_Customer_Comments__c = commentCountMap.get(caseId).get('Customer');
                    caseList.add(c);
                }

                if(!caseList.isEmpty())
                    update caseList;
            } catch(Exception e){
                System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                UtilityClass.insertGenericErrorLog(e, 'Comment Count Updation');                
            }
    }


    public static void afterUpdate(List<Case_Comment__c> ccList){

        System.debug('**afterUpdate');

        try{
                List<FeedItem> fiList = new List<FeedItem>();

                for(Case_Comment__c cc: ccList){
                    if(!cc.System_Generated_Comment__c && cc.Case__c != null){
                        FeedItem item = new FeedItem(
                            parentId = cc.Case__c, // where to post message
                            body = cc.Comment__c,
                            isRichText = true
                        );  

                        fiList.add(item);                      
                    }
                }

                System.debug('**fiList: ' + fiList + '\n Size of fiList: ' + fiList.size());                

                if(!fiList.isEmpty()){
                    insert fiList;
                }
            
            } catch(Exception e){
                    System.debug('**Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'CaseCommentTriggerHandler afterUpdate');      
            }           
    }
}