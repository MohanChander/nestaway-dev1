public class AssetTriggerHandler {
    
    public static void afterInsert(Map<Id, Asset> newMap){
        Set<Id> assetIdSet = new Set<Id>();
        List<Asset> assetList = new List<Asset>();
        for(Asset a: newMap.values()){
            assetIdSet.add(a.Id);
            assetList.add(a);
        }
	}

	public static void beforeInsert(List<Asset> newlist){

      if(!newlist.isEmpty()){
      	AssetHelper.assetAutoIdCreation(newlist);
      }
  }

}