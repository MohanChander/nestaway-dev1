@isTest
public  class BanktriggerHandlerTest {
     public Static TestMethod void bankTest()
     {
          Id accRecordTypeId  = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
            Account accObj      = new Account();
            accObj.firstname ='Bony';
            accObj.lastName         = 'TestAcc';
            accObj.Phone        = '8952385962';
            accObj.PersonEmail  = 'test@gmail.com';
            accObj.RecordTypeId = accRecordTypeId;
            insert accObj;

            Bank_Detail__c bk=new Bank_Detail__c();
            bk.Related_Account__c=accObj.id;
            bk.Account_Number__c='123456709876';
            bk.Bank_Name__c='SBI';
            bk.Branch_Name__c='Golmuri';
            bk.IFSC_Code__c='SBIN0010823';
            bk.Is_A_House_Owner__c=true;
            insert bk;

            accObj.firstname='Deeepak';
            update accObj;
            bk.Related_Account__c=accObj.id;
            update bk;


     }
}