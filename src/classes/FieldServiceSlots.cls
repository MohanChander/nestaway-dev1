/***************************************
    Created By : Mohan
    Purpose    : Fetch the slots from the FLS getSlots method and 
                 Expose the slots to the WebApp. These slots will be shown 
                 to the tenat while booking the tickets
****************************************/                
@RestResource(urlMapping='/FieldService/getSlots')
global class FieldServiceSlots {


/************************************************************    
    Created By : Mohan
    Purpose    : 1) input Params : Problem Id and List of House Id's
                 2) slots related to that Houses
*************************************************************/                 
    @HttpPost
    global static FieldServiceSlotsService.JsonResponseBody getSlots() {        

        System.debug('**getSlots Http web Service executed');

        RestRequest request = RestContext.request;
        String reqBodyString = request.requestBody.toString();     

        System.debug('**reqBodyString ' +  reqBodyString);

        JsonRequestBody reqBodyObj = new JsonRequestBody();

        reqBodyObj = (JsonRequestBody) JSON.deserialize(reqBodyString, JsonRequestBody.class);   

        System.debug('**problem_id ' + reqBodyObj.problem_id); 
        System.debug('**house_ids ' + reqBodyObj.house_ids);

        Id problemId = reqBodyObj.problem_id;
        Set<Id> houseIdSet = new Set<Id>(reqBodyObj.house_ids);

        FieldServiceSlotsService.JsonResponseBody responseBody = FieldServiceSlotsService.getSlots(problemId, houseIdSet);
        
        return responseBody;
    }    

    //Object to parse the incoming request body
    global class JsonRequestBody {
        public Id problem_id;
        public List<Id> house_ids;
    }
}