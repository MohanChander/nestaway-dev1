@RestResource(urlMapping='/WBHouseExecutiveFeedback/*')
global with sharing class WBHouseExecutiveFeedback {
    
    // Response wrapper
    global class HouseExecutiveFeedbackWrapper {
        public String status;
        public String action;
        public string feedbackToken;            
        public boolean hasException;        
        public boolean success;
        public String executiveName;
        public Decimal rating;
        public String feedbackComment;
    }   
    
    
    @HttpPost   
    global static HouseExecutiveFeedbackWrapper getFeedbackDetails() {
        
        
         RestRequest req = RestContext.request;
         HouseExecutiveFeedbackWrapper resp = new HouseExecutiveFeedbackWrapper();         
          try{   
          
            String bodyOfReq = RestContext.request.requestBody.toString(); 
            system.debug('****bodyOfReq' + bodyOfReq);      
            HouseExecutiveFeedbackWrapper params = new HouseExecutiveFeedbackWrapper();
            params = (HouseExecutiveFeedbackWrapper)JSON.deserialize(bodyOfReq,HouseExecutiveFeedbackWrapper.Class);
            if(params!=null && params.feedbackToken!=null && params.action!=null){
                
                List<House__c> houseLst=[select id,Feedback_Token__c,Feedback_Status__c,Rating__c,Feedback_Executive__c,Feedback_Executive__r.Name,Feedback_Comment__c from House__c where Feedback_Token__c=:params.feedbackToken];
                if(houseLst.size()>0){
                    
                     if(houseLst.size()==1 && houseLst.get(0).Feedback_Executive__c!=null){
                         
                          House__c house=houseLst.get(0);
                         
                          resp.action=params.action;
                          resp.feedbackToken=params.feedbackToken;
                          
                        if(params.action=='initiate'){
                            
                              if(house.Feedback_Status__c==null || house.Feedback_Status__c==''){
                                  
                                  resp.status='PENDING';
                              }
                              else if(house.Feedback_Status__c=='COMPLETED'){
                                  resp.status='COMPLETED';
                              }                              
                              else{
                                  
                                   resp.status='PENDING';
                              }
                              
                              resp.rating=house.Rating__c;
                              resp.executiveName=house.Feedback_Executive__r.Name;
                              resp.feedbackComment=house.Feedback_Comment__c;
                              resp.success=true;
                              resp.hasException=false;
                               
                          
                        }                         
                         
                        else if(params.action=='COMPLETED'){
                            
                              house.Rating__c=params.rating;
                              house.Feedback_Comment__c=params.feedbackComment;
                              house.Feedback_Status__c='COMPLETED';
                              update house;
                              resp.status=house.Feedback_Status__c;
                              resp.success=true;
                              resp.hasException=false;
                              
                          }
                          else{
                         
                           resp.success=false;
                        }
                         
                     }
                     else{
                         
                        resp.success=false;
                     }
                    
                }
                else{
                    
                   resp.success=false;
                }
                
                
            }
            else{
                resp.success=false;
            }
            
          }
          catch(Exception e){    
              resp.success=false;
              resp.hasException=false;
              UtilityClass.insertErrorLog('Webservices','****exception in Feedback API: -'+e.getMessage()+' at line :-'+e.getLineNumber());
              system.debug('****exception in Feedback API: -'+e.getMessage()+' at line :-'+e.getLineNumber());             
            
        }
        
        return resp; 
        
    }

}