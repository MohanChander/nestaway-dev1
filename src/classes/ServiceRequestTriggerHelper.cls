/**********************************************
Created By : Deepak
Purpose    : Service Request WorkOrder
WO should be assigned automatically based on the queue selected
Care agent/Service manager should be able to create WO
When WO is marked done or dropped the case owner should get notification
**********************************************/
public class ServiceRequestTriggerHelper {
    
    
    public static Id serviceRequestWO = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(WorkOrder_Constants.WORKORDER_Service_Request_WorkOrder).getRecordTypeId();
    static Map<String, Map<String, Map<Id, Integer>>> WorkLoadMap = new Map<String, Map<String, Map<Id, Integer>>>();
    static Id serviceRequestMappingRTId = Schema.SObjectType.Zone_and_OM_Mapping__c.getRecordTypeInfosByName().get(Constants.ZONE_MAPPING_SERVICE_REQUEST_MAPPING_RECORD_TYPE).getRecordTypeId();
    Static Id serviceRequestZoneRTId = Schema.SObjectType.Zone__c.getRecordTypeInfosByName().get(Constants.ZONE_RECORD_TYPE_SERVICE_REQUEST_ZONE).getRecordTypeId();
    
    public static void beforeinsert(List<WorkOrder> woList){
        system.debug('ServiceRequestTriggerHelper');
        
        Set<Id> setOfCase = new Set<Id>();
        List<Case> caseList = new List<Case>();
        Map<ID,Case> mapOfcase = new Map<Id,Case>();
        try{
            for(Workorder each : woList){    
                setOfCase.add(each.caseid);
            }
            
            system.debug('setofcase'+setOfCase);
            if(!setOfCase.isEmpty()){
                caseList = [Select Id,house1__c,AccountId,house1__r.APM__c,house1__r.service_zonecode__c,subject,Description,house1__r.houseId__c,Owner.email,Account.PersonEmail,Account.PersonMobilePhone
                            from Case where id in:setOfCase];
            }
            if(!caseList.isEmpty()){
                for(Case each :caseList)
                    mapOfcase.put(each.id,each);
            }
            system.debug('mapOfcase'+mapOfcase);
            
            for(workOrder each : woList){
                if( mapOfCase.get(each.caseID).Owner.email != null){
                    each.Case_Owner_Email__c = mapOfcase.get(each.caseid).Owner.email;
                }
                if( mapOfCase.get(each.caseID).AccountId != null){
                    each.AccountId = mapOfcase.get(each.caseid).AccountId;
                }
                if( mapOfCase.get(each.caseID).house1__c != null){
                    each.house__c =  mapOfCase.get(each.caseID).house1__c;
                }
                if(mapOfCase.get(each.caseID).house1__r.service_zonecode__c != null){
                    each.Service_Zone_Code__c = mapOfCase.get(each.caseID).house1__r.service_zonecode__c;
                }
                if( mapOfCase.get(each.caseID).subject != null){
                    each.Subject =  mapOfCase.get(each.caseID).subject;
                }
                if( mapOfCase.get(each.caseID).Description != null){
                    each.Description = mapOfCase.get(each.caseID).Description;
                }
                each.StartDate = system.now();
                if(mapOfCase.get(each.caseID).house1__r.houseId__c != null){
                    each.HouseID__c =  mapOfCase.get(each.caseID).house1__r.houseId__c;
                }
                if(mapOfCase.get(each.caseID).Account.PersonEmail != null){
                    each.Tenant_Email__c=mapOfCase.get(each.caseID).Account.PersonEmail;
                }
                if(mapOfCase.get(each.caseID).Account.PersonMobilePhone != null){
                    each.Tenant_PhoneNo__c=mapOfCase.get(each.caseID).Account.PersonMobilePhone;
                }
                if(mapOfCase.get(each.caseID).AccountId != null){
                    each.Tenant__c = mapOfcase.get(each.caseid).AccountId;
                }
                if(mapOfCase.get(each.caseID).house1__r.APM__c != null){
                    each.APM__c  =  mapOfCase.get(each.caseID).house1__r.APM__c;
                }
            }
            if(!woList.isEmpty()){
                workOrderAssignment(woList);
            }
        }catch(Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e);
        } 
    }
    
    public static void afterInsert(List<WorkOrder> woList){
        system.debug('afterInsert');
        List<AggregateResult> OpenWo= new List<AggregateResult>();
        List<AggregateResult> closedWO= new List<AggregateResult>();
        List<AggregateResult> TotalWO= new List<AggregateResult>();
        Map<Id,Integer> mapOfOpenWork= new Map<Id,Integer>();
        Map<Id,Integer> mapOfClosdWork= new Map<Id,Integer>();
        Map<Id,Integer> mapOfTotalWork= new Map<Id,Integer>();
        Set<Id> setOfCase = new Set<Id>();
        List<Case> caseList = new List<Case>();
        List<Case> caseListToUpdate = new List<Case>();
        Map<Id,String> mapofWorkOrderStatus = new Map<Id,String>();
        for(WorkOrder each : woList){
            setOfCase.add(each.caseId);
            mapofWorkOrderStatus.put(each.caseid,each.status);
        }
        if(!setOfCase.isEmpty()){
            OpenWo=[select caseId,Count(Id) countWO from WorkOrder where (Status ='Open' or Status ='Work in Progress') And caseId in:setOfCase group by caseId ];
            closedWO=[select caseId,Count(Id) countWo from WorkOrder where (Status ='Closed' or Status ='dropped') AND caseId in:setOfCase group by caseId ];
            TotalWO=[select caseId,Count(ID) countWo from WorkOrder where   caseId in:setOfCase group by caseId ];
        }
        for(AggregateResult arg:OpenWo)
        {
            mapOfOpenWork.put(String.valueOf(arg.get('caseId')),Integer.valueOf(arg.get('countWO')));
            
        }
        
        
        for(AggregateResult arg:closedWO)
        {
            mapOfClosdWork.put(String.valueOf(arg.get('caseId')),Integer.valueOf(arg.get('countWO')));
        }
        
        
        for(AggregateResult arg:TotalWO)
        {
            mapOfTotalWork.put(String.valueOf(arg.get('caseId')),Integer.valueOf(arg.get('countWo')));
        }
        
        system.debug('mapofWorkOrderStatus'+mapofWorkOrderStatus);
        
        if(!setOfCase.isEmpty()){
            caseList = [Select id,Service_Request_Work_Order_Status__c,Open_WO_Count__c,Closed_WO_Count__c,Total_WO_Count__c from case where id in :setOfCase];
        }
        system.debug('caseList'+caseList);
        if(!caseList.isEmpty()){
            for(Case each : caseList){
                each.Service_Request_Work_Order_Status__c = 'Open';
               if(!mapOfOpenWork.containsKey(each.id) ){
                    each.Open_WO_Count__c = 0;
                }
                else{
                    each.Open_WO_Count__c = mapOfOpenWork.get(each.id);
                }
                if(!mapOfClosdWork.containsKey(each.id)){
                    each.Closed_WO_Count__c =0;
                }
                else{
                    each.Closed_WO_Count__c = mapOfClosdWork.get(each.id);
                }
                
                if(!mapOfTotalWork.containsKey(each.id)){
                    
                    each.Total_WO_Count__c = 0;
                }
                else{
                    each.Total_WO_Count__c = mapOfTotalWork.get(each.id);
                }
                caseListToUpdate.add(each);
            }
        }
        system.debug('caseListToUpdate'+caseListToUpdate);
        
        if(!caseListToUpdate.isEmpty()){
            update caseListToUpdate;
        }
    }
    public static void afterupdate(List<WorkOrder> woList){
        Set<Id> setOfCase = new Set<Id>();
        Map<Id,String> mapofWorkOrderStatus = new Map<Id,String>();
        List<Case> caseList = new List<Case>();
        List<Case> caseList1 = new List<Case>();
        List<Case> caseListToUpdate = new List<Case>();
        List<Case> caseListToUpdate1 = new List<Case>();

        List<AggregateResult> OpenWo= new List<AggregateResult>();
        List<AggregateResult> closedWO= new List<AggregateResult>();
        List<AggregateResult> TotalWO= new List<AggregateResult>();
        Map<Id,Integer> mapOfOpenWork= new Map<Id,Integer>();
        Map<Id,Integer> mapOfClosdWork= new Map<Id,Integer>();
        Map<Id,Integer> mapOfTotalWork= new Map<Id,Integer>();
        for(WorkOrder each: woList){
            setOfCase.add(each.caseId);
            mapofWorkOrderStatus.put(each.caseid,each.status);
        }
        if(!setofCase.isEmpty()){
            OpenWo=[select caseId,Count(Id) countWO from WorkOrder where (Status ='Open' or Status ='Work in Progress') And caseId in:setOfCase group by caseId ];
            closedWO=[select caseId,Count(Id) countWo from WorkOrder where (Status ='Closed' or Status ='dropped') AND caseId in:setOfCase group by caseId ];
            TotalWO=[select caseId,Count(ID) countWo from WorkOrder where   caseId in:setOfCase group by caseId ];
        }
        for(AggregateResult arg:OpenWo)
        {
            mapOfOpenWork.put(String.valueOf(arg.get('caseId')),Integer.valueOf(arg.get('countWO')));
            
        }
        
        
        for(AggregateResult arg:closedWO)
        {
            mapOfClosdWork.put(String.valueOf(arg.get('caseId')),Integer.valueOf(arg.get('countWO')));
        }
        
        
        for(AggregateResult arg:TotalWO)
        {
            mapOfTotalWork.put(String.valueOf(arg.get('caseId')),Integer.valueOf(arg.get('countWo')));
        }
        
        
        system.debug('mapOfOpenWork'+mapOfOpenWork);
        system.debug('mapOfClosdWork'+mapOfClosdWork);
        system.debug('mapOfTotalWork'+mapOfTotalWork);
        
        if(!setOfCase.isEmpty()){
            caseList = [Select id,Service_Request_Work_Order_Status__c,Open_WO_Count__c,Closed_WO_Count__c,Total_WO_Count__c,(Select id,status from WorkOrders  where (status = 'Open' or Status ='Work in Progress')) from case where id in :setOfCase];
        }
      
        system.debug('caseList'+caseList);
          for(Case each : caseList){
              
              if(each.WorkOrders.size() == 0){
                each.Service_Request_Work_Order_Status__c = 'Closed';
            }
              if(!mapOfOpenWork.containsKey(each.id) ){
                    each.Open_WO_Count__c = 0;
                }
                else{
                    each.Open_WO_Count__c = mapOfOpenWork.get(each.id);
                }
                if(!mapOfClosdWork.containsKey(each.id)){
                    each.Closed_WO_Count__c =0;
                }
                else{
                    each.Closed_WO_Count__c = mapOfClosdWork.get(each.id);
                }
                
                if(!mapOfTotalWork.containsKey(each.id)){
                    
                    each.Total_WO_Count__c = 0;
                }
                else{
                    each.Total_WO_Count__c = mapOfTotalWork.get(each.id);
                }
                caseListToUpdate.add(each);
            }
        
        
       
          if(!caseListToUpdate.isEmpty()){
            update caseListToUpdate;
        }
    }
    public static void  changeOwnerEmailOnWorkOrder(List<Case> caseList){
        Set<Id> setOfCase = new Set<Id>();
        List<Case> caseListOwner = new List<Case>();
        List<WorkOrder> woUpdateList = new List<WorkOrder>();
        for(Case each : caseList){
            setOfCase.add(each.id);
        }
        if(!setOfCase.isEmpty()){
            caseListOwner = [Select Id,Owner.email,(Select Id, Case_Owner_Email__c  from  WorkOrders ) from Case where id in:setOfCase];
        }
        system.debug('caseListOwner'+caseListOwner);
        for(Case each : caseListOwner){
            for(WorkOrder wo : each.WorkOrders){
                wo.Case_Owner_Email__c= each.Owner.email;
                woUpdateList.add(wo);
            }
        }
        if(!woUpdateList.isEmpty()){
            update woUpdateList;
        }
    }
    
    public static void workOrderAssignment(List<WorkOrder> WorkList){
        
        system.debug('workOrderAssignment');
        List<WorkOrder> woListPmAssignment = new List<WorkOrder>();
        Map<Id, Integer> openWorkOrderMap = new Map<Id, Integer>();       
        Map<String, Id> queueMap = new Map<String, Id>();             //Map of global Queues
        Map<String, Map<Id, Integer>> queueLoadMap = new Map<String, Map<Id, Integer>>();
        Map<String, Id> zoneManagerMap = new Map<String, Id>();
        Map<String, Id> romMap = new Map<String, Id>();
        Map<Id, User> managerMap = new Map<Id, User>(UserSelector.getAllUserDetails());
        List<Zone__c> zoneList = new List<Zone__c>(ZoneAndUserMappingSelector.getZoneRecordsForRecordTypeId(serviceRequestZoneRTId));
        // 
        Id unassignedServiceRequesWorkOrderQueueId = GroupAndGroupMemberSelector.getIdfromName(Constants.QUEUE_NAME_UNASSIGNED_SERVICE_REQUEST);
        
        try{
            for(City__c city: CitySelector.getCityDetails()){
                romMap.put(city.Name, city.ROM__c);
            }
            
            for(Zone__c zone: zoneList){
                zoneManagerMap.put(zone.Zone_Code__c, zone.ZOM__c);
            }
            
            for(AggregateResult ar: getNoOfOpenWokrOrderPerUser()){
                Integer noOfWorkOkrder;
                if((Integer)ar.get('expr0') == null){
                    noOfWorkOkrder = 0;
                } else {
                    noOfWorkOkrder = (Integer)ar.get('expr0');
                }
                openWorkOrderMap.put((Id)ar.get('OwnerId'), noOfWorkOkrder);
            }
            
            
            system.debug('openWorkOrderMap'+openWorkOrderMap);
            set<String> globalQueues = new Set<String>(Label.Global_Care_Queues.split(';'));
            system.debug('globalQueues'+globalQueues);
            
            for(Group grp: [select Id, Name from Group where Name =: globalQueues]){
                queueMap.put(grp.Name, grp.id);
                queueLoadMap.put(grp.Name, new Map<Id, Integer>{});
            }
            
            
            system.debug('queueMap'+queueMap);
            system.debug('queueLoadMap'+queueLoadMap);
            for(GroupMember member: [Select UserOrGroupId, Group.Name, GroupId From GroupMember where Group.Name =: globalQueues
                                     and UserOrGroupId in (select Id from User where isActive = true and isAvailableForAssignment__c = true)]){
                                         if(queueLoadMap.containsKey(member.Group.Name)){
                                             queueLoadMap.get(member.Group.Name).put(member.UserOrGroupId, openWorkOrderMap.get(member.UserOrGroupId) == null ? 0 : openWorkOrderMap.get(member.UserOrGroupId));
                                         } else{
                                             queueLoadMap.put(member.Group.Name, new Map<Id, Integer>{member.UserOrGroupId => openWorkOrderMap.get(member.UserOrGroupId) == null ? 0 : openWorkOrderMap.get(member.UserOrGroupId)});
                                         }  
                                     }
            
            
            
            List<Zone_and_OM_Mapping__c> ZoneWithUserList = [select Id, Name, User__c, isActive__c, Zone__r.Zone_Code__c, 
                                                             Zone__r.ZOM__c, Available_Queues__c
                                                             from Zone_and_OM_Mapping__c where RecordTypeId =: serviceRequestMappingRTId
                                                             and isActive__c = true];
            
            // Populate the WorkLoadMap
            for(Zone_and_OM_Mapping__c zu: ZoneWithUserList){
                if(WorkLoadMap.containsKey(zu.Zone__r.Zone_Code__c) && zu.Available_Queues__c != null){
                    for(String str: zu.Available_Queues__c.split(';')){
                        if(WorkLoadMap.get(zu.Zone__r.Zone_Code__c).containsKey(str))
                            WorkLoadMap.get(zu.Zone__r.Zone_Code__c).get(str).put(zu.User__c, openWorkOrderMap.get(zu.User__c) == null ? 0 : openWorkOrderMap.get(zu.User__c));
                        else
                            WorkLoadMap.get(zu.Zone__r.Zone_Code__c).put(str, new Map<Id, Integer>{zu.User__c => openWorkOrderMap.get(zu.User__c) == null ? 0 : openWorkOrderMap.get(zu.User__c)});
                    }
                } else {
                    Map<String, Map<Id, Integer>> innerMap = new Map<String, Map<Id, Integer>>();
                    if(zu.Available_Queues__c != null){
                        for(String str: zu.Available_Queues__c.split(';')){
                            innerMap.put(str, new Map<Id, Integer>{zu.User__c => openWorkOrderMap.get(zu.User__c) == null ? 0 : openWorkOrderMap.get(zu.User__c)});
                        }
                        WorkLoadMap.put(zu.Zone__r.Zone_Code__c, innerMap);
                    }
                }
            }
            
            for(WorkOrder each : WorkList){
                String queueName;
                if(each.Queue__c != null){
                    queueName = each.Queue__c;
                    system.debug('queueName'+queueName);
                    if(queueName == 'Property Manager'){
                        woListPmAssignment.add(each);
                    }
                    else if(!queueLoadMap.isEmpty() && queueLoadMap.containsKey(queueName) ){
                        List<AssignmentWrapper> awList = new List<AssignmentWrapper>();
                        for(Id userId: queueLoadMap.get(queueName).keySet()){
                            awList.add(new AssignmentWrapper(userId, queueLoadMap.get(queueName).get(userId)));
                        }  
                        system.debug('awList'+awList);
                        if(awList.size()>0){  
                            system.debug('awList'+awList);
                            awList.sort();
                            each.OwnerId = awList[0].UserId;
                            Integer noOfWorkOkrder = queueLoadMap.get(queueName).get(awList[0].UserId);
                            queueLoadMap.get(queueName).put(awList[0].UserId, noOfWorkOkrder+1); 
                        }
                        else{
                            if(queueMap.containsKey(queueName))
                                each.OwnerId=queueMap.get(queueName);
                        }
                    }
                    else if(each.Service_Zone_Code__c  != null && each.Queue__c != null && !WorkLoadMap.isEmpty() && WorkLoadMap.containsKey(each.Service_Zone_Code__c )){
                        
                        System.debug('*********Case Queue ' + each.Queue__c + 'Case Zone ' + each.Service_Zone_Code__c);
                        
                        //assign the Case to the Agent with least number of Cases
                        if(!WorkLoadMap.isEmpty() && WorkLoadMap.containsKey(each.Service_Zone_Code__c ) && 
                           WorkLoadMap.get(each.Service_Zone_Code__c).containsKey(each.Queue__c) && 
                           WorkLoadMap.get(each.Service_Zone_Code__c).get(each.Queue__c) != null){
                               System.debug('**********************Inside the round robin if block');
                               List<AssignmentWrapper> awList = new List<AssignmentWrapper>();
                               for(Id userId: WorkLoadMap.get(each.Service_Zone_Code__c).get(each.Queue__c).keySet()){
                                   awList.add(new AssignmentWrapper(userId, WorkLoadMap.get(each.Service_Zone_Code__c).get(each.Queue__c).get(userId)));
                               }
                               awList.sort();
                               each.OwnerId = awList[0].UserId;
                               Integer noOfCases = WorkLoadMap.get(each.Service_Zone_Code__c ).get(each.Queue__c).get(awList[0].UserId);
                               WorkLoadMap.get(each.Service_Zone_Code__c ).get(each.Queue__c).put(awList[0].UserId, noOfCases+1);                            
                           } 
                        //if there is no user for that particular Queue/ There is no Queue - assign the ticket to Zone incharge/ROM/Ussigned Queue
                        else {
                            if(zoneManagerMap.containsKey(each.Service_Zone_Code__c) && zoneManagerMap.get(each.Service_Zone_Code__c) != null)
                                each.OwnerId = zoneManagerMap.get(each.Service_Zone_Code__c);
                            else if(each.House_City__c != null && romMap.containsKey(each.House_City__c) && romMap.get(each.House_City__c) != null)
                                each.OwnerId = romMap.get(each.House_City__c);
                            else 
                                each.OwnerId = unassignedServiceRequesWorkOrderQueueId;                            
                        }
                    }
                    else if(each.Service_Zone_Code__c == null && each.House_City__c == null){
                        each.OwnerId = unassignedServiceRequesWorkOrderQueueId;
                    } 
                    else if(each.Service_Zone_Code__c != null){
                        if(zoneManagerMap.containsKey(each.Service_Zone_Code__c) && zoneManagerMap.get(each.Service_Zone_Code__c) != null)
                            each.OwnerId = zoneManagerMap.get(each.Service_Zone_Code__c);
                        else if(each.House_City__c != null && romMap.containsKey(each.House_City__c) && romMap.get(each.House_City__c) != null)
                            each.OwnerId = romMap.get(each.House_City__c);
                        else 
                            each.OwnerId = unassignedServiceRequesWorkOrderQueueId;                     
                    }
                    else if(each.House_City__c != null){
                        if(each.House_City__c != null && romMap.containsKey(each.House_City__c) && romMap.get(each.House_City__c) != null)
                            each.OwnerId = romMap.get(each.House_City__c);
                        else 
                            each.OwnerId = unassignedServiceRequesWorkOrderQueueId;                     
                    } 
                    
                }
            }
            if(!woListPmAssignment.isEmpty()){
                WorkOrderPMAssignment.assignmentOfOwners(woListPmAssignment);
            }
        }catch (Exception e){
            System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e, 'work order Care');              
        }
    }
    public static List<AggregateResult> getNoOfOpenWokrOrderPerUser(){
        return [select OwnerId, count(Id) from WorkOrder where status != 'Closed' and 
                RecordTypeId =: serviceRequestWO group by OwnerId ];
    }
    /* Wrapper Class for sorting the least number of cases assigned to the Agent */
    public class AssignmentWrapper implements Comparable{
        public Id userId{set; get;}
        public Integer noOfWorkOkrder {set; get;}
        
        public AssignmentWrapper(Id UserId, Integer noOfWorkOkrder){
            this.userId = userId;
            this.noOfWorkOkrder = noOfWorkOkrder;
        }
        
        // Implement the compareTo() method
        public Integer compareTo(Object compareTo) {
            AssignmentWrapper compareToObj = (AssignmentWrapper)compareTo;
            if (noOfWorkOkrder == compareToObj.noOfWorkOkrder) return 0;
            if (noOfWorkOkrder > compareToObj.noOfWorkOkrder) return 1;
            return -1;        
        }       
    }   
    
}