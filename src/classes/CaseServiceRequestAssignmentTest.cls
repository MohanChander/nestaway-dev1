@Istest
public class CaseServiceRequestAssignmentTest {
        Static Id serviceRequestZoneRTId = Schema.SObjectType.Zone__c.getRecordTypeInfosByName().get(Constants.ZONE_RECORD_TYPE_SERVICE_REQUEST_ZONE).getRecordTypeId();

          public  static  Id ServiceRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_SERVICE_REQUEST).getRecordTypeId();
     public  static Id serviceRequestMappingRTId = Schema.SObjectType.Zone_and_OM_Mapping__c.getRecordTypeInfosByName().get(Constants.ZONE_MAPPING_SERVICE_REQUEST_MAPPING_RECORD_TYPE).getRecordTypeId();
    Public Static TestMethod Case doTest1(){
		 Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Furnished House Onboarding').getRecordTypeId();
        Id ZoneRecordTypeId = Schema.SObjectType.Zone__c.getRecordTypeInfosByName().get(Constants.ZONE_RECORD_TYPE_HO_ZONE ).getRecordTypeId();
        Id opRecordTypeId = Schema.SObjectType.Operational_Process__c.getRecordTypeInfosByName().get('House Onboarding').getRecordTypeId();
        Id zuRecordTypeId = Schema.SObjectType.Zone_and_OM_Mapping__c.getRecordTypeInfosByName().get('Onboarding').getRecordTypeId();
        
        List<Operational_Process__c> opList = new List<Operational_Process__c>();
        List<Zone_and_OM_Mapping__c> zuList = new List<Zone_and_OM_Mapping__c>();
        List<Case>  cList =  new List<Case>();
        List<Case>  updatedcList =  new List<Case>();
        User newUser = Test_library.createStandardUser(1);
        newUser.isActive=true;
        newUser.isAvailableForAssignment__c = true;
        insert newuser;
        
        //create Operation Process
        Operational_Process__c op = new Operational_Process__c();
        op.Task_Type__c = Constants.CASE_STATUS_FUR_DTH_WIFI_CONNECT;
        opList.add(op);
        
        Operational_Process__c op2 = new Operational_Process__c();
        op2.Task_Type__c = 'Photography';
        opList.add(op2);
        
        Operational_Process__c op3 = new Operational_Process__c();
        op3.Task_Type__c = 'Key Handling';
        opList.add(op3);  
        
        insert opList;              

        zone__c zc= new zone__c();
        zc.Zone_code__c ='test';
        zc.ZOM__c = newuser.Id;
        zc.ZOM_as_Onboarding_Manager__c = true;
        zc.Name='Test';
        zc.RecordTypeId = serviceRequestZoneRTId;
        insert zc;
        
        Zone_and_OM_Mapping__c zu1 = new Zone_and_OM_Mapping__c();
        zu1.User__c = newuser.Id;
        zu1.FE__c = true;
        zu1.Zone__c = zc.Id;
        zu1.isActive__c=true;
        zu1.RecordTypeId = serviceRequestMappingRTId;
        zuList.add(zu1);
        
        Zone_and_OM_Mapping__c zu2 = new Zone_and_OM_Mapping__c();
        zu2.User__c = newuser.Id;
        zu2.Zone__c = zc.Id;
        zu2.FE__c = true;
        zu2.isActive__c=true;
        zu2.RecordTypeId = serviceRequestMappingRTId;
        zuList.add(zu2); 
        
        insert zuList;  
        City__c newcity= new City__c();
        newcity.name='bangalore';
        insert newcity;
        
        
        House__c hos= new House__c();
        hos.name='house1';
        hos.City_Master__c=newcity.id;
        hos.Onboarding_Zone_Code__c  = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.City__c='Bangalore';
        hos.Furnishing_Type__c = Constants.FURNISHING_CONDITION_SEMI_FURNISHED;
        insert hos;
        
        Account accobj= new Account();
        accobj.Name='Nestaway';
        insert accobj;
        Entitlement ent = new Entitlement();
        ent.AccountId=accobj.id;
        ent.StartDate=system.today();
        ent.EndDate=system.today();
        ent.Name='Nestaway';
        insert ent;
        
        Case c=  new Case();
        c.EntitlementId=ent.id;
        c.OwnerId = newuser.Id;
        c.AccountId=accobj.id;
        c.RecordTypeId= ServiceRecordTypeId;
        c.Queue__c='Finance';
        c.House1__c=hos.id;
        c.RecordTypeId=devRecordTypeId;
        c.Status='new';
        insert c;
        cList.add(c);
        Map<id,case> oldmap = new Map<id,case>();
        oldmap.put(c.id,c);
        case c1= new case(id=c.id);
        Map<id,case> newmap = new Map<id,case>();
        newmap.put(c1.id,c1);
        
        CaseServiceRequestAssignment.serviceRequestCaseAssignment(cList);
        CaseServiceRequestAssignment.moveOutAssignment(cList);
        CaseServiceRequestAssignment.AssignmentWrapper cth = new   CaseServiceRequestAssignment.AssignmentWrapper(newuser.id,2);
        cth.compareTo(cth);
        return c;
    }
    
    public Static TestMethod void testMethod1(){
        Case c = doTest1();
        List<Case>  cList =  new List<Case>();
        
        Test.startTest();
        c.Status = Constants.CASE_STATUS_KEY_HANDLING;
        c.Origin = Constants.CASE_ORIGIN_EMAIL;
        c.RecordTypeId= ServiceRecordTypeId;
        update c;
        cList.add(c);
        CaseServiceRequestAssignment.serviceRequestCaseAssignment(cList);
        CaseServiceRequestAssignment.moveOutAssignment(cList);
        Test.stopTest();
    }
    
      public Static TestMethod void testMethod2(){
        Case c = doTest1();
        List<Case>  cList =  new List<Case>();
        
        Test.startTest();
        c.Status = Constants.CASE_STATUS_KEY_HANDLING;
        c.Origin = 'Web';
        c.RecordTypeId= ServiceRecordTypeId;
        update c;
        cList.add(c);
        CaseServiceRequestAssignment.serviceRequestCaseAssignment(cList);
        CaseServiceRequestAssignment.moveOutAssignment(cList);
        Test.stopTest();
    }
    
}