public  class OpportunityConstants {
	 //Opportunity Stage
    public static String OPPORTUNITY_STAGE_DOCS_COLL='Docs Collected';
    public static String OPPORTUNITY_STAGE_HOUSE='House';
    public static String OPPORTUNITY_STAGE_KEY_REC='Keys Collected';
    public static String OPPORTUNITY_STAGE_HOUSE_INSPECTION = 'House Inspection';
    public static String OPPORTUNITY_STAGE_FINAL_CONTACT = 'Final Contract';
    public static String OPPORTUNITY_STAGE_SAMPLE_CONTACT = 'Sample Contract';
    public static String OPPORTUNITY_STAGE_QUOTE = 'Quote Creation';
    public static String OPPORTUNITY_STAGE_SALES_ORDER = 'Sales Order';
    public static String OPPORTUNITY_STAGE_DROPPED = 'Dropped';
    public static String OPPORTUNITY_STAGE_LOST = 'Lost'; 

     public static String ZAM_ESCALATION='ZAM Escalation';
	 public static String RM_ESCALATION='RM Escalation';
	 public static String CT_ESCALATION='CT Escalation';  
     
     public static String OPPORTUNITY_OWNER_REQ_FOLLOWUP='Owner requested a call back';  
}