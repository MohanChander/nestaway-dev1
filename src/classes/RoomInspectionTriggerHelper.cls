/**********************************************
    Created By : Mohan
    Purpose    : Helper Class for RoomInspection Trigger 
**********************************************/
public class RoomInspectionTriggerHelper {

/**********************************************
    Created By : Mohan
    Purpose    : When the MIMO RIC is updated - Update all the open Move in and Move Out RIC's
**********************************************/	
    public static void syncMoveInMoveOutChecklistsWithMIMO(List<Room_Inspection__c> mimoRicList){

        System.debug('*******************syncMoveInMoveOutChecklistsWithMIMO');

        try{
                Set<Id> ricIdSet = new Set<Id>();
                Map<Id, Room_Inspection__c> mimoRicMap = new Map<Id, Room_Inspection__c>();
                List<Room_Inspection__c> UpdatedRicList = new List<Room_Inspection__c>();

                for(Room_Inspection__c mimoRic: mimoRicList){
                    ricIdSet.add(mimoRic.CheckFor__c); 
                    mimoRicMap.put(mimoRic.CheckFor__c, mimoRic);
                }    

                //query for the unverified Move In and Move Out Ric checklists
                List<Room_Inspection__c> queriedRicList = RoomInspectionSelector.getUnverifiedChecklists(ricIdSet);

                System.debug('***************queriedRicList: ' + queriedRicList + '\n Size of queriedRicList: ' + queriedRicList.size());

                for(Room_Inspection__c ric: queriedRicList){
                    Room_Inspection__c updatedRic = new Room_Inspection__c();
                    updatedRic = mimoRicMap.get(ric.CheckFor__c).clone(false, true, false, false);
                    updatedRic.Id = ric.Id;
                    updatedRic.Name = ric.Name;
                    updatedRic.RecordTypeId = ric.RecordTypeId;
                    updatedRic.Type_of_RIC__c = ric.Type_of_RIC__c;
                    updatedRic.Work_Order__c = ric.Work_Order__c;
                    updatedRic.CheckList_Verified__c = ric.CheckList_Verified__c;
                    updatedRic.MIMO_Comment__c = ric.MIMO_Comment__c;
                    updatedRicList.add(updatedRic);                    
                }

                if(!updatedRicList.isEmpty()){
                    update updatedRicList;
                }

            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'Sync Move In Move Out HIC with MIMO HIC');      
            }                    
    }
    
}