// added by chandu to call the House2Json Class from future methods.

public class House2JsonQueueable implements Queueable {
    
    Id houseId;
    
    public House2JsonQueueable(Id houseId){
        
         this.houseId=houseId;
    }
    
    public void execute(QueueableContext context) {
               
               House2Json.createJson(houseId);
    }
}