public class CreateCaseForHicController {
    Public House_Inspection_Checklist__c hs{get;set;}  
    public Boolean Plumbing{get;set;}
    public Boolean Carpentry{get;set;}
    public Boolean Electrical{get;set;}
    public Boolean Others{get;set;}
    public String HouseOwner;
    public String HouseID;
    public Integer count1;
    public Integer count2;
    public Integer count3;
    public Integer count4;
    public string plumbingDescription{get;set;}
    public string CarpentryDescription{get;set;}
    public string ElectricalDescription{get;set;}  
    public string OthersDescription{get;set;}
    public boolean Onboarding{get;set;}
    public static Id caseServiceRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_SERVICE_REQUEST).getRecordTypeId();
    Public Static Id furnieshedRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Furnished_House_Onboarding).getRecordTypeId();  
    Public Static Id unfurnieshedRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_Unfurnished_House_Onboarding).getRecordTypeId();  

    public CreateCaseForHicController (ApexPages.StandardController sController) {
        List<Case> caseList = new List<Case>();
        Plumbing = false;
        Carpentry = false;
        Electrical = false;
        Others = false;
        Onboarding = false;
        count1=0;
        count2=0;
        count3=0;
        count4=0;
        system.debug('hs'+hs);
        hs= [Select Id,House__r.House_Owner__c,House__r.Plumbing__c,House__r.Carpentry__c,House__r.Electrical__c,House__r.Others__c from House_Inspection_Checklist__c  WHERE id =: ApexPages.currentPage().getParameters().get('id')];
       
        if(hs != null){
             system.debug('hs'+hs);
            HouseID= hs.House__r.id;
        }
        caseList = [Select id from case where house__C =: HouseID  and (recordtypeid =: furnieshedRTId or recordtypeid =: unfurnieshedRTId)];
        if(caseList.isEmpty()){
            Onboarding = true;
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,' Onboarding case is yet to be created.'));
        }
        if(hs.House__r.House_Owner__c != null){
            HouseOwner = hs.House__r.House_Owner__c;
        }
        if(hs.House__r.Plumbing__c == true){
            count1++;
            Plumbing = true;
        }
        else{
            Plumbing = false;
        }
        
        if(hs.House__r.Carpentry__c == true){
            Carpentry = true;
            count2++;
        }
        else{
            Carpentry = false;
            
        }
        if(hs.House__r.Electrical__c == true){
            Electrical = true;
            count3++;
        }
        else{
            Electrical= false;
        }
        if(hs.House__r.Others__c == true){
            Others = true;
            count4++;
        }
        else{
            Others= false;
        }
    }
    public  void CreateCase(){
        system.debug('Plumbing'+Plumbing);
        system.debug('Carpentry'+Carpentry);
        system.debug('Electrical'+Electrical);
        system.debug('Others'+Others);
        system.debug('Plumbing'+count1);
        if(Plumbing == true && count1 ==0){
            Case c = new Case();
            c.RecordTypeId = caseServiceRTId;
            c.Status='open';
            c.Origin='Web';
            c.accountId= HouseOwner;
            c.House1__c =HouseID;
            c.Problem__c='a0Q0l000000LS4g';
            c.Description=plumbingDescription;
            insert c;
           
            hs.House__r.Plumbing__c =true;
            update hs;
        }
        if(Carpentry == true && count2 ==0){
            Case c = new Case();
            c.RecordTypeId = caseServiceRTId;
            c.Status='open';
            c.Origin='Web';
            c.House1__c =HouseID;
            c.accountId= HouseOwner;
             c.Description=CarpentryDescription;
            c.Problem__c='a0Q0l000000LRzE';
            insert c;
            hs.House__r.Carpentry__c  =true;
            update hs;
        }
        if(Electrical == true && count3 ==0){
            Case c = new Case();
            c.RecordTypeId = caseServiceRTId;
            c.Status='open';
            c.Origin='Web';
            c.House1__c =HouseID;
            c.accountId= HouseOwner;
            c.Description=ElectricalDescription;
            c.Problem__c='a0Q0l000000LRzg';
            insert c;
            hs.House__r.Electrical__c   =true;
            update hs;
        }
        if(Others == true && count4==0){
            Case c = new Case();
            c.RecordTypeId = caseServiceRTId;
            c.Status='open';
            c.Origin='Web';
            c.Description=OthersDescription;
            c.House1__c =HouseID;
            c.accountId= HouseOwner;
            c.Problem__c='a0Q0l000000LRwh';
            insert c;
            hs.House__r.Others__c    =true;
            update hs;
        }
        
        
    }
}