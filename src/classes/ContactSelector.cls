public class ContactSelector {

    public static List<Contact> getContactsFromAccountIdSet(Set<Id> accIdSet){
        return [select Id, AccountId from Contact where AccountId =: accIdSet];
    }
}