global class BatchToSyncNewVersionFields implements Database.Batchable<sObject>,  Database.AllowsCallouts,Schedulable {
    
    public string query;    
    public Date runForDate;
    
    public BatchToSyncNewVersionFields(string query,Date runForDate){
        
        this.query=query;
        this.runForDate=runForDate;
    }
    
    global void execute(SchedulableContext sc) {        
        
      BatchToSyncNewVersionFields batch = new BatchToSyncNewVersionFields(query,date.today()); 
      database.executebatch(batch,1);
   }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {       
        
       // String query = 'Select id,Commercial_Field_Sync_Was_Stop__c,Stop_Commercial_Field_Sync__c,Sync_Commercial_Fields_Date__c from contract where Sync_Commercial_Fields_Date__c<=Date.today()';
        System.debug('***query'+query);
        Date todayDate=Date.today();
        if(runForDate!=null){
            todayDate=runForDate;
        }
        query=query+':todayDate order by createddate asc ';
        
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<contract> contractList) {             
        
        try{
               StopRecursion.HouseSwitch=false;
               StopRecursion.DisabledRoomTermHouseSyncAPI=true;        
                
               For(contract con: contractList){                  
                   HouseJson.createJSONContractMethod(con.id,true);
               }
        }
        catch(exception e){
            System.debug('***exception in BatchToSyncNewVersionFields Batch :'+e.getLineNumber()+' Error: '+e.getMessage()+' \n '+e);  
            UtilityClass.insertErrorLog('Batch','***exception in BatchToSyncNewVersionFields Batch for records :'+string.valueOf(contractList)); 
        }

         
    }   
    
    global void finish(Database.BatchableContext BC) {
       
    }
}