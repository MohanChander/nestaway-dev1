@IsTest
public class CannotDeleteTest {
    public static TestMethod void Test1()
    {
	   List<Sobject> soList=new List<Sobject>();
       Account newAcc=Test_library.createAccount();
       soList.add(newAcc);
       CannotDelete.onlyAdmin(soList);
    }
      public static TestMethod void Test2()
    {
	   List<Sobject> soList=new List<Sobject>();
       Account newAcc=Test_library.createAccount();
       insert newAcc;
       soList.add(newAcc);
       User us=Test_library.createStandardUser(1);
       insert us;
        System.runAs(us)
        {
           CannotDelete.onlyAdmin(soList);
        }
    }
}