@isTest
public Class  HouseImagesUploadExtensionTest {
    public Static TestMethod void testMethod1(){

            NestAway_End_Point__c  cusSet = new NestAway_End_Point__c();
            cusSet.name = 'nestaway';
            cusSet.Nestaway_URL__c = 'www.test.com';
            insert cusSet;

        Set<id> setOfHouse = new Set<Id>();
        House__c hos= new House__c();
        hos.name='house1';
        insert hos;
        
        Bathroom__c bac= new Bathroom__c ();
        bac.House__c=hos.Id;
        insert bac;
        room_terms__c rt= new room_terms__c();
        rt.house__c=hos.ID;
        insert rt;
        List<room_terms__c> rtlist= new List<room_terms__c>();
        rtlist.add(rt);
        Photo__c ph= new Photo__c ();
        	ph.Room_Term__c=rt.Id;
                ph.House__c=hos.ID;
                	ph.Bathroom__c=bac.ID;
        insert ph;
        List<String> p = new List<String>();
        p.add(ph.id);
      
        ApexPages.currentPage().getParameters().put('id',hos.Id);
        ApexPages.StandardController sC = new ApexPages.standardController(hos);
        HouseImagesUploadExtension chp= new HouseImagesUploadExtension(sC);
  		
       
        HouseImagesUploadExtension.updatePhotosToserver(p);
        chp.setTruefalseForPicklist();
        HouseImagesUploadExtension.resyncHouse();
        HouseImagesUploadExtension.onLoadPhotoUploadPg(hos.id);
        HouseImagesUploadExtension.callApexToCreatePhotos(rt.id,ph.id,'Room','Bathroom','www');
   
            }
 
}