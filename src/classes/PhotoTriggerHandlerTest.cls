@istest
public class PhotoTriggerHandlerTest {
 Public Static TestMethod Photo__c doTest()
  {
  	Photo__c ph =new Photo__c();
  	 ph.name='photo1';
  	 ph.Image_Url__c='https://login.salesforce.com/img/logo198.png';
  	
  	 return ph;

  }

  @istest
  public static void Test1()
  {
  	Photo__c ph= dotest();
    insert ph;
     ph.Image_Url__c='http://www.impulseinfosoft.in/assets/img/courses/sales.png';
     update ph;
  }
  @istest
   public static void Test2()
  {
    Photo__c ph= dotest();
   
     insert ph;
     ph.Approval_Status__c='Approved';
     update ph;
  }
   @istest
   public static void Test3()
  {
    Photo__c ph= dotest();
     insert ph;
     ph.Comments__c='Photograph not matching';
     ph.Approval_Status__c='Rejected';
     update ph;
  }
   @istest
  public static void Test4()
  {
     Id DocumentRecordTypeID = Schema.SObjectType.Photo__c.getRecordTypeInfosByName().get(Constants.DOCUMENT_RT).getRecordTypeId();
     Id PersonAccRecordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Constants.Person_Acoount).getRecordTypeId();
    Photo__c ph= dotest();
    ph.RecordTypeId=DocumentRecordTypeID;
    insert ph;
    
  }
   @istest
  public static void Test5()
  {
    House__c hou = new House__c();
    hou.name='House1';
   // hou.Stage__c='House Live';
    insert hou;
    Photo__c ph= dotest();
    ph.House__c=hou.id;
    HouseJsonOptimizer.PHOTO_WEB_ENTITY_HOUSE_FLAG=true;
    insert ph;    
  }
    @istest
  public static void Test6()
  {
     Id DocumentRecordTypeID = Schema.SObjectType.Photo__c.getRecordTypeInfosByName().get(Constants.DOCUMENT_RT).getRecordTypeId();
     Id PersonAccRecordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Constants.Person_Acoount).getRecordTypeId();
     Id TanentRecordId= Schema.SObjectType.Account.getRecordTypeInfosByName().get(Constants.ACCOUNT_RECORD_TYPE_TENANT).getRecordTypeId();
   Account acc=new Account();
   acc.LastName='test';
   acc.PersonEmail='test1@gmail.com'; 
   acc.RecordTypeId=TanentRecordId;
   insert acc;
    Photo__c ph= dotest();
    ph.RecordTypeId=DocumentRecordTypeID;
    ph.Tenant__c=acc.id;
    insert ph;
    
  }
  public static void Test7()
  {
     Id DocumentRecordTypeID = Schema.SObjectType.Photo__c.getRecordTypeInfosByName().get(Constants.DOCUMENT_RT).getRecordTypeId();
     Id PersonAccRecordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Constants.Person_Acoount).getRecordTypeId();
     Id TanentRecordId= Schema.SObjectType.Account.getRecordTypeInfosByName().get(Constants.ACCOUNT_RECORD_TYPE_TENANT).getRecordTypeId();
   Account acc=new Account();
   acc.LastName='test';
   acc.PersonEmail='test1@gmail.com'; 
   acc.RecordTypeId=TanentRecordId;
   insert acc;
    Photo__c ph= dotest();
    ph.RecordTypeId=DocumentRecordTypeID;
    ph.Tenant__c=acc.id;
    ph.Document_Type__c = 'Address Proof';
    ph.Approval_Status__c='Approved';
    insert ph;
    
  }
    public static void Test8()
  {
     Id DocumentRecordTypeID = Schema.SObjectType.Photo__c.getRecordTypeInfosByName().get(Constants.DOCUMENT_RT).getRecordTypeId();
     Id PersonAccRecordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Constants.Person_Acoount).getRecordTypeId();
     Id TanentRecordId= Schema.SObjectType.Account.getRecordTypeInfosByName().get(Constants.ACCOUNT_RECORD_TYPE_TENANT).getRecordTypeId();
   Account acc=new Account();
   acc.LastName='test';
   acc.PersonEmail='test1@gmail.com'; 
   acc.RecordTypeId=TanentRecordId;
   insert acc;
    List<Photo__c> phoList= new List<Photo__c>();
    Photo__c ph =new Photo__c();
  	 ph.name='photo1';
  	 ph.Image_Url__c='https://login.salesforce.com/img/logo198.png';
    ph.RecordTypeId=DocumentRecordTypeID;
    ph.Tenant__c=acc.id;
    ph.Document_Type__c = 'Address Proof';
    ph.Approval_Status__c='Approved';
      insert ph;
      Photo__c ph3 =new Photo__c();
  	 ph3.name='photo1';
  	 ph3.Image_Url__c='https://login.salesforce.com/img/logo198.png';
    ph3.RecordTypeId=DocumentRecordTypeID;
    ph3.Tenant__c=acc.id;
    ph3.Document_Type__c = 'Address Proof';
    ph3.Approval_Status__c='Approved';
      insert ph3;
        Map<Id, Photo__c> oldMap = new Map<Id, Photo__c>();
          oldMap.put(ph.id,ph);
      Photo__c ph2= new Photo__c(id=ph.id);
  	 ph2.Image_Url__c='https://login.salesforce.com/img/logo198.png';
    ph2.RecordTypeId=DocumentRecordTypeID;
    ph2.Tenant__c=acc.id;
    ph2.Document_Type__c = 'Address Proof';
    ph2.Approval_Status__c='Approved';
      update ph2;
      Map<Id, Photo__c> newMap = new Map<Id, Photo__c>();
          newMap.put(ph2.id,ph2);
      newmap.put(ph3.id,ph3);
      PhotoTriggerHandler.updateDocuments(newMap,oldmap);
    
  }
}