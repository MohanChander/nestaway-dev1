@istest
public class EmailToLeadTest {
    //Test Method for main class
    
    static testMethod void TestinBoundEmail()
    {
        // create a new email and envelope object
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        List<String> listofstring=new List<String>();
        listofstring.add('test@gmail.com');
        // setup the data for the email
        email.subject = 'Create Contact';
        email.fromAddress = 'test@gmail.com';
        email.plainTextBody = 'email body\n2225256325\nTitle';
        email.toAddresses=listofstring;
        // add an Binary attachment
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        attachment.body = blob.valueOf('my attachment text');
        attachment.fileName = 'textfileone.txt';
        attachment.mimeTypeSubType = 'text/plain';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
            
            
            
            // add an Text atatchment
            
            Messaging.InboundEmail.TextAttachment attachmenttext = new Messaging.InboundEmail.TextAttachment();
        attachmenttext.body ='chd';
        
        attachmenttext.fileName = 'textfiletwo3.txt';
        attachmenttext.mimeTypeSubType = 'texttwo/plain';
        email.textAttachments =   new Messaging.inboundEmail.TextAttachment[] { attachmenttext };
            
            
            // call the email service class and test it with the data in the testMethod
            EmailToLead  testInbound=new EmailToLead ();
        testInbound.handleInboundEmail(email, env);
        
        
        
    }
}