global class UpdateHouseInHouse implements Database.Batchable<sObject>{

   global final String Query;
   global final String Entity;
   global final String Field;
   global final String Value;

   global UpdateHouseInHouse(String q, String e, String f, String v){

      Query=q; Entity=e; Field=f;Value=v;
   }

   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<sObject> scope){
      StopRecursion.DisabledHICTrigger=true;
        Map<Id,Id> hicLst = new Map<Id,Id>();
            Map<Id,Id> oppToHouse = new Map<Id,Id>();
            Set<Id> oppId = new Set<Id>();
            List<House_Inspection_Checklist__c> chkLst=(List<House_Inspection_Checklist__c>)scope;
            for(House_Inspection_Checklist__c hic: chkLst){

                oppId.add(hic.Opportunity__c);
                hicLst.put(hic.id,hic.Opportunity__c);

            }


            List<House__c> houseLst= [select id,Opportunity__c from House__c where Opportunity__c IN:oppId];

            for(House__c h : houseLst){
                oppToHouse.put(h.Opportunity__c,h.id);

            }


            for(House_Inspection_Checklist__c hic: chkLst){
                if(hicLst.containskey(hic.id) && oppToHouse.containskey(hicLst.get(hic.id))){
                    hic.house__c=oppToHouse.get(hicLst.get(hic.id));
                }


            }

            Database.update(chkLst);
      
      
      
    }

   global void finish(Database.BatchableContext BC){
   }
}