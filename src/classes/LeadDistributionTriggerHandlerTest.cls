@isTest
public Class LeadDistributionTriggerHandlerTest{
    
    public Static TestMethod void leadDistributionTest(){
      Test.StartTest();
          Round_Robin__c rouRob = new Round_Robin__c();
            rouRob.Queue_Name__c ='Bangalore HSR-BTM';
            rouRob.Total_Assigned_Percentage__c = 100;
            insert rouRob;
            
            Lead_Distribution__c leadDist = new Lead_Distribution__c();
            leadDist.Assigned__c = 50;
            leadDist.Round_Robin__c = rouRob.Id;
            insert leadDist;
            
            leadDist.Assigned__c = 60;
            update leadDist;
            
            delete leadDist;
            
      Test.StopTest(); 
    }
}