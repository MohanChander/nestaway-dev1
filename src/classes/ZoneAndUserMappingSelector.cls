/*  Created By : Mohan
    Purpose    : Zone and User Mapping related Queries are here */
public class ZoneAndUserMappingSelector {

    //get Zone and User Mapping for Particular Zone Type 
    public Static List<Zone_and_OM_Mapping__c> getZoneAndUserMappingForParticularZoneType(Id zoneRecordTypeId){
        return [select Id, Name, User__c, isActive__c, Zone__r.Zone_Code__c, 
                                                         Zone__r.ZOM__c, Available_Queues__c, Zone__r.HRM__c
                                                        from Zone_and_OM_Mapping__c where RecordTypeId =: zoneRecordTypeId
                                                        and isActive__c = true and User__c in (select Id from User where 
                                                        isActive = true and isAvailableForAssignment__c = true)];   
    }

    //get Zone Records based on RecordTypeId 
    public static List<Zone__c> getZoneRecordsForRecordTypeId(Id zoneRecordTypeId){
        return [select Id, Zone_code__c,HRM__r.Name, HRM__c,HRM__r.Email, ZOM__c from Zone__c where RecordTypeId =: zoneRecordTypeId];
    }
}