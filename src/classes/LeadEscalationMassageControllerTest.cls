@Istest
public with sharing class LeadEscalationMassageControllerTest {
	
	public static TestMethod Void doTest1(){
	    	Id ownerldRecId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Owner Lead').getRecordTypeId();
	        Lead objLead = new Lead();
            objLead.FirstName                  = 'Test';
            objLead.LastName                   = 'Test';
            objLead.Phone                      = '9620600507';
            objLead.MobilePhone                = '9620600507';      
            objLead.Email                      = 'Test@test.com';
            objLead.Source_Type__c             = 'Assisted';
            //objLead.Area_Code__c               = '375';         
            objLead.LeadSource                 = 'City Marketing';
            objLead.City_Marketing_Activity__c = 'Roadshow';
            objLead.Street                     = 'Test';
            objLead.City                       = 'Bangalore';
            objLead.State                      = 'Karnataka';
            objLead.Status                     = 'New';
            objLead.PostalCode                 = '560078';
            objLead.Country                    = 'India';
            objLead.recordtypeID        	   =ownerldRecId;
            objLead.IsValidPrimaryContact__c   = True;  
            objLead.Tenancy_Type__c='Singles';     
            objLead.House_Visit_Scheduled_Date__c=System.now().addhours(5);
            insert objLead;

            objLead.New_Stage_Escalation_Level__c='ZAM Escalation';
            update objLead;

            objLead.New_Stage_Escalation_Level__c='RM Escalation';
            update objLead;

            objLead.Status                     = 'Open';
            update objLead;

            ApexPages.StandardController stdController2=new ApexPages.StandardController(objLead);
            LeadEscalationMassageController myPage2 =new LeadEscalationMassageController(stdController2);

            objLead.Open_Stage_Escalation_Level__c='ZAM Escalation';
            update objLead;

            objLead.Open_Stage_Escalation_Level__c='RM Escalation';
            update objLead;

             ApexPages.StandardController stdController1=new ApexPages.StandardController(objLead);
            LeadEscalationMassageController myPage1 =new LeadEscalationMassageController(stdController1);

            objLead.Open_Stage_Escalation_Level__c='City Escalation';
            update objLead;

            objLead.Status                     = 'House Visit Scheduled';
            update objLead;

            ApexPages.StandardController stdController=new ApexPages.StandardController(objLead);
            LeadEscalationMassageController myPage =new LeadEscalationMassageController(stdController);


	}
}