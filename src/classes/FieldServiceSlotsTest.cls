@IsTest
public class FieldServiceSlotsTest {

    @isTest static void FieldServiceSlotsTest() {
                
        House__c house = TestDataFactory.createHouse();
        
        Problem__c prob = FieldServiceTestLibrary.createProblem();
        insert prob;

        FieldServiceTestLibrary.createOperatingHours(Label.Operating_Hours_for_Scheduling);
        
        //create request body
        FieldServiceSlots.JsonRequestBody requestBody = new FieldServiceSlots.JsonRequestBody();
        requestBody.problem_id = prob.Id;
        requestBody.house_ids = new List<Id>{house.Id};
            
		String jsonBody = JSON.serialize(requestBody);			            
        
        // Set up a test request
        RestRequest request = new RestRequest();
        request.requestUri =
            URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/FieldService/getSlots';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(jsonBody);
        RestContext.request = request;
        
        // Call the method to test
        FieldServiceSlots.getSlots();


    }
}