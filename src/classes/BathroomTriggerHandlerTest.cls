@istest
public class BathroomTriggerHandlerTest {
    Static Id serviceRequestZoneRTId = Schema.SObjectType.Zone__c.getRecordTypeInfosByName().get(Constants.ZONE_RECORD_TYPE_SERVICE_REQUEST_ZONE).getRecordTypeId();
    public  static  Id ServiceRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(Constants.CASE_RT_SERVICE_REQUEST).getRecordTypeId();
    public  static Id serviceRequestMappingRTId = Schema.SObjectType.Zone_and_OM_Mapping__c.getRecordTypeInfosByName().get(Constants.ZONE_MAPPING_SERVICE_REQUEST_MAPPING_RECORD_TYPE).getRecordTypeId();
    Public Static TestMethod Void doTest(){
        User newUser = Test_library.createStandardUser(1);
        newUser.isActive=true;
        newUser.isAvailableForAssignment__c = true;
        newuser.Phone='1234567890';
        insert newuser;      
        
        City__c newcity= new City__c();
        newcity.name='bangalore';
        insert newcity;
        
        House__c hos= new House__c();
        hos.name='house1';
        hos.City_Master__c=newcity.id;
        hos.Onboarding_Zone_Code__c  = '123';
        hos.Assets_Created__c=false;
        hos.Booking_Type__c = 'Shared House';
        hos.City__c='Bangalore';
        hos.Furnishing_Type__c = Constants.FURNISHING_CONDITION_SEMI_FURNISHED;
        insert hos;
         House_Inspection_Checklist__c hic = new House_Inspection_Checklist__c ();
        hic.House__c=hos.id;
        insert hic;
        bathroom__c bc= new bathroom__c();
        bc.name='test';
        bc.Checklist__c=hic.id;
        bc.House__c=hos.id;
        insert bc;
        
        Map<Id, Bathroom__c> newMap = new Map<Id, Bathroom__c> ();
        newmap.put(bc.id,bc);
        
    }
}