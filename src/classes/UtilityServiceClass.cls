public class UtilityServiceClass {
    
    public static user getUserRoleDetails(id userid){
        return [select UserRoleId,UserRole.name 
                    from User where Id =: userid];
    }
    public static Map<Id,User> getUsers(Set<Id> allSubRoleIds){
        Map<Id,User> users = new Map<Id, User>([Select Id, Name From User where UserRoleId IN :allSubRoleIds]); 
        return users ;
    }
    
    public static list<UserRole> getRoleIdsFromParentRoleIds(Set<ID> roleIds){
        return [select Id from UserRole where ParentRoleId 
                                IN :roleIds AND ParentRoleID != null];
    }
    
    public static map<id,id> getUserFromZone(String zonecode){
        list <Zone_and_OM_Mapping__c> zone_user_list = new list<Zone_and_OM_Mapping__c> ();
        list<Zone__c> zonelist = new list<Zone__c>();
        zonelist = [Select id, Zone_code__c from zone__c where Zone_code__c =: zonecode];
        if(zonelist.size() > 0){
            zone_user_list = [Select Serviceable__c ,User__c ,Zone__c,User__r.UserRole.name 
                                from Zone_and_OM_Mapping__c 
                          where Zone__c =: zonelist[0].id];
        }
        
        map<id,id> zone_user_map = new map <id,id>();
        if(zone_user_list.size() > 0  && zone_user_list[0].Serviceable__c == true){// && zone_user_list.User__r.UserRole.name == ''){
          zone_user_map.put(zone_user_list[0].Zone__c,zone_user_list[0].User__c);  
        }
        if(zone_user_map.size()>0)
            return zone_user_map;
        else
            return null;
        
    }
    
    
    public static String getQueryString(String objectName){
        Map<String, Schema.SObjectType> objMap = new Map<String, Schema.SObjectType>();
        objMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = objMap.get(objectName).getDescribe().fields.getMap();
        string queryString = 'SELECT ';
        for(Schema.SObjectField field :fieldMap.values()){
            queryString = queryString + field.getDescribe().getName() + ', ';
        }
        queryString = queryString.substring(0,queryString.length() - 2);
        return queryString;
    }
    
/*******************************************
 * Created By : Mohan
 * purpose    : get date from Datetime
 * ****************************************/
    public static Date getDateFromDatetime(Datetime inputDt){
        Date d = date.newinstance(inputDt.year(), inputDt.month(), inputDt.day());
        return d;
    }    

}