public class LeadEscalationMessageController {
    
    public String Massage{get;set;}

    public LeadEscalationMessageController(ApexPages.StandardController stdController) {
        try{
                Id ldId=stdController.getRecord().id;
                Lead ld=[Select id,New_Stage_Escalation_Level__c,Open_Stage_Escalation_Level__c from lead where id=:ldId];
                List<Agent_History__c> ahList =[select id,Lead__c,Stage__c,Esclation_Level__c from Agent_History__c where Lead__c=:ld.id];
        
                Map<string,Integer> escalateMap=new Map<string,Integer>();
                escalateMap.put('ZAM Escalation',1);
                escalateMap.put('RM Escalation',2);
                escalateMap.put('CT Escalation',3);
        
                Integer newEscal=0;
                Integer openEscal=0;
                String newEscalLevel='';
                String openEscalLevel='';
        
                for(Agent_History__c ah:ahList){
                    if(ah.Stage__c=='New' && ah.Esclation_Level__c!=null && escalateMap.get(ah.Esclation_Level__c)>newEscal){
                        newEscal=escalateMap.get(ah.Esclation_Level__c);
                        newEscalLevel=ah.Esclation_Level__c;
                    }
                    if(ah.Stage__c=='Open' && ah.Esclation_Level__c!=null && escalateMap.get(ah.Esclation_Level__c)>openEscal){
                        openEscal=escalateMap.get(ah.Esclation_Level__c);
                        openEscalLevel=ah.Esclation_Level__c;
                    }
                }
        
                if(ld.New_Stage_Escalation_Level__c!=null && escalateMap.get(ld.New_Stage_Escalation_Level__c)>newEscal)
                {
                    newEscal=escalateMap.get(ld.New_Stage_Escalation_Level__c);
                    newEscalLevel=ld.New_Stage_Escalation_Level__c;
                }
        
                if(ld.Open_Stage_Escalation_Level__c!=null && escalateMap.get(ld.Open_Stage_Escalation_Level__c)>openEscal)
                {
                    openEscal=escalateMap.get(ld.Open_Stage_Escalation_Level__c);
                    openEscalLevel=ld.Open_Stage_Escalation_Level__c;
                }
        
                if(newEscal>0 && openEscal>0){
                 Massage='Lead have been Escalated to : '+newEscalLevel+' at New Stage and Escalated to : '+openEscalLevel+' at Open Stage';
                }
                else if(newEscal==0 && openEscal>0){
                  Massage='Lead have been Escalated to : '+openEscalLevel+' at Open Stage';
                }
                else if(newEscal>0 && openEscal==0){
                  Massage='Lead have been Escalated to : '+newEscalLevel+' at New Stage';
                }

            }  Catch(Exception e) {
                 System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                // UtilityClass.insertGenericErrorLog(e,'lead Ecsalation page');
             }
        
    }
}