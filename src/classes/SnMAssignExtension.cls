public with sharing class SnMAssignExtension
 {

	public Datetime startTime {get;set;}
    public Datetime endTime {get;set;}
    public String username {get;set;}
    public String vendername {get;set;}
    public String assignTo {get;set;}
    public id wrkid;
    public Workorder wrk {get;set;}
    public List<SelectOption> userOptions {get;set;}  
    public List<SelectOption> venderOptions {get;set;}
    public Integer wrkCount {get;set;}
    public String addErrorMessage;
    public boolean assintovender {get;set;}
    public boolean assintoaggre {get;set;}

	public SnMAssignExtension(ApexPages.StandardController stdController) 
	{
        wrkid=stdController.getRecord().id;
        userOptions = new List<SelectOption>();
        venderOptions = new List<SelectOption>();
        List<SelectOption> assignOptions=new List<SelectOption>();
        assignOptions.add(new SelectOption('Assign To Technician','Assign To Technician'));
        assignOptions.add(new SelectOption('Assign To Aggregator','Assign To Aggregator'));
        wrk=[Select id,StartDate,EndDate,ownerid,case.Zone_Code__c,Problem__r.Skills__c,Problem__c from workorder where id=:wrkid];
        startTime= wrk.StartDate;
        endTime= wrk.Enddate;
        User use=[Select id,Account.Vendor_Type__c,Vendor_Account_name__c,name from User  where id=:wrk.Ownerid];
        if((use.Account.Vendor_Type__c==Constants.CONTACT_VENDOR_TYPE_INHOUSE) ||(use.Account.Vendor_Type__c==Constants.CONTACT_VENDOR_TYPE_3RD_PARTY))
        {
           userOptions.add(new SelectOption(use.name,use.name+' - ('+use.Vendor_Account_name__c+')'));
           venderOptions.add(new SelectOption('-None-','-None-'));
        }
        else if(use.Account.Vendor_Type__c==Constants.CONTACT_VENDOR_TYPE_3RD_PARTY_AGGREGUTERS)
        {
           venderOptions.add(new SelectOption(use.name,use.name));
           userOptions.add(new SelectOption('-None-','-None-'));
        }
	
	}
	/********************************************************************************************************************************************************************************************/
	 public List<SelectOption> getAssignOptions()
    {
        List<SelectOption> assignOptions=new List<SelectOption>();
        assignOptions.add(new SelectOption('Assign To Technician','Assign To Technician'));
        assignOptions.add(new SelectOption('Assign To Aggregator','Assign To Aggregator'));
        return assignOptions;
    }
     public void Assign()
     {
     if(assignTo=='Assign To Aggregator') 
     {
        assintovender=true;
        assintoaggre=false;
     } 
     else if(assignTo=='Assign To Technician') 
     {
        assintovender=false;
        assintoaggre=true;
     } 

    }
	/********************************************************************************************************************************************************************************************/
	   public void Options()
       { 
            
        System.debug('*******'+startTime);
        List<Zone_and_OM_Mapping__c> znMapList= [select User__c from Zone_and_OM_Mapping__c where Zone_Code__c=:wrk.case.Zone_Code__c and isActive__c=true and Type_of_Contact__c=:Constants.TYPE_OF_CONTACT_TECHNICIAN];
        Set<id> usrIDSet = new Set<id>();
        for(Zone_and_OM_Mapping__c zm:znMapList)
        {
            usrIDSet.add(zm.User__c);  
        } 
        List<Holiday__c> holiList = [select User__c,End_Date_Time__c,Start_Date_Time__c from Holiday__c where User__c=:usrIDSet and ((( Start_Date_Time__c < :startTime)  and (End_Date_Time__c > :startTime ))or ((Start_Date_Time__c < :endTime) and (End_Date_Time__c > :endTime)))];
        Map<id,List<Holiday__c>> holiMapUsrId = new Map<id,List<Holiday__c>>();
        List<Workorder> wrkList = [select OwnerId,StartDate,EndDate from Workorder where OwnerId=:usrIDSet and status!=:Constants.CASE_STATUS_RESOLVED and ((( StartDate <= :startTime)  and (EndDate > :startTime ))or ((StartDate < :endTime) and (EndDate >= :endTime)))];
        Map<id,List<Workorder>> wrkMapUsrId = new Map<id,List<Workorder>>();
        for(id us:usrIDSet)
        {
           List<Holiday__c> holi=new List<Holiday__c>();
           List<Workorder> wrk=new List<Workorder>();
           if(holiList!=null)
           {           
               for(Holiday__c ho:holiList)
               {
                 if(ho.User__c==us)
                   holi.add(ho);
               }   
               holiMapUsrId.put(us,holi);
           }
             if(wrkList!=null)
           {           
               for(Workorder wo:wrkList)
               {
                 if(wo.OwnerId==us)
                   wrk.add(wo);
               }   
               wrkMapUsrId.put(us,wrk);
           }
        }

        Set<id> userIdToPass = new set<Id>();
        for(id ui:usrIDSet)
        {
            if((holiMapUsrId.get(ui)==null || holiMapUsrId.get(ui).size()==0) && (wrkMapUsrId.get(ui)==null || wrkMapUsrId.get(ui).size()==0))
            {
               userIdToPass.add(ui); 
            }
        }
        List<User> usrList=[select id,username,name,Vendor_Account_name__c from User where id=:userIdToPass and ((Vendor_Capability__c Includes(:wrk.Problem__r.Skills__c)) OR (Vendor_Capability__c Includes('Generalist'))) and ((Account.Vendor_Type__c=:Constants.CONTACT_VENDOR_TYPE_INHOUSE) or (Account.Vendor_Type__c=:Constants.CONTACT_VENDOR_TYPE_3RD_PARTY))];
        System.debug('******USERLIST****'+usrList);
        if(!usrList.isEmpty())
        {
            for(User us:usrList)
            {
                userOptions.add(new SelectOption(us.name,us.name+' - ('+us.Vendor_Account_name__c+')'));
            }
        }
        Vender();
    }
/********************************************************************************************************************************************************************************************/
	 public void Vender() 
    {
        
        List<Zone_and_OM_Mapping__c> znMapList= [select User__c from Zone_and_OM_Mapping__c where Zone_Code__c=:wrk.case.Zone_Code__c and isActive__c=true and Type_of_Contact__c=:Constants.TYPE_OF_CONTACT_VENDOR];
        Set<id> usrIDSet = new Set<id>();
        for(Zone_and_OM_Mapping__c zm:znMapList)
        {
            usrIDSet.add(zm.User__c);  
        } 
        List<User> usrList=[select id,username,name from User where id=:usrIDSet and (Vendor_Capability__c Includes(:wrk.Problem__r.Skills__c) OR Vendor_Capability__c Includes('Generalist')) and (Vendor_Type__c=:Constants.CONTACT_VENDOR_TYPE_3RD_PARTY_AGGREGUTERS)];
        System.debug('*******Vender USer***'+usrList);
        if(!usrList.isEmpty())
        {
            for(User us:usrList)
            {
                venderOptions.add(new SelectOption(us.name,us.name));
            }
         }   
    }
/********************************************************************************************************************************************************************************************/
	  public void VenderWrkCount()
     {  if(vendername!=null)
        {  System.debug('*****Count*******');
                    User us = [select id,name from user where name=:vendername];
                    List<Workorder> wrkList = [select OwnerId,StartDate,EndDate from Workorder where OwnerId=:us.id and ((( StartDate <= :startTime)  and (EndDate > :startTime ))or ((StartDate < :endTime) and (EndDate >= :endTime)))];
                    
                    if(wrkList!=null || !wrkList.isEmpty())
                    {
                      wrkCount=wrkList.size();
                    }
                    else
                    {   
                       wrkCount=0;
                    }
        }
        else
        wrkCount=0;

        System.debug(vendername+'**********count*****'+wrkCount);
     }
     /*************************************************************************************************************************************************************************************************************************************/
   public pagereference InsertWorkorder()
    { 
        try
        {     
        	Case cas=[select id,Service_Visit_Time__c from Case where id=:wrk.Caseid];
        	  User assignTO =new User();
        	   if(startTime==null || endTime==null)
                {
                   addErrorMessage='StartTime and EndTime Cannot be bkank.'; 
                }
                 if(startTime > endTime)
                {
                   addErrorMessage='StartTime should be smaller then EndTime.'; 
                }

               wrk.StartDate=startTime;
               wrk.EndDate=endTime;
                if(assintovender==false)
                {
                    assignTO=[Select id from user where name=:username];
                    wrk.ownerid=assignTO.id;
                }
                else 
                {
                    assignTO=[Select id from user where name=:vendername];
                    wrk.ownerid=assignTO.id;
                }
                 System.debug('******Bony1******'+assignTO);
                 Update wrk;
                 cas.Service_Visit_Time__c=startTime;
                 update cas;
                 PageReference parentPage;
                if(wrk!=null)
                {
                    parentPage = new PageReference('/' + wrk.Id);
                    parentPage.setRedirect(true);
                    return parentPage;
                } 
        }
        catch (Exception e)
        {

            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,addErrorMessage));
        }
        return null;
    }
     public pagereference cancel(){
       
        PageReference parentPage;
        if(wrk!=null){
            parentPage = new PageReference('/' + wrk.Id);
            parentPage.setRedirect(true);
            return parentPage;
        }
        return null;
    }
}