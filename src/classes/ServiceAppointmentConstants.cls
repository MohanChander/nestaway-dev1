public class ServiceAppointmentConstants {

	public static String STATUS_COMPLETED = 'Completed';
	public static String STATUS_IN_PROGRESS = 'In Progress';
	public static String STATUS_CANCELED = 'Canceled';
	public static String STATUS_NONE = 'None';
	public static String STATUS_CANNOT_COMPLETE = 'Cannot Complete';
	public static String STATUS_SCHEDULED = 'Scheduled';

}