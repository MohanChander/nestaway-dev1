@isTest(seealldata = True)
public Class HouseInspectionTriggHandlerTest{
    
    public Static TestMethod void houseInspectionTest(){
        Test.StartTest();
        Id recordTypeId = [Select Id from RecordType where Name=:'House Inspection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id; 
        List<House_Inspection_Checklist__c> houseList = new List<House_Inspection_Checklist__c>();
        set<ID> houseIdSet = new set<ID>();
        List<Room_Inspection__c> roomList = new List<Room_Inspection__c>();
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Final Contract';
        opp.House_Layout__c = '1 BHK';
        insert opp;
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone__c=zc.id;
        hos.Assets_Created__c=false;
        insert hos;
        
        House_Inspection_Checklist__c houseIC = new House_Inspection_Checklist__c();
        houseIC.House_Lat_Long_details__Latitude__s = 13.0646914;
        houseIC.House_Lat_Long_details__Longitude__s=77.6296346;
        houseIC.RecordTypeId = recordTypeId;
        houseIC.Opportunity__c = opp.Id;
        houseIc.House__c=hos.id;
        houseIC.House_Layout__c = '1 BHK';
        houseIC.Status__c = 'House Inspection In Progress';           
        houseList.add(houseIC);
        
        House_Inspection_Checklist__c houseIC1 = new House_Inspection_Checklist__c();
        houseIC1.RecordTypeId = recordTypeId;
        houseIC1.House_Lat_Long_details__Latitude__s = 13.0646914;
        houseIC1.House_Lat_Long_details__Longitude__s = 77.6296346;
        houseIC1.Opportunity__c = opp.Id;
        houseIC1.House_Layout__c = '1 R';
        houseIc1.House__c=hos.id;
        houseIC1.Status__c = 'House Inspection In Progress';           
        houseList.add(houseIC1);
        
        insert houseList;
        
        for(House_Inspection_Checklist__c hse : houseList){
            houseIdSet.add(hse.Id);
        }
        
        houseIC.House_layout__c = '4 BHK';    
        houseIC.ID_proof_number__c = '1234';
        houseIC.Identity_proof_document__c = 'PAN Card';
        update houseIC;
        
        houseIC1.House_layout__c = '1 BK';    
        houseIC1.ID_proof_number__c = '1234';
        houseIC1.Identity_proof_document__c = 'PAN Card';
        update houseIC1;
        
        for(Room_Inspection__c room : [SELECT Id,Area_of_the_room_in_sqft__c FROM Room_Inspection__c WHERE House_Inspection_Checklist__c in:houseIdSet]){
            room.Area_of_the_room_in_sqft__c = '10';
            roomList.add(room);
        }
        system.debug('-----'+roomList);
        update roomList;
        
        houseIC.Status__c = 'House Inspection Completed';
        update houseIC;
        
        houseIC1.Status__c = 'House Inspection Completed';
        update houseIC1;
        
        
        Test.StopTest();
    }
    
    
    
    public Static TestMethod void houseInspectionTest3(){
        Test.StartTest();
        Id recordTypeId = [Select Id from RecordType where Name=:'House Inspection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id; 
        List<House_Inspection_Checklist__c> houseList = new List<House_Inspection_Checklist__c>();
        set<ID> houseIdSet = new set<ID>();
        List<Room_Inspection__c> roomList = new List<Room_Inspection__c>();
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Final Contract';
        opp.House_Layout__c = '1 BHK';
        insert opp;
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone__c=zc.id;
        hos.Assets_Created__c=false;
        insert hos;
        
        House_Inspection_Checklist__c houseIC = new House_Inspection_Checklist__c();
        houseIC.House_Lat_Long_details__Latitude__s= 13.0646914;
        houseIC.House_Lat_Long_details__Longitude__s=77.6296346;
        houseIC.RecordTypeId = recordTypeId;
        houseIC.Opportunity__c = opp.Id;
        houseIc.House__c=hos.id;
        houseIC.House_Layout__c = '1 BHK';
        houseIC.Verification_Done__c ='Yes';
        houseIC.Status__c = 'House Inspection In Progress';           
        insert houseIc;
        
        
        Map<Id, House_Inspection_Checklist__c> newMap = new Map<Id, House_Inspection_Checklist__c>();
        newMap.put(houseIC.id,houseIC);
        houseIC.Status__c = 'House Inspection Completed';
        houseIC.House_Layout__c = '1 BH';
        update houseIC;
        list<House_Inspection_Checklist__c> lstNewHouseInspectionList = new List<House_Inspection_Checklist__c>();
        lstNewHouseInspectionList.add(houseIc);
        Test.StopTest();
    }
    public Static TestMethod void houseInspectionTest4(){
        Test.StartTest();
        Id recordTypeId = [Select Id from RecordType where Name=:'House Inspection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id; 
        List<House_Inspection_Checklist__c> houseList = new List<House_Inspection_Checklist__c>();
        set<ID> houseIdSet = new set<ID>();
        List<Room_Inspection__c> roomList = new List<Room_Inspection__c>();
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Final Contract';
        opp.House_Layout__c = '1 BHK';
        insert opp;
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone__c=zc.id;
        hos.Assets_Created__c=false;
        insert hos;
        
        House_Inspection_Checklist__c houseIC = new House_Inspection_Checklist__c();
        houseIC.House_Lat_Long_details__Latitude__s= 13.0646914;
        houseIC.House_Lat_Long_details__Longitude__s=77.6296346;
        houseIC.RecordTypeId = recordTypeId;
        houseIC.Opportunity__c = opp.Id;
        houseIc.House__c=hos.id;
        houseIC.House_Layout__c = '1 BHK';
        houseIC.Status__c = 'House Inspection In Progress';           
        insert houseIc;
        
        
        Map<Id, House_Inspection_Checklist__c> newMap = new Map<Id, House_Inspection_Checklist__c>();
        newMap.put(houseIC.id,houseIC);
        houseIC.Status__c = 'House Inspection Completed';
        houseIC.House_Layout__c = '1 BK';
        update houseIC;
        list<House_Inspection_Checklist__c> lstNewHouseInspectionList = new List<House_Inspection_Checklist__c>();
        lstNewHouseInspectionList.add(houseIc);
        Test.StopTest();
    }
    public Static TestMethod void houseInspectionTest5(){
        
        Id recordTypeId = [Select Id from RecordType where Name=:'House Inspection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id; 
        List<House_Inspection_Checklist__c> houseList = new List<House_Inspection_Checklist__c>();
        set<ID> houseIdSet = new set<ID>();
        List<Room_Inspection__c> roomList = new List<Room_Inspection__c>();
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Final Contract';
        opp.House_Layout__c = '1 BHK';
        insert opp;
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone__c=zc.id;
        hos.Assets_Created__c=false;
        insert hos;
        test.starttest();
        House_Inspection_Checklist__c houseIC = new House_Inspection_Checklist__c();
        houseIC.House_Lat_Long_details__Latitude__s= 13.0646914;
        houseIC.House_Lat_Long_details__Longitude__s=77.6296346;
        houseIC.RecordTypeId = recordTypeId;
        houseIC.Opportunity__c = opp.Id;
        houseIc.House__c=hos.id;
        houseIC.House_Layout__c = '1 BHK';
        houseIC.Status__c = 'House Inspection In Progress';  
        houseIC.Verification_Done__c ='Yes';
        houseIC.Number_of_common_bathrooms__c=1;         
        insert houseIc;
        case cc= new case();
        cc.House__c=hos.id;
        cc.Checklist__c=houseIC.id;
        insert cc;
        Bathroom__c batchRoom= new Bathroom__c();
        batchRoom.Checklist__c=houseIC.id;
        batchRoom.name='Common Bathroom '; 
        batchRoom.Type__c='Common';
        
        insert batchRoom; 
        Bathroom__c batchRoom1= new Bathroom__c();
        batchRoom1.Checklist__c=houseIC.id;
        batchRoom1.name='Common Bathroom '; 
        batchRoom1.Type__c='Common';
        insert batchRoom1;
        
        Map<Id, House_Inspection_Checklist__c> newMap = new Map<Id, House_Inspection_Checklist__c>();
        newMap.put(houseIC.id,houseIC);
        houseIC.Status__c = 'House Inspection Completed';
        houseIC.Verification_Done__c ='On Hold'; 
        houseIC.House_Layout__c = '1 R';
        update houseIC;
        list<House_Inspection_Checklist__c> lstNewHouseInspectionList = new List<House_Inspection_Checklist__c>();
        lstNewHouseInspectionList.add(houseIc);
        HouseInspectionTriggHandler.wrapper wc= new HouseInspectionTriggHandler.wrapper();
        Test.StopTest();
    }
       public Static TestMethod void houseInspectionTest6(){
        
        Id recordTypeId = [Select Id from RecordType where Name=:'House Inspection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id; 
        List<House_Inspection_Checklist__c> houseList = new List<House_Inspection_Checklist__c>();
        set<ID> houseIdSet = new set<ID>();
        List<Room_Inspection__c> roomList = new List<Room_Inspection__c>();
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Final Contract';
        opp.House_Layout__c = '1 BHK';
        insert opp;
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone__c=zc.id;
        hos.Assets_Created__c=false;
        insert hos;
        test.starttest();
        House_Inspection_Checklist__c houseIC = new House_Inspection_Checklist__c();
        houseIC.House_Lat_Long_details__Latitude__s= 13.0646914;
        houseIC.House_Lat_Long_details__Longitude__s=77.6296346;
        houseIC.RecordTypeId = recordTypeId;
        houseIC.Opportunity__c = opp.Id;
        houseIc.House__c=hos.id;
        houseIC.House_Layout__c = '1 BHK';
        houseIC.Status__c = 'House Inspection In Progress';  
        houseIC.Verification_Done__c ='Yes';
        houseIC.Number_of_common_bathrooms__c=4;         
        insert houseIc;
        case cc= new case();
        cc.House__c=hos.id;
        cc.Checklist__c=houseIC.id;
        insert cc;
        Bathroom__c batchRoom= new Bathroom__c();
        batchRoom.Checklist__c=houseIC.id;
        batchRoom.name='Common Bathroom '; 
        batchRoom.Type__c='Common';
        
        insert batchRoom; 
        Bathroom__c batchRoom1= new Bathroom__c();
        batchRoom1.Checklist__c=houseIC.id;
        batchRoom1.name='Common Bathroom '; 
        batchRoom1.Type__c='Common';
        insert batchRoom1;
        
        Map<Id, House_Inspection_Checklist__c> newMap = new Map<Id, House_Inspection_Checklist__c>();
        newMap.put(houseIC.id,houseIC);
        houseIC.Status__c = 'House Inspection Completed';
        houseIC.Verification_Done__c ='On Hold'; 
        houseIC.House_Layout__c = '1 R';
        update houseIC;
        list<House_Inspection_Checklist__c> lstNewHouseInspectionList = new List<House_Inspection_Checklist__c>();
        lstNewHouseInspectionList.add(houseIc);
        HouseInspectionTriggHandler.wrapper wc= new HouseInspectionTriggHandler.wrapper();
        Test.StopTest();
    }
      public Static TestMethod void houseInspectionTest7(){
        
        Id recordTypeId = [Select Id from RecordType where Name=:'House Inspection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id; 
        List<House_Inspection_Checklist__c> houseList = new List<House_Inspection_Checklist__c>();
        set<ID> houseIdSet = new set<ID>();
        List<Room_Inspection__c> roomList = new List<Room_Inspection__c>();
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Final Contract';
        opp.House_Layout__c = '1 BHK';
        insert opp;
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone__c=zc.id;
        hos.Assets_Created__c=false;
        insert hos;
        test.starttest();
        House_Inspection_Checklist__c houseIC = new House_Inspection_Checklist__c();
        houseIC.House_Lat_Long_details__Latitude__s= 13.0646914;
        houseIC.House_Lat_Long_details__Longitude__s=77.6296346;
        houseIC.RecordTypeId = recordTypeId;
        houseIC.Opportunity__c = opp.Id;
        houseIc.House__c=hos.id;
        houseIC.House_Layout__c = '1 BHK';
        houseIC.Status__c = 'House Inspection In Progress';  
        houseIC.Verification_Done__c ='Yes';
        houseIC.Number_of_common_bathrooms__c=1;         
        insert houseIc;
        case cc= new case();
        cc.House__c=hos.id;
        cc.Checklist__c=houseIC.id;
        insert cc;
        Bathroom__c batchRoom= new Bathroom__c();
        batchRoom.Checklist__c=houseIC.id;
        batchRoom.name='Common Bathroom '; 
        batchRoom.Type__c='Common';
        
        insert batchRoom; 
      
        Map<Id, House_Inspection_Checklist__c> newMap = new Map<Id, House_Inspection_Checklist__c>();
              newMap.put(houseIC.id,houseIC);
    
        houseIC.Status__c = 'House Inspection Completed';
        houseIC.Verification_Done__c ='On Hold'; 
        houseIC.House_Layout__c = '1 R';
        update houseIC;
         
        list<House_Inspection_Checklist__c> lstNewHouseInspectionList = new List<House_Inspection_Checklist__c>();
        lstNewHouseInspectionList.add(houseIc);
                HouseInspectionTriggHandler.wrapper wc= new HouseInspectionTriggHandler.wrapper();
        Test.StopTest();
    }
     public Static TestMethod void houseInspectionTest16(){
        
        Id dccRecordTypeId = Schema.SObjectType.House_Inspection_Checklist__c.getRecordTypeInfosByName().get('Document Collection Checklist').getRecordTypeId();
        List<House_Inspection_Checklist__c> houseList = new List<House_Inspection_Checklist__c>();
        set<ID> houseIdSet = new set<ID>();
        List<Room_Inspection__c> roomList = new List<Room_Inspection__c>();
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Final Contract';
        opp.House_Layout__c = '1 BHK';
        insert opp;
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone__c=zc.id;
        hos.Assets_Created__c=false;
        insert hos;
        test.starttest();
        House_Inspection_Checklist__c houseIC = new House_Inspection_Checklist__c();
        houseIC.House_Lat_Long_details__Latitude__s= 13.0646914;
        houseIC.House_Lat_Long_details__Longitude__s=77.6296346;
        houseIC.RecordTypeId = dccRecordTypeId;
        houseIC.Opportunity__c = opp.Id;
        houseIc.House__c=hos.id;
        houseIC.House_Layout__c = '1 BHK';
        houseIC.Status__c = 'House Inspection In Progress';  
        houseIC.Verification_Done__c ='Yes';
        houseIC.Number_of_common_bathrooms__c=1;         
        insert houseIc;
        case cc= new case();
        cc.House__c=hos.id;
        cc.Checklist__c=houseIC.id;
        insert cc;
        Bathroom__c batchRoom= new Bathroom__c();
        batchRoom.Checklist__c=houseIC.id;
        batchRoom.name='Common Bathroom '; 
        batchRoom.Type__c='Common';
        
        insert batchRoom; 
      
        Map<Id, House_Inspection_Checklist__c> newMap = new Map<Id, House_Inspection_Checklist__c>();
              newMap.put(houseIC.id,houseIC);
    
        houseIC.Status__c = 'House Inspection Completed';
        houseIC.Verification_Done__c ='On Hold'; 
        houseIC.House_Layout__c = '1 R';
         houseIC.House_Ownership_Document__c ='Latest Tax Statement';
         houseIC.Electric_Bill_Account_Number__c ='27336';
         houseIC.Verified_Electric_Bill_For_No_Dues__c ='yes';
         houseIC.Address_Proof_Document__c ='Aadhar Card';
         houseIC.PAN_Card_Number__c ='2373722';
             houseIC.PAN_Card_Available__c ='Yes';
             houseIC.Address_Proof_ID__c ='2735';
        update houseIC;
         
        list<House_Inspection_Checklist__c> lstNewHouseInspectionList = new List<House_Inspection_Checklist__c>();
        lstNewHouseInspectionList.add(houseIc);
                HouseInspectionTriggHandler.wrapper wc= new HouseInspectionTriggHandler.wrapper();
        Test.StopTest();
    }
     public Static TestMethod void houseInspectionTest17(){
        
        Id recordTypeId = [Select Id from RecordType where Name=:'House Inspection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id; 
        List<House_Inspection_Checklist__c> houseList = new List<House_Inspection_Checklist__c>();
        set<ID> houseIdSet = new set<ID>();
        List<Room_Inspection__c> roomList = new List<Room_Inspection__c>();
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Final Contract';
        opp.House_Layout__c = '1 BHK';
        insert opp;
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone__c=zc.id;
        hos.Assets_Created__c=false;
        insert hos;
        test.starttest();
        House_Inspection_Checklist__c houseIC = new House_Inspection_Checklist__c();
        houseIC.House_Lat_Long_details__Latitude__s= 13.0646914;
        houseIC.House_Lat_Long_details__Longitude__s=77.6296346;
        houseIC.RecordTypeId = recordTypeId;
        houseIC.Opportunity__c = opp.Id;
        houseIc.House__c=hos.id;
        houseIC.House_Layout__c = '1 BHK';
        houseIC.Status__c = 'House Inspection In Progress';  
        houseIC.Verification_Done__c ='Yes';
        houseIC.Number_of_common_bathrooms__c=1;         
        insert houseIc;
        case cc= new case();
        cc.House__c=hos.id;
        cc.Checklist__c=houseIC.id;
        insert cc;
        Bathroom__c batchRoom= new Bathroom__c();
        batchRoom.Checklist__c=houseIC.id;
        batchRoom.name='Common Bathroom '; 
        batchRoom.Type__c='Common';
        
        insert batchRoom; 
      
       
    
        houseIC.Status__c = 'House Inspection Completed';
        houseIC.Verification_Done__c ='On Hold'; 
        houseIC.House_Layout__c = '1 R';
        update houseIC;
          Map<Id, House_Inspection_Checklist__c> newMap = new Map<Id, House_Inspection_Checklist__c>();
              newMap.put(houseIC.id,houseIC);
        list<House_Inspection_Checklist__c> lstNewHouseInspectionList = new List<House_Inspection_Checklist__c>();
        lstNewHouseInspectionList.add(houseIc);
          HouseInspectionTriggHandler.sendWebEntityHouseJson(newMap);
        HouseInspectionTriggHandler.wrapper wc= new HouseInspectionTriggHandler.wrapper();
        Test.StopTest();
    }
      public Static TestMethod void houseInspectionTest8(){
        
        Id recordTypeId = [Select Id from RecordType where Name=:'House Inspection Checklist' and SObjectType = 'House_Inspection_Checklist__c' limit 1].Id; 
        List<House_Inspection_Checklist__c> houseList = new List<House_Inspection_Checklist__c>();
        set<ID> houseIdSet = new set<ID>();
        List<Room_Inspection__c> roomList = new List<Room_Inspection__c>();
        Account accObj = new Account();
        accObj.Name = 'TestAcc';
        insert accObj;
        
        Opportunity opp = new Opportunity();
        opp.Name ='Test Opp';
        opp.AccountId = accObj.Id;
        opp.CloseDate = System.Today();
        opp.StageName = 'Final Contract';
        opp.House_Layout__c = '1 BHK';
        insert opp;
        User newUser = Test_library.createStandardUser(1);
        insert newuser;
        zone__c zc= new zone__c();
        zc.Zone_code__c ='text';
        zc.Name='Test';
        insert zc;
        House__c hos= new House__c();
        hos.name='house1';
        hos.Onboarding_Zone__c=zc.id;
        hos.Assets_Created__c=false;
        insert hos;
        test.starttest();
        House_Inspection_Checklist__c houseIC = new House_Inspection_Checklist__c();
        houseIC.House_Lat_Long_details__Latitude__s= 13.0646914;
        houseIC.House_Lat_Long_details__Longitude__s=77.6296346;
        houseIC.RecordTypeId = recordTypeId;
        houseIC.Opportunity__c = opp.Id;
        houseIc.House__c=hos.id;
        houseIC.House_Layout__c = '1 BHK';
        houseIC.Status__c = 'House Inspection In Progress';  
        houseIC.Verification_Done__c ='Yes';
        houseIC.Number_of_attached_bathrooms__c=1;         
        insert houseIc;
        case cc= new case();
        cc.House__c=hos.id;
        cc.Checklist__c=houseIC.id;
        insert cc;
        Bathroom__c batchRoom= new Bathroom__c();
        batchRoom.Checklist__c=houseIC.id;
        batchRoom.name='Common Bathroom '; 
        batchRoom.Type__c='Common';
        
        insert batchRoom; 
      
        Map<Id, House_Inspection_Checklist__c> newMap = new Map<Id, House_Inspection_Checklist__c>();
        newMap.put(houseIC.id,houseIC);
        houseIC.Status__c = 'House Inspection Completed';
        houseIC.Verification_Done__c ='On Hold'; 
        houseIC.House_Layout__c = '1 R';
          houseIC.Number_of_attached_bathrooms__c=3;
        update houseIC;
        list<House_Inspection_Checklist__c> lstNewHouseInspectionList = new List<House_Inspection_Checklist__c>();
        lstNewHouseInspectionList.add(houseIc);
        HouseInspectionTriggHandler.wrapper wc= new HouseInspectionTriggHandler.wrapper();
        Test.StopTest();
    }


}