/*  Created By   : Mohan - WarpDrive Tech Works
    Purpose      : All the queries related to the City Object are here */

public class CitySelector {

    public static List<City__c> getCityDetails(){
        return [select Id, Name, Rent_Min_Limit__c, ROM__c from City__c];
    }
}