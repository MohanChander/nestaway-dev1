/******************************************
 * Created By : Mohan
 * Purpose    : Trigger Handler for Custom Object : Order_Item__c
 *****************************************/
public class OrdItemTriggerHandler {

	public static void afterInsert(Map<Id, Order_Item__c> newMap){
        try{
		 		List<OrderItem> oiList = new List<OrderItem>();	
		 		List<Order_Item__c> ordItemList = new List<Order_Item__c>();  
		 		      	
        		for(Order_Item__c oi: newMap.values()){
        			if(oi.PO_Number__c != null){
        				ordItemList.add(oi);
        			}
        		}

        		if(!ordItemList.isEmpty()){
        			OrderItemTriggerHelper.updateAmountOnPO(oiList, ordItemList);
        		}
            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'after Insert for Object Order_Item__c');      
            }   

	}

	public static void afterUpdate(Map<Id, Order_Item__c> newMap, Map<Id, Order_Item__c> oldMap){
        try{
		 		List<OrderItem> oiList = new List<OrderItem>();	
		 		List<Order_Item__c> ordItemList = new List<Order_Item__c>();	

		 		for(Order_Item__c oi: newMap.values()){
		 			//check if there is change in Amount
		 			if(oi.Amount__c != oldMap.get(oi.Id).Amount__c && oi.PO_Number__c != null){
		 				ordItemList.add(oi);
		 			}		 			
		 		}

		 		if(!ordItemList.isEmpty()){
		 			OrderItemTriggerHelper.updateAmountOnPO(oiList, ordItemList);
		 		}

            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'After Update on Order Product');      
            }  
	}	
}