/*********************************************
 * Created By : Mohan
 * Purpose    : Trigger Helper for Order Item
 *********************************************/
public class OrderItemTriggerHelper {

/*********************************************
 * Created By : Mohan
 * Purpose    : Whenever PO Line Items are created and updated - update the Order
 *********************************************/
	public static void updateAmountOnPO(List<OrderItem> oiList, List<Order_Item__c> ordItemList){

        try{	
        		Set<Id> orderIdSet = new Set<Id>();
        		List<Order> ordList = new List<Order>();
        		Map<Id, AggregateResult> oiMap = new Map<Id, AggregateResult>();
        		Map<Id, AggregateResult> ordItemMap = new Map<Id, AggregateResult>();
        		
        		for(OrderItem oi: oiList){
        			orderIdSet.add(oi.OrderId);
        		}

        		for(Order_Item__c oi: ordItemList){
        			orderIdSet.add(oi.PO_Number__c);
        		}

        		List<AggregateResult> oiGroupedResult = [select OrderId, count(Id) lineItemCount, Sum(TotalPrice) amount from OrderItem 
        												 where OrderId =: orderIdSet
                                       					 group by OrderId];

        		List<AggregateResult> ordItemGroupedResult = [select PO_Number__c OrderId, count(Id) lineItemCount, Sum(Amount__c) amount from Order_Item__c 
        												 	  where PO_Number__c =: orderIdSet
                                       					 	  group by PO_Number__c];	

				for(AggregateResult ar: oiGroupedResult){
					oiMap.put((Id)ar.get('OrderId'), ar);
				}     

				for(AggregateResult ar: ordItemGroupedResult){
					ordItemMap.put((Id)ar.get('OrderId'), ar);
				}  

				for(Id ordId: orderIdSet){
					Order ord = new Order();
					ord.Id = ordId;

					Decimal ordAmount = 0;
					if(oiMap.containsKey(ordId)){
						ordAmount = (Decimal)oiMap.get(ordId).get('amount');
					}
					if(ordItemMap.containsKey(ordId)){
						ordAmount = ordAmount + (Decimal)ordItemMap.get(ordId).get('amount');
					}	

					Integer count = 0;
					if(oiMap.containsKey(ordId)){
						count = Integer.valueOf(oiMap.get(ordId).get('lineItemCount'));
					}
					if(ordItemMap.containsKey(ordId)){
						count = count + Integer.valueOf(ordItemMap.get(ordId).get('lineItemCount'));
					}

					ord.Amount__c = ordAmount;
					ord.No_of_Items__c = count;
					ordList.add(ord);
				} 

				if(!ordList.isEmpty()){
					update ordList;
				}
            } catch(Exception e){
                    System.debug('Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
                    UtilityClass.insertGenericErrorLog(e, 'Updating Amount on PO');      
            }   
	} 	
}