global class batchHICLatLonUpdate implements Database.Batchable<sObject>,  Database.AllowsCallouts {
    
    public final string query;
    public final boolean runHouseOnboardingbatch;
    public final boolean runTenantAcqubatch;
    public final boolean runSNMbatch;
    public final boolean runHouseOnboardingAssignmentbatch;
    public final boolean runTenantAcquAssignmentBatch;
    
    public batchHICLatLonUpdate(string query,boolean runHouseOnboardingbatch,boolean runTenantAcqubatch,boolean runSNMbatch,boolean runHouseOnboardingAssignmentbatch,boolean runTenantAcquAssignmentBatch){
        
        this.query=query;
        this.runHouseOnboardingbatch=runHouseOnboardingbatch;
        this.runTenantAcqubatch=runTenantAcqubatch;
        this.runSNMbatch=runSNMbatch;
        this.runHouseOnboardingAssignmentbatch=runHouseOnboardingAssignmentbatch;
        this.runTenantAcquAssignmentBatch=runTenantAcquAssignmentBatch;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
       
       /* String query = 'Select id,Onboarding_Zone_code__c,House_Lat_Long_details__Latitude__s,'+
                        'House_Lat_Long_details__Longitude__s , Acquisition_Zone_code__c, House__c'+
                        'FROM House_Inspection_Checklist__c'+
                        'where (Acquisition_Zone_code__c = NULL OR Onboarding_Zone_code__c = NULL)'+
                    'AND (House_Lat_Long_details__Longitude__s != NULL AND House_Lat_Long_details__Latitude__s != NULL )';
        */
       // String query = 'Select id,Acquisition_Zone_Code__c,Onboarding_Zone_Code__c,Service_ZoneCode__c,House_Lattitude__c,House_Longitude__c,city__c,HRM__c,ZAM__c,Property_Management_Zone__c,Property_Manager__c,APM__c from house__c';
        System.debug('***query'+query);
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<house__c> houseList) {
      
        system.debug('******allflags'+runHouseOnboardingbatch+runTenantAcqubatch+runSNMbatch);
        StopRecursion.HouseSwitch = false;       
        list <house__c> houseListToUpdate = new list <house__c> ();
        list <house__c> houseLstToUpdatePMAndAPM = new list <house__c> ();
        list <Zing_API_URL__c> zingapiurl = [SELECT id, URL__c,Snm_Tag__c  FROM Zing_API_URL__C];
        List<WBMIMOJSONWrapperClasses.wrapperZone > wrpList = new List< WBMIMOJSONWrapperClasses.wrapperZone >(); 
        try { 
        Date dateToday = Date.today();
            
        String sMonth = String.valueof(dateToday.month());
        String sDay = String.valueof(dateToday.day());
        if(sMonth.length()==1){
            sMonth = '0' + sMonth;
        }
        if(sDay.length()==1){
            sDay = '0' + sDay;
        }
        String sToday = String.valueof(dateToday.year()) +'-'+sMonth +'-'+ sDay ;
        for(house__c hic : houseList) {
               
                String ApiEndpoint='';
                String APIEndpoint_acquisition = ''; 
                String ApiEndpoint_Snm = '';
                String SnmTagName = '';             
                string lat = String.valueOf(hic.House_Lattitude__c) ;       // '12.9201963' ;  
                string lon = String.valueOf(hic.House_Longitude__c) ;      //'77.6481260' ;
                
                String timestamp = sToday;//'2017-05-23';
                if(zingapiurl.size() > 0) {
                    ApiEndpoint = zingapiurl[0].URL__c ;
                    APIEndpoint_acquisition = zingapiurl[0].URL__c ;
                    ApiEndpoint_Snm = zingapiurl[0].URL__c;
                    SnmTagName = zingapiurl[0].Snm_Tag__c;
                }
                if(runHouseOnboardingbatch && ApiEndpoint!= null && ApiEndpoint!= '' && lat!= null) {
                    ApiEndpoint+='tag=HouseOnboarding&long='+lon+'&lat='+lat+'&timestamp='+timestamp;
                    //ApiEndpoint = 'https://staging.nestaway.xyz/admin/zinc/lat_long?tag=OwnerAcquisition&long=77.6481260&lat=12.9201963&timestamp=2017-04-21';
                    
                    System.debug('***ApiEndpoint'+ApiEndpoint);
                    HttpRequest req = new HttpRequest();
                    HttpResponse res = new HttpResponse();
                    Http http = new Http();
                    req.setEndpoint(ApiEndpoint);
                    req.setMethod('GET');
                    req.setHeader('Content-Type', 'application/json');
                    req.setHeader('Accept', 'application/json');
                    req.setCompressed(true); // otherwise we hit a limit of 32000
                    
                    try {
                        System.debug('try');
                        res = http.send(req);
                        System.debug('****'+res.getStatusCode());
                        if(res != null){
                            String str = res.getBody();
                            if(res.getStatusCode()==200){
                                System.debug('***RES '+res);
                                System.debug('***List  str'+str);
                                
                                wrpList = (List<WBMIMOJSONWrapperClasses.wrapperZone>)JSON.deserialize(str,List<WBMIMOJSONWrapperClasses.wrapperZone>.class);
                                System.debug('***wrapper list= '+wrpList);
                                if(wrpList.size() > 0 ){
                                      WBMIMOJSONWrapperClasses.wrapperZone firstWrap=wrpList.get(0);
                                        WBMIMOJSONWrapperClasses.wrapperZone lastWrap=wrpList.get(wrpList.size()-1);
                                        boolean hasAssigned=false;
                                        for(WBMIMOJSONWrapperClasses.wrapperZone eachWrap: wrpList){

                                            if(!hasAssigned && eachWrap.area_id!=null && eachWrap.serviceable=='t'){
                                                hic.Onboarding_Zone_Code__c=eachWrap.area_id;
                                             System.debug('==each.Onboarding_Zone_code__c inside=='+hic.Onboarding_Zone_code__c);
                                                hasAssigned=true;
                                            }

                                        }

                                        if(!hasAssigned){
                                            hic.Onboarding_Zone_Code__c=lastWrap.area_id;                                         

                                        }          
                                }
                                
                                else{
                                    System.debug('***Error' +res.toString());
                                }
                                
                            }
                            
                        }
                    } 
                    catch(System.CalloutException e) {
                        System.debug('***EXCEPTION: '+ e);
                        UtilityClass.insertGenericErrorLog(e, 'Exception in gettting Zone code line no - 107');
                    }
                }               

                if( runTenantAcqubatch && APIEndpoint_acquisition != null && APIEndpoint_acquisition != '' && lat!= null ) {
                    APIEndpoint_acquisition+='tag=OwnerAcquisition&long='+lon+'&lat='+lat+'&timestamp='+timestamp;
                    System.debug('***ApiEndpoint'+ApiEndpoint);
                    HttpRequest req = new HttpRequest();
                    HttpResponse res = new HttpResponse();
                    Http http = new Http();
                    req.setEndpoint(APIEndpoint_acquisition);
                    req.setMethod('GET');
                    req.setHeader('Content-Type', 'application/json');
                    req.setHeader('Accept', 'application/json');
                    req.setCompressed(true); // otherwise we hit a limit of 32000

                    try {
                        System.debug('try1');
                        res = http.send(req);
                        System.debug('****'+res.getStatusCode());
                        if(res != null){
                            String str = res.getBody();
                            if(res.getStatusCode()==200){
                                System.debug('***RES '+res);
                                System.debug('***List  str'+str);
                                
                                wrpList = (List<WBMIMOJSONWrapperClasses.wrapperZone>)JSON.deserialize(str,List<WBMIMOJSONWrapperClasses.wrapperZone>.class);
                                System.debug('***wrapper list= '+wrpList);
                                if(wrpList.size() > 0 ) {
                                    System.debug('***Area details '+wrpList[0].area_id+'\n'+wrpList[0].area_name);

                                    if(wrpList[0].area_id != null) {
                                        hic.Acquisition_Zone_code__c   = wrpList[0].area_id ;
                                   /*     //hic_to_Update.add(hic);
                                        if(house.id != null ) {
                                            house.Acquisition_Zone_Code__c = wrpList[0].area_id ;
                                            //houseListToUpdate.add(house);
                                            System.debug('\n***'+houseListToUpdate);
                                        }
                                        
                                        System.debug('****'+hic_to_Update);
                                     */ 
                                    }
                                }
                                else{
                                    System.debug('***Error' +res.toString());
                                }
                            }
                        }
                    } 
                    catch(System.CalloutException e) {
                        System.debug('***EXCEPTION: '+ e);
                        UtilityClass.insertGenericErrorLog(e, 'Exception in gettting Zone code line no - 151');
                    }
                }
              // hic_to_Update.add(hic);

              if(runTenantAcquAssignmentBatch && hic.Acquisition_Zone_Code__c!=null){
                                   
                        Id acquisitionRecTypeId = Schema.SObjectType.Zone__c.getRecordTypeInfosByName().get(Constants.ZONE_RT_ACQUISITION).getRecordTypeId();
                        Map<string,Zone__c> zoneCodeToZoneMap = new Map<string,Zone__c>();
                        Map<String,City__c> cityNameToCityMap = new Map<String,City__c>();
                        Set<String> zoneCodes = new Set<String>();
                        zoneCodes.add(hic.Acquisition_Zone_Code__c);
                        List<City__c> allCityLst=[select id,Name,HRM__c,ZAM__c from City__c];

                        for(City__c city: allCityLst){      

                            cityNameToCityMap.put(city.Name.toUpperCase(),city);        
                        }

                        List<Zone__c> allZones=[select id,Zone_code__c,HRM__c,ZAM__c from Zone__c where Zone_code__c IN:zoneCodes and recordtypeId=:acquisitionRecTypeId];
                        for(Zone__c zone:allZones){

                           zoneCodeToZoneMap.put(zone.Zone_code__c,zone);
                        }


                        
                    

                           City__c city;
                           if(hic.city__c!=null && cityNameToCityMap.containsKey(hic.city__c.toUpperCase())){
                               
                                city=cityNameToCityMap.get(hic.city__c.toUpperCase());
                            } 
                          
                          
                           if(hic.Acquisition_Zone_Code__c!=null && zoneCodeToZoneMap.containsKey(hic.Acquisition_Zone_Code__c)){
                                
                                 
                                 Zone__c zone=zoneCodeToZoneMap.get(hic.Acquisition_Zone_Code__c);
                                  
                                  
                                  if(zone.HRM__c!=null){
                                       hic.HRM__c=zone.HRM__c;
                                  }
                                  else if(city!=null && city.HRM__c!=null){
                                       hic.HRM__c=city.HRM__c;
                                  }
                                  
                                  if(zone.ZAM__c!=null){
                                       hic.ZAM__c=zone.ZAM__c;
                                  }
                                  else if(city!=null && city.ZAM__c!=null){
                                       hic.ZAM__c=city.ZAM__c;
                                  }
                                  
                               
                           }
                           else if(city!=null){      
                               
                                   if(city.HRM__c!=null){
                                       
                                         hic.HRM__c=city.HRM__c;
                                   }
                                   if(city.ZAM__c!=null){
                                       
                                        hic.ZAM__c=city.ZAM__c;
                                   }
                           }          
                        
                  
              }
              
             if(runSNMbatch && ApiEndpoint_Snm != null && ApiEndpoint_Snm != '' && lat!= null) {
                    ApiEndpoint_Snm+='tag='+SnmTagName+'&long='+lon+'&lat='+lat+'&timestamp='+timestamp;
                    System.debug('***ApiEndpoint'+ApiEndpoint_Snm);
                    HttpRequest req = new HttpRequest();
                    HttpResponse res = new HttpResponse();
                    Http http = new Http();
                    req.setEndpoint(ApiEndpoint_Snm);
                    req.setMethod('GET');
                    req.setHeader('Content-Type', 'application/json');
                    req.setHeader('Accept', 'application/json');
                    req.setCompressed(true); // otherwise we hit a limit of 32000
                    
                    try {
                        System.debug('try1');
                        res = http.send(req);
                        System.debug('****'+res.getStatusCode());
                        if(res != null){
                            String str = res.getBody();
                            if(res.getStatusCode()==200){
                                System.debug('***RES '+res);
                                System.debug('***List  str'+str);
                                
                                wrpList = (List<WBMIMOJSONWrapperClasses.wrapperZone>)JSON.deserialize(str,List<WBMIMOJSONWrapperClasses.wrapperZone>.class);
                                System.debug('***wrapper list= '+wrpList);
                                if(wrpList.size() > 0 ) {
                                    System.debug('***Area details '+wrpList[0].area_id+'\n'+wrpList[0].area_name);
                                    
                                    if(wrpList[0].area_id != null) {
                                        hic.Service_ZoneCode__c    = wrpList[0].area_id ;
                                        //hic_to_Update.add(hic);                                      
                                    }
                                }
                                else{
                                    System.debug('***Error' +res.toString());
                                }
                            }
                        }
                    } 
                    catch(System.CalloutException e) {
                        System.debug('***EXCEPTION: '+ e);
                        UtilityClass.insertGenericErrorLog(e, 'Exception in gettting Zone code line no - 151');
                    }
              
              }
              
              
                houseListToUpdate.add(hic);

            } 
        
            if(houseListToUpdate.size() > 0) {
                
                  if(runHouseOnboardingAssignmentbatch){
                      populatePMOnHouse(houseListToUpdate,false);
                  } 
                 StopRecursion.HouseSwitch = false;
                 update houseListToUpdate;
                

            }
            
        }
        catch(exception e){
            System.debug('***exception '+e.getLineNumber()+'  '+e.getMessage()+' \n '+e);  
            UtilityClass.insertGenericErrorLog(e, 'Exception in gettting Zone code line no - 151'); 
        }

         
    }   
    
    
    public void populatePMOnHouse(List<House__c> HosList, Boolean isbeforeContext){
        
          boolean runPopulatePMOnHouse=true;
         boolean runPopulateAPmOnHouse=true;
      if(runPopulatePMOnHouse){
          runPopulatePMOnHouse=false;
        try{
            Map<String,Id> cityToHeadId = new Map<String,Id>();
            Map<Id, Integer> openworkOrderMapPm = new Map<Id, Integer>();
            Map<Id, Integer> openworkOrderMapAPm = new Map<Id, Integer>();
            Map<String, Set<Id>> zoneAndPmMap = new Map<String, Set<Id>>();
            Set<ID> SetOfHOuse= new Set<ID>();
            Set<String> setOfZone = new Set<String>();
            List<House__c> houseList = new List<House__c>();
            List<Zone__C> zoneList = new List<Zone__C>(); 
            Map<String,ID> mapOfZonecodeAndZOne = new Map<String,ID>();
            Map<ID,String> mapOfHouseZone = new Map<ID,String>();
            List<House__c> houseToBeUPdated = new List<House__c>();
            Set<ID> setOfPm = new Set<Id>();
            
            List<House__c> updateHouseLst= new List<house__c>();
            
            System.debug('houseList'+houseList);
            
            for(House__c hos :HosList){
                if(hos.Onboarding_Zone_Code__c != null){
                    setOfZone.add(hos.Onboarding_Zone_Code__c);
                }                
            }
            
            System.debug('setOfZone'+setOfZone);
            
            if(!setOFZone.isEmpty()){
                
                zoneList = [Select id, name, Zone_code__c from Zone__c where Zone_code__c =: setOfZone
                            and RecordType.Name = 'Property Management Zone'];
            
            
            System.debug('zoneList'+zoneList);
            
            if(!ZoneList.isEmpty()){
                for(Zone__c each:ZoneList){
                    mapOfZonecodeAndZOne.put(each.Zone_code__c, each.id);
                }
            }
            
            System.debug('mapOfZonecodeAndZOne'+mapOfZonecodeAndZOne);
            
           
            
            List<Zone_and_OM_Mapping__c> pmList = [select Id, Zone__r.Zone_Code__c, User__c from Zone_and_OM_Mapping__c 
                                                   where Zone__r.Zone_Code__c in :setOFZone and RecordType.Name = 'Property Manager'
                                                   and Zone__r.RecordType.Name = 'Property Management Zone' and User__c!=null];
            if(pmList.size()>0){
                //PM ID SET
                for(Zone_and_OM_Mapping__c each : pmList){
                    setOfPm.add(each.User__c);
                    openworkOrderMapPM.put(each.User__c, 0);
                }
                
                System.debug('**openworkOrderMapPM'+openworkOrderMapPM);
                
                //populate Zone and related PM Map
                for(Zone_and_OM_Mapping__c pm: pmList){
                    if(zoneAndPmMap.containsKey(pm.Zone__r.Zone_Code__c)){
                        zoneAndPmMap.get(pm.Zone__r.Zone_Code__c).add(pm.User__c);
                    } else {
                        zoneAndPmMap.put(pm.Zone__r.Zone_Code__c, new Set<Id>{pm.User__c});
                    }
                }
                
                List<AggregateResult> getNoOfPmPerHouse= [Select Property_Manager__c, count(Id) from House__c 
                                                          where Property_Manager__c != null AND 
                                                          Property_Manager__c in : setOfPm group by Property_Manager__c ];
                
                //for Pm
                for(AggregateResult ar:  getNoOfPmPerHouse){
                    System.debug('**getNoOfPmPerHouse ' + getNoOfPmPerHouse);
                    System.debug('**ar' + ar);
                    Integer noOfCases = 0;
                    if((Integer)ar.get('expr0') == null){
                        noOfCases = 0;
                    } else {
                        noOfCases = (Integer)ar.get('expr0');
                    }
                    openworkOrderMapPM.put((Id)ar.get('Property_Manager__c'), noOfCases);
                }    
                
                System.debug('zoneAndPmMap'+zoneAndPmMap);
                // for PM
                For(House__c each:HosList){
                    
                    if(mapOfZonecodeAndZOne.containsKey(each.Onboarding_Zone_Code__c)){
                        each.Property_Management_Zone__c = mapOfZonecodeAndZOne.get(each.Onboarding_Zone_Code__c);
                    }                
                    
                    if(zoneAndPmMap.containsKey(each.Onboarding_Zone_Code__c)){
                        List<AssignmentWrapper> awList = new List<AssignmentWrapper>();
                        for(Id userId: zoneAndPmMap.get(each.Onboarding_Zone_Code__c)){
                            system.debug('openworkOrderMapPm.get(userId)'+openworkOrderMapPm.get(userId));
                            awList.add(new AssignmentWrapper(userId, openworkOrderMapPm.get(userId)));
                        }
                        System.debug('awList'+awList);
                        awList.sort();
                        each.Property_Manager__c  = awList[0].UserId; 
                        
                        Integer noOfCases = openworkOrderMapPm.get(awList[0].UserId);
                        noOfCases=noOfCases+1;
                        openworkOrderMapPm.put(each.Property_Manager__c, noOfCases); 
                        updateHouseLst.add(each);
                    }
                    
                }
                
                //update the list of House when it is not called from before context
                if(!isbeforeContext && updateHouseLst.size()>0){                       
                    //update updateHouseLst;  
                    populateAPmOnHouse(updateHouseLst,isbeforeContext);
                    
                }
            }   
          
         }    
                          
        } catch(Exception e){
            System.debug('**Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e, 'Populate PM on House');      
        }   
      } 
    }
    
      
    
     public void populateAPmOnHouse(List<House__c> HosList ,Boolean isbeforeContext){
           boolean runPopulatePMOnHouse=true;
         boolean runPopulateAPmOnHouse=true;
      if(runPopulateAPmOnHouse){
          runPopulateAPmOnHouse=false;  
        try{
            Map<String,Id> cityToHeadId = new Map<String,Id>();
            Map<Id, Integer> openworkOrderMapPm = new Map<Id, Integer>();
            Map<Id, Integer> openworkOrderMapAPm = new Map<Id, Integer>();
            Map<String, Set<Id>> zoneAndAPmMap = new Map<String, Set<Id>>();
            Set<ID> SetOfHOuse= new Set<ID>();
            Set<ID> SetOfPropertyManager = new Set<ID>();
            Set<String> setOfZone = new Set<String>();
            List<House__c> houseList = new List<House__c>();
            List<Zone__C> zoneList = new List<Zone__C>();
            Map<String,ID> mapOfZonecodeAndZOne = new Map<String,ID>();
            Map<ID,String> mapOfHouseZone = new Map<ID,String>();
            List<House__c> houseToBeUPdated = new List<House__c>();
            Set<ID> setOfAPm = new Set<Id>();
            Set<Id> SetofZOnePmMAp = new Set<ID>();
            List<Zone_and_OM_Mapping__c> apmList = new  List<Zone_and_OM_Mapping__c>();
            List<Zone_and_OM_Mapping__c> pmList = new  List<Zone_and_OM_Mapping__c>();
            List<AggregateResult> getNoOfAPmPerHouse = new List<AggregateResult>();
            
            List<House__c> updateHouseLst= new List<house__c>();
            
            
            if(!HosList.isEmpty()){
                for(House__c hos :HosList){
                    if(hos.Onboarding_Zone_Code__c != null && hos.Property_Manager__c!=null){
                        setOfZone.add(hos.Onboarding_Zone_Code__c);
                        SetOfPropertyManager.add(hos.Property_Manager__c);
                    }                
                }
            }
            system.debug('setOfZone'+setOfZone);
            system.debug('SetOfPropertyManager'+SetOfPropertyManager);       
            if(!SetOfPropertyManager.isEmpty()){
                pmList = [select Id, Zone__r.Zone_Code__c, User__c from Zone_and_OM_Mapping__c 
                          where User__c  in :SetOfPropertyManager and Zone__r.Zone_Code__c in :Setofzone and RecordType.Name = 'Property Manager'];
            
            
                System.debug('pmList'+pmList);
                if(!pmList.isEmpty()){
                    for(Zone_and_OM_Mapping__c each : pmList){
                        SetofZOnePmMAp.add(each.id);
                    }
                }
                
                
                
                if(!SetofZOnePmMAp.isEmpty()){
                    apmList = [select Id,Property_Manager__c,Property_Manager__r.User__c, Property_Manager__r.Zone__r.Zone_code__c, User__c from Zone_and_OM_Mapping__c 
                               where Property_Manager__c  in :SetofZOnePmMAp and RecordType.Name = 'Assistance Property Manager' and Zone__r.Zone_Code__c in :Setofzone and User__c!=null];
                }
                
                //PM ID SET
                if(!apmList.isEmpty()){
                    for(Zone_and_OM_Mapping__c each : apmList){
                        setOfAPm.add(each.User__c);
                        openworkOrderMapPM.put(each.User__c, 0);
                    }
                }
                System.debug('**openworkOrderMapPM'+openworkOrderMapPM);
                
                Map<String,Map<Id,Set<Id>>> zoneToPmAndApmMap = new Map<String,Map<Id,Set<Id>>>();
                
                //populate Zone and related PM Map
                if(!apmList.isEmpty()){
                    for(Zone_and_OM_Mapping__c pm: apmList){
                        if(zoneToPmAndApmMap.containsKey(pm.property_Manager__r.Zone__r.Zone_code__c)){
                             Map<Id,Set<Id>> pmToAPmMap=zoneToPmAndApmMap.get(pm.property_Manager__r.Zone__r.Zone_code__c);
                             if(pmToAPmMap.containsKey(pm.Property_Manager__r.User__c)){
                                 
                                 pmToAPmMap.get(pm.Property_Manager__r.User__c).add(pm.User__c);
                             }
                             else{
                                 
                                 pmToAPmMap.put(pm.Property_Manager__r.User__c, new Set<Id>{pm.User__c});
                             }
                             
                             
                             zoneToPmAndApmMap.put(pm.property_Manager__r.Zone__r.Zone_code__c,pmToAPmMap);                      
                             
                        } else {
                             Map<Id,Set<Id>> pmToAPmMap = new Map<Id,Set<Id>>();
                             pmToAPmMap.put(pm.Property_Manager__r.User__c, new Set<Id>{pm.User__c});
                            zoneToPmAndApmMap.put(pm.property_Manager__r.Zone__r.Zone_code__c, pmToAPmMap);
                        }
                    }   
                }
                if(!setOfAPm.isEmpty()){
                    getNoOfAPmPerHouse= [Select APM__c, count(Id) from House__c 
                                         where APM__c != null AND 
                                         APM__c in : setOfAPm group by APM__c ];
                }
                
                //for Pm
                if(!getNoOfAPmPerHouse.isEmpty()){
                    for(AggregateResult ar:  getNoOfAPmPerHouse){
                        System.debug('**getNoOfPmPerHouse ' + getNoOfAPmPerHouse);
                        System.debug('**ar' + ar);
                        Integer noOfCases = 0;
                        if((Integer)ar.get('expr0') == null){
                            noOfCases = 0;
                        } else {
                            noOfCases = (Integer)ar.get('expr0');
                        }
                        openworkOrderMapPM.put((Id)ar.get('APM__c'), noOfCases);
                    }    
                }
                System.debug('zoneAndPmMap'+zoneToPmAndApmMap);
                // for PM
                For(House__c each:HosList){
                     System.debug('**each.Property_Manager__c'+each.Property_Manager__c);
                        
                    if(each.Property_Manager__c!=null && zoneToPmAndApmMap.containsKey(each.Onboarding_Zone_Code__c) && zoneToPmAndApmMap.get(each.Onboarding_Zone_Code__c).containsKey(each.Property_Manager__c)){
                        List<AssignmentWrapper> awList = new List<AssignmentWrapper>();
                        for(Id userId: zoneToPmAndApmMap.get(each.Onboarding_Zone_Code__c).get(each.Property_Manager__c)){
                            system.debug('openworkOrderMapPm.get(userId)'+openworkOrderMapPm.get(userId));
                            awList.add(new AssignmentWrapper(userId, openworkOrderMapPm.get(userId)));
                        }
                        System.debug('awList'+awList);
                        awList.sort();
                        each.APM__c  = awList[0].UserId; 
                        
                        Integer noOfCases = openworkOrderMapPm.get(awList[0].UserId);
                        noOfCases=noOfCases+1;
                        openworkOrderMapPm.put(each.APM__c, noOfCases);
                        updateHouseLst.add(each);                   
                    }
                }
                //update the list of House when it is not called from before context
                if(!isbeforeContext && updateHouseLst.size()>0){
                          
                    // update updateHouseLst;
                  
                }
            } 
            
        } catch(Exception e){
            System.debug('**Error Message: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + '\nStack Trace ' + e.getStackTraceString());
            UtilityClass.insertGenericErrorLog(e, 'Populate APM on House');      
        } 
      }         
    }
    
    
     /* Wrapper Class for sorting the least number of cases assigned to the Agent */
    public class AssignmentWrapper implements Comparable{
        public Id userId{set; get;}
        public Integer noOfCases {set; get;}
        
        public AssignmentWrapper(Id UserId, Integer noOfCases){
            this.userId = userId;
            this.noOfCases = noOfCases;
        }
        
        // Implement the compareTo() method
        public Integer compareTo(Object compareTo) {
            AssignmentWrapper compareToObj = (AssignmentWrapper)compareTo;
            if (noOfCases == compareToObj.noOfCases) return 0;
            if (noOfCases > compareToObj.noOfCases) return 1;
            return -1;        
        }       
    }   
    
    
    global void finish(Database.BatchableContext BC) {
        System.debug('***One batch Executed -- Ameed');
    }
    
    
}