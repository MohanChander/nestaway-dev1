/*  Created By   : Mohan - WarpDrive Tech Works
Created Date : 31/05/2017
Purpose      : All the queries related to the Bathroom Object are here */

public class BedSelector {
    
    //query for all the Beds related to the given Opportunities
    public static List<Bed__c> getBedsForOpportunities(Set<Id> oppIdSet){
        
        return [Select Id, Name, Room_Terms__r.Contract__r.Opportunity__c, House__c from 
                Bed__c where Room_Terms__r.Contract__r.Opportunity__c =: oppIdSet];
    }
    
    //get bed aggregation from Room Terms
    public static List<AggregateResult> getBedAggregationFromRoomTerms(Set<Id> roomIdSet){
        
        return [select Room_Terms__c, Sum(Actual_Rent__c) from Bed__c group by Room_Terms__c having Room_Terms__c =: roomIdSet];
    }
    
    
    //get Beds List with House Status
    public static List<Bed__c> getBedsWithHouseStatus(set<Id> bedIdSet){
        
        return [select Id, House__c, House__r.Stage__c from Bed__c where Id =: bedIdSet];
    }
    
    //get Bed List with Rents
    public static List<Bed__c> getBedsWithRent(set<Id> houseIdSet){
        return [select Id, Name, Status__c, House__c, Actual_Rent__c from Bed__c where House__c =: houseIdSet];
    }
}